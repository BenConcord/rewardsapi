package com.rewards.pojo.logging;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Evgeni Stoykov
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppLogHeader", propOrder = {
        "operationName",
        "transactionId",
        "correlationId",
        "requestInfo",
        "timeStamp"
})
public class AppLogHeader {

    @XmlElement(required = true, name = "OperationName")
    protected String operationName;

    @XmlElement(required = true, name = "TransactionId")
    protected String transactionId;

    @XmlElement(required = true, name = "CorrelationId")
    protected String correlationId;

    @XmlElement(required = true, name = "RequestInfo")
    protected String requestInfo;

    @XmlElement(required = true, name = "TimeStamp")
    protected Long timeStamp;

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getRequestInfo() {
        return requestInfo;
    }

    public void setRequestInfo(String requestInfo) {
        this.requestInfo = requestInfo;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
