package com.rewards.pojo.logging;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Evgeni Stoykov
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppLogMessage", propOrder = {
        "header",
        "body"
})
public class AppLogMessage {

    @XmlElement(name = "Header")
    protected String header;

    @XmlElement(required = true, name = "Message")
    protected String body;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
