package com.rewards.pojo.logging;

import javax.xml.bind.annotation.*;

/**
 * @author Evgeni Stoykov
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "header",
        "message",
        "status",
        "processId",
        "exceptionCode",
        "stackTrace"
})
@XmlRootElement(name = "AppLogRequest")
public class AppLogRequest {

    @XmlElement(required = true, name = "AppLogHeader")
    protected AppLogHeader header;

    @XmlElement(required = true, name = "AppLogMessage")
    protected AppLogMessage message;

    @XmlElement(required = true, name = "Status")
    protected String status;

    @XmlElement(required = true, name = "ProcessId")
    protected String processId;

    @XmlElement(name = "ExceptionCode")
    protected String exceptionCode;

    @XmlElement(name = "StackTrace")
    protected String stackTrace;

    public AppLogHeader getHeader() {
        return header;
    }

    public void setHeader(AppLogHeader header) {
        this.header = header;
    }

    public AppLogMessage getMessage() {
        return message;
    }

    public void setMessage(AppLogMessage message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionCode(String exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }
}
