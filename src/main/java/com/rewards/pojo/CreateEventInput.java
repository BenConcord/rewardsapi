package com.rewards.pojo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CreateEventInput {

    private String loyaltyNumber;

    private boolean dateOfBirth;

    private boolean address;

    public String getLoyaltyNumber() {
        return loyaltyNumber;
    }

    public void setLoyaltyNumber(String loyaltyNumber) {
        this.loyaltyNumber = loyaltyNumber;
    }

    public boolean getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(boolean dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean getAddress() {
        return address;
    }

    public void setAddress(boolean address) {
        this.address = address;
    }
}
