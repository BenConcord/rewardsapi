
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperRedemptionsOrderItemsByDateResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfRedemptionOrderItem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperRedemptionsOrderItemsByDateResult"
})
@XmlRootElement(name = "GetShopperRedemptionsOrderItemsByDateResponse")
public class GetShopperRedemptionsOrderItemsByDateResponse {

    @XmlElement(name = "GetShopperRedemptionsOrderItemsByDateResult")
    protected ArrayOfRedemptionOrderItem getShopperRedemptionsOrderItemsByDateResult;

    /**
     * Gets the value of the getShopperRedemptionsOrderItemsByDateResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRedemptionOrderItem }
     *     
     */
    public ArrayOfRedemptionOrderItem getGetShopperRedemptionsOrderItemsByDateResult() {
        return getShopperRedemptionsOrderItemsByDateResult;
    }

    /**
     * Sets the value of the getShopperRedemptionsOrderItemsByDateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRedemptionOrderItem }
     *     
     */
    public void setGetShopperRedemptionsOrderItemsByDateResult(ArrayOfRedemptionOrderItem value) {
        this.getShopperRedemptionsOrderItemsByDateResult = value;
    }

}
