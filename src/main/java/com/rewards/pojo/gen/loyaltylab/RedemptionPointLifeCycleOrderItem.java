
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RedemptionPointLifeCycleOrderItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RedemptionPointLifeCycleOrderItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Redemption" type="{http://www.loyaltylab.com/loyaltyapi/}Redemption" minOccurs="0"/&gt;
 *         &lt;element name="RedemptionPointState" type="{http://www.loyaltylab.com/loyaltyapi/}RedemptionPointState" minOccurs="0"/&gt;
 *         &lt;element name="OrderItem" type="{http://www.loyaltylab.com/loyaltyapi/}OrderItem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RedemptionPointLifeCycleOrderItem", propOrder = {
    "redemption",
    "redemptionPointState",
    "orderItem"
})
public class RedemptionPointLifeCycleOrderItem {

    @XmlElement(name = "Redemption")
    protected Redemption redemption;
    @XmlElement(name = "RedemptionPointState")
    protected RedemptionPointState redemptionPointState;
    @XmlElement(name = "OrderItem")
    protected OrderItem orderItem;

    /**
     * Gets the value of the redemption property.
     * 
     * @return
     *     possible object is
     *     {@link Redemption }
     *     
     */
    public Redemption getRedemption() {
        return redemption;
    }

    /**
     * Sets the value of the redemption property.
     * 
     * @param value
     *     allowed object is
     *     {@link Redemption }
     *     
     */
    public void setRedemption(Redemption value) {
        this.redemption = value;
    }

    /**
     * Gets the value of the redemptionPointState property.
     * 
     * @return
     *     possible object is
     *     {@link RedemptionPointState }
     *     
     */
    public RedemptionPointState getRedemptionPointState() {
        return redemptionPointState;
    }

    /**
     * Sets the value of the redemptionPointState property.
     * 
     * @param value
     *     allowed object is
     *     {@link RedemptionPointState }
     *     
     */
    public void setRedemptionPointState(RedemptionPointState value) {
        this.redemptionPointState = value;
    }

    /**
     * Gets the value of the orderItem property.
     * 
     * @return
     *     possible object is
     *     {@link OrderItem }
     *     
     */
    public OrderItem getOrderItem() {
        return orderItem;
    }

    /**
     * Sets the value of the orderItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderItem }
     *     
     */
    public void setOrderItem(OrderItem value) {
        this.orderItem = value;
    }

}
