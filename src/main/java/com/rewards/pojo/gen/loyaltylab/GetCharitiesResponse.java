
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetCharitiesResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfDonationEntity" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCharitiesResult"
})
@XmlRootElement(name = "GetCharitiesResponse")
public class GetCharitiesResponse {

    @XmlElement(name = "GetCharitiesResult")
    protected ArrayOfDonationEntity getCharitiesResult;

    /**
     * Gets the value of the getCharitiesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDonationEntity }
     *     
     */
    public ArrayOfDonationEntity getGetCharitiesResult() {
        return getCharitiesResult;
    }

    /**
     * Sets the value of the getCharitiesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDonationEntity }
     *     
     */
    public void setGetCharitiesResult(ArrayOfDonationEntity value) {
        this.getCharitiesResult = value;
    }

}
