
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetCustomQuestionsForShopperResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfShopperQuestionAnswerSet" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomQuestionsForShopperResult"
})
@XmlRootElement(name = "GetCustomQuestionsForShopperResponse")
public class GetCustomQuestionsForShopperResponse {

    @XmlElement(name = "GetCustomQuestionsForShopperResult")
    protected ArrayOfShopperQuestionAnswerSet getCustomQuestionsForShopperResult;

    /**
     * Gets the value of the getCustomQuestionsForShopperResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShopperQuestionAnswerSet }
     *     
     */
    public ArrayOfShopperQuestionAnswerSet getGetCustomQuestionsForShopperResult() {
        return getCustomQuestionsForShopperResult;
    }

    /**
     * Sets the value of the getCustomQuestionsForShopperResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShopperQuestionAnswerSet }
     *     
     */
    public void setGetCustomQuestionsForShopperResult(ArrayOfShopperQuestionAnswerSet value) {
        this.getCustomQuestionsForShopperResult = value;
    }

}
