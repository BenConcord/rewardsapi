
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for QuestionnaireAnswer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuestionnaireAnswer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AnswerID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="QuestionID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AnswerPosition" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="AnswerText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuestionnaireAnswer", propOrder = {
    "answerID",
    "questionID",
    "status",
    "answerPosition",
    "answerText"
})
@XmlSeeAlso({
    ShopperAnswer.class
})
public class QuestionnaireAnswer {

    @XmlElement(name = "AnswerID")
    protected int answerID;
    @XmlElement(name = "QuestionID")
    protected int questionID;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "AnswerPosition")
    protected int answerPosition;
    @XmlElement(name = "AnswerText")
    protected String answerText;

    /**
     * Gets the value of the answerID property.
     * 
     */
    public int getAnswerID() {
        return answerID;
    }

    /**
     * Sets the value of the answerID property.
     * 
     */
    public void setAnswerID(int value) {
        this.answerID = value;
    }

    /**
     * Gets the value of the questionID property.
     * 
     */
    public int getQuestionID() {
        return questionID;
    }

    /**
     * Sets the value of the questionID property.
     * 
     */
    public void setQuestionID(int value) {
        this.questionID = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the answerPosition property.
     * 
     */
    public int getAnswerPosition() {
        return answerPosition;
    }

    /**
     * Sets the value of the answerPosition property.
     * 
     */
    public void setAnswerPosition(int value) {
        this.answerPosition = value;
    }

    /**
     * Gets the value of the answerText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnswerText() {
        return answerText;
    }

    /**
     * Sets the value of the answerText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnswerText(String value) {
        this.answerText = value;
    }

}
