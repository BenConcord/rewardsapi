
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomEntityDataUpsertReturn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomEntityDataUpsertReturn"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReferenceId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ReferenceTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReferenceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EntityDataRows" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfCustomEntityDataUpsertRow" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomEntityDataUpsertReturn", propOrder = {
    "referenceId",
    "referenceTag",
    "referenceType",
    "entityDataRows"
})
public class CustomEntityDataUpsertReturn {

    @XmlElement(name = "ReferenceId")
    protected int referenceId;
    @XmlElement(name = "ReferenceTag")
    protected String referenceTag;
    @XmlElement(name = "ReferenceType")
    protected String referenceType;
    @XmlElement(name = "EntityDataRows")
    protected ArrayOfCustomEntityDataUpsertRow entityDataRows;

    /**
     * Gets the value of the referenceId property.
     * 
     */
    public int getReferenceId() {
        return referenceId;
    }

    /**
     * Sets the value of the referenceId property.
     * 
     */
    public void setReferenceId(int value) {
        this.referenceId = value;
    }

    /**
     * Gets the value of the referenceTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceTag() {
        return referenceTag;
    }

    /**
     * Sets the value of the referenceTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceTag(String value) {
        this.referenceTag = value;
    }

    /**
     * Gets the value of the referenceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceType() {
        return referenceType;
    }

    /**
     * Sets the value of the referenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceType(String value) {
        this.referenceType = value;
    }

    /**
     * Gets the value of the entityDataRows property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomEntityDataUpsertRow }
     *     
     */
    public ArrayOfCustomEntityDataUpsertRow getEntityDataRows() {
        return entityDataRows;
    }

    /**
     * Sets the value of the entityDataRows property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomEntityDataUpsertRow }
     *     
     */
    public void setEntityDataRows(ArrayOfCustomEntityDataUpsertRow value) {
        this.entityDataRows = value;
    }

}
