
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfKohlsTransactionDetailItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfKohlsTransactionDetailItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="KohlsTransactionDetailItem" type="{http://www.loyaltylab.com/loyaltyapi/}KohlsTransactionDetailItem" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfKohlsTransactionDetailItem", propOrder = {
    "kohlsTransactionDetailItem"
})
public class ArrayOfKohlsTransactionDetailItem {

    @XmlElement(name = "KohlsTransactionDetailItem", nillable = true)
    protected List<KohlsTransactionDetailItem> kohlsTransactionDetailItem;

    /**
     * Gets the value of the kohlsTransactionDetailItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the kohlsTransactionDetailItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKohlsTransactionDetailItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KohlsTransactionDetailItem }
     * 
     * 
     */
    public List<KohlsTransactionDetailItem> getKohlsTransactionDetailItem() {
        if (kohlsTransactionDetailItem == null) {
            kohlsTransactionDetailItem = new ArrayList<KohlsTransactionDetailItem>();
        }
        return this.kohlsTransactionDetailItem;
    }

}
