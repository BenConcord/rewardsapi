
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReturnsAward complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReturnsAward"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OrignalPurchaseAward" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="AvailableBalance" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ResultCode" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReturnsAward", propOrder = {
    "orignalPurchaseAward",
    "availableBalance",
    "resultCode"
})
public class ReturnsAward {

    @XmlElement(name = "OrignalPurchaseAward")
    protected int orignalPurchaseAward;
    @XmlElement(name = "AvailableBalance")
    protected int availableBalance;
    @XmlElement(name = "ResultCode")
    protected int resultCode;

    /**
     * Gets the value of the orignalPurchaseAward property.
     * 
     */
    public int getOrignalPurchaseAward() {
        return orignalPurchaseAward;
    }

    /**
     * Sets the value of the orignalPurchaseAward property.
     * 
     */
    public void setOrignalPurchaseAward(int value) {
        this.orignalPurchaseAward = value;
    }

    /**
     * Gets the value of the availableBalance property.
     * 
     */
    public int getAvailableBalance() {
        return availableBalance;
    }

    /**
     * Sets the value of the availableBalance property.
     * 
     */
    public void setAvailableBalance(int value) {
        this.availableBalance = value;
    }

    /**
     * Gets the value of the resultCode property.
     * 
     */
    public int getResultCode() {
        return resultCode;
    }

    /**
     * Sets the value of the resultCode property.
     * 
     */
    public void setResultCode(int value) {
        this.resultCode = value;
    }

}
