
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fromShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="toShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fromShopperId",
    "toShopperId",
    "description",
    "note"
})
@XmlRootElement(name = "MergeShoppers")
public class MergeShoppers {

    protected int fromShopperId;
    protected int toShopperId;
    protected String description;
    protected String note;

    /**
     * Gets the value of the fromShopperId property.
     * 
     */
    public int getFromShopperId() {
        return fromShopperId;
    }

    /**
     * Sets the value of the fromShopperId property.
     * 
     */
    public void setFromShopperId(int value) {
        this.fromShopperId = value;
    }

    /**
     * Gets the value of the toShopperId property.
     * 
     */
    public int getToShopperId() {
        return toShopperId;
    }

    /**
     * Sets the value of the toShopperId property.
     * 
     */
    public void setToShopperId(int value) {
        this.toShopperId = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

}
