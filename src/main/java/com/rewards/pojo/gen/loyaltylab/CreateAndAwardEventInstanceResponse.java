
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateAndAwardEventInstanceResult" type="{http://www.loyaltylab.com/loyaltyapi/}EventAwards" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createAndAwardEventInstanceResult"
})
@XmlRootElement(name = "CreateAndAwardEventInstanceResponse")
public class CreateAndAwardEventInstanceResponse {

    @XmlElement(name = "CreateAndAwardEventInstanceResult")
    protected EventAwards createAndAwardEventInstanceResult;

    /**
     * Gets the value of the createAndAwardEventInstanceResult property.
     * 
     * @return
     *     possible object is
     *     {@link EventAwards }
     *     
     */
    public EventAwards getCreateAndAwardEventInstanceResult() {
        return createAndAwardEventInstanceResult;
    }

    /**
     * Sets the value of the createAndAwardEventInstanceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventAwards }
     *     
     */
    public void setCreateAndAwardEventInstanceResult(EventAwards value) {
        this.createAndAwardEventInstanceResult = value;
    }

}
