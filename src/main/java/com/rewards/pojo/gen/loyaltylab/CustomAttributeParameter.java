
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomAttributeParameter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomAttributeParameter"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReferenceId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ReferenceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributesToSet" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfAttributeToSet" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomAttributeParameter", propOrder = {
    "referenceId",
    "referenceType",
    "attributesToSet"
})
public class CustomAttributeParameter {

    @XmlElement(name = "ReferenceId")
    protected int referenceId;
    @XmlElement(name = "ReferenceType")
    protected String referenceType;
    @XmlElement(name = "AttributesToSet")
    protected ArrayOfAttributeToSet attributesToSet;

    /**
     * Gets the value of the referenceId property.
     * 
     */
    public int getReferenceId() {
        return referenceId;
    }

    /**
     * Sets the value of the referenceId property.
     * 
     */
    public void setReferenceId(int value) {
        this.referenceId = value;
    }

    /**
     * Gets the value of the referenceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceType() {
        return referenceType;
    }

    /**
     * Sets the value of the referenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceType(String value) {
        this.referenceType = value;
    }

    /**
     * Gets the value of the attributesToSet property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAttributeToSet }
     *     
     */
    public ArrayOfAttributeToSet getAttributesToSet() {
        return attributesToSet;
    }

    /**
     * Sets the value of the attributesToSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAttributeToSet }
     *     
     */
    public void setAttributesToSet(ArrayOfAttributeToSet value) {
        this.attributesToSet = value;
    }

}
