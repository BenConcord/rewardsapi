
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperOffersExtendedResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfOfferStatusExtended" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperOffersExtendedResult"
})
@XmlRootElement(name = "GetShopperOffersExtendedResponse")
public class GetShopperOffersExtendedResponse {

    @XmlElement(name = "GetShopperOffersExtendedResult")
    protected ArrayOfOfferStatusExtended getShopperOffersExtendedResult;

    /**
     * Gets the value of the getShopperOffersExtendedResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOfferStatusExtended }
     *     
     */
    public ArrayOfOfferStatusExtended getGetShopperOffersExtendedResult() {
        return getShopperOffersExtendedResult;
    }

    /**
     * Sets the value of the getShopperOffersExtendedResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOfferStatusExtended }
     *     
     */
    public void setGetShopperOffersExtendedResult(ArrayOfOfferStatusExtended value) {
        this.getShopperOffersExtendedResult = value;
    }

}
