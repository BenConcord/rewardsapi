
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KohlsAwardDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KohlsAwardDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.loyaltylab.com/loyaltyapi/}KohlsAwardDetail"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RewardValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KohlsAwardDetails", propOrder = {
    "rewardValue"
})
public class KohlsAwardDetails
    extends KohlsAwardDetail
{

    @XmlElement(name = "RewardValue")
    protected String rewardValue;

    /**
     * Gets the value of the rewardValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRewardValue() {
        return rewardValue;
    }

    /**
     * Sets the value of the rewardValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRewardValue(String value) {
        this.rewardValue = value;
    }

}
