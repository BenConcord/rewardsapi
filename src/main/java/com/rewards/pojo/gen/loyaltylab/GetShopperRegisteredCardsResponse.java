
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperRegisteredCardsResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfRegisteredCard" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperRegisteredCardsResult"
})
@XmlRootElement(name = "GetShopperRegisteredCardsResponse")
public class GetShopperRegisteredCardsResponse {

    @XmlElement(name = "GetShopperRegisteredCardsResult")
    protected ArrayOfRegisteredCard getShopperRegisteredCardsResult;

    /**
     * Gets the value of the getShopperRegisteredCardsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRegisteredCard }
     *     
     */
    public ArrayOfRegisteredCard getGetShopperRegisteredCardsResult() {
        return getShopperRegisteredCardsResult;
    }

    /**
     * Sets the value of the getShopperRegisteredCardsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRegisteredCard }
     *     
     */
    public void setGetShopperRegisteredCardsResult(ArrayOfRegisteredCard value) {
        this.getShopperRegisteredCardsResult = value;
    }

}
