
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OfferCategory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferCategory"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OfferCategoryId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RetailerGuid" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DisplayRank" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CreatedByUserId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UpdateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="LastModifiedByUserId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ReferenceTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferCategory", propOrder = {
    "offerCategoryId",
    "retailerGuid",
    "name",
    "description",
    "displayRank",
    "createDateTime",
    "createdByUserId",
    "updateDateTime",
    "lastModifiedByUserId",
    "referenceTag"
})
public class OfferCategory {

    @XmlElement(name = "OfferCategoryId")
    protected int offerCategoryId;
    @XmlElement(name = "RetailerGuid", required = true)
    protected String retailerGuid;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "DisplayRank")
    protected int displayRank;
    @XmlElement(name = "CreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDateTime;
    @XmlElement(name = "CreatedByUserId")
    protected int createdByUserId;
    @XmlElement(name = "UpdateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateDateTime;
    @XmlElement(name = "LastModifiedByUserId")
    protected int lastModifiedByUserId;
    @XmlElement(name = "ReferenceTag")
    protected String referenceTag;

    /**
     * Gets the value of the offerCategoryId property.
     * 
     */
    public int getOfferCategoryId() {
        return offerCategoryId;
    }

    /**
     * Sets the value of the offerCategoryId property.
     * 
     */
    public void setOfferCategoryId(int value) {
        this.offerCategoryId = value;
    }

    /**
     * Gets the value of the retailerGuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerGuid() {
        return retailerGuid;
    }

    /**
     * Sets the value of the retailerGuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerGuid(String value) {
        this.retailerGuid = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the displayRank property.
     * 
     */
    public int getDisplayRank() {
        return displayRank;
    }

    /**
     * Sets the value of the displayRank property.
     * 
     */
    public void setDisplayRank(int value) {
        this.displayRank = value;
    }

    /**
     * Gets the value of the createDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the value of the createDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDateTime(XMLGregorianCalendar value) {
        this.createDateTime = value;
    }

    /**
     * Gets the value of the createdByUserId property.
     * 
     */
    public int getCreatedByUserId() {
        return createdByUserId;
    }

    /**
     * Sets the value of the createdByUserId property.
     * 
     */
    public void setCreatedByUserId(int value) {
        this.createdByUserId = value;
    }

    /**
     * Gets the value of the updateDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUpdateDateTime() {
        return updateDateTime;
    }

    /**
     * Sets the value of the updateDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUpdateDateTime(XMLGregorianCalendar value) {
        this.updateDateTime = value;
    }

    /**
     * Gets the value of the lastModifiedByUserId property.
     * 
     */
    public int getLastModifiedByUserId() {
        return lastModifiedByUserId;
    }

    /**
     * Sets the value of the lastModifiedByUserId property.
     * 
     */
    public void setLastModifiedByUserId(int value) {
        this.lastModifiedByUserId = value;
    }

    /**
     * Gets the value of the referenceTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceTag() {
        return referenceTag;
    }

    /**
     * Sets the value of the referenceTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceTag(String value) {
        this.referenceTag = value;
    }

}
