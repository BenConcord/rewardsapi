
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetPlcAvailablePointsByDateByPointGroupResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPlcAvailablePointsByDateByPointGroupResult"
})
@XmlRootElement(name = "GetPlcAvailablePointsByDateByPointGroupResponse")
public class GetPlcAvailablePointsByDateByPointGroupResponse {

    @XmlElement(name = "GetPlcAvailablePointsByDateByPointGroupResult")
    protected int getPlcAvailablePointsByDateByPointGroupResult;

    /**
     * Gets the value of the getPlcAvailablePointsByDateByPointGroupResult property.
     * 
     */
    public int getGetPlcAvailablePointsByDateByPointGroupResult() {
        return getPlcAvailablePointsByDateByPointGroupResult;
    }

    /**
     * Sets the value of the getPlcAvailablePointsByDateByPointGroupResult property.
     * 
     */
    public void setGetPlcAvailablePointsByDateByPointGroupResult(int value) {
        this.getPlcAvailablePointsByDateByPointGroupResult = value;
    }

}
