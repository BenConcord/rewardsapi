
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateCustomEntityResult" type="{http://www.loyaltylab.com/loyaltyapi/}CustomEntityDataUpsertReturn" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateCustomEntityResult"
})
@XmlRootElement(name = "UpdateCustomEntityResponse")
public class UpdateCustomEntityResponse {

    @XmlElement(name = "UpdateCustomEntityResult")
    protected CustomEntityDataUpsertReturn updateCustomEntityResult;

    /**
     * Gets the value of the updateCustomEntityResult property.
     * 
     * @return
     *     possible object is
     *     {@link CustomEntityDataUpsertReturn }
     *     
     */
    public CustomEntityDataUpsertReturn getUpdateCustomEntityResult() {
        return updateCustomEntityResult;
    }

    /**
     * Sets the value of the updateCustomEntityResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomEntityDataUpsertReturn }
     *     
     */
    public void setUpdateCustomEntityResult(CustomEntityDataUpsertReturn value) {
        this.updateCustomEntityResult = value;
    }

}
