
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RedeemOfferResult" type="{http://www.loyaltylab.com/loyaltyapi/}ShopperRewardItemRedemption" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "redeemOfferResult"
})
@XmlRootElement(name = "RedeemOfferResponse")
public class RedeemOfferResponse {

    @XmlElement(name = "RedeemOfferResult")
    protected ShopperRewardItemRedemption redeemOfferResult;

    /**
     * Gets the value of the redeemOfferResult property.
     * 
     * @return
     *     possible object is
     *     {@link ShopperRewardItemRedemption }
     *     
     */
    public ShopperRewardItemRedemption getRedeemOfferResult() {
        return redeemOfferResult;
    }

    /**
     * Sets the value of the redeemOfferResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShopperRewardItemRedemption }
     *     
     */
    public void setRedeemOfferResult(ShopperRewardItemRedemption value) {
        this.redeemOfferResult = value;
    }

}
