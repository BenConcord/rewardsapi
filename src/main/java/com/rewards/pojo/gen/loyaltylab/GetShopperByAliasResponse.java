
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperByAliasResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfKohlsShopperDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperByAliasResult"
})
@XmlRootElement(name = "GetShopperByAliasResponse")
public class GetShopperByAliasResponse {

    @XmlElement(name = "GetShopperByAliasResult")
    protected ArrayOfKohlsShopperDetails getShopperByAliasResult;

    /**
     * Gets the value of the getShopperByAliasResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKohlsShopperDetails }
     *     
     */
    public ArrayOfKohlsShopperDetails getGetShopperByAliasResult() {
        return getShopperByAliasResult;
    }

    /**
     * Sets the value of the getShopperByAliasResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKohlsShopperDetails }
     *     
     */
    public void setGetShopperByAliasResult(ArrayOfKohlsShopperDetails value) {
        this.getShopperByAliasResult = value;
    }

}
