
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetReferenceObjectFromUniqueCustomEntityResult" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getReferenceObjectFromUniqueCustomEntityResult"
})
@XmlRootElement(name = "GetReferenceObjectFromUniqueCustomEntityResponse")
public class GetReferenceObjectFromUniqueCustomEntityResponse {

    @XmlElement(name = "GetReferenceObjectFromUniqueCustomEntityResult")
    protected Object getReferenceObjectFromUniqueCustomEntityResult;

    /**
     * Gets the value of the getReferenceObjectFromUniqueCustomEntityResult property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getGetReferenceObjectFromUniqueCustomEntityResult() {
        return getReferenceObjectFromUniqueCustomEntityResult;
    }

    /**
     * Sets the value of the getReferenceObjectFromUniqueCustomEntityResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setGetReferenceObjectFromUniqueCustomEntityResult(Object value) {
        this.getReferenceObjectFromUniqueCustomEntityResult = value;
    }

}
