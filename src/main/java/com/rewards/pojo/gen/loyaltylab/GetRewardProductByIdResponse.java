
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetRewardProductByIdResult" type="{http://www.loyaltylab.com/loyaltyapi/}RewardProduct" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRewardProductByIdResult"
})
@XmlRootElement(name = "GetRewardProductByIdResponse")
public class GetRewardProductByIdResponse {

    @XmlElement(name = "GetRewardProductByIdResult")
    protected RewardProduct getRewardProductByIdResult;

    /**
     * Gets the value of the getRewardProductByIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link RewardProduct }
     *     
     */
    public RewardProduct getGetRewardProductByIdResult() {
        return getRewardProductByIdResult;
    }

    /**
     * Sets the value of the getRewardProductByIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardProduct }
     *     
     */
    public void setGetRewardProductByIdResult(RewardProduct value) {
        this.getRewardProductByIdResult = value;
    }

}
