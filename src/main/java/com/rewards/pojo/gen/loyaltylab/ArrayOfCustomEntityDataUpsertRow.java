
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfCustomEntityDataUpsertRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomEntityDataUpsertRow"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomEntityDataUpsertRow" type="{http://www.loyaltylab.com/loyaltyapi/}CustomEntityDataUpsertRow" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomEntityDataUpsertRow", propOrder = {
    "customEntityDataUpsertRow"
})
public class ArrayOfCustomEntityDataUpsertRow {

    @XmlElement(name = "CustomEntityDataUpsertRow", nillable = true)
    protected List<CustomEntityDataUpsertRow> customEntityDataUpsertRow;

    /**
     * Gets the value of the customEntityDataUpsertRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customEntityDataUpsertRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomEntityDataUpsertRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomEntityDataUpsertRow }
     * 
     * 
     */
    public List<CustomEntityDataUpsertRow> getCustomEntityDataUpsertRow() {
        if (customEntityDataUpsertRow == null) {
            customEntityDataUpsertRow = new ArrayList<CustomEntityDataUpsertRow>();
        }
        return this.customEntityDataUpsertRow;
    }

}
