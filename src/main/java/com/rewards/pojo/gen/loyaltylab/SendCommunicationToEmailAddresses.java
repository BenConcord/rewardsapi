
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="emailAddresses" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfString" minOccurs="0"/&gt;
 *         &lt;element name="communicationID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="forceEmailSend" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "emailAddresses",
    "communicationID",
    "forceEmailSend"
})
@XmlRootElement(name = "SendCommunicationToEmailAddresses")
public class SendCommunicationToEmailAddresses {

    protected ArrayOfString emailAddresses;
    protected int communicationID;
    protected boolean forceEmailSend;

    /**
     * Gets the value of the emailAddresses property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getEmailAddresses() {
        return emailAddresses;
    }

    /**
     * Sets the value of the emailAddresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setEmailAddresses(ArrayOfString value) {
        this.emailAddresses = value;
    }

    /**
     * Gets the value of the communicationID property.
     * 
     */
    public int getCommunicationID() {
        return communicationID;
    }

    /**
     * Sets the value of the communicationID property.
     * 
     */
    public void setCommunicationID(int value) {
        this.communicationID = value;
    }

    /**
     * Gets the value of the forceEmailSend property.
     * 
     */
    public boolean isForceEmailSend() {
        return forceEmailSend;
    }

    /**
     * Sets the value of the forceEmailSend property.
     * 
     */
    public void setForceEmailSend(boolean value) {
        this.forceEmailSend = value;
    }

}
