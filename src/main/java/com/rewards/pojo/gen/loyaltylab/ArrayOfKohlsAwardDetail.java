
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfKohlsAwardDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfKohlsAwardDetail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="KohlsAwardDetail" type="{http://www.loyaltylab.com/loyaltyapi/}KohlsAwardDetail" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfKohlsAwardDetail", propOrder = {
    "kohlsAwardDetail"
})
public class ArrayOfKohlsAwardDetail {

    @XmlElement(name = "KohlsAwardDetail", nillable = true)
    protected List<KohlsAwardDetail> kohlsAwardDetail;

    /**
     * Gets the value of the kohlsAwardDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the kohlsAwardDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKohlsAwardDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KohlsAwardDetail }
     * 
     * 
     */
    public List<KohlsAwardDetail> getKohlsAwardDetail() {
        if (kohlsAwardDetail == null) {
            kohlsAwardDetail = new ArrayList<KohlsAwardDetail>();
        }
        return this.kohlsAwardDetail;
    }

}
