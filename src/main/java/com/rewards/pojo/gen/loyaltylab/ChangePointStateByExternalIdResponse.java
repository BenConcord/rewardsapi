
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ChangePointStateByExternalIdResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "changePointStateByExternalIdResult"
})
@XmlRootElement(name = "ChangePointStateByExternalIdResponse")
public class ChangePointStateByExternalIdResponse {

    @XmlElement(name = "ChangePointStateByExternalIdResult")
    protected boolean changePointStateByExternalIdResult;

    /**
     * Gets the value of the changePointStateByExternalIdResult property.
     * 
     */
    public boolean isChangePointStateByExternalIdResult() {
        return changePointStateByExternalIdResult;
    }

    /**
     * Sets the value of the changePointStateByExternalIdResult property.
     * 
     */
    public void setChangePointStateByExternalIdResult(boolean value) {
        this.changePointStateByExternalIdResult = value;
    }

}
