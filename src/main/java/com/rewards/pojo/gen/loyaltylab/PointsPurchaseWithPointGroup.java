
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sku" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="basePrice" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="effectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="retailerShopperId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="newPointState" type="{http://www.loyaltylab.com/loyaltyapi/}PointStateEnum"/&gt;
 *         &lt;element name="pointGroupRefTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sku",
    "basePrice",
    "effectiveDate",
    "retailerShopperId",
    "newPointState",
    "pointGroupRefTag"
})
@XmlRootElement(name = "PointsPurchaseWithPointGroup")
public class PointsPurchaseWithPointGroup {

    protected String sku;
    protected double basePrice;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    protected String retailerShopperId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected PointStateEnum newPointState;
    protected String pointGroupRefTag;

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSku() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSku(String value) {
        this.sku = value;
    }

    /**
     * Gets the value of the basePrice property.
     * 
     */
    public double getBasePrice() {
        return basePrice;
    }

    /**
     * Sets the value of the basePrice property.
     * 
     */
    public void setBasePrice(double value) {
        this.basePrice = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the retailerShopperId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerShopperId() {
        return retailerShopperId;
    }

    /**
     * Sets the value of the retailerShopperId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerShopperId(String value) {
        this.retailerShopperId = value;
    }

    /**
     * Gets the value of the newPointState property.
     * 
     * @return
     *     possible object is
     *     {@link PointStateEnum }
     *     
     */
    public PointStateEnum getNewPointState() {
        return newPointState;
    }

    /**
     * Sets the value of the newPointState property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointStateEnum }
     *     
     */
    public void setNewPointState(PointStateEnum value) {
        this.newPointState = value;
    }

    /**
     * Gets the value of the pointGroupRefTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointGroupRefTag() {
        return pointGroupRefTag;
    }

    /**
     * Sets the value of the pointGroupRefTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointGroupRefTag(String value) {
        this.pointGroupRefTag = value;
    }

}
