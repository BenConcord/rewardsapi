
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for ReturnOrderItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReturnOrderItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="OriginalRetailerOrderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OriginalRetailerReceiptDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="DollarRevenue" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReturnOrderItem", propOrder = {
    "shopperId",
    "originalRetailerOrderId",
    "originalRetailerReceiptDateTime",
    "quantity",
    "dollarRevenue",
    "sku"
})
public class ReturnOrderItem {

    @XmlElement(name = "ShopperId")
    protected int shopperId;
    @XmlElement(name = "OriginalRetailerOrderId")
    protected String originalRetailerOrderId;
    @XmlElement(name = "OriginalRetailerReceiptDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar originalRetailerReceiptDateTime;
    @XmlElement(name = "Quantity")
    protected int quantity;
    @XmlElement(name = "DollarRevenue", required = true)
    protected BigDecimal dollarRevenue;
    @XmlElement(name = "SKU")
    protected String sku;

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the originalRetailerOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalRetailerOrderId() {
        return originalRetailerOrderId;
    }

    /**
     * Sets the value of the originalRetailerOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalRetailerOrderId(String value) {
        this.originalRetailerOrderId = value;
    }

    /**
     * Gets the value of the originalRetailerReceiptDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOriginalRetailerReceiptDateTime() {
        return originalRetailerReceiptDateTime;
    }

    /**
     * Sets the value of the originalRetailerReceiptDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOriginalRetailerReceiptDateTime(XMLGregorianCalendar value) {
        this.originalRetailerReceiptDateTime = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     */
    public void setQuantity(int value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the dollarRevenue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDollarRevenue() {
        return dollarRevenue;
    }

    /**
     * Sets the value of the dollarRevenue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDollarRevenue(BigDecimal value) {
        this.dollarRevenue = value;
    }

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSKU() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSKU(String value) {
        this.sku = value;
    }

}
