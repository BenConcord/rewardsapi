
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PointStateEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PointStateEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Unknown"/&gt;
 *     &lt;enumeration value="PendingEarn"/&gt;
 *     &lt;enumeration value="Available"/&gt;
 *     &lt;enumeration value="PendingRedeem"/&gt;
 *     &lt;enumeration value="Redeemed"/&gt;
 *     &lt;enumeration value="Expired"/&gt;
 *     &lt;enumeration value="Cancelled"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PointStateEnum")
@XmlEnum
public enum PointStateEnum {

    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    @XmlEnumValue("PendingEarn")
    PENDING_EARN("PendingEarn"),
    @XmlEnumValue("Available")
    AVAILABLE("Available"),
    @XmlEnumValue("PendingRedeem")
    PENDING_REDEEM("PendingRedeem"),
    @XmlEnumValue("Redeemed")
    REDEEMED("Redeemed"),
    @XmlEnumValue("Expired")
    EXPIRED("Expired"),
    @XmlEnumValue("Cancelled")
    CANCELLED("Cancelled");
    private final String value;

    PointStateEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PointStateEnum fromValue(String v) {
        for (PointStateEnum c: PointStateEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
