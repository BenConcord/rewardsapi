
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PurchaseTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseTransaction"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShopperIdentifier" type="{http://www.loyaltylab.com/loyaltyapi/}ShopperIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="TransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StoreId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SalesAssociateId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegisterId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReceiptDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="LineItems" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfLineItem" minOccurs="0"/&gt;
 *         &lt;element name="Tenders" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfTender" minOccurs="0"/&gt;
 *         &lt;element name="Coupons" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfCoupon" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseTransaction", propOrder = {
    "shopperIdentifier",
    "transactionId",
    "storeId",
    "salesAssociateId",
    "registerId",
    "receiptDateTime",
    "lineItems",
    "tenders",
    "coupons"
})
public class PurchaseTransaction {

    @XmlElement(name = "ShopperIdentifier")
    protected ShopperIdentifier shopperIdentifier;
    @XmlElement(name = "TransactionId")
    protected String transactionId;
    @XmlElement(name = "StoreId")
    protected String storeId;
    @XmlElement(name = "SalesAssociateId")
    protected String salesAssociateId;
    @XmlElement(name = "RegisterId")
    protected String registerId;
    @XmlElement(name = "ReceiptDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receiptDateTime;
    @XmlElement(name = "LineItems")
    protected ArrayOfLineItem lineItems;
    @XmlElement(name = "Tenders")
    protected ArrayOfTender tenders;
    @XmlElement(name = "Coupons")
    protected ArrayOfCoupon coupons;

    /**
     * Gets the value of the shopperIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link ShopperIdentifier }
     *     
     */
    public ShopperIdentifier getShopperIdentifier() {
        return shopperIdentifier;
    }

    /**
     * Sets the value of the shopperIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShopperIdentifier }
     *     
     */
    public void setShopperIdentifier(ShopperIdentifier value) {
        this.shopperIdentifier = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the storeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * Sets the value of the storeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreId(String value) {
        this.storeId = value;
    }

    /**
     * Gets the value of the salesAssociateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesAssociateId() {
        return salesAssociateId;
    }

    /**
     * Sets the value of the salesAssociateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesAssociateId(String value) {
        this.salesAssociateId = value;
    }

    /**
     * Gets the value of the registerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterId() {
        return registerId;
    }

    /**
     * Sets the value of the registerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterId(String value) {
        this.registerId = value;
    }

    /**
     * Gets the value of the receiptDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceiptDateTime() {
        return receiptDateTime;
    }

    /**
     * Sets the value of the receiptDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceiptDateTime(XMLGregorianCalendar value) {
        this.receiptDateTime = value;
    }

    /**
     * Gets the value of the lineItems property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfLineItem }
     *     
     */
    public ArrayOfLineItem getLineItems() {
        return lineItems;
    }

    /**
     * Sets the value of the lineItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfLineItem }
     *     
     */
    public void setLineItems(ArrayOfLineItem value) {
        this.lineItems = value;
    }

    /**
     * Gets the value of the tenders property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTender }
     *     
     */
    public ArrayOfTender getTenders() {
        return tenders;
    }

    /**
     * Sets the value of the tenders property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTender }
     *     
     */
    public void setTenders(ArrayOfTender value) {
        this.tenders = value;
    }

    /**
     * Gets the value of the coupons property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCoupon }
     *     
     */
    public ArrayOfCoupon getCoupons() {
        return coupons;
    }

    /**
     * Sets the value of the coupons property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCoupon }
     *     
     */
    public void setCoupons(ArrayOfCoupon value) {
        this.coupons = value;
    }

}
