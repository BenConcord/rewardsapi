
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Shopper complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Shopper"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RetailerGUID" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmailFrequency" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EmailFrequencyUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmailFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MiddleInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Address1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Address2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MobilePhoneEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProfileCreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ProfileUpdateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PasswordLastChanged" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="Origin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailerShopperId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FileImportId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="BulkEmail" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LoyaltyMember" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="PersonStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailerRegistered" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="MailOptIn" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="PhoneOptIn" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="SourceReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailerShopperCreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="BulkEmailSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BulkEmailSourceDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LoyaltyLabCreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="StatusUpdateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Shopper", propOrder = {
    "shopperId",
    "retailerGUID",
    "emailAddress",
    "emailFrequency",
    "emailFrequencyUnit",
    "emailFormat",
    "password",
    "status",
    "lastName",
    "middleInitial",
    "firstName",
    "address1",
    "address2",
    "city",
    "state",
    "zip",
    "phoneNumber",
    "mobilePhoneEmail",
    "profileCreateDateTime",
    "profileUpdateDateTime",
    "createDateTime",
    "passwordLastChanged",
    "origin",
    "retailerShopperId",
    "fileImportId",
    "bulkEmail",
    "loyaltyMember",
    "personStatus",
    "retailerRegistered",
    "mailOptIn",
    "phoneOptIn",
    "sourceReference",
    "retailerShopperCreationDate",
    "bulkEmailSource",
    "bulkEmailSourceDescription",
    "loyaltyLabCreateDateTime",
    "statusUpdateDateTime",
    "userName"
})
@XmlRootElement
public class Shopper {

    @XmlElement(name = "ShopperId")
    protected int shopperId;
    @XmlElement(name = "RetailerGUID", required = true)
    protected String retailerGUID;
    @XmlElement(name = "EmailAddress")
    protected String emailAddress;
    @XmlElement(name = "EmailFrequency")
    protected int emailFrequency;
    @XmlElement(name = "EmailFrequencyUnit")
    protected String emailFrequencyUnit;
    @XmlElement(name = "EmailFormat")
    protected String emailFormat;
    @XmlElement(name = "Password")
    protected String password;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "MiddleInitial")
    protected String middleInitial;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "Address1")
    protected String address1;
    @XmlElement(name = "Address2")
    protected String address2;
    @XmlElement(name = "City")
    protected String city;
    @XmlElement(name = "State")
    protected String state;
    @XmlElement(name = "Zip")
    protected String zip;
    @XmlElement(name = "PhoneNumber")
    protected String phoneNumber;
    @XmlElement(name = "MobilePhoneEmail")
    protected String mobilePhoneEmail;
    @XmlElement(name = "ProfileCreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar profileCreateDateTime;
    @XmlElement(name = "ProfileUpdateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar profileUpdateDateTime;
    @XmlElement(name = "CreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDateTime;
    @XmlElement(name = "PasswordLastChanged", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar passwordLastChanged;
    @XmlElement(name = "Origin")
    protected String origin;
    @XmlElement(name = "RetailerShopperId")
    protected String retailerShopperId;
    @XmlElement(name = "FileImportId")
    protected int fileImportId;
    @XmlElement(name = "BulkEmail")
    protected int bulkEmail;
    @XmlElement(name = "LoyaltyMember")
    protected boolean loyaltyMember;
    @XmlElement(name = "PersonStatus")
    protected String personStatus;
    @XmlElement(name = "RetailerRegistered")
    protected boolean retailerRegistered;
    @XmlElement(name = "MailOptIn")
    protected boolean mailOptIn;
    @XmlElement(name = "PhoneOptIn")
    protected boolean phoneOptIn;
    @XmlElement(name = "SourceReference")
    protected String sourceReference;
    @XmlElement(name = "RetailerShopperCreationDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar retailerShopperCreationDate;
    @XmlElement(name = "BulkEmailSource")
    protected String bulkEmailSource;
    @XmlElement(name = "BulkEmailSourceDescription")
    protected String bulkEmailSourceDescription;
    @XmlElement(name = "LoyaltyLabCreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar loyaltyLabCreateDateTime;
    @XmlElement(name = "StatusUpdateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusUpdateDateTime;
    @XmlElement(name = "UserName")
    protected String userName;

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the retailerGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerGUID() {
        return retailerGUID;
    }

    /**
     * Sets the value of the retailerGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerGUID(String value) {
        this.retailerGUID = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the emailFrequency property.
     * 
     */
    public int getEmailFrequency() {
        return emailFrequency;
    }

    /**
     * Sets the value of the emailFrequency property.
     * 
     */
    public void setEmailFrequency(int value) {
        this.emailFrequency = value;
    }

    /**
     * Gets the value of the emailFrequencyUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailFrequencyUnit() {
        return emailFrequencyUnit;
    }

    /**
     * Sets the value of the emailFrequencyUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailFrequencyUnit(String value) {
        this.emailFrequencyUnit = value;
    }

    /**
     * Gets the value of the emailFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailFormat() {
        return emailFormat;
    }

    /**
     * Sets the value of the emailFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailFormat(String value) {
        this.emailFormat = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the middleInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleInitial() {
        return middleInitial;
    }

    /**
     * Sets the value of the middleInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleInitial(String value) {
        this.middleInitial = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the address1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the value of the address1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress1(String value) {
        this.address1 = value;
    }

    /**
     * Gets the value of the address2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the value of the address2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress2(String value) {
        this.address2 = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the zip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZip() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZip(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the mobilePhoneEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilePhoneEmail() {
        return mobilePhoneEmail;
    }

    /**
     * Sets the value of the mobilePhoneEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilePhoneEmail(String value) {
        this.mobilePhoneEmail = value;
    }

    /**
     * Gets the value of the profileCreateDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProfileCreateDateTime() {
        return profileCreateDateTime;
    }

    /**
     * Sets the value of the profileCreateDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProfileCreateDateTime(XMLGregorianCalendar value) {
        this.profileCreateDateTime = value;
    }

    /**
     * Gets the value of the profileUpdateDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProfileUpdateDateTime() {
        return profileUpdateDateTime;
    }

    /**
     * Sets the value of the profileUpdateDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProfileUpdateDateTime(XMLGregorianCalendar value) {
        this.profileUpdateDateTime = value;
    }

    /**
     * Gets the value of the createDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the value of the createDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDateTime(XMLGregorianCalendar value) {
        this.createDateTime = value;
    }

    /**
     * Gets the value of the passwordLastChanged property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPasswordLastChanged() {
        return passwordLastChanged;
    }

    /**
     * Sets the value of the passwordLastChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPasswordLastChanged(XMLGregorianCalendar value) {
        this.passwordLastChanged = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Gets the value of the retailerShopperId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerShopperId() {
        return retailerShopperId;
    }

    /**
     * Sets the value of the retailerShopperId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerShopperId(String value) {
        this.retailerShopperId = value;
    }

    /**
     * Gets the value of the fileImportId property.
     * 
     */
    public int getFileImportId() {
        return fileImportId;
    }

    /**
     * Sets the value of the fileImportId property.
     * 
     */
    public void setFileImportId(int value) {
        this.fileImportId = value;
    }

    /**
     * Gets the value of the bulkEmail property.
     * 
     */
    public int getBulkEmail() {
        return bulkEmail;
    }

    /**
     * Sets the value of the bulkEmail property.
     * 
     */
    public void setBulkEmail(int value) {
        this.bulkEmail = value;
    }

    /**
     * Gets the value of the loyaltyMember property.
     * 
     */
    public boolean isLoyaltyMember() {
        return loyaltyMember;
    }

    /**
     * Sets the value of the loyaltyMember property.
     * 
     */
    public void setLoyaltyMember(boolean value) {
        this.loyaltyMember = value;
    }

    /**
     * Gets the value of the personStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonStatus() {
        return personStatus;
    }

    /**
     * Sets the value of the personStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonStatus(String value) {
        this.personStatus = value;
    }

    /**
     * Gets the value of the retailerRegistered property.
     * 
     */
    public boolean isRetailerRegistered() {
        return retailerRegistered;
    }

    /**
     * Sets the value of the retailerRegistered property.
     * 
     */
    public void setRetailerRegistered(boolean value) {
        this.retailerRegistered = value;
    }

    /**
     * Gets the value of the mailOptIn property.
     * 
     */
    public boolean isMailOptIn() {
        return mailOptIn;
    }

    /**
     * Sets the value of the mailOptIn property.
     * 
     */
    public void setMailOptIn(boolean value) {
        this.mailOptIn = value;
    }

    /**
     * Gets the value of the phoneOptIn property.
     * 
     */
    public boolean isPhoneOptIn() {
        return phoneOptIn;
    }

    /**
     * Sets the value of the phoneOptIn property.
     * 
     */
    public void setPhoneOptIn(boolean value) {
        this.phoneOptIn = value;
    }

    /**
     * Gets the value of the sourceReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceReference() {
        return sourceReference;
    }

    /**
     * Sets the value of the sourceReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceReference(String value) {
        this.sourceReference = value;
    }

    /**
     * Gets the value of the retailerShopperCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRetailerShopperCreationDate() {
        return retailerShopperCreationDate;
    }

    /**
     * Sets the value of the retailerShopperCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRetailerShopperCreationDate(XMLGregorianCalendar value) {
        this.retailerShopperCreationDate = value;
    }

    /**
     * Gets the value of the bulkEmailSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBulkEmailSource() {
        return bulkEmailSource;
    }

    /**
     * Sets the value of the bulkEmailSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBulkEmailSource(String value) {
        this.bulkEmailSource = value;
    }

    /**
     * Gets the value of the bulkEmailSourceDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBulkEmailSourceDescription() {
        return bulkEmailSourceDescription;
    }

    /**
     * Sets the value of the bulkEmailSourceDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBulkEmailSourceDescription(String value) {
        this.bulkEmailSourceDescription = value;
    }

    /**
     * Gets the value of the loyaltyLabCreateDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLoyaltyLabCreateDateTime() {
        return loyaltyLabCreateDateTime;
    }

    /**
     * Sets the value of the loyaltyLabCreateDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLoyaltyLabCreateDateTime(XMLGregorianCalendar value) {
        this.loyaltyLabCreateDateTime = value;
    }

    /**
     * Gets the value of the statusUpdateDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusUpdateDateTime() {
        return statusUpdateDateTime;
    }

    /**
     * Sets the value of the statusUpdateDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusUpdateDateTime(XMLGregorianCalendar value) {
        this.statusUpdateDateTime = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

}
