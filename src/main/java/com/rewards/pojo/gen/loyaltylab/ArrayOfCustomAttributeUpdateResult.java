
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfCustomAttributeUpdateResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomAttributeUpdateResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomAttributeUpdateResult" type="{http://www.loyaltylab.com/loyaltyapi/}CustomAttributeUpdateResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomAttributeUpdateResult", propOrder = {
    "customAttributeUpdateResult"
})
@XmlRootElement
public class ArrayOfCustomAttributeUpdateResult {

    @XmlElement(name = "CustomAttributeUpdateResult", nillable = true)
    protected List<CustomAttributeUpdateResult> customAttributeUpdateResult;

    /**
     * Gets the value of the customAttributeUpdateResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customAttributeUpdateResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomAttributeUpdateResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomAttributeUpdateResult }
     * 
     * 
     */
    public List<CustomAttributeUpdateResult> getCustomAttributeUpdateResult() {
        if (customAttributeUpdateResult == null) {
            customAttributeUpdateResult = new ArrayList<CustomAttributeUpdateResult>();
        }
        return this.customAttributeUpdateResult;
    }

}
