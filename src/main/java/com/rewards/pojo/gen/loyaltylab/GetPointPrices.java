
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RetailerShopperId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProductsToPrice" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfProductToPrice" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retailerShopperId",
    "productsToPrice"
})
@XmlRootElement(name = "GetPointPrices")
public class GetPointPrices {

    @XmlElement(name = "RetailerShopperId")
    protected String retailerShopperId;
    @XmlElement(name = "ProductsToPrice")
    protected ArrayOfProductToPrice productsToPrice;

    /**
     * Gets the value of the retailerShopperId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerShopperId() {
        return retailerShopperId;
    }

    /**
     * Sets the value of the retailerShopperId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerShopperId(String value) {
        this.retailerShopperId = value;
    }

    /**
     * Gets the value of the productsToPrice property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProductToPrice }
     *     
     */
    public ArrayOfProductToPrice getProductsToPrice() {
        return productsToPrice;
    }

    /**
     * Sets the value of the productsToPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProductToPrice }
     *     
     */
    public void setProductsToPrice(ArrayOfProductToPrice value) {
        this.productsToPrice = value;
    }

}
