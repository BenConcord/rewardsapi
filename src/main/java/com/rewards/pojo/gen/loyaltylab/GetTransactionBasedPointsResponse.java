
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetTransactionBasedPointsResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfKohlsTransaction" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionBasedPointsResult"
})
@XmlRootElement(name = "GetTransactionBasedPointsResponse")
public class GetTransactionBasedPointsResponse {

    @XmlElement(name = "GetTransactionBasedPointsResult")
    protected ArrayOfKohlsTransaction getTransactionBasedPointsResult;

    /**
     * Gets the value of the getTransactionBasedPointsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKohlsTransaction }
     *     
     */
    public ArrayOfKohlsTransaction getGetTransactionBasedPointsResult() {
        return getTransactionBasedPointsResult;
    }

    /**
     * Sets the value of the getTransactionBasedPointsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKohlsTransaction }
     *     
     */
    public void setGetTransactionBasedPointsResult(ArrayOfKohlsTransaction value) {
        this.getTransactionBasedPointsResult = value;
    }

}
