
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetCurrentPointPriceResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCurrentPointPriceResult"
})
@XmlRootElement(name = "GetCurrentPointPriceResponse")
public class GetCurrentPointPriceResponse {

    @XmlElement(name = "GetCurrentPointPriceResult")
    protected int getCurrentPointPriceResult;

    /**
     * Gets the value of the getCurrentPointPriceResult property.
     * 
     */
    public int getGetCurrentPointPriceResult() {
        return getCurrentPointPriceResult;
    }

    /**
     * Sets the value of the getCurrentPointPriceResult property.
     * 
     */
    public void setGetCurrentPointPriceResult(int value) {
        this.getCurrentPointPriceResult = value;
    }

}
