
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditAward complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditAward"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AwardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AwardText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AwardMonetaryValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditAward", propOrder = {
    "awardType",
    "awardText",
    "awardMonetaryValue"
})
public class CreditAward {

    @XmlElement(name = "AwardType")
    protected String awardType;
    @XmlElement(name = "AwardText")
    protected String awardText;
    @XmlElement(name = "AwardMonetaryValue")
    protected String awardMonetaryValue;

    /**
     * Gets the value of the awardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardType() {
        return awardType;
    }

    /**
     * Sets the value of the awardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardType(String value) {
        this.awardType = value;
    }

    /**
     * Gets the value of the awardText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardText() {
        return awardText;
    }

    /**
     * Sets the value of the awardText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardText(String value) {
        this.awardText = value;
    }

    /**
     * Gets the value of the awardMonetaryValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardMonetaryValue() {
        return awardMonetaryValue;
    }

    /**
     * Sets the value of the awardMonetaryValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardMonetaryValue(String value) {
        this.awardMonetaryValue = value;
    }

}
