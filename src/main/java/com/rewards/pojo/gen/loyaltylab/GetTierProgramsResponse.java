
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetTierProgramsResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfTierDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTierProgramsResult"
})
@XmlRootElement(name = "GetTierProgramsResponse")
public class GetTierProgramsResponse {

    @XmlElement(name = "GetTierProgramsResult")
    protected ArrayOfTierDetails getTierProgramsResult;

    /**
     * Gets the value of the getTierProgramsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTierDetails }
     *     
     */
    public ArrayOfTierDetails getGetTierProgramsResult() {
        return getTierProgramsResult;
    }

    /**
     * Sets the value of the getTierProgramsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTierDetails }
     *     
     */
    public void setGetTierProgramsResult(ArrayOfTierDetails value) {
        this.getTierProgramsResult = value;
    }

}
