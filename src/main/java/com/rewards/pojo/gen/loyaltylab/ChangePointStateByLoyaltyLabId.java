
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="loyaltyLabPurchaseId" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="newState" type="{http://www.loyaltylab.com/loyaltyapi/}PointStateEnum"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loyaltyLabPurchaseId",
    "newState"
})
@XmlRootElement(name = "ChangePointStateByLoyaltyLabId")
public class ChangePointStateByLoyaltyLabId {

    @XmlElement(required = true)
    protected String loyaltyLabPurchaseId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected PointStateEnum newState;

    /**
     * Gets the value of the loyaltyLabPurchaseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyLabPurchaseId() {
        return loyaltyLabPurchaseId;
    }

    /**
     * Sets the value of the loyaltyLabPurchaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyLabPurchaseId(String value) {
        this.loyaltyLabPurchaseId = value;
    }

    /**
     * Gets the value of the newState property.
     * 
     * @return
     *     possible object is
     *     {@link PointStateEnum }
     *     
     */
    public PointStateEnum getNewState() {
        return newState;
    }

    /**
     * Sets the value of the newState property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointStateEnum }
     *     
     */
    public void setNewState(PointStateEnum value) {
        this.newState = value;
    }

}
