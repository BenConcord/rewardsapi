
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for KohlsNonTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KohlsNonTransaction"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PointActivityDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="AwardHeadline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AwardName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Points" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="BarcodeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EventType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KohlsNonTransaction", propOrder = {
    "pointActivityDateTime",
    "awardHeadline",
    "awardName",
    "points",
    "barcodeNumber",
    "eventType"
})
public class KohlsNonTransaction {

    @XmlElement(name = "PointActivityDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar pointActivityDateTime;
    @XmlElement(name = "AwardHeadline")
    protected String awardHeadline;
    @XmlElement(name = "AwardName")
    protected String awardName;
    @XmlElement(name = "Points")
    protected int points;
    @XmlElement(name = "BarcodeNumber")
    protected String barcodeNumber;
    @XmlElement(name = "EventType")
    protected String eventType;

    /**
     * Gets the value of the pointActivityDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPointActivityDateTime() {
        return pointActivityDateTime;
    }

    /**
     * Sets the value of the pointActivityDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPointActivityDateTime(XMLGregorianCalendar value) {
        this.pointActivityDateTime = value;
    }

    /**
     * Gets the value of the awardHeadline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardHeadline() {
        return awardHeadline;
    }

    /**
     * Sets the value of the awardHeadline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardHeadline(String value) {
        this.awardHeadline = value;
    }

    /**
     * Gets the value of the awardName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardName() {
        return awardName;
    }

    /**
     * Sets the value of the awardName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardName(String value) {
        this.awardName = value;
    }

    /**
     * Gets the value of the points property.
     * 
     */
    public int getPoints() {
        return points;
    }

    /**
     * Sets the value of the points property.
     * 
     */
    public void setPoints(int value) {
        this.points = value;
    }

    /**
     * Gets the value of the barcodeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarcodeNumber() {
        return barcodeNumber;
    }

    /**
     * Sets the value of the barcodeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarcodeNumber(String value) {
        this.barcodeNumber = value;
    }

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventType(String value) {
        this.eventType = value;
    }

}
