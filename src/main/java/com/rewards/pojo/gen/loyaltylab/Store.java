
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Store complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Store"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StoreId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RetailerGUID" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="StoreDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailerStoreId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ManagerLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ManagerFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Address1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Address2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZipCodeMapId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="MapURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ManagerEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FileImportId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Store", propOrder = {
    "storeId",
    "retailerGUID",
    "storeDisplayName",
    "retailerStoreId",
    "managerLastName",
    "managerFirstName",
    "address1",
    "address2",
    "city",
    "state",
    "zip",
    "phone",
    "status",
    "zipCodeMapId",
    "mapURL",
    "managerEmail",
    "fileImportId"
})
public class Store {

    @XmlElement(name = "StoreId")
    protected int storeId;
    @XmlElement(name = "RetailerGUID", required = true)
    protected String retailerGUID;
    @XmlElement(name = "StoreDisplayName")
    protected String storeDisplayName;
    @XmlElement(name = "RetailerStoreId")
    protected String retailerStoreId;
    @XmlElement(name = "ManagerLastName")
    protected String managerLastName;
    @XmlElement(name = "ManagerFirstName")
    protected String managerFirstName;
    @XmlElement(name = "Address1")
    protected String address1;
    @XmlElement(name = "Address2")
    protected String address2;
    @XmlElement(name = "City")
    protected String city;
    @XmlElement(name = "State")
    protected String state;
    @XmlElement(name = "Zip")
    protected String zip;
    @XmlElement(name = "Phone")
    protected String phone;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "ZipCodeMapId")
    protected int zipCodeMapId;
    @XmlElement(name = "MapURL")
    protected String mapURL;
    @XmlElement(name = "ManagerEmail")
    protected String managerEmail;
    @XmlElement(name = "FileImportId")
    protected int fileImportId;

    /**
     * Gets the value of the storeId property.
     * 
     */
    public int getStoreId() {
        return storeId;
    }

    /**
     * Sets the value of the storeId property.
     * 
     */
    public void setStoreId(int value) {
        this.storeId = value;
    }

    /**
     * Gets the value of the retailerGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerGUID() {
        return retailerGUID;
    }

    /**
     * Sets the value of the retailerGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerGUID(String value) {
        this.retailerGUID = value;
    }

    /**
     * Gets the value of the storeDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreDisplayName() {
        return storeDisplayName;
    }

    /**
     * Sets the value of the storeDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreDisplayName(String value) {
        this.storeDisplayName = value;
    }

    /**
     * Gets the value of the retailerStoreId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerStoreId() {
        return retailerStoreId;
    }

    /**
     * Sets the value of the retailerStoreId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerStoreId(String value) {
        this.retailerStoreId = value;
    }

    /**
     * Gets the value of the managerLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManagerLastName() {
        return managerLastName;
    }

    /**
     * Sets the value of the managerLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManagerLastName(String value) {
        this.managerLastName = value;
    }

    /**
     * Gets the value of the managerFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManagerFirstName() {
        return managerFirstName;
    }

    /**
     * Sets the value of the managerFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManagerFirstName(String value) {
        this.managerFirstName = value;
    }

    /**
     * Gets the value of the address1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the value of the address1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress1(String value) {
        this.address1 = value;
    }

    /**
     * Gets the value of the address2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the value of the address2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress2(String value) {
        this.address2 = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the zip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZip() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZip(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the zipCodeMapId property.
     * 
     */
    public int getZipCodeMapId() {
        return zipCodeMapId;
    }

    /**
     * Sets the value of the zipCodeMapId property.
     * 
     */
    public void setZipCodeMapId(int value) {
        this.zipCodeMapId = value;
    }

    /**
     * Gets the value of the mapURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMapURL() {
        return mapURL;
    }

    /**
     * Sets the value of the mapURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMapURL(String value) {
        this.mapURL = value;
    }

    /**
     * Gets the value of the managerEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManagerEmail() {
        return managerEmail;
    }

    /**
     * Sets the value of the managerEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManagerEmail(String value) {
        this.managerEmail = value;
    }

    /**
     * Gets the value of the fileImportId property.
     * 
     */
    public int getFileImportId() {
        return fileImportId;
    }

    /**
     * Sets the value of the fileImportId property.
     * 
     */
    public void setFileImportId(int value) {
        this.fileImportId = value;
    }

}
