
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetRewardDetailsResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfKohlsAwardDetail" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRewardDetailsResult"
})
@XmlRootElement(name = "GetRewardDetailsResponse")
public class GetRewardDetailsResponse {

    @XmlElement(name = "GetRewardDetailsResult")
    protected ArrayOfKohlsAwardDetail getRewardDetailsResult;

    /**
     * Gets the value of the getRewardDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKohlsAwardDetail }
     *     
     */
    public ArrayOfKohlsAwardDetail getGetRewardDetailsResult() {
        return getRewardDetailsResult;
    }

    /**
     * Sets the value of the getRewardDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKohlsAwardDetail }
     *     
     */
    public void setGetRewardDetailsResult(ArrayOfKohlsAwardDetail value) {
        this.getRewardDetailsResult = value;
    }

}
