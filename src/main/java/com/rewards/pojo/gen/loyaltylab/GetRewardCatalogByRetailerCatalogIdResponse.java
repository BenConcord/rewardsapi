
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetRewardCatalogByRetailerCatalogIdResult" type="{http://www.loyaltylab.com/loyaltyapi/}RewardCatalog" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRewardCatalogByRetailerCatalogIdResult"
})
@XmlRootElement(name = "GetRewardCatalogByRetailerCatalogIdResponse")
public class GetRewardCatalogByRetailerCatalogIdResponse {

    @XmlElement(name = "GetRewardCatalogByRetailerCatalogIdResult")
    protected RewardCatalog getRewardCatalogByRetailerCatalogIdResult;

    /**
     * Gets the value of the getRewardCatalogByRetailerCatalogIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link RewardCatalog }
     *     
     */
    public RewardCatalog getGetRewardCatalogByRetailerCatalogIdResult() {
        return getRewardCatalogByRetailerCatalogIdResult;
    }

    /**
     * Sets the value of the getRewardCatalogByRetailerCatalogIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardCatalog }
     *     
     */
    public void setGetRewardCatalogByRetailerCatalogIdResult(RewardCatalog value) {
        this.getRewardCatalogByRetailerCatalogIdResult = value;
    }

}
