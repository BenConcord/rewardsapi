
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShoppersByPhoneNumberFragmentResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfShopper" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShoppersByPhoneNumberFragmentResult"
})
@XmlRootElement(name = "GetShoppersByPhoneNumberFragmentResponse")
public class GetShoppersByPhoneNumberFragmentResponse {

    @XmlElement(name = "GetShoppersByPhoneNumberFragmentResult")
    protected ArrayOfShopper getShoppersByPhoneNumberFragmentResult;

    /**
     * Gets the value of the getShoppersByPhoneNumberFragmentResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShopper }
     *     
     */
    public ArrayOfShopper getGetShoppersByPhoneNumberFragmentResult() {
        return getShoppersByPhoneNumberFragmentResult;
    }

    /**
     * Sets the value of the getShoppersByPhoneNumberFragmentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShopper }
     *     
     */
    public void setGetShoppersByPhoneNumberFragmentResult(ArrayOfShopper value) {
        this.getShoppersByPhoneNumberFragmentResult = value;
    }

}
