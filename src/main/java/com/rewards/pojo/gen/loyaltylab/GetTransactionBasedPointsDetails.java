
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LoyaltyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InternalTransactionId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loyaltyId",
    "internalTransactionId"
})
@XmlRootElement(name = "GetTransactionBasedPointsDetails")
public class GetTransactionBasedPointsDetails {

    @XmlElement(name = "LoyaltyId")
    protected String loyaltyId;
    @XmlElement(name = "InternalTransactionId")
    protected int internalTransactionId;

    /**
     * Gets the value of the loyaltyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyId() {
        return loyaltyId;
    }

    /**
     * Sets the value of the loyaltyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyId(String value) {
        this.loyaltyId = value;
    }

    /**
     * Gets the value of the internalTransactionId property.
     * 
     */
    public int getInternalTransactionId() {
        return internalTransactionId;
    }

    /**
     * Sets the value of the internalTransactionId property.
     * 
     */
    public void setInternalTransactionId(int value) {
        this.internalTransactionId = value;
    }

}
