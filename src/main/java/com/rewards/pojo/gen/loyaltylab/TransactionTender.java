
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for TransactionTender complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionTender"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CardReferenceId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CardReferenceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="IsEligibleForAward" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="QualifyingTenderAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="TenderAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="TenderTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LoyaltyLabOrderTenderId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionTender", propOrder = {
    "cardReferenceId",
    "cardReferenceType",
    "createDateTime",
    "isEligibleForAward",
    "qualifyingTenderAmount",
    "tenderAmount",
    "tenderTypeCode",
    "loyaltyLabOrderTenderId",
    "lastModifiedDateTime"
})
public class TransactionTender {

    @XmlElement(name = "CardReferenceId")
    protected int cardReferenceId;
    @XmlElement(name = "CardReferenceType")
    protected String cardReferenceType;
    @XmlElement(name = "CreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDateTime;
    @XmlElement(name = "IsEligibleForAward")
    protected boolean isEligibleForAward;
    @XmlElement(name = "QualifyingTenderAmount", required = true)
    protected BigDecimal qualifyingTenderAmount;
    @XmlElement(name = "TenderAmount", required = true)
    protected BigDecimal tenderAmount;
    @XmlElement(name = "TenderTypeCode")
    protected String tenderTypeCode;
    @XmlElement(name = "LoyaltyLabOrderTenderId")
    protected int loyaltyLabOrderTenderId;
    @XmlElement(name = "LastModifiedDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;

    /**
     * Gets the value of the cardReferenceId property.
     * 
     */
    public int getCardReferenceId() {
        return cardReferenceId;
    }

    /**
     * Sets the value of the cardReferenceId property.
     * 
     */
    public void setCardReferenceId(int value) {
        this.cardReferenceId = value;
    }

    /**
     * Gets the value of the cardReferenceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardReferenceType() {
        return cardReferenceType;
    }

    /**
     * Sets the value of the cardReferenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardReferenceType(String value) {
        this.cardReferenceType = value;
    }

    /**
     * Gets the value of the createDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the value of the createDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDateTime(XMLGregorianCalendar value) {
        this.createDateTime = value;
    }

    /**
     * Gets the value of the isEligibleForAward property.
     * 
     */
    public boolean isIsEligibleForAward() {
        return isEligibleForAward;
    }

    /**
     * Sets the value of the isEligibleForAward property.
     * 
     */
    public void setIsEligibleForAward(boolean value) {
        this.isEligibleForAward = value;
    }

    /**
     * Gets the value of the qualifyingTenderAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQualifyingTenderAmount() {
        return qualifyingTenderAmount;
    }

    /**
     * Sets the value of the qualifyingTenderAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQualifyingTenderAmount(BigDecimal value) {
        this.qualifyingTenderAmount = value;
    }

    /**
     * Gets the value of the tenderAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTenderAmount() {
        return tenderAmount;
    }

    /**
     * Sets the value of the tenderAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTenderAmount(BigDecimal value) {
        this.tenderAmount = value;
    }

    /**
     * Gets the value of the tenderTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTenderTypeCode() {
        return tenderTypeCode;
    }

    /**
     * Sets the value of the tenderTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTenderTypeCode(String value) {
        this.tenderTypeCode = value;
    }

    /**
     * Gets the value of the loyaltyLabOrderTenderId property.
     * 
     */
    public int getLoyaltyLabOrderTenderId() {
        return loyaltyLabOrderTenderId;
    }

    /**
     * Sets the value of the loyaltyLabOrderTenderId property.
     * 
     */
    public void setLoyaltyLabOrderTenderId(int value) {
        this.loyaltyLabOrderTenderId = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

}
