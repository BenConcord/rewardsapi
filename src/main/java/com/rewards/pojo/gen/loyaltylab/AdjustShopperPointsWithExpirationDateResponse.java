
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AdjustShopperPointsWithExpirationDateResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "adjustShopperPointsWithExpirationDateResult"
})
@XmlRootElement(name = "AdjustShopperPointsWithExpirationDateResponse")
public class AdjustShopperPointsWithExpirationDateResponse {

    @XmlElement(name = "AdjustShopperPointsWithExpirationDateResult")
    protected int adjustShopperPointsWithExpirationDateResult;

    /**
     * Gets the value of the adjustShopperPointsWithExpirationDateResult property.
     * 
     */
    public int getAdjustShopperPointsWithExpirationDateResult() {
        return adjustShopperPointsWithExpirationDateResult;
    }

    /**
     * Sets the value of the adjustShopperPointsWithExpirationDateResult property.
     * 
     */
    public void setAdjustShopperPointsWithExpirationDateResult(int value) {
        this.adjustShopperPointsWithExpirationDateResult = value;
    }

}
