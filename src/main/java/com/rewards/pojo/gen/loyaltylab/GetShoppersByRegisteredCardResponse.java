
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShoppersByRegisteredCardResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfShopper" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShoppersByRegisteredCardResult"
})
@XmlRootElement(name = "GetShoppersByRegisteredCardResponse")
public class GetShoppersByRegisteredCardResponse {

    @XmlElement(name = "GetShoppersByRegisteredCardResult")
    protected ArrayOfShopper getShoppersByRegisteredCardResult;

    /**
     * Gets the value of the getShoppersByRegisteredCardResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShopper }
     *     
     */
    public ArrayOfShopper getGetShoppersByRegisteredCardResult() {
        return getShoppersByRegisteredCardResult;
    }

    /**
     * Sets the value of the getShoppersByRegisteredCardResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShopper }
     *     
     */
    public void setGetShoppersByRegisteredCardResult(ArrayOfShopper value) {
        this.getShoppersByRegisteredCardResult = value;
    }

}
