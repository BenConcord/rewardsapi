
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RegisteredCard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegisteredCard"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RegisteredCardId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CommonName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AlternateCardIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExpirationMonth" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ExpirationYear" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LastFour" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SecurityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CardHolderName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="IsPreferred" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FileImportId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegisteredCard", propOrder = {
    "registeredCardId",
    "cardNumber",
    "shopperId",
    "commonName",
    "alternateCardIdentifier",
    "cardType",
    "expirationMonth",
    "expirationYear",
    "lastFour",
    "securityCode",
    "cardHolderName",
    "status",
    "createDateTime",
    "isPreferred",
    "fileImportId"
})
public class RegisteredCard {

    @XmlElement(name = "RegisteredCardId")
    protected int registeredCardId;
    @XmlElement(name = "CardNumber")
    protected String cardNumber;
    @XmlElement(name = "ShopperId")
    protected int shopperId;
    @XmlElement(name = "CommonName")
    protected String commonName;
    @XmlElement(name = "AlternateCardIdentifier")
    protected String alternateCardIdentifier;
    @XmlElement(name = "CardType")
    protected String cardType;
    @XmlElement(name = "ExpirationMonth")
    protected int expirationMonth;
    @XmlElement(name = "ExpirationYear")
    protected int expirationYear;
    @XmlElement(name = "LastFour")
    protected String lastFour;
    @XmlElement(name = "SecurityCode")
    protected String securityCode;
    @XmlElement(name = "CardHolderName")
    protected String cardHolderName;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "CreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDateTime;
    @XmlElement(name = "IsPreferred")
    protected String isPreferred;
    @XmlElement(name = "FileImportId")
    protected int fileImportId;

    /**
     * Gets the value of the registeredCardId property.
     * 
     */
    public int getRegisteredCardId() {
        return registeredCardId;
    }

    /**
     * Sets the value of the registeredCardId property.
     * 
     */
    public void setRegisteredCardId(int value) {
        this.registeredCardId = value;
    }

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the commonName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommonName() {
        return commonName;
    }

    /**
     * Sets the value of the commonName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommonName(String value) {
        this.commonName = value;
    }

    /**
     * Gets the value of the alternateCardIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateCardIdentifier() {
        return alternateCardIdentifier;
    }

    /**
     * Sets the value of the alternateCardIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateCardIdentifier(String value) {
        this.alternateCardIdentifier = value;
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardType(String value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the expirationMonth property.
     * 
     */
    public int getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * Sets the value of the expirationMonth property.
     * 
     */
    public void setExpirationMonth(int value) {
        this.expirationMonth = value;
    }

    /**
     * Gets the value of the expirationYear property.
     * 
     */
    public int getExpirationYear() {
        return expirationYear;
    }

    /**
     * Sets the value of the expirationYear property.
     * 
     */
    public void setExpirationYear(int value) {
        this.expirationYear = value;
    }

    /**
     * Gets the value of the lastFour property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastFour() {
        return lastFour;
    }

    /**
     * Sets the value of the lastFour property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastFour(String value) {
        this.lastFour = value;
    }

    /**
     * Gets the value of the securityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecurityCode() {
        return securityCode;
    }

    /**
     * Sets the value of the securityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecurityCode(String value) {
        this.securityCode = value;
    }

    /**
     * Gets the value of the cardHolderName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardHolderName() {
        return cardHolderName;
    }

    /**
     * Sets the value of the cardHolderName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardHolderName(String value) {
        this.cardHolderName = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the createDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the value of the createDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDateTime(XMLGregorianCalendar value) {
        this.createDateTime = value;
    }

    /**
     * Gets the value of the isPreferred property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPreferred() {
        return isPreferred;
    }

    /**
     * Sets the value of the isPreferred property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPreferred(String value) {
        this.isPreferred = value;
    }

    /**
     * Gets the value of the fileImportId property.
     * 
     */
    public int getFileImportId() {
        return fileImportId;
    }

    /**
     * Sets the value of the fileImportId property.
     * 
     */
    public void setFileImportId(int value) {
        this.fileImportId = value;
    }

}
