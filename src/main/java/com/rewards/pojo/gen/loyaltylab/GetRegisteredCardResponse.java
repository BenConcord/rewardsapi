
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetRegisteredCardResult" type="{http://www.loyaltylab.com/loyaltyapi/}RegisteredCard" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRegisteredCardResult"
})
@XmlRootElement(name = "GetRegisteredCardResponse")
public class GetRegisteredCardResponse {

    @XmlElement(name = "GetRegisteredCardResult")
    protected RegisteredCard getRegisteredCardResult;

    /**
     * Gets the value of the getRegisteredCardResult property.
     * 
     * @return
     *     possible object is
     *     {@link RegisteredCard }
     *     
     */
    public RegisteredCard getGetRegisteredCardResult() {
        return getRegisteredCardResult;
    }

    /**
     * Sets the value of the getRegisteredCardResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegisteredCard }
     *     
     */
    public void setGetRegisteredCardResult(RegisteredCard value) {
        this.getRegisteredCardResult = value;
    }

}
