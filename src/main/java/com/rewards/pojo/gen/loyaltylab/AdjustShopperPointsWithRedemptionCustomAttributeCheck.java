
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="pointChange" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="pointType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customAttributeHead" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfBulkCustomAttributeHead" minOccurs="0"/&gt;
 *         &lt;element name="redemptionCustomAttributeReferenceTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="redemptionCustomattributeValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="caMustExist" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "shopperId",
    "pointChange",
    "pointType",
    "description",
    "customAttributeHead",
    "redemptionCustomAttributeReferenceTag",
    "redemptionCustomattributeValue",
    "caMustExist"
})
@XmlRootElement(name = "AdjustShopperPointsWithRedemptionCustomAttributeCheck")
public class AdjustShopperPointsWithRedemptionCustomAttributeCheck {

    protected int shopperId;
    protected int pointChange;
    protected String pointType;
    protected String description;
    protected ArrayOfBulkCustomAttributeHead customAttributeHead;
    protected String redemptionCustomAttributeReferenceTag;
    protected String redemptionCustomattributeValue;
    protected boolean caMustExist;

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the pointChange property.
     * 
     */
    public int getPointChange() {
        return pointChange;
    }

    /**
     * Sets the value of the pointChange property.
     * 
     */
    public void setPointChange(int value) {
        this.pointChange = value;
    }

    /**
     * Gets the value of the pointType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointType() {
        return pointType;
    }

    /**
     * Sets the value of the pointType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointType(String value) {
        this.pointType = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the customAttributeHead property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBulkCustomAttributeHead }
     *     
     */
    public ArrayOfBulkCustomAttributeHead getCustomAttributeHead() {
        return customAttributeHead;
    }

    /**
     * Sets the value of the customAttributeHead property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBulkCustomAttributeHead }
     *     
     */
    public void setCustomAttributeHead(ArrayOfBulkCustomAttributeHead value) {
        this.customAttributeHead = value;
    }

    /**
     * Gets the value of the redemptionCustomAttributeReferenceTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedemptionCustomAttributeReferenceTag() {
        return redemptionCustomAttributeReferenceTag;
    }

    /**
     * Sets the value of the redemptionCustomAttributeReferenceTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedemptionCustomAttributeReferenceTag(String value) {
        this.redemptionCustomAttributeReferenceTag = value;
    }

    /**
     * Gets the value of the redemptionCustomattributeValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedemptionCustomattributeValue() {
        return redemptionCustomattributeValue;
    }

    /**
     * Sets the value of the redemptionCustomattributeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedemptionCustomattributeValue(String value) {
        this.redemptionCustomattributeValue = value;
    }

    /**
     * Gets the value of the caMustExist property.
     * 
     */
    public boolean isCaMustExist() {
        return caMustExist;
    }

    /**
     * Sets the value of the caMustExist property.
     * 
     */
    public void setCaMustExist(boolean value) {
        this.caMustExist = value;
    }

}
