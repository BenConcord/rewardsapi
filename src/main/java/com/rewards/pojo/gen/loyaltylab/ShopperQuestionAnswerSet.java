
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShopperQuestionAnswerSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShopperQuestionAnswerSet"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Question" type="{http://www.loyaltylab.com/loyaltyapi/}QuestionnaireQuestion" minOccurs="0"/&gt;
 *         &lt;element name="Answers" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfShopperAnswer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShopperQuestionAnswerSet", propOrder = {
    "question",
    "answers"
})
public class ShopperQuestionAnswerSet {

    @XmlElement(name = "Question")
    protected QuestionnaireQuestion question;
    @XmlElement(name = "Answers")
    protected ArrayOfShopperAnswer answers;

    /**
     * Gets the value of the question property.
     * 
     * @return
     *     possible object is
     *     {@link QuestionnaireQuestion }
     *     
     */
    public QuestionnaireQuestion getQuestion() {
        return question;
    }

    /**
     * Sets the value of the question property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuestionnaireQuestion }
     *     
     */
    public void setQuestion(QuestionnaireQuestion value) {
        this.question = value;
    }

    /**
     * Gets the value of the answers property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShopperAnswer }
     *     
     */
    public ArrayOfShopperAnswer getAnswers() {
        return answers;
    }

    /**
     * Sets the value of the answers property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShopperAnswer }
     *     
     */
    public void setAnswers(ArrayOfShopperAnswer value) {
        this.answers = value;
    }

}
