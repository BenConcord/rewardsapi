
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MergeAccountsResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mergeAccountsResult"
})
@XmlRootElement(name = "MergeAccountsResponse")
public class MergeAccountsResponse {

    @XmlElement(name = "MergeAccountsResult")
    protected String mergeAccountsResult;

    /**
     * Gets the value of the mergeAccountsResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMergeAccountsResult() {
        return mergeAccountsResult;
    }

    /**
     * Sets the value of the mergeAccountsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMergeAccountsResult(String value) {
        this.mergeAccountsResult = value;
    }

}
