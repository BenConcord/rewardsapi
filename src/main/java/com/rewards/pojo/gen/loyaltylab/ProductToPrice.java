
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ProductToPrice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductToPrice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="BasePrice" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="Multiplier" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="PointPrice" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="IsValidInput" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="ErrorReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductToPrice", propOrder = {
    "sku",
    "effectiveDate",
    "basePrice",
    "multiplier",
    "pointPrice",
    "isValidInput",
    "errorReason"
})
public class ProductToPrice {

    @XmlElement(name = "SKU")
    protected String sku;
    @XmlElement(name = "EffectiveDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "BasePrice")
    protected double basePrice;
    @XmlElement(name = "Multiplier")
    protected double multiplier;
    @XmlElement(name = "PointPrice")
    protected int pointPrice;
    @XmlElement(name = "IsValidInput")
    protected boolean isValidInput;
    @XmlElement(name = "ErrorReason")
    protected String errorReason;

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSKU() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSKU(String value) {
        this.sku = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the basePrice property.
     * 
     */
    public double getBasePrice() {
        return basePrice;
    }

    /**
     * Sets the value of the basePrice property.
     * 
     */
    public void setBasePrice(double value) {
        this.basePrice = value;
    }

    /**
     * Gets the value of the multiplier property.
     * 
     */
    public double getMultiplier() {
        return multiplier;
    }

    /**
     * Sets the value of the multiplier property.
     * 
     */
    public void setMultiplier(double value) {
        this.multiplier = value;
    }

    /**
     * Gets the value of the pointPrice property.
     * 
     */
    public int getPointPrice() {
        return pointPrice;
    }

    /**
     * Sets the value of the pointPrice property.
     * 
     */
    public void setPointPrice(int value) {
        this.pointPrice = value;
    }

    /**
     * Gets the value of the isValidInput property.
     * 
     */
    public boolean isIsValidInput() {
        return isValidInput;
    }

    /**
     * Sets the value of the isValidInput property.
     * 
     */
    public void setIsValidInput(boolean value) {
        this.isValidInput = value;
    }

    /**
     * Gets the value of the errorReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorReason() {
        return errorReason;
    }

    /**
     * Sets the value of the errorReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorReason(String value) {
        this.errorReason = value;
    }

}
