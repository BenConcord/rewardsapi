
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetProductBySKUResult" type="{http://www.loyaltylab.com/loyaltyapi/}Product" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getProductBySKUResult"
})
@XmlRootElement(name = "GetProductBySKUResponse")
public class GetProductBySKUResponse {

    @XmlElement(name = "GetProductBySKUResult")
    protected Product getProductBySKUResult;

    /**
     * Gets the value of the getProductBySKUResult property.
     * 
     * @return
     *     possible object is
     *     {@link Product }
     *     
     */
    public Product getGetProductBySKUResult() {
        return getProductBySKUResult;
    }

    /**
     * Sets the value of the getProductBySKUResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Product }
     *     
     */
    public void setGetProductBySKUResult(Product value) {
        this.getProductBySKUResult = value;
    }

}
