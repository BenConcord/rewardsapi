
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Program complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Program"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProgramId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RetailerGUID" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="ProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProgramIteration" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RecordType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsTiered" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="QualifyingStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="QualifyingEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="QualifyingResetDuration" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="QualifyingResetDurationUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AwardStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="AwardDuration" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="AwardDurationUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ParticipationLimit" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ParticipationNotificationLevel" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="MoreInfoUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SupportEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ConditionTypeId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ProgramGraphicUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InvitationGraphicUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AppearanceId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LogoPath" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Headline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreatedByUserId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="LastModifiedByUserId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="AutoEnroll" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Benefits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TermsConditions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomerServiceInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InternalNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActiveMembersCount" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Program", propOrder = {
    "programId",
    "retailerGUID",
    "programName",
    "programIteration",
    "recordType",
    "isTiered",
    "status",
    "startDate",
    "endDate",
    "qualifyingStartDate",
    "qualifyingEndDate",
    "qualifyingResetDuration",
    "qualifyingResetDurationUnit",
    "awardStartDate",
    "awardDuration",
    "awardDurationUnit",
    "participationLimit",
    "participationNotificationLevel",
    "moreInfoUrl",
    "supportEmail",
    "conditionTypeId",
    "programGraphicUrl",
    "invitationGraphicUrl",
    "appearanceId",
    "logoPath",
    "description",
    "headline",
    "createdByUserId",
    "createDateTime",
    "lastModifiedByUserId",
    "lastModifiedDateTime",
    "autoEnroll",
    "benefits",
    "termsConditions",
    "customerServiceInfo",
    "internalNote",
    "activeMembersCount"
})
public class Program {

    @XmlElement(name = "ProgramId")
    protected int programId;
    @XmlElement(name = "RetailerGUID", required = true)
    protected String retailerGUID;
    @XmlElement(name = "ProgramName")
    protected String programName;
    @XmlElement(name = "ProgramIteration")
    protected int programIteration;
    @XmlElement(name = "RecordType")
    protected String recordType;
    @XmlElement(name = "IsTiered")
    protected boolean isTiered;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "StartDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlElement(name = "EndDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    @XmlElement(name = "QualifyingStartDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar qualifyingStartDate;
    @XmlElement(name = "QualifyingEndDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar qualifyingEndDate;
    @XmlElement(name = "QualifyingResetDuration")
    protected int qualifyingResetDuration;
    @XmlElement(name = "QualifyingResetDurationUnit")
    protected String qualifyingResetDurationUnit;
    @XmlElement(name = "AwardStartDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar awardStartDate;
    @XmlElement(name = "AwardDuration")
    protected int awardDuration;
    @XmlElement(name = "AwardDurationUnit")
    protected String awardDurationUnit;
    @XmlElement(name = "ParticipationLimit")
    protected int participationLimit;
    @XmlElement(name = "ParticipationNotificationLevel")
    protected int participationNotificationLevel;
    @XmlElement(name = "MoreInfoUrl")
    protected String moreInfoUrl;
    @XmlElement(name = "SupportEmail")
    protected String supportEmail;
    @XmlElement(name = "ConditionTypeId")
    protected int conditionTypeId;
    @XmlElement(name = "ProgramGraphicUrl")
    protected String programGraphicUrl;
    @XmlElement(name = "InvitationGraphicUrl")
    protected String invitationGraphicUrl;
    @XmlElement(name = "AppearanceId")
    protected int appearanceId;
    @XmlElement(name = "LogoPath")
    protected String logoPath;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Headline")
    protected String headline;
    @XmlElement(name = "CreatedByUserId")
    protected int createdByUserId;
    @XmlElement(name = "CreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDateTime;
    @XmlElement(name = "LastModifiedByUserId")
    protected int lastModifiedByUserId;
    @XmlElement(name = "LastModifiedDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;
    @XmlElement(name = "AutoEnroll")
    protected boolean autoEnroll;
    @XmlElement(name = "Benefits")
    protected String benefits;
    @XmlElement(name = "TermsConditions")
    protected String termsConditions;
    @XmlElement(name = "CustomerServiceInfo")
    protected String customerServiceInfo;
    @XmlElement(name = "InternalNote")
    protected String internalNote;
    @XmlElement(name = "ActiveMembersCount")
    protected int activeMembersCount;

    /**
     * Gets the value of the programId property.
     * 
     */
    public int getProgramId() {
        return programId;
    }

    /**
     * Sets the value of the programId property.
     * 
     */
    public void setProgramId(int value) {
        this.programId = value;
    }

    /**
     * Gets the value of the retailerGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerGUID() {
        return retailerGUID;
    }

    /**
     * Sets the value of the retailerGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerGUID(String value) {
        this.retailerGUID = value;
    }

    /**
     * Gets the value of the programName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramName() {
        return programName;
    }

    /**
     * Sets the value of the programName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramName(String value) {
        this.programName = value;
    }

    /**
     * Gets the value of the programIteration property.
     * 
     */
    public int getProgramIteration() {
        return programIteration;
    }

    /**
     * Sets the value of the programIteration property.
     * 
     */
    public void setProgramIteration(int value) {
        this.programIteration = value;
    }

    /**
     * Gets the value of the recordType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * Sets the value of the recordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordType(String value) {
        this.recordType = value;
    }

    /**
     * Gets the value of the isTiered property.
     * 
     */
    public boolean isIsTiered() {
        return isTiered;
    }

    /**
     * Sets the value of the isTiered property.
     * 
     */
    public void setIsTiered(boolean value) {
        this.isTiered = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the qualifyingStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQualifyingStartDate() {
        return qualifyingStartDate;
    }

    /**
     * Sets the value of the qualifyingStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQualifyingStartDate(XMLGregorianCalendar value) {
        this.qualifyingStartDate = value;
    }

    /**
     * Gets the value of the qualifyingEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQualifyingEndDate() {
        return qualifyingEndDate;
    }

    /**
     * Sets the value of the qualifyingEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQualifyingEndDate(XMLGregorianCalendar value) {
        this.qualifyingEndDate = value;
    }

    /**
     * Gets the value of the qualifyingResetDuration property.
     * 
     */
    public int getQualifyingResetDuration() {
        return qualifyingResetDuration;
    }

    /**
     * Sets the value of the qualifyingResetDuration property.
     * 
     */
    public void setQualifyingResetDuration(int value) {
        this.qualifyingResetDuration = value;
    }

    /**
     * Gets the value of the qualifyingResetDurationUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualifyingResetDurationUnit() {
        return qualifyingResetDurationUnit;
    }

    /**
     * Sets the value of the qualifyingResetDurationUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualifyingResetDurationUnit(String value) {
        this.qualifyingResetDurationUnit = value;
    }

    /**
     * Gets the value of the awardStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAwardStartDate() {
        return awardStartDate;
    }

    /**
     * Sets the value of the awardStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAwardStartDate(XMLGregorianCalendar value) {
        this.awardStartDate = value;
    }

    /**
     * Gets the value of the awardDuration property.
     * 
     */
    public int getAwardDuration() {
        return awardDuration;
    }

    /**
     * Sets the value of the awardDuration property.
     * 
     */
    public void setAwardDuration(int value) {
        this.awardDuration = value;
    }

    /**
     * Gets the value of the awardDurationUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardDurationUnit() {
        return awardDurationUnit;
    }

    /**
     * Sets the value of the awardDurationUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardDurationUnit(String value) {
        this.awardDurationUnit = value;
    }

    /**
     * Gets the value of the participationLimit property.
     * 
     */
    public int getParticipationLimit() {
        return participationLimit;
    }

    /**
     * Sets the value of the participationLimit property.
     * 
     */
    public void setParticipationLimit(int value) {
        this.participationLimit = value;
    }

    /**
     * Gets the value of the participationNotificationLevel property.
     * 
     */
    public int getParticipationNotificationLevel() {
        return participationNotificationLevel;
    }

    /**
     * Sets the value of the participationNotificationLevel property.
     * 
     */
    public void setParticipationNotificationLevel(int value) {
        this.participationNotificationLevel = value;
    }

    /**
     * Gets the value of the moreInfoUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoreInfoUrl() {
        return moreInfoUrl;
    }

    /**
     * Sets the value of the moreInfoUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoreInfoUrl(String value) {
        this.moreInfoUrl = value;
    }

    /**
     * Gets the value of the supportEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportEmail() {
        return supportEmail;
    }

    /**
     * Sets the value of the supportEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportEmail(String value) {
        this.supportEmail = value;
    }

    /**
     * Gets the value of the conditionTypeId property.
     * 
     */
    public int getConditionTypeId() {
        return conditionTypeId;
    }

    /**
     * Sets the value of the conditionTypeId property.
     * 
     */
    public void setConditionTypeId(int value) {
        this.conditionTypeId = value;
    }

    /**
     * Gets the value of the programGraphicUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramGraphicUrl() {
        return programGraphicUrl;
    }

    /**
     * Sets the value of the programGraphicUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramGraphicUrl(String value) {
        this.programGraphicUrl = value;
    }

    /**
     * Gets the value of the invitationGraphicUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvitationGraphicUrl() {
        return invitationGraphicUrl;
    }

    /**
     * Sets the value of the invitationGraphicUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvitationGraphicUrl(String value) {
        this.invitationGraphicUrl = value;
    }

    /**
     * Gets the value of the appearanceId property.
     * 
     */
    public int getAppearanceId() {
        return appearanceId;
    }

    /**
     * Sets the value of the appearanceId property.
     * 
     */
    public void setAppearanceId(int value) {
        this.appearanceId = value;
    }

    /**
     * Gets the value of the logoPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogoPath() {
        return logoPath;
    }

    /**
     * Sets the value of the logoPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogoPath(String value) {
        this.logoPath = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the headline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeadline() {
        return headline;
    }

    /**
     * Sets the value of the headline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeadline(String value) {
        this.headline = value;
    }

    /**
     * Gets the value of the createdByUserId property.
     * 
     */
    public int getCreatedByUserId() {
        return createdByUserId;
    }

    /**
     * Sets the value of the createdByUserId property.
     * 
     */
    public void setCreatedByUserId(int value) {
        this.createdByUserId = value;
    }

    /**
     * Gets the value of the createDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the value of the createDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDateTime(XMLGregorianCalendar value) {
        this.createDateTime = value;
    }

    /**
     * Gets the value of the lastModifiedByUserId property.
     * 
     */
    public int getLastModifiedByUserId() {
        return lastModifiedByUserId;
    }

    /**
     * Sets the value of the lastModifiedByUserId property.
     * 
     */
    public void setLastModifiedByUserId(int value) {
        this.lastModifiedByUserId = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the autoEnroll property.
     * 
     */
    public boolean isAutoEnroll() {
        return autoEnroll;
    }

    /**
     * Sets the value of the autoEnroll property.
     * 
     */
    public void setAutoEnroll(boolean value) {
        this.autoEnroll = value;
    }

    /**
     * Gets the value of the benefits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBenefits() {
        return benefits;
    }

    /**
     * Sets the value of the benefits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBenefits(String value) {
        this.benefits = value;
    }

    /**
     * Gets the value of the termsConditions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermsConditions() {
        return termsConditions;
    }

    /**
     * Sets the value of the termsConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermsConditions(String value) {
        this.termsConditions = value;
    }

    /**
     * Gets the value of the customerServiceInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerServiceInfo() {
        return customerServiceInfo;
    }

    /**
     * Sets the value of the customerServiceInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerServiceInfo(String value) {
        this.customerServiceInfo = value;
    }

    /**
     * Gets the value of the internalNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalNote() {
        return internalNote;
    }

    /**
     * Sets the value of the internalNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalNote(String value) {
        this.internalNote = value;
    }

    /**
     * Gets the value of the activeMembersCount property.
     * 
     */
    public int getActiveMembersCount() {
        return activeMembersCount;
    }

    /**
     * Sets the value of the activeMembersCount property.
     * 
     */
    public void setActiveMembersCount(int value) {
        this.activeMembersCount = value;
    }

}
