
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfAwardDetail2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAwardDetail2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TenderAward" type="{http://www.loyaltylab.com/loyaltyapi/}AwardDetail" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAwardDetail2", propOrder = {
    "tenderAward"
})
public class ArrayOfAwardDetail2 {

    @XmlElement(name = "TenderAward", nillable = true)
    protected List<AwardDetail> tenderAward;

    /**
     * Gets the value of the tenderAward property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tenderAward property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTenderAward().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AwardDetail }
     * 
     * 
     */
    public List<AwardDetail> getTenderAward() {
        if (tenderAward == null) {
            tenderAward = new ArrayList<AwardDetail>();
        }
        return this.tenderAward;
    }

}
