
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.loyalty.pojo.gen.loyaltylab package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AuthenticationResult_QNAME = new QName("http://www.loyaltylab.com/loyaltyapi/", "AuthenticationResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.loyalty.pojo.gen.loyaltylab
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetEventDefinitionsResponse }
     * 
     */
    public GetEventDefinitionsResponse createGetEventDefinitionsResponse() {
        return new GetEventDefinitionsResponse();
    }

    /**
     * Create an instance of {@link ChangePointStateByLoyaltyLabId }
     * 
     */
    public ChangePointStateByLoyaltyLabId createChangePointStateByLoyaltyLabId() {
        return new ChangePointStateByLoyaltyLabId();
    }

    /**
     * Create an instance of {@link ChangePointStateByLoyaltyLabIdResponse }
     * 
     */
    public ChangePointStateByLoyaltyLabIdResponse createChangePointStateByLoyaltyLabIdResponse() {
        return new ChangePointStateByLoyaltyLabIdResponse();
    }

    /**
     * Create an instance of {@link AuthenticationResult }
     * 
     */
    public AuthenticationResult createAuthenticationResult() {
        return new AuthenticationResult();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByPointState }
     * 
     */
    public GetShopperPointBalanceByPointState createGetShopperPointBalanceByPointState() {
        return new GetShopperPointBalanceByPointState();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByPointStateResponse }
     * 
     */
    public GetShopperPointBalanceByPointStateResponse createGetShopperPointBalanceByPointStateResponse() {
        return new GetShopperPointBalanceByPointStateResponse();
    }

    /**
     * Create an instance of {@link GetCurrentPointPrice }
     * 
     */
    public GetCurrentPointPrice createGetCurrentPointPrice() {
        return new GetCurrentPointPrice();
    }

    /**
     * Create an instance of {@link GetCurrentPointPriceResponse }
     * 
     */
    public GetCurrentPointPriceResponse createGetCurrentPointPriceResponse() {
        return new GetCurrentPointPriceResponse();
    }

    /**
     * Create an instance of {@link GetCurrentPointPrices }
     * 
     */
    public GetCurrentPointPrices createGetCurrentPointPrices() {
        return new GetCurrentPointPrices();
    }

    /**
     * Create an instance of {@link ArrayOfPointPricingData }
     * 
     */
    public ArrayOfPointPricingData createArrayOfPointPricingData() {
        return new ArrayOfPointPricingData();
    }

    /**
     * Create an instance of {@link GetCurrentPointPricesResponse }
     * 
     */
    public GetCurrentPointPricesResponse createGetCurrentPointPricesResponse() {
        return new GetCurrentPointPricesResponse();
    }

    /**
     * Create an instance of {@link GetPointPrices }
     * 
     */
    public GetPointPrices createGetPointPrices() {
        return new GetPointPrices();
    }

    /**
     * Create an instance of {@link ArrayOfProductToPrice }
     * 
     */
    public ArrayOfProductToPrice createArrayOfProductToPrice() {
        return new ArrayOfProductToPrice();
    }

    /**
     * Create an instance of {@link GetPointPricesResponse }
     * 
     */
    public GetPointPricesResponse createGetPointPricesResponse() {
        return new GetPointPricesResponse();
    }

    /**
     * Create an instance of {@link PointsPurchase }
     * 
     */
    public PointsPurchase createPointsPurchase() {
        return new PointsPurchase();
    }

    /**
     * Create an instance of {@link PointsPurchaseResponse }
     * 
     */
    public PointsPurchaseResponse createPointsPurchaseResponse() {
        return new PointsPurchaseResponse();
    }

    /**
     * Create an instance of {@link PointPurchaseResult }
     * 
     */
    public PointPurchaseResult createPointPurchaseResult() {
        return new PointPurchaseResult();
    }

    /**
     * Create an instance of {@link PointsPurchaseWithPointGroup }
     * 
     */
    public PointsPurchaseWithPointGroup createPointsPurchaseWithPointGroup() {
        return new PointsPurchaseWithPointGroup();
    }

    /**
     * Create an instance of {@link PointsPurchaseWithPointGroupResponse }
     * 
     */
    public PointsPurchaseWithPointGroupResponse createPointsPurchaseWithPointGroupResponse() {
        return new PointsPurchaseWithPointGroupResponse();
    }

    /**
     * Create an instance of {@link AssociatePurchase }
     * 
     */
    public AssociatePurchase createAssociatePurchase() {
        return new AssociatePurchase();
    }

    /**
     * Create an instance of {@link AssociatePurchaseResponse }
     * 
     */
    public AssociatePurchaseResponse createAssociatePurchaseResponse() {
        return new AssociatePurchaseResponse();
    }

    /**
     * Create an instance of {@link GetShopperRedemptionsByLoyaltyProgramId }
     * 
     */
    public GetShopperRedemptionsByLoyaltyProgramId createGetShopperRedemptionsByLoyaltyProgramId() {
        return new GetShopperRedemptionsByLoyaltyProgramId();
    }

    /**
     * Create an instance of {@link GetShopperRedemptionsByLoyaltyProgramIdResponse }
     * 
     */
    public GetShopperRedemptionsByLoyaltyProgramIdResponse createGetShopperRedemptionsByLoyaltyProgramIdResponse() {
        return new GetShopperRedemptionsByLoyaltyProgramIdResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRedemptionPointLifeCycleOrderItem }
     * 
     */
    public ArrayOfRedemptionPointLifeCycleOrderItem createArrayOfRedemptionPointLifeCycleOrderItem() {
        return new ArrayOfRedemptionPointLifeCycleOrderItem();
    }

    /**
     * Create an instance of {@link GetShopperRedemptionsByShopperId }
     * 
     */
    public GetShopperRedemptionsByShopperId createGetShopperRedemptionsByShopperId() {
        return new GetShopperRedemptionsByShopperId();
    }

    /**
     * Create an instance of {@link GetShopperRedemptionsByShopperIdResponse }
     * 
     */
    public GetShopperRedemptionsByShopperIdResponse createGetShopperRedemptionsByShopperIdResponse() {
        return new GetShopperRedemptionsByShopperIdResponse();
    }

    /**
     * Create an instance of {@link GetRewards }
     * 
     */
    public GetRewards createGetRewards() {
        return new GetRewards();
    }

    /**
     * Create an instance of {@link GetRewardsResponse }
     * 
     */
    public GetRewardsResponse createGetRewardsResponse() {
        return new GetRewardsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRewardItem }
     * 
     */
    public ArrayOfRewardItem createArrayOfRewardItem() {
        return new ArrayOfRewardItem();
    }

    /**
     * Create an instance of {@link GetEventDefinitions }
     * 
     */
    public GetEventDefinitions createGetEventDefinitions() {
        return new GetEventDefinitions();
    }

    /**
     * Create an instance of {@link GetEventDefinitionsResponse.GetEventDefinitionsResult }
     * 
     */
    public GetEventDefinitionsResponse.GetEventDefinitionsResult createGetEventDefinitionsResponseGetEventDefinitionsResult() {
        return new GetEventDefinitionsResponse.GetEventDefinitionsResult();
    }

    /**
     * Create an instance of {@link CreateEventInstance }
     * 
     */
    public CreateEventInstance createCreateEventInstance() {
        return new CreateEventInstance();
    }

    /**
     * Create an instance of {@link EventInstance }
     * 
     */
    public EventInstance createEventInstance() {
        return new EventInstance();
    }

    /**
     * Create an instance of {@link CreateEventInstanceResponse }
     * 
     */
    public CreateEventInstanceResponse createCreateEventInstanceResponse() {
        return new CreateEventInstanceResponse();
    }

    /**
     * Create an instance of {@link CreateAndAwardEventInstance }
     * 
     */
    public CreateAndAwardEventInstance createCreateAndAwardEventInstance() {
        return new CreateAndAwardEventInstance();
    }

    /**
     * Create an instance of {@link CreateAndAwardEventInstanceResponse }
     * 
     */
    public CreateAndAwardEventInstanceResponse createCreateAndAwardEventInstanceResponse() {
        return new CreateAndAwardEventInstanceResponse();
    }

    /**
     * Create an instance of {@link EventAwards }
     * 
     */
    public EventAwards createEventAwards() {
        return new EventAwards();
    }

    /**
     * Create an instance of {@link MergeAccounts }
     * 
     */
    public MergeAccounts createMergeAccounts() {
        return new MergeAccounts();
    }

    /**
     * Create an instance of {@link MergeAccountsResponse }
     * 
     */
    public MergeAccountsResponse createMergeAccountsResponse() {
        return new MergeAccountsResponse();
    }

    /**
     * Create an instance of {@link MergeShoppers }
     * 
     */
    public MergeShoppers createMergeShoppers() {
        return new MergeShoppers();
    }

    /**
     * Create an instance of {@link MergeShoppersResponse }
     * 
     */
    public MergeShoppersResponse createMergeShoppersResponse() {
        return new MergeShoppersResponse();
    }

    /**
     * Create an instance of {@link SendCommunicationToEmailAddress }
     * 
     */
    public SendCommunicationToEmailAddress createSendCommunicationToEmailAddress() {
        return new SendCommunicationToEmailAddress();
    }

    /**
     * Create an instance of {@link SendCommunicationToEmailAddressResponse }
     * 
     */
    public SendCommunicationToEmailAddressResponse createSendCommunicationToEmailAddressResponse() {
        return new SendCommunicationToEmailAddressResponse();
    }

    /**
     * Create an instance of {@link SendCommunicationToEmailAddresses }
     * 
     */
    public SendCommunicationToEmailAddresses createSendCommunicationToEmailAddresses() {
        return new SendCommunicationToEmailAddresses();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link SendCommunicationToEmailAddressesResponse }
     * 
     */
    public SendCommunicationToEmailAddressesResponse createSendCommunicationToEmailAddressesResponse() {
        return new SendCommunicationToEmailAddressesResponse();
    }

    /**
     * Create an instance of {@link SendCommunicationToShopper }
     * 
     */
    public SendCommunicationToShopper createSendCommunicationToShopper() {
        return new SendCommunicationToShopper();
    }

    /**
     * Create an instance of {@link SendCommunicationToShopperResponse }
     * 
     */
    public SendCommunicationToShopperResponse createSendCommunicationToShopperResponse() {
        return new SendCommunicationToShopperResponse();
    }

    /**
     * Create an instance of {@link SendCommunicationToShoppers }
     * 
     */
    public SendCommunicationToShoppers createSendCommunicationToShoppers() {
        return new SendCommunicationToShoppers();
    }

    /**
     * Create an instance of {@link SendCommunicationToShoppersResponse }
     * 
     */
    public SendCommunicationToShoppersResponse createSendCommunicationToShoppersResponse() {
        return new SendCommunicationToShoppersResponse();
    }

    /**
     * Create an instance of {@link GetCodesForShopper }
     * 
     */
    public GetCodesForShopper createGetCodesForShopper() {
        return new GetCodesForShopper();
    }

    /**
     * Create an instance of {@link GetCodesForShopperResponse }
     * 
     */
    public GetCodesForShopperResponse createGetCodesForShopperResponse() {
        return new GetCodesForShopperResponse();
    }

    /**
     * Create an instance of {@link ArrayOfShopperRewardItemRedemption }
     * 
     */
    public ArrayOfShopperRewardItemRedemption createArrayOfShopperRewardItemRedemption() {
        return new ArrayOfShopperRewardItemRedemption();
    }

    /**
     * Create an instance of {@link GetCodesWithSkuForShopper }
     * 
     */
    public GetCodesWithSkuForShopper createGetCodesWithSkuForShopper() {
        return new GetCodesWithSkuForShopper();
    }

    /**
     * Create an instance of {@link GetCodesWithSkuForShopperResponse }
     * 
     */
    public GetCodesWithSkuForShopperResponse createGetCodesWithSkuForShopperResponse() {
        return new GetCodesWithSkuForShopperResponse();
    }

    /**
     * Create an instance of {@link ArrayOfShopperRewardItemRedemptionExtended }
     * 
     */
    public ArrayOfShopperRewardItemRedemptionExtended createArrayOfShopperRewardItemRedemptionExtended() {
        return new ArrayOfShopperRewardItemRedemptionExtended();
    }

    /**
     * Create an instance of {@link GetTotalPointsEarnedByShopperId }
     * 
     */
    public GetTotalPointsEarnedByShopperId createGetTotalPointsEarnedByShopperId() {
        return new GetTotalPointsEarnedByShopperId();
    }

    /**
     * Create an instance of {@link GetTotalPointsEarnedByShopperIdResponse }
     * 
     */
    public GetTotalPointsEarnedByShopperIdResponse createGetTotalPointsEarnedByShopperIdResponse() {
        return new GetTotalPointsEarnedByShopperIdResponse();
    }

    /**
     * Create an instance of {@link GetTotalPointsEarnedByRetailerShopperId }
     * 
     */
    public GetTotalPointsEarnedByRetailerShopperId createGetTotalPointsEarnedByRetailerShopperId() {
        return new GetTotalPointsEarnedByRetailerShopperId();
    }

    /**
     * Create an instance of {@link GetTotalPointsEarnedByRetailerShopperIdResponse }
     * 
     */
    public GetTotalPointsEarnedByRetailerShopperIdResponse createGetTotalPointsEarnedByRetailerShopperIdResponse() {
        return new GetTotalPointsEarnedByRetailerShopperIdResponse();
    }

    /**
     * Create an instance of {@link GetTotalPointsEarnedByEmailAddress }
     * 
     */
    public GetTotalPointsEarnedByEmailAddress createGetTotalPointsEarnedByEmailAddress() {
        return new GetTotalPointsEarnedByEmailAddress();
    }

    /**
     * Create an instance of {@link GetTotalPointsEarnedByEmailAddressResponse }
     * 
     */
    public GetTotalPointsEarnedByEmailAddressResponse createGetTotalPointsEarnedByEmailAddressResponse() {
        return new GetTotalPointsEarnedByEmailAddressResponse();
    }

    /**
     * Create an instance of {@link GetTotalPointsEarnedByLoyaltyCardNumber }
     * 
     */
    public GetTotalPointsEarnedByLoyaltyCardNumber createGetTotalPointsEarnedByLoyaltyCardNumber() {
        return new GetTotalPointsEarnedByLoyaltyCardNumber();
    }

    /**
     * Create an instance of {@link GetTotalPointsEarnedByLoyaltyCardNumberResponse }
     * 
     */
    public GetTotalPointsEarnedByLoyaltyCardNumberResponse createGetTotalPointsEarnedByLoyaltyCardNumberResponse() {
        return new GetTotalPointsEarnedByLoyaltyCardNumberResponse();
    }

    /**
     * Create an instance of {@link GetShoppersByPhoneNumber }
     * 
     */
    public GetShoppersByPhoneNumber createGetShoppersByPhoneNumber() {
        return new GetShoppersByPhoneNumber();
    }

    /**
     * Create an instance of {@link GetShoppersByPhoneNumberResponse }
     * 
     */
    public GetShoppersByPhoneNumberResponse createGetShoppersByPhoneNumberResponse() {
        return new GetShoppersByPhoneNumberResponse();
    }

    /**
     * Create an instance of {@link ArrayOfShopper }
     * 
     */
    public ArrayOfShopper createArrayOfShopper() {
        return new ArrayOfShopper();
    }

    /**
     * Create an instance of {@link GetShoppersByPhoneNumberFragment }
     * 
     */
    public GetShoppersByPhoneNumberFragment createGetShoppersByPhoneNumberFragment() {
        return new GetShoppersByPhoneNumberFragment();
    }

    /**
     * Create an instance of {@link GetShoppersByPhoneNumberFragmentResponse }
     * 
     */
    public GetShoppersByPhoneNumberFragmentResponse createGetShoppersByPhoneNumberFragmentResponse() {
        return new GetShoppersByPhoneNumberFragmentResponse();
    }

    /**
     * Create an instance of {@link PreviewTransaction }
     * 
     */
    public PreviewTransaction createPreviewTransaction() {
        return new PreviewTransaction();
    }

    /**
     * Create an instance of {@link PurchaseTransaction }
     * 
     */
    public PurchaseTransaction createPurchaseTransaction() {
        return new PurchaseTransaction();
    }

    /**
     * Create an instance of {@link PreviewTransactionResponse }
     * 
     */
    public PreviewTransactionResponse createPreviewTransactionResponse() {
        return new PreviewTransactionResponse();
    }

    /**
     * Create an instance of {@link PurchaseAwards }
     * 
     */
    public PurchaseAwards createPurchaseAwards() {
        return new PurchaseAwards();
    }

    /**
     * Create an instance of {@link ImportTransaction }
     * 
     */
    public ImportTransaction createImportTransaction() {
        return new ImportTransaction();
    }

    /**
     * Create an instance of {@link ImportTransactionResponse }
     * 
     */
    public ImportTransactionResponse createImportTransactionResponse() {
        return new ImportTransactionResponse();
    }

    /**
     * Create an instance of {@link AddShopperToProgram }
     * 
     */
    public AddShopperToProgram createAddShopperToProgram() {
        return new AddShopperToProgram();
    }

    /**
     * Create an instance of {@link AddShopperToProgramResponse }
     * 
     */
    public AddShopperToProgramResponse createAddShopperToProgramResponse() {
        return new AddShopperToProgramResponse();
    }

    /**
     * Create an instance of {@link GetTierPrograms }
     * 
     */
    public GetTierPrograms createGetTierPrograms() {
        return new GetTierPrograms();
    }

    /**
     * Create an instance of {@link GetTierProgramsResponse }
     * 
     */
    public GetTierProgramsResponse createGetTierProgramsResponse() {
        return new GetTierProgramsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTierDetails }
     * 
     */
    public ArrayOfTierDetails createArrayOfTierDetails() {
        return new ArrayOfTierDetails();
    }

    /**
     * Create an instance of {@link AddShopperToTier }
     * 
     */
    public AddShopperToTier createAddShopperToTier() {
        return new AddShopperToTier();
    }

    /**
     * Create an instance of {@link AddShopperToTierResponse }
     * 
     */
    public AddShopperToTierResponse createAddShopperToTierResponse() {
        return new AddShopperToTierResponse();
    }

    /**
     * Create an instance of {@link ScoreShopper }
     * 
     */
    public ScoreShopper createScoreShopper() {
        return new ScoreShopper();
    }

    /**
     * Create an instance of {@link ScoreShopperResponse }
     * 
     */
    public ScoreShopperResponse createScoreShopperResponse() {
        return new ScoreShopperResponse();
    }

    /**
     * Create an instance of {@link AdjustShopperPoints }
     * 
     */
    public AdjustShopperPoints createAdjustShopperPoints() {
        return new AdjustShopperPoints();
    }

    /**
     * Create an instance of {@link AdjustShopperPointsResponse }
     * 
     */
    public AdjustShopperPointsResponse createAdjustShopperPointsResponse() {
        return new AdjustShopperPointsResponse();
    }

    /**
     * Create an instance of {@link AdjustShopperPointsWithRedemptionCustomAttributeCheck }
     * 
     */
    public AdjustShopperPointsWithRedemptionCustomAttributeCheck createAdjustShopperPointsWithRedemptionCustomAttributeCheck() {
        return new AdjustShopperPointsWithRedemptionCustomAttributeCheck();
    }

    /**
     * Create an instance of {@link ArrayOfBulkCustomAttributeHead }
     * 
     */
    public ArrayOfBulkCustomAttributeHead createArrayOfBulkCustomAttributeHead() {
        return new ArrayOfBulkCustomAttributeHead();
    }

    /**
     * Create an instance of {@link AdjustShopperPointsWithRedemptionCustomAttributeCheckResponse }
     * 
     */
    public AdjustShopperPointsWithRedemptionCustomAttributeCheckResponse createAdjustShopperPointsWithRedemptionCustomAttributeCheckResponse() {
        return new AdjustShopperPointsWithRedemptionCustomAttributeCheckResponse();
    }

    /**
     * Create an instance of {@link AdjustShopperPointsCustomAttributes }
     * 
     */
    public AdjustShopperPointsCustomAttributes createAdjustShopperPointsCustomAttributes() {
        return new AdjustShopperPointsCustomAttributes();
    }

    /**
     * Create an instance of {@link AdjustShopperPointsCustomAttributesResponse }
     * 
     */
    public AdjustShopperPointsCustomAttributesResponse createAdjustShopperPointsCustomAttributesResponse() {
        return new AdjustShopperPointsCustomAttributesResponse();
    }

    /**
     * Create an instance of {@link AdjustShopperPointsWithExpirationDate }
     * 
     */
    public AdjustShopperPointsWithExpirationDate createAdjustShopperPointsWithExpirationDate() {
        return new AdjustShopperPointsWithExpirationDate();
    }

    /**
     * Create an instance of {@link AdjustShopperPointsWithExpirationDateResponse }
     * 
     */
    public AdjustShopperPointsWithExpirationDateResponse createAdjustShopperPointsWithExpirationDateResponse() {
        return new AdjustShopperPointsWithExpirationDateResponse();
    }

    /**
     * Create an instance of {@link AdjustShopperPointsWithExpirationDateCustomAttributes }
     * 
     */
    public AdjustShopperPointsWithExpirationDateCustomAttributes createAdjustShopperPointsWithExpirationDateCustomAttributes() {
        return new AdjustShopperPointsWithExpirationDateCustomAttributes();
    }

    /**
     * Create an instance of {@link AdjustShopperPointsWithExpirationDateCustomAttributesResponse }
     * 
     */
    public AdjustShopperPointsWithExpirationDateCustomAttributesResponse createAdjustShopperPointsWithExpirationDateCustomAttributesResponse() {
        return new AdjustShopperPointsWithExpirationDateCustomAttributesResponse();
    }

    /**
     * Create an instance of {@link GetShopperPointBalance }
     * 
     */
    public GetShopperPointBalance createGetShopperPointBalance() {
        return new GetShopperPointBalance();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceResponse }
     * 
     */
    public GetShopperPointBalanceResponse createGetShopperPointBalanceResponse() {
        return new GetShopperPointBalanceResponse();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByBalanceType }
     * 
     */
    public GetShopperPointBalanceByBalanceType createGetShopperPointBalanceByBalanceType() {
        return new GetShopperPointBalanceByBalanceType();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByBalanceTypeResponse }
     * 
     */
    public GetShopperPointBalanceByBalanceTypeResponse createGetShopperPointBalanceByBalanceTypeResponse() {
        return new GetShopperPointBalanceByBalanceTypeResponse();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByPointGroup }
     * 
     */
    public GetShopperPointBalanceByPointGroup createGetShopperPointBalanceByPointGroup() {
        return new GetShopperPointBalanceByPointGroup();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByPointGroupResponse }
     * 
     */
    public GetShopperPointBalanceByPointGroupResponse createGetShopperPointBalanceByPointGroupResponse() {
        return new GetShopperPointBalanceByPointGroupResponse();
    }

    /**
     * Create an instance of {@link GetShopperPointsExpiring }
     * 
     */
    public GetShopperPointsExpiring createGetShopperPointsExpiring() {
        return new GetShopperPointsExpiring();
    }

    /**
     * Create an instance of {@link GetShopperPointsExpiringResponse }
     * 
     */
    public GetShopperPointsExpiringResponse createGetShopperPointsExpiringResponse() {
        return new GetShopperPointsExpiringResponse();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByRetailerID }
     * 
     */
    public GetShopperPointBalanceByRetailerID createGetShopperPointBalanceByRetailerID() {
        return new GetShopperPointBalanceByRetailerID();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByRetailerIDResponse }
     * 
     */
    public GetShopperPointBalanceByRetailerIDResponse createGetShopperPointBalanceByRetailerIDResponse() {
        return new GetShopperPointBalanceByRetailerIDResponse();
    }

    /**
     * Create an instance of {@link GetShopperByID }
     * 
     */
    public GetShopperByID createGetShopperByID() {
        return new GetShopperByID();
    }

    /**
     * Create an instance of {@link GetShopperByIDResponse }
     * 
     */
    public GetShopperByIDResponse createGetShopperByIDResponse() {
        return new GetShopperByIDResponse();
    }

    /**
     * Create an instance of {@link Shopper }
     * 
     */
    public Shopper createShopper() {
        return new Shopper();
    }

    /**
     * Create an instance of {@link GetShopperByEmail }
     * 
     */
    public GetShopperByEmail createGetShopperByEmail() {
        return new GetShopperByEmail();
    }

    /**
     * Create an instance of {@link GetShopperByEmailResponse }
     * 
     */
    public GetShopperByEmailResponse createGetShopperByEmailResponse() {
        return new GetShopperByEmailResponse();
    }

    /**
     * Create an instance of {@link CreateShopper }
     * 
     */
    public CreateShopper createCreateShopper() {
        return new CreateShopper();
    }

    /**
     * Create an instance of {@link CreateShopperResponse }
     * 
     */
    public CreateShopperResponse createCreateShopperResponse() {
        return new CreateShopperResponse();
    }

    /**
     * Create an instance of {@link CreateShopperWithCard }
     * 
     */
    public CreateShopperWithCard createCreateShopperWithCard() {
        return new CreateShopperWithCard();
    }

    /**
     * Create an instance of {@link RegisteredCard }
     * 
     */
    public RegisteredCard createRegisteredCard() {
        return new RegisteredCard();
    }

    /**
     * Create an instance of {@link CreateShopperWithCardResponse }
     * 
     */
    public CreateShopperWithCardResponse createCreateShopperWithCardResponse() {
        return new CreateShopperWithCardResponse();
    }

    /**
     * Create an instance of {@link CreateAndScoreShopper }
     * 
     */
    public CreateAndScoreShopper createCreateAndScoreShopper() {
        return new CreateAndScoreShopper();
    }

    /**
     * Create an instance of {@link CreateAndScoreShopperResponse }
     * 
     */
    public CreateAndScoreShopperResponse createCreateAndScoreShopperResponse() {
        return new CreateAndScoreShopperResponse();
    }

    /**
     * Create an instance of {@link CreateUnregisteredShopper }
     * 
     */
    public CreateUnregisteredShopper createCreateUnregisteredShopper() {
        return new CreateUnregisteredShopper();
    }

    /**
     * Create an instance of {@link CreateUnregisteredShopperResponse }
     * 
     */
    public CreateUnregisteredShopperResponse createCreateUnregisteredShopperResponse() {
        return new CreateUnregisteredShopperResponse();
    }

    /**
     * Create an instance of {@link UpdateShopper }
     * 
     */
    public UpdateShopper createUpdateShopper() {
        return new UpdateShopper();
    }

    /**
     * Create an instance of {@link UpdateShopperResponse }
     * 
     */
    public UpdateShopperResponse createUpdateShopperResponse() {
        return new UpdateShopperResponse();
    }

    /**
     * Create an instance of {@link UnEnrollShopper }
     * 
     */
    public UnEnrollShopper createUnEnrollShopper() {
        return new UnEnrollShopper();
    }

    /**
     * Create an instance of {@link UnEnrollShopperResponse }
     * 
     */
    public UnEnrollShopperResponse createUnEnrollShopperResponse() {
        return new UnEnrollShopperResponse();
    }

    /**
     * Create an instance of {@link GetShopperByRetailerID }
     * 
     */
    public GetShopperByRetailerID createGetShopperByRetailerID() {
        return new GetShopperByRetailerID();
    }

    /**
     * Create an instance of {@link GetShopperByRetailerIDResponse }
     * 
     */
    public GetShopperByRetailerIDResponse createGetShopperByRetailerIDResponse() {
        return new GetShopperByRetailerIDResponse();
    }

    /**
     * Create an instance of {@link GetShopperByMergedVictimID }
     * 
     */
    public GetShopperByMergedVictimID createGetShopperByMergedVictimID() {
        return new GetShopperByMergedVictimID();
    }

    /**
     * Create an instance of {@link GetShopperByMergedVictimIDResponse }
     * 
     */
    public GetShopperByMergedVictimIDResponse createGetShopperByMergedVictimIDResponse() {
        return new GetShopperByMergedVictimIDResponse();
    }

    /**
     * Create an instance of {@link GetShopperByUserName }
     * 
     */
    public GetShopperByUserName createGetShopperByUserName() {
        return new GetShopperByUserName();
    }

    /**
     * Create an instance of {@link GetShopperByUserNameResponse }
     * 
     */
    public GetShopperByUserNameResponse createGetShopperByUserNameResponse() {
        return new GetShopperByUserNameResponse();
    }

    /**
     * Create an instance of {@link GetShopperByRegisteredCard }
     * 
     */
    public GetShopperByRegisteredCard createGetShopperByRegisteredCard() {
        return new GetShopperByRegisteredCard();
    }

    /**
     * Create an instance of {@link GetShopperByRegisteredCardResponse }
     * 
     */
    public GetShopperByRegisteredCardResponse createGetShopperByRegisteredCardResponse() {
        return new GetShopperByRegisteredCardResponse();
    }

    /**
     * Create an instance of {@link GetShoppersByRegisteredCard }
     * 
     */
    public GetShoppersByRegisteredCard createGetShoppersByRegisteredCard() {
        return new GetShoppersByRegisteredCard();
    }

    /**
     * Create an instance of {@link GetShoppersByRegisteredCardResponse }
     * 
     */
    public GetShoppersByRegisteredCardResponse createGetShoppersByRegisteredCardResponse() {
        return new GetShoppersByRegisteredCardResponse();
    }

    /**
     * Create an instance of {@link CreateRegisteredCard }
     * 
     */
    public CreateRegisteredCard createCreateRegisteredCard() {
        return new CreateRegisteredCard();
    }

    /**
     * Create an instance of {@link CreateRegisteredCardResponse }
     * 
     */
    public CreateRegisteredCardResponse createCreateRegisteredCardResponse() {
        return new CreateRegisteredCardResponse();
    }

    /**
     * Create an instance of {@link GetShopperRegisteredCards }
     * 
     */
    public GetShopperRegisteredCards createGetShopperRegisteredCards() {
        return new GetShopperRegisteredCards();
    }

    /**
     * Create an instance of {@link GetShopperRegisteredCardsResponse }
     * 
     */
    public GetShopperRegisteredCardsResponse createGetShopperRegisteredCardsResponse() {
        return new GetShopperRegisteredCardsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRegisteredCard }
     * 
     */
    public ArrayOfRegisteredCard createArrayOfRegisteredCard() {
        return new ArrayOfRegisteredCard();
    }

    /**
     * Create an instance of {@link GetRegisteredCard }
     * 
     */
    public GetRegisteredCard createGetRegisteredCard() {
        return new GetRegisteredCard();
    }

    /**
     * Create an instance of {@link GetRegisteredCardResponse }
     * 
     */
    public GetRegisteredCardResponse createGetRegisteredCardResponse() {
        return new GetRegisteredCardResponse();
    }

    /**
     * Create an instance of {@link GetProfilesByShopperId }
     * 
     */
    public GetProfilesByShopperId createGetProfilesByShopperId() {
        return new GetProfilesByShopperId();
    }

    /**
     * Create an instance of {@link GetProfilesByShopperIdResponse }
     * 
     */
    public GetProfilesByShopperIdResponse createGetProfilesByShopperIdResponse() {
        return new GetProfilesByShopperIdResponse();
    }

    /**
     * Create an instance of {@link ArrayOfProfile }
     * 
     */
    public ArrayOfProfile createArrayOfProfile() {
        return new ArrayOfProfile();
    }

    /**
     * Create an instance of {@link GetCustomAttribute }
     * 
     */
    public GetCustomAttribute createGetCustomAttribute() {
        return new GetCustomAttribute();
    }

    /**
     * Create an instance of {@link GetCustomAttributeResponse }
     * 
     */
    public GetCustomAttributeResponse createGetCustomAttributeResponse() {
        return new GetCustomAttributeResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomAttribute }
     * 
     */
    public UpdateCustomAttribute createUpdateCustomAttribute() {
        return new UpdateCustomAttribute();
    }

    /**
     * Create an instance of {@link UpdateCustomAttributeResponse }
     * 
     */
    public UpdateCustomAttributeResponse createUpdateCustomAttributeResponse() {
        return new UpdateCustomAttributeResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomAttributes }
     * 
     */
    public UpdateCustomAttributes createUpdateCustomAttributes() {
        return new UpdateCustomAttributes();
    }

    /**
     * Create an instance of {@link ArrayOfCustomAttributeParameter }
     * 
     */
    public ArrayOfCustomAttributeParameter createArrayOfCustomAttributeParameter() {
        return new ArrayOfCustomAttributeParameter();
    }

    /**
     * Create an instance of {@link UpdateCustomAttributesResponse }
     * 
     */
    public UpdateCustomAttributesResponse createUpdateCustomAttributesResponse() {
        return new UpdateCustomAttributesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCustomAttributeUpdateResult }
     * 
     */
    public ArrayOfCustomAttributeUpdateResult createArrayOfCustomAttributeUpdateResult() {
        return new ArrayOfCustomAttributeUpdateResult();
    }

    /**
     * Create an instance of {@link UpdateShopperOfferStatus }
     * 
     */
    public UpdateShopperOfferStatus createUpdateShopperOfferStatus() {
        return new UpdateShopperOfferStatus();
    }

    /**
     * Create an instance of {@link OfferStatus }
     * 
     */
    public OfferStatus createOfferStatus() {
        return new OfferStatus();
    }

    /**
     * Create an instance of {@link UpdateShopperOfferStatusResponse }
     * 
     */
    public UpdateShopperOfferStatusResponse createUpdateShopperOfferStatusResponse() {
        return new UpdateShopperOfferStatusResponse();
    }

    /**
     * Create an instance of {@link ReferFriend }
     * 
     */
    public ReferFriend createReferFriend() {
        return new ReferFriend();
    }

    /**
     * Create an instance of {@link ReferFriendResponse }
     * 
     */
    public ReferFriendResponse createReferFriendResponse() {
        return new ReferFriendResponse();
    }

    /**
     * Create an instance of {@link GetReferedFriends }
     * 
     */
    public GetReferedFriends createGetReferedFriends() {
        return new GetReferedFriends();
    }

    /**
     * Create an instance of {@link GetReferedFriendsResponse }
     * 
     */
    public GetReferedFriendsResponse createGetReferedFriendsResponse() {
        return new GetReferedFriendsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfReferedFriend }
     * 
     */
    public ArrayOfReferedFriend createArrayOfReferedFriend() {
        return new ArrayOfReferedFriend();
    }

    /**
     * Create an instance of {@link UpdateCustomQuestionForShopper }
     * 
     */
    public UpdateCustomQuestionForShopper createUpdateCustomQuestionForShopper() {
        return new UpdateCustomQuestionForShopper();
    }

    /**
     * Create an instance of {@link ShopperQuestionAnswerSet }
     * 
     */
    public ShopperQuestionAnswerSet createShopperQuestionAnswerSet() {
        return new ShopperQuestionAnswerSet();
    }

    /**
     * Create an instance of {@link UpdateCustomQuestionForShopperResponse }
     * 
     */
    public UpdateCustomQuestionForShopperResponse createUpdateCustomQuestionForShopperResponse() {
        return new UpdateCustomQuestionForShopperResponse();
    }

    /**
     * Create an instance of {@link GetCustomQuestionsForShopper }
     * 
     */
    public GetCustomQuestionsForShopper createGetCustomQuestionsForShopper() {
        return new GetCustomQuestionsForShopper();
    }

    /**
     * Create an instance of {@link GetCustomQuestionsForShopperResponse }
     * 
     */
    public GetCustomQuestionsForShopperResponse createGetCustomQuestionsForShopperResponse() {
        return new GetCustomQuestionsForShopperResponse();
    }

    /**
     * Create an instance of {@link ArrayOfShopperQuestionAnswerSet }
     * 
     */
    public ArrayOfShopperQuestionAnswerSet createArrayOfShopperQuestionAnswerSet() {
        return new ArrayOfShopperQuestionAnswerSet();
    }

    /**
     * Create an instance of {@link RedeemReward }
     * 
     */
    public RedeemReward createRedeemReward() {
        return new RedeemReward();
    }

    /**
     * Create an instance of {@link RedeemRewardResponse }
     * 
     */
    public RedeemRewardResponse createRedeemRewardResponse() {
        return new RedeemRewardResponse();
    }

    /**
     * Create an instance of {@link GetShopperOffers }
     * 
     */
    public GetShopperOffers createGetShopperOffers() {
        return new GetShopperOffers();
    }

    /**
     * Create an instance of {@link GetShopperOffersResponse }
     * 
     */
    public GetShopperOffersResponse createGetShopperOffersResponse() {
        return new GetShopperOffersResponse();
    }

    /**
     * Create an instance of {@link ArrayOfOfferStatus }
     * 
     */
    public ArrayOfOfferStatus createArrayOfOfferStatus() {
        return new ArrayOfOfferStatus();
    }

    /**
     * Create an instance of {@link GetShopperOffersExtended }
     * 
     */
    public GetShopperOffersExtended createGetShopperOffersExtended() {
        return new GetShopperOffersExtended();
    }

    /**
     * Create an instance of {@link GetShopperOffersExtendedResponse }
     * 
     */
    public GetShopperOffersExtendedResponse createGetShopperOffersExtendedResponse() {
        return new GetShopperOffersExtendedResponse();
    }

    /**
     * Create an instance of {@link ArrayOfOfferStatusExtended }
     * 
     */
    public ArrayOfOfferStatusExtended createArrayOfOfferStatusExtended() {
        return new ArrayOfOfferStatusExtended();
    }

    /**
     * Create an instance of {@link GetShopperPrograms }
     * 
     */
    public GetShopperPrograms createGetShopperPrograms() {
        return new GetShopperPrograms();
    }

    /**
     * Create an instance of {@link GetShopperProgramsResponse }
     * 
     */
    public GetShopperProgramsResponse createGetShopperProgramsResponse() {
        return new GetShopperProgramsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfProgram }
     * 
     */
    public ArrayOfProgram createArrayOfProgram() {
        return new ArrayOfProgram();
    }

    /**
     * Create an instance of {@link GetTransactions }
     * 
     */
    public GetTransactions createGetTransactions() {
        return new GetTransactions();
    }

    /**
     * Create an instance of {@link GetTransactionsResponse }
     * 
     */
    public GetTransactionsResponse createGetTransactionsResponse() {
        return new GetTransactionsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfShopperTransaction }
     * 
     */
    public ArrayOfShopperTransaction createArrayOfShopperTransaction() {
        return new ArrayOfShopperTransaction();
    }

    /**
     * Create an instance of {@link GetTransactionsWithTenders }
     * 
     */
    public GetTransactionsWithTenders createGetTransactionsWithTenders() {
        return new GetTransactionsWithTenders();
    }

    /**
     * Create an instance of {@link GetTransactionsWithTendersResponse }
     * 
     */
    public GetTransactionsWithTendersResponse createGetTransactionsWithTendersResponse() {
        return new GetTransactionsWithTendersResponse();
    }

    /**
     * Create an instance of {@link ArrayOfShopperDetailedTransaction }
     * 
     */
    public ArrayOfShopperDetailedTransaction createArrayOfShopperDetailedTransaction() {
        return new ArrayOfShopperDetailedTransaction();
    }

    /**
     * Create an instance of {@link GetShopperTransactionsWithCompleteProductInfo }
     * 
     */
    public GetShopperTransactionsWithCompleteProductInfo createGetShopperTransactionsWithCompleteProductInfo() {
        return new GetShopperTransactionsWithCompleteProductInfo();
    }

    /**
     * Create an instance of {@link GetShopperTransactionsWithCompleteProductInfoResponse }
     * 
     */
    public GetShopperTransactionsWithCompleteProductInfoResponse createGetShopperTransactionsWithCompleteProductInfoResponse() {
        return new GetShopperTransactionsWithCompleteProductInfoResponse();
    }

    /**
     * Create an instance of {@link GetTransactionsByDate }
     * 
     */
    public GetTransactionsByDate createGetTransactionsByDate() {
        return new GetTransactionsByDate();
    }

    /**
     * Create an instance of {@link GetTransactionsByDateResponse }
     * 
     */
    public GetTransactionsByDateResponse createGetTransactionsByDateResponse() {
        return new GetTransactionsByDateResponse();
    }

    /**
     * Create an instance of {@link AddTransaction }
     * 
     */
    public AddTransaction createAddTransaction() {
        return new AddTransaction();
    }

    /**
     * Create an instance of {@link OrderItem }
     * 
     */
    public OrderItem createOrderItem() {
        return new OrderItem();
    }

    /**
     * Create an instance of {@link AddTransactionResponse }
     * 
     */
    public AddTransactionResponse createAddTransactionResponse() {
        return new AddTransactionResponse();
    }

    /**
     * Create an instance of {@link GetShopperAuthenticationToken }
     * 
     */
    public GetShopperAuthenticationToken createGetShopperAuthenticationToken() {
        return new GetShopperAuthenticationToken();
    }

    /**
     * Create an instance of {@link GetShopperAuthenticationTokenResponse }
     * 
     */
    public GetShopperAuthenticationTokenResponse createGetShopperAuthenticationTokenResponse() {
        return new GetShopperAuthenticationTokenResponse();
    }

    /**
     * Create an instance of {@link GetShopperAuthenticationTokenByRetailerID }
     * 
     */
    public GetShopperAuthenticationTokenByRetailerID createGetShopperAuthenticationTokenByRetailerID() {
        return new GetShopperAuthenticationTokenByRetailerID();
    }

    /**
     * Create an instance of {@link GetShopperAuthenticationTokenByRetailerIDResponse }
     * 
     */
    public GetShopperAuthenticationTokenByRetailerIDResponse createGetShopperAuthenticationTokenByRetailerIDResponse() {
        return new GetShopperAuthenticationTokenByRetailerIDResponse();
    }

    /**
     * Create an instance of {@link GetCSRAuthenticationToken }
     * 
     */
    public GetCSRAuthenticationToken createGetCSRAuthenticationToken() {
        return new GetCSRAuthenticationToken();
    }

    /**
     * Create an instance of {@link GetCSRAuthenticationTokenResponse }
     * 
     */
    public GetCSRAuthenticationTokenResponse createGetCSRAuthenticationTokenResponse() {
        return new GetCSRAuthenticationTokenResponse();
    }

    /**
     * Create an instance of {@link GetShopper }
     * 
     */
    public GetShopper createGetShopper() {
        return new GetShopper();
    }

    /**
     * Create an instance of {@link GetShopperResponse }
     * 
     */
    public GetShopperResponse createGetShopperResponse() {
        return new GetShopperResponse();
    }

    /**
     * Create an instance of {@link ExtendedShopper }
     * 
     */
    public ExtendedShopper createExtendedShopper() {
        return new ExtendedShopper();
    }

    /**
     * Create an instance of {@link GetProductBySKU }
     * 
     */
    public GetProductBySKU createGetProductBySKU() {
        return new GetProductBySKU();
    }

    /**
     * Create an instance of {@link GetProductBySKUResponse }
     * 
     */
    public GetProductBySKUResponse createGetProductBySKUResponse() {
        return new GetProductBySKUResponse();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link CreateRewardRedemption }
     * 
     */
    public CreateRewardRedemption createCreateRewardRedemption() {
        return new CreateRewardRedemption();
    }

    /**
     * Create an instance of {@link CreateRewardRedemptionResponse }
     * 
     */
    public CreateRewardRedemptionResponse createCreateRewardRedemptionResponse() {
        return new CreateRewardRedemptionResponse();
    }

    /**
     * Create an instance of {@link Redemption }
     * 
     */
    public Redemption createRedemption() {
        return new Redemption();
    }

    /**
     * Create an instance of {@link CreateRewardRedemptionByPointGroup }
     * 
     */
    public CreateRewardRedemptionByPointGroup createCreateRewardRedemptionByPointGroup() {
        return new CreateRewardRedemptionByPointGroup();
    }

    /**
     * Create an instance of {@link CreateRewardRedemptionByPointGroupResponse }
     * 
     */
    public CreateRewardRedemptionByPointGroupResponse createCreateRewardRedemptionByPointGroupResponse() {
        return new CreateRewardRedemptionByPointGroupResponse();
    }

    /**
     * Create an instance of {@link GetShopperRedemptionsByDate }
     * 
     */
    public GetShopperRedemptionsByDate createGetShopperRedemptionsByDate() {
        return new GetShopperRedemptionsByDate();
    }

    /**
     * Create an instance of {@link GetShopperRedemptionsByDateResponse }
     * 
     */
    public GetShopperRedemptionsByDateResponse createGetShopperRedemptionsByDateResponse() {
        return new GetShopperRedemptionsByDateResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRedemption }
     * 
     */
    public ArrayOfRedemption createArrayOfRedemption() {
        return new ArrayOfRedemption();
    }

    /**
     * Create an instance of {@link GetPlcAvailablePointsByDate }
     * 
     */
    public GetPlcAvailablePointsByDate createGetPlcAvailablePointsByDate() {
        return new GetPlcAvailablePointsByDate();
    }

    /**
     * Create an instance of {@link GetPlcAvailablePointsByDateResponse }
     * 
     */
    public GetPlcAvailablePointsByDateResponse createGetPlcAvailablePointsByDateResponse() {
        return new GetPlcAvailablePointsByDateResponse();
    }

    /**
     * Create an instance of {@link GetPlcAvailablePointsByDateByPointGroup }
     * 
     */
    public GetPlcAvailablePointsByDateByPointGroup createGetPlcAvailablePointsByDateByPointGroup() {
        return new GetPlcAvailablePointsByDateByPointGroup();
    }

    /**
     * Create an instance of {@link GetPlcAvailablePointsByDateByPointGroupResponse }
     * 
     */
    public GetPlcAvailablePointsByDateByPointGroupResponse createGetPlcAvailablePointsByDateByPointGroupResponse() {
        return new GetPlcAvailablePointsByDateByPointGroupResponse();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByDate }
     * 
     */
    public GetShopperPointBalanceByDate createGetShopperPointBalanceByDate() {
        return new GetShopperPointBalanceByDate();
    }

    /**
     * Create an instance of {@link GetShopperPointBalanceByDateResponse }
     * 
     */
    public GetShopperPointBalanceByDateResponse createGetShopperPointBalanceByDateResponse() {
        return new GetShopperPointBalanceByDateResponse();
    }

    /**
     * Create an instance of {@link GetShopperRedemptionsOrderItemsByDate }
     * 
     */
    public GetShopperRedemptionsOrderItemsByDate createGetShopperRedemptionsOrderItemsByDate() {
        return new GetShopperRedemptionsOrderItemsByDate();
    }

    /**
     * Create an instance of {@link GetShopperRedemptionsOrderItemsByDateResponse }
     * 
     */
    public GetShopperRedemptionsOrderItemsByDateResponse createGetShopperRedemptionsOrderItemsByDateResponse() {
        return new GetShopperRedemptionsOrderItemsByDateResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRedemptionOrderItem }
     * 
     */
    public ArrayOfRedemptionOrderItem createArrayOfRedemptionOrderItem() {
        return new ArrayOfRedemptionOrderItem();
    }

    /**
     * Create an instance of {@link EnrollShopper }
     * 
     */
    public EnrollShopper createEnrollShopper() {
        return new EnrollShopper();
    }

    /**
     * Create an instance of {@link EnrollShopperResponse }
     * 
     */
    public EnrollShopperResponse createEnrollShopperResponse() {
        return new EnrollShopperResponse();
    }

    /**
     * Create an instance of {@link GetPurchaseAwardForReturns }
     * 
     */
    public GetPurchaseAwardForReturns createGetPurchaseAwardForReturns() {
        return new GetPurchaseAwardForReturns();
    }

    /**
     * Create an instance of {@link ArrayOfReturnOrderItem }
     * 
     */
    public ArrayOfReturnOrderItem createArrayOfReturnOrderItem() {
        return new ArrayOfReturnOrderItem();
    }

    /**
     * Create an instance of {@link GetPurchaseAwardForReturnsResponse }
     * 
     */
    public GetPurchaseAwardForReturnsResponse createGetPurchaseAwardForReturnsResponse() {
        return new GetPurchaseAwardForReturnsResponse();
    }

    /**
     * Create an instance of {@link ReturnsAward }
     * 
     */
    public ReturnsAward createReturnsAward() {
        return new ReturnsAward();
    }

    /**
     * Create an instance of {@link ReversePurchaseAwardForReturns }
     * 
     */
    public ReversePurchaseAwardForReturns createReversePurchaseAwardForReturns() {
        return new ReversePurchaseAwardForReturns();
    }

    /**
     * Create an instance of {@link ReversePurchaseAwardForReturnsResponse }
     * 
     */
    public ReversePurchaseAwardForReturnsResponse createReversePurchaseAwardForReturnsResponse() {
        return new ReversePurchaseAwardForReturnsResponse();
    }

    /**
     * Create an instance of {@link CreateRewardRedemptionByPointGroupAndCustomAttributes }
     * 
     */
    public CreateRewardRedemptionByPointGroupAndCustomAttributes createCreateRewardRedemptionByPointGroupAndCustomAttributes() {
        return new CreateRewardRedemptionByPointGroupAndCustomAttributes();
    }

    /**
     * Create an instance of {@link CreateRewardRedemptionByPointGroupAndCustomAttributesResponse }
     * 
     */
    public CreateRewardRedemptionByPointGroupAndCustomAttributesResponse createCreateRewardRedemptionByPointGroupAndCustomAttributesResponse() {
        return new CreateRewardRedemptionByPointGroupAndCustomAttributesResponse();
    }

    /**
     * Create an instance of {@link UpdateShopperPointsBalanceWithCustomAttributes }
     * 
     */
    public UpdateShopperPointsBalanceWithCustomAttributes createUpdateShopperPointsBalanceWithCustomAttributes() {
        return new UpdateShopperPointsBalanceWithCustomAttributes();
    }

    /**
     * Create an instance of {@link UpdateShopperPointsBalanceWithCustomAttributesResponse }
     * 
     */
    public UpdateShopperPointsBalanceWithCustomAttributesResponse createUpdateShopperPointsBalanceWithCustomAttributesResponse() {
        return new UpdateShopperPointsBalanceWithCustomAttributesResponse();
    }

    /**
     * Create an instance of {@link AuditTrailGroup }
     * 
     */
    public AuditTrailGroup createAuditTrailGroup() {
        return new AuditTrailGroup();
    }

    /**
     * Create an instance of {@link CreateRewardProduct }
     * 
     */
    public CreateRewardProduct createCreateRewardProduct() {
        return new CreateRewardProduct();
    }

    /**
     * Create an instance of {@link RewardProduct }
     * 
     */
    public RewardProduct createRewardProduct() {
        return new RewardProduct();
    }

    /**
     * Create an instance of {@link CreateRewardProductResponse }
     * 
     */
    public CreateRewardProductResponse createCreateRewardProductResponse() {
        return new CreateRewardProductResponse();
    }

    /**
     * Create an instance of {@link UpdateRewardProduct }
     * 
     */
    public UpdateRewardProduct createUpdateRewardProduct() {
        return new UpdateRewardProduct();
    }

    /**
     * Create an instance of {@link UpdateRewardProductResponse }
     * 
     */
    public UpdateRewardProductResponse createUpdateRewardProductResponse() {
        return new UpdateRewardProductResponse();
    }

    /**
     * Create an instance of {@link GetRewardProductById }
     * 
     */
    public GetRewardProductById createGetRewardProductById() {
        return new GetRewardProductById();
    }

    /**
     * Create an instance of {@link GetRewardProductByIdResponse }
     * 
     */
    public GetRewardProductByIdResponse createGetRewardProductByIdResponse() {
        return new GetRewardProductByIdResponse();
    }

    /**
     * Create an instance of {@link GetRewardProductByUniqueSKU }
     * 
     */
    public GetRewardProductByUniqueSKU createGetRewardProductByUniqueSKU() {
        return new GetRewardProductByUniqueSKU();
    }

    /**
     * Create an instance of {@link GetRewardProductByUniqueSKUResponse }
     * 
     */
    public GetRewardProductByUniqueSKUResponse createGetRewardProductByUniqueSKUResponse() {
        return new GetRewardProductByUniqueSKUResponse();
    }

    /**
     * Create an instance of {@link GetRewardCatalogByRetailerCatalogId }
     * 
     */
    public GetRewardCatalogByRetailerCatalogId createGetRewardCatalogByRetailerCatalogId() {
        return new GetRewardCatalogByRetailerCatalogId();
    }

    /**
     * Create an instance of {@link GetRewardCatalogByRetailerCatalogIdResponse }
     * 
     */
    public GetRewardCatalogByRetailerCatalogIdResponse createGetRewardCatalogByRetailerCatalogIdResponse() {
        return new GetRewardCatalogByRetailerCatalogIdResponse();
    }

    /**
     * Create an instance of {@link RewardCatalog }
     * 
     */
    public RewardCatalog createRewardCatalog() {
        return new RewardCatalog();
    }

    /**
     * Create an instance of {@link GetRewardCatalogById }
     * 
     */
    public GetRewardCatalogById createGetRewardCatalogById() {
        return new GetRewardCatalogById();
    }

    /**
     * Create an instance of {@link GetRewardCatalogByIdResponse }
     * 
     */
    public GetRewardCatalogByIdResponse createGetRewardCatalogByIdResponse() {
        return new GetRewardCatalogByIdResponse();
    }

    /**
     * Create an instance of {@link AddRewardProductToCatalog }
     * 
     */
    public AddRewardProductToCatalog createAddRewardProductToCatalog() {
        return new AddRewardProductToCatalog();
    }

    /**
     * Create an instance of {@link AddRewardProductToCatalogResponse }
     * 
     */
    public AddRewardProductToCatalogResponse createAddRewardProductToCatalogResponse() {
        return new AddRewardProductToCatalogResponse();
    }

    /**
     * Create an instance of {@link RemoveRewardProductFromCatalog }
     * 
     */
    public RemoveRewardProductFromCatalog createRemoveRewardProductFromCatalog() {
        return new RemoveRewardProductFromCatalog();
    }

    /**
     * Create an instance of {@link RemoveRewardProductFromCatalogResponse }
     * 
     */
    public RemoveRewardProductFromCatalogResponse createRemoveRewardProductFromCatalogResponse() {
        return new RemoveRewardProductFromCatalogResponse();
    }

    /**
     * Create an instance of {@link GetRewardCatalogsByRewardProductId }
     * 
     */
    public GetRewardCatalogsByRewardProductId createGetRewardCatalogsByRewardProductId() {
        return new GetRewardCatalogsByRewardProductId();
    }

    /**
     * Create an instance of {@link GetRewardCatalogsByRewardProductIdResponse }
     * 
     */
    public GetRewardCatalogsByRewardProductIdResponse createGetRewardCatalogsByRewardProductIdResponse() {
        return new GetRewardCatalogsByRewardProductIdResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRewardCatalog }
     * 
     */
    public ArrayOfRewardCatalog createArrayOfRewardCatalog() {
        return new ArrayOfRewardCatalog();
    }

    /**
     * Create an instance of {@link GetCustomEntitiesByReferenceTag }
     * 
     */
    public GetCustomEntitiesByReferenceTag createGetCustomEntitiesByReferenceTag() {
        return new GetCustomEntitiesByReferenceTag();
    }

    /**
     * Create an instance of {@link GetCustomEntitiesByReferenceTagResponse }
     * 
     */
    public GetCustomEntitiesByReferenceTagResponse createGetCustomEntitiesByReferenceTagResponse() {
        return new GetCustomEntitiesByReferenceTagResponse();
    }

    /**
     * Create an instance of {@link CustomEntitiesWrapper }
     * 
     */
    public CustomEntitiesWrapper createCustomEntitiesWrapper() {
        return new CustomEntitiesWrapper();
    }

    /**
     * Create an instance of {@link UpdateCustomEntity }
     * 
     */
    public UpdateCustomEntity createUpdateCustomEntity() {
        return new UpdateCustomEntity();
    }

    /**
     * Create an instance of {@link ArrayOfAPICustomEntity1 }
     * 
     */
    public ArrayOfAPICustomEntity1 createArrayOfAPICustomEntity1() {
        return new ArrayOfAPICustomEntity1();
    }

    /**
     * Create an instance of {@link UpdateCustomEntityResponse }
     * 
     */
    public UpdateCustomEntityResponse createUpdateCustomEntityResponse() {
        return new UpdateCustomEntityResponse();
    }

    /**
     * Create an instance of {@link CustomEntityDataUpsertReturn }
     * 
     */
    public CustomEntityDataUpsertReturn createCustomEntityDataUpsertReturn() {
        return new CustomEntityDataUpsertReturn();
    }

    /**
     * Create an instance of {@link GetReferenceObjectFromUniqueCustomEntity }
     * 
     */
    public GetReferenceObjectFromUniqueCustomEntity createGetReferenceObjectFromUniqueCustomEntity() {
        return new GetReferenceObjectFromUniqueCustomEntity();
    }

    /**
     * Create an instance of {@link APICustomEntity }
     * 
     */
    public APICustomEntity createAPICustomEntity() {
        return new APICustomEntity();
    }

    /**
     * Create an instance of {@link GetReferenceObjectFromUniqueCustomEntityResponse }
     * 
     */
    public GetReferenceObjectFromUniqueCustomEntityResponse createGetReferenceObjectFromUniqueCustomEntityResponse() {
        return new GetReferenceObjectFromUniqueCustomEntityResponse();
    }

    /**
     * Create an instance of {@link GetReferenceObjectsFromSearchableCustomEntity }
     * 
     */
    public GetReferenceObjectsFromSearchableCustomEntity createGetReferenceObjectsFromSearchableCustomEntity() {
        return new GetReferenceObjectsFromSearchableCustomEntity();
    }

    /**
     * Create an instance of {@link GetReferenceObjectsFromSearchableCustomEntityResponse }
     * 
     */
    public GetReferenceObjectsFromSearchableCustomEntityResponse createGetReferenceObjectsFromSearchableCustomEntityResponse() {
        return new GetReferenceObjectsFromSearchableCustomEntityResponse();
    }

    /**
     * Create an instance of {@link ArrayOfAnyType }
     * 
     */
    public ArrayOfAnyType createArrayOfAnyType() {
        return new ArrayOfAnyType();
    }

    /**
     * Create an instance of {@link RedeemShopperRewardCerificate }
     * 
     */
    public RedeemShopperRewardCerificate createRedeemShopperRewardCerificate() {
        return new RedeemShopperRewardCerificate();
    }

    /**
     * Create an instance of {@link ShopperIdentifier }
     * 
     */
    public ShopperIdentifier createShopperIdentifier() {
        return new ShopperIdentifier();
    }

    /**
     * Create an instance of {@link RedeemShopperRewardCerificateResponse }
     * 
     */
    public RedeemShopperRewardCerificateResponse createRedeemShopperRewardCerificateResponse() {
        return new RedeemShopperRewardCerificateResponse();
    }

    /**
     * Create an instance of {@link SendShopperPassword }
     * 
     */
    public SendShopperPassword createSendShopperPassword() {
        return new SendShopperPassword();
    }

    /**
     * Create an instance of {@link SendShopperPasswordResponse }
     * 
     */
    public SendShopperPasswordResponse createSendShopperPasswordResponse() {
        return new SendShopperPasswordResponse();
    }

    /**
     * Create an instance of {@link ShopperSignIn }
     * 
     */
    public ShopperSignIn createShopperSignIn() {
        return new ShopperSignIn();
    }

    /**
     * Create an instance of {@link ShopperSignInResponse }
     * 
     */
    public ShopperSignInResponse createShopperSignInResponse() {
        return new ShopperSignInResponse();
    }

    /**
     * Create an instance of {@link RedeemOffer }
     * 
     */
    public RedeemOffer createRedeemOffer() {
        return new RedeemOffer();
    }

    /**
     * Create an instance of {@link RedeemOfferResponse }
     * 
     */
    public RedeemOfferResponse createRedeemOfferResponse() {
        return new RedeemOfferResponse();
    }

    /**
     * Create an instance of {@link ShopperRewardItemRedemption }
     * 
     */
    public ShopperRewardItemRedemption createShopperRewardItemRedemption() {
        return new ShopperRewardItemRedemption();
    }

    /**
     * Create an instance of {@link GetStores }
     * 
     */
    public GetStores createGetStores() {
        return new GetStores();
    }

    /**
     * Create an instance of {@link GetStoresResponse }
     * 
     */
    public GetStoresResponse createGetStoresResponse() {
        return new GetStoresResponse();
    }

    /**
     * Create an instance of {@link ArrayOfStore }
     * 
     */
    public ArrayOfStore createArrayOfStore() {
        return new ArrayOfStore();
    }

    /**
     * Create an instance of {@link ImportTransactions }
     * 
     */
    public ImportTransactions createImportTransactions() {
        return new ImportTransactions();
    }

    /**
     * Create an instance of {@link ArrayOfISCLogTransactionType }
     * 
     */
    public ArrayOfISCLogTransactionType createArrayOfISCLogTransactionType() {
        return new ArrayOfISCLogTransactionType();
    }

    /**
     * Create an instance of {@link ImportTransactionsResponse }
     * 
     */
    public ImportTransactionsResponse createImportTransactionsResponse() {
        return new ImportTransactionsResponse();
    }

    /**
     * Create an instance of {@link ChangePointStateByExternalId }
     * 
     */
    public ChangePointStateByExternalId createChangePointStateByExternalId() {
        return new ChangePointStateByExternalId();
    }

    /**
     * Create an instance of {@link ChangePointStateByExternalIdResponse }
     * 
     */
    public ChangePointStateByExternalIdResponse createChangePointStateByExternalIdResponse() {
        return new ChangePointStateByExternalIdResponse();
    }

    /**
     * Create an instance of {@link AuthenticateUser }
     * 
     */
    public AuthenticateUser createAuthenticateUser() {
        return new AuthenticateUser();
    }

    /**
     * Create an instance of {@link AuthenticateUserResponse }
     * 
     */
    public AuthenticateUserResponse createAuthenticateUserResponse() {
        return new AuthenticateUserResponse();
    }

    /**
     * Create an instance of {@link PointPricingData }
     * 
     */
    public PointPricingData createPointPricingData() {
        return new PointPricingData();
    }

    /**
     * Create an instance of {@link ProductToPrice }
     * 
     */
    public ProductToPrice createProductToPrice() {
        return new ProductToPrice();
    }

    /**
     * Create an instance of {@link RedemptionPointLifeCycleOrderItem }
     * 
     */
    public RedemptionPointLifeCycleOrderItem createRedemptionPointLifeCycleOrderItem() {
        return new RedemptionPointLifeCycleOrderItem();
    }

    /**
     * Create an instance of {@link RedemptionPointState }
     * 
     */
    public RedemptionPointState createRedemptionPointState() {
        return new RedemptionPointState();
    }

    /**
     * Create an instance of {@link RewardItem }
     * 
     */
    public RewardItem createRewardItem() {
        return new RewardItem();
    }

    /**
     * Create an instance of {@link ArrayOfEventAwardDetail }
     * 
     */
    public ArrayOfEventAwardDetail createArrayOfEventAwardDetail() {
        return new ArrayOfEventAwardDetail();
    }

    /**
     * Create an instance of {@link EventAwardDetail }
     * 
     */
    public EventAwardDetail createEventAwardDetail() {
        return new EventAwardDetail();
    }

    /**
     * Create an instance of {@link RewardCode }
     * 
     */
    public RewardCode createRewardCode() {
        return new RewardCode();
    }

    /**
     * Create an instance of {@link CreditAward }
     * 
     */
    public CreditAward createCreditAward() {
        return new CreditAward();
    }

    /**
     * Create an instance of {@link ShopperRewardItemRedemptionExtended }
     * 
     */
    public ShopperRewardItemRedemptionExtended createShopperRewardItemRedemptionExtended() {
        return new ShopperRewardItemRedemptionExtended();
    }

    /**
     * Create an instance of {@link ArrayOfLineItem }
     * 
     */
    public ArrayOfLineItem createArrayOfLineItem() {
        return new ArrayOfLineItem();
    }

    /**
     * Create an instance of {@link LineItem }
     * 
     */
    public LineItem createLineItem() {
        return new LineItem();
    }

    /**
     * Create an instance of {@link ArrayOfTender }
     * 
     */
    public ArrayOfTender createArrayOfTender() {
        return new ArrayOfTender();
    }

    /**
     * Create an instance of {@link Tender }
     * 
     */
    public Tender createTender() {
        return new Tender();
    }

    /**
     * Create an instance of {@link ArrayOfCoupon }
     * 
     */
    public ArrayOfCoupon createArrayOfCoupon() {
        return new ArrayOfCoupon();
    }

    /**
     * Create an instance of {@link Coupon }
     * 
     */
    public Coupon createCoupon() {
        return new Coupon();
    }

    /**
     * Create an instance of {@link ArrayOfAwardDetail }
     * 
     */
    public ArrayOfAwardDetail createArrayOfAwardDetail() {
        return new ArrayOfAwardDetail();
    }

    /**
     * Create an instance of {@link AwardDetail }
     * 
     */
    public AwardDetail createAwardDetail() {
        return new AwardDetail();
    }

    /**
     * Create an instance of {@link ArrayOfLineItemWithAwards }
     * 
     */
    public ArrayOfLineItemWithAwards createArrayOfLineItemWithAwards() {
        return new ArrayOfLineItemWithAwards();
    }

    /**
     * Create an instance of {@link LineItemWithAwards }
     * 
     */
    public LineItemWithAwards createLineItemWithAwards() {
        return new LineItemWithAwards();
    }

    /**
     * Create an instance of {@link ArrayOfAwardDetail1 }
     * 
     */
    public ArrayOfAwardDetail1 createArrayOfAwardDetail1() {
        return new ArrayOfAwardDetail1();
    }

    /**
     * Create an instance of {@link ArrayOfTenderWithAwards }
     * 
     */
    public ArrayOfTenderWithAwards createArrayOfTenderWithAwards() {
        return new ArrayOfTenderWithAwards();
    }

    /**
     * Create an instance of {@link TenderWithAwards }
     * 
     */
    public TenderWithAwards createTenderWithAwards() {
        return new TenderWithAwards();
    }

    /**
     * Create an instance of {@link ArrayOfAwardDetail2 }
     * 
     */
    public ArrayOfAwardDetail2 createArrayOfAwardDetail2() {
        return new ArrayOfAwardDetail2();
    }

    /**
     * Create an instance of {@link TierDetails }
     * 
     */
    public TierDetails createTierDetails() {
        return new TierDetails();
    }

    /**
     * Create an instance of {@link BulkCustomAttributeHead }
     * 
     */
    public BulkCustomAttributeHead createBulkCustomAttributeHead() {
        return new BulkCustomAttributeHead();
    }

    /**
     * Create an instance of {@link ArrayOfBulkCustomAttributeInfo }
     * 
     */
    public ArrayOfBulkCustomAttributeInfo createArrayOfBulkCustomAttributeInfo() {
        return new ArrayOfBulkCustomAttributeInfo();
    }

    /**
     * Create an instance of {@link BulkCustomAttributeInfo }
     * 
     */
    public BulkCustomAttributeInfo createBulkCustomAttributeInfo() {
        return new BulkCustomAttributeInfo();
    }

    /**
     * Create an instance of {@link Profile }
     * 
     */
    public Profile createProfile() {
        return new Profile();
    }

    /**
     * Create an instance of {@link CustomAttributeParameter }
     * 
     */
    public CustomAttributeParameter createCustomAttributeParameter() {
        return new CustomAttributeParameter();
    }

    /**
     * Create an instance of {@link ArrayOfAttributeToSet }
     * 
     */
    public ArrayOfAttributeToSet createArrayOfAttributeToSet() {
        return new ArrayOfAttributeToSet();
    }

    /**
     * Create an instance of {@link AttributeToSet }
     * 
     */
    public AttributeToSet createAttributeToSet() {
        return new AttributeToSet();
    }

    /**
     * Create an instance of {@link CustomAttributeUpdateResult }
     * 
     */
    public CustomAttributeUpdateResult createCustomAttributeUpdateResult() {
        return new CustomAttributeUpdateResult();
    }

    /**
     * Create an instance of {@link Offer }
     * 
     */
    public Offer createOffer() {
        return new Offer();
    }

    /**
     * Create an instance of {@link ArrayOfOfferCategory }
     * 
     */
    public ArrayOfOfferCategory createArrayOfOfferCategory() {
        return new ArrayOfOfferCategory();
    }

    /**
     * Create an instance of {@link OfferCategory }
     * 
     */
    public OfferCategory createOfferCategory() {
        return new OfferCategory();
    }

    /**
     * Create an instance of {@link ReferedFriend }
     * 
     */
    public ReferedFriend createReferedFriend() {
        return new ReferedFriend();
    }

    /**
     * Create an instance of {@link QuestionnaireQuestion }
     * 
     */
    public QuestionnaireQuestion createQuestionnaireQuestion() {
        return new QuestionnaireQuestion();
    }

    /**
     * Create an instance of {@link ArrayOfShopperAnswer }
     * 
     */
    public ArrayOfShopperAnswer createArrayOfShopperAnswer() {
        return new ArrayOfShopperAnswer();
    }

    /**
     * Create an instance of {@link ShopperAnswer }
     * 
     */
    public ShopperAnswer createShopperAnswer() {
        return new ShopperAnswer();
    }

    /**
     * Create an instance of {@link QuestionnaireAnswer }
     * 
     */
    public QuestionnaireAnswer createQuestionnaireAnswer() {
        return new QuestionnaireAnswer();
    }

    /**
     * Create an instance of {@link OfferStatusExtended }
     * 
     */
    public OfferStatusExtended createOfferStatusExtended() {
        return new OfferStatusExtended();
    }

    /**
     * Create an instance of {@link OfferExtended }
     * 
     */
    public OfferExtended createOfferExtended() {
        return new OfferExtended();
    }

    /**
     * Create an instance of {@link Program }
     * 
     */
    public Program createProgram() {
        return new Program();
    }

    /**
     * Create an instance of {@link ShopperTransaction }
     * 
     */
    public ShopperTransaction createShopperTransaction() {
        return new ShopperTransaction();
    }

    /**
     * Create an instance of {@link ShopperDetailedTransaction }
     * 
     */
    public ShopperDetailedTransaction createShopperDetailedTransaction() {
        return new ShopperDetailedTransaction();
    }

    /**
     * Create an instance of {@link ArrayOfTransactionDetail }
     * 
     */
    public ArrayOfTransactionDetail createArrayOfTransactionDetail() {
        return new ArrayOfTransactionDetail();
    }

    /**
     * Create an instance of {@link TransactionDetail }
     * 
     */
    public TransactionDetail createTransactionDetail() {
        return new TransactionDetail();
    }

    /**
     * Create an instance of {@link TransactionProduct }
     * 
     */
    public TransactionProduct createTransactionProduct() {
        return new TransactionProduct();
    }

    /**
     * Create an instance of {@link TransactionLineItem }
     * 
     */
    public TransactionLineItem createTransactionLineItem() {
        return new TransactionLineItem();
    }

    /**
     * Create an instance of {@link ArrayOfTransactionTender }
     * 
     */
    public ArrayOfTransactionTender createArrayOfTransactionTender() {
        return new ArrayOfTransactionTender();
    }

    /**
     * Create an instance of {@link TransactionTender }
     * 
     */
    public TransactionTender createTransactionTender() {
        return new TransactionTender();
    }

    /**
     * Create an instance of {@link RedemptionOrderItem }
     * 
     */
    public RedemptionOrderItem createRedemptionOrderItem() {
        return new RedemptionOrderItem();
    }

    /**
     * Create an instance of {@link ReturnOrderItem }
     * 
     */
    public ReturnOrderItem createReturnOrderItem() {
        return new ReturnOrderItem();
    }

    /**
     * Create an instance of {@link ArrayOfAPICustomEntity }
     * 
     */
    public ArrayOfAPICustomEntity createArrayOfAPICustomEntity() {
        return new ArrayOfAPICustomEntity();
    }

    /**
     * Create an instance of {@link ArrayOfAPICustomEntityField }
     * 
     */
    public ArrayOfAPICustomEntityField createArrayOfAPICustomEntityField() {
        return new ArrayOfAPICustomEntityField();
    }

    /**
     * Create an instance of {@link APICustomEntityField }
     * 
     */
    public APICustomEntityField createAPICustomEntityField() {
        return new APICustomEntityField();
    }

    /**
     * Create an instance of {@link ArrayOfCustomEntityDataUpsertRow }
     * 
     */
    public ArrayOfCustomEntityDataUpsertRow createArrayOfCustomEntityDataUpsertRow() {
        return new ArrayOfCustomEntityDataUpsertRow();
    }

    /**
     * Create an instance of {@link CustomEntityDataUpsertRow }
     * 
     */
    public CustomEntityDataUpsertRow createCustomEntityDataUpsertRow() {
        return new CustomEntityDataUpsertRow();
    }

    /**
     * Create an instance of {@link Store }
     * 
     */
    public Store createStore() {
        return new Store();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticationResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.loyaltylab.com/loyaltyapi/", name = "AuthenticationResult")
    public JAXBElement<AuthenticationResult> createAuthenticationResult(AuthenticationResult value) {
        return new JAXBElement<AuthenticationResult>(_AuthenticationResult_QNAME, AuthenticationResult.class, null, value);
    }

}
