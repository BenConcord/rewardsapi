
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for KohlsTransactionDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KohlsTransactionDetail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransactionDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="StoreId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StoreName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPoints" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="BasePoints" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="BonusPoints" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SalesTax" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ShippingTax" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ShippingCharge" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="QualifyingAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Items" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfKohlsTransactionDetailItem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KohlsTransactionDetail", propOrder = {
    "transactionID",
    "transactionDateTime",
    "storeId",
    "storeName",
    "totalPoints",
    "basePoints",
    "bonusPoints",
    "salesTax",
    "shippingTax",
    "shippingCharge",
    "qualifyingAmount",
    "totalAmount",
    "items"
})
public class KohlsTransactionDetail {

    @XmlElement(name = "TransactionID")
    protected String transactionID;
    @XmlElement(name = "TransactionDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transactionDateTime;
    @XmlElement(name = "StoreId")
    protected String storeId;
    @XmlElement(name = "StoreName")
    protected String storeName;
    @XmlElement(name = "TotalPoints")
    protected int totalPoints;
    @XmlElement(name = "BasePoints")
    protected int basePoints;
    @XmlElement(name = "BonusPoints")
    protected int bonusPoints;
    @XmlElement(name = "SalesTax", required = true)
    protected BigDecimal salesTax;
    @XmlElement(name = "ShippingTax", required = true)
    protected BigDecimal shippingTax;
    @XmlElement(name = "ShippingCharge", required = true)
    protected BigDecimal shippingCharge;
    @XmlElement(name = "QualifyingAmount", required = true)
    protected BigDecimal qualifyingAmount;
    @XmlElement(name = "TotalAmount", required = true)
    protected BigDecimal totalAmount;
    @XmlElement(name = "Items")
    protected ArrayOfKohlsTransactionDetailItem items;

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the transactionDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransactionDateTime() {
        return transactionDateTime;
    }

    /**
     * Sets the value of the transactionDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransactionDateTime(XMLGregorianCalendar value) {
        this.transactionDateTime = value;
    }

    /**
     * Gets the value of the storeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * Sets the value of the storeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreId(String value) {
        this.storeId = value;
    }

    /**
     * Gets the value of the storeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * Sets the value of the storeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreName(String value) {
        this.storeName = value;
    }

    /**
     * Gets the value of the totalPoints property.
     * 
     */
    public int getTotalPoints() {
        return totalPoints;
    }

    /**
     * Sets the value of the totalPoints property.
     * 
     */
    public void setTotalPoints(int value) {
        this.totalPoints = value;
    }

    /**
     * Gets the value of the basePoints property.
     * 
     */
    public int getBasePoints() {
        return basePoints;
    }

    /**
     * Sets the value of the basePoints property.
     * 
     */
    public void setBasePoints(int value) {
        this.basePoints = value;
    }

    /**
     * Gets the value of the bonusPoints property.
     * 
     */
    public int getBonusPoints() {
        return bonusPoints;
    }

    /**
     * Sets the value of the bonusPoints property.
     * 
     */
    public void setBonusPoints(int value) {
        this.bonusPoints = value;
    }

    /**
     * Gets the value of the salesTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSalesTax() {
        return salesTax;
    }

    /**
     * Sets the value of the salesTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSalesTax(BigDecimal value) {
        this.salesTax = value;
    }

    /**
     * Gets the value of the shippingTax property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShippingTax() {
        return shippingTax;
    }

    /**
     * Sets the value of the shippingTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShippingTax(BigDecimal value) {
        this.shippingTax = value;
    }

    /**
     * Gets the value of the shippingCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShippingCharge() {
        return shippingCharge;
    }

    /**
     * Sets the value of the shippingCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShippingCharge(BigDecimal value) {
        this.shippingCharge = value;
    }

    /**
     * Gets the value of the qualifyingAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQualifyingAmount() {
        return qualifyingAmount;
    }

    /**
     * Sets the value of the qualifyingAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQualifyingAmount(BigDecimal value) {
        this.qualifyingAmount = value;
    }

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAmount(BigDecimal value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKohlsTransactionDetailItem }
     *     
     */
    public ArrayOfKohlsTransactionDetailItem getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKohlsTransactionDetailItem }
     *     
     */
    public void setItems(ArrayOfKohlsTransactionDetailItem value) {
        this.items = value;
    }

}
