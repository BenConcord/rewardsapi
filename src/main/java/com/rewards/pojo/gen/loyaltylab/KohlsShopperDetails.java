
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KohlsShopperDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KohlsShopperDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Shopper" type="{http://www.loyaltylab.com/loyaltyapi/}Shopper" minOccurs="0"/&gt;
 *         &lt;element name="CustomAttributeDetails" type="{http://www.loyaltylab.com/loyaltyapi/}KohlsShopperCustomAttributeDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KohlsShopperDetails", propOrder = {
    "shopper",
    "customAttributeDetails"
})
public class KohlsShopperDetails {

    @XmlElement(name = "Shopper")
    protected Shopper shopper;
    @XmlElement(name = "CustomAttributeDetails")
    protected KohlsShopperCustomAttributeDetails customAttributeDetails;

    /**
     * Gets the value of the shopper property.
     * 
     * @return
     *     possible object is
     *     {@link Shopper }
     *     
     */
    public Shopper getShopper() {
        return shopper;
    }

    /**
     * Sets the value of the shopper property.
     * 
     * @param value
     *     allowed object is
     *     {@link Shopper }
     *     
     */
    public void setShopper(Shopper value) {
        this.shopper = value;
    }

    /**
     * Gets the value of the customAttributeDetails property.
     * 
     * @return
     *     possible object is
     *     {@link KohlsShopperCustomAttributeDetails }
     *     
     */
    public KohlsShopperCustomAttributeDetails getCustomAttributeDetails() {
        return customAttributeDetails;
    }

    /**
     * Sets the value of the customAttributeDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link KohlsShopperCustomAttributeDetails }
     *     
     */
    public void setCustomAttributeDetails(KohlsShopperCustomAttributeDetails value) {
        this.customAttributeDetails = value;
    }

}
