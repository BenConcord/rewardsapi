
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Java class for TierDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TierDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TierRanking" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TierName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProgramId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TierEntryValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TierDetails", propOrder = {
    "tierRanking",
    "tierName",
    "programId",
    "tierEntryValue"
})
public class TierDetails {

    @XmlElement(name = "TierRanking")
    protected int tierRanking;
    @XmlElement(name = "TierName")
    protected String tierName;
    @XmlElement(name = "ProgramId")
    protected int programId;
    @XmlElement(name = "TierEntryValue", required = true)
    protected BigDecimal tierEntryValue;

    /**
     * Gets the value of the tierRanking property.
     * 
     */
    public int getTierRanking() {
        return tierRanking;
    }

    /**
     * Sets the value of the tierRanking property.
     * 
     */
    public void setTierRanking(int value) {
        this.tierRanking = value;
    }

    /**
     * Gets the value of the tierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierName() {
        return tierName;
    }

    /**
     * Sets the value of the tierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierName(String value) {
        this.tierName = value;
    }

    /**
     * Gets the value of the programId property.
     * 
     */
    public int getProgramId() {
        return programId;
    }

    /**
     * Sets the value of the programId property.
     * 
     */
    public void setProgramId(int value) {
        this.programId = value;
    }

    /**
     * Gets the value of the tierEntryValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTierEntryValue() {
        return tierEntryValue;
    }

    /**
     * Sets the value of the tierEntryValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTierEntryValue(BigDecimal value) {
        this.tierEntryValue = value;
    }

}
