
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RedeemRewardResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "redeemRewardResult"
})
@XmlRootElement(name = "RedeemRewardResponse")
public class RedeemRewardResponse {

    @XmlElement(name = "RedeemRewardResult")
    protected boolean redeemRewardResult;

    /**
     * Gets the value of the redeemRewardResult property.
     * 
     */
    public boolean isRedeemRewardResult() {
        return redeemRewardResult;
    }

    /**
     * Sets the value of the redeemRewardResult property.
     * 
     */
    public void setRedeemRewardResult(boolean value) {
        this.redeemRewardResult = value;
    }

}
