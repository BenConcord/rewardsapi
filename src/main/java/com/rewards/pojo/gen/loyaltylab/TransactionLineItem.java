
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for TransactionLineItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionLineItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DollarRevenue" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ProductId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="OrderDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LoyaltyLabOrderDetailId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="OriginalLoyaltyLabOrderDetailId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="IsEligibleForAward" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="LastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionLineItem", propOrder = {
    "dollarRevenue",
    "productId",
    "quantity",
    "orderDateTime",
    "sequenceNumber",
    "shipDateTime",
    "sku",
    "loyaltyLabOrderDetailId",
    "originalLoyaltyLabOrderDetailId",
    "isEligibleForAward",
    "createDateTime",
    "lastModifiedDateTime"
})
public class TransactionLineItem {

    @XmlElement(name = "DollarRevenue", required = true)
    protected BigDecimal dollarRevenue;
    @XmlElement(name = "ProductId")
    protected int productId;
    @XmlElement(name = "Quantity")
    protected int quantity;
    @XmlElement(name = "OrderDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar orderDateTime;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "ShipDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar shipDateTime;
    @XmlElement(name = "SKU")
    protected String sku;
    @XmlElement(name = "LoyaltyLabOrderDetailId")
    protected int loyaltyLabOrderDetailId;
    @XmlElement(name = "OriginalLoyaltyLabOrderDetailId")
    protected int originalLoyaltyLabOrderDetailId;
    @XmlElement(name = "IsEligibleForAward")
    protected boolean isEligibleForAward;
    @XmlElement(name = "CreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDateTime;
    @XmlElement(name = "LastModifiedDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;

    /**
     * Gets the value of the dollarRevenue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDollarRevenue() {
        return dollarRevenue;
    }

    /**
     * Sets the value of the dollarRevenue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDollarRevenue(BigDecimal value) {
        this.dollarRevenue = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     */
    public void setProductId(int value) {
        this.productId = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     */
    public void setQuantity(int value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the orderDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOrderDateTime() {
        return orderDateTime;
    }

    /**
     * Sets the value of the orderDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOrderDateTime(XMLGregorianCalendar value) {
        this.orderDateTime = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the shipDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getShipDateTime() {
        return shipDateTime;
    }

    /**
     * Sets the value of the shipDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setShipDateTime(XMLGregorianCalendar value) {
        this.shipDateTime = value;
    }

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSKU() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSKU(String value) {
        this.sku = value;
    }

    /**
     * Gets the value of the loyaltyLabOrderDetailId property.
     * 
     */
    public int getLoyaltyLabOrderDetailId() {
        return loyaltyLabOrderDetailId;
    }

    /**
     * Sets the value of the loyaltyLabOrderDetailId property.
     * 
     */
    public void setLoyaltyLabOrderDetailId(int value) {
        this.loyaltyLabOrderDetailId = value;
    }

    /**
     * Gets the value of the originalLoyaltyLabOrderDetailId property.
     * 
     */
    public int getOriginalLoyaltyLabOrderDetailId() {
        return originalLoyaltyLabOrderDetailId;
    }

    /**
     * Sets the value of the originalLoyaltyLabOrderDetailId property.
     * 
     */
    public void setOriginalLoyaltyLabOrderDetailId(int value) {
        this.originalLoyaltyLabOrderDetailId = value;
    }

    /**
     * Gets the value of the isEligibleForAward property.
     * 
     */
    public boolean isIsEligibleForAward() {
        return isEligibleForAward;
    }

    /**
     * Sets the value of the isEligibleForAward property.
     * 
     */
    public void setIsEligibleForAward(boolean value) {
        this.isEligibleForAward = value;
    }

    /**
     * Gets the value of the createDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the value of the createDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDateTime(XMLGregorianCalendar value) {
        this.createDateTime = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

}
