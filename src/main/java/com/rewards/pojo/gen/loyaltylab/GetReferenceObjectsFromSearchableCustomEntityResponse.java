
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetReferenceObjectsFromSearchableCustomEntityResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfAnyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getReferenceObjectsFromSearchableCustomEntityResult"
})
@XmlRootElement(name = "GetReferenceObjectsFromSearchableCustomEntityResponse")
public class GetReferenceObjectsFromSearchableCustomEntityResponse {

    @XmlElement(name = "GetReferenceObjectsFromSearchableCustomEntityResult")
    protected ArrayOfAnyType getReferenceObjectsFromSearchableCustomEntityResult;

    /**
     * Gets the value of the getReferenceObjectsFromSearchableCustomEntityResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAnyType }
     *     
     */
    public ArrayOfAnyType getGetReferenceObjectsFromSearchableCustomEntityResult() {
        return getReferenceObjectsFromSearchableCustomEntityResult;
    }

    /**
     * Sets the value of the getReferenceObjectsFromSearchableCustomEntityResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAnyType }
     *     
     */
    public void setGetReferenceObjectsFromSearchableCustomEntityResult(ArrayOfAnyType value) {
        this.getReferenceObjectsFromSearchableCustomEntityResult = value;
    }

}
