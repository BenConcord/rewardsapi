
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperAuthenticationTokenByRetailerIDResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperAuthenticationTokenByRetailerIDResult"
})
@XmlRootElement(name = "GetShopperAuthenticationTokenByRetailerIDResponse")
public class GetShopperAuthenticationTokenByRetailerIDResponse {

    @XmlElement(name = "GetShopperAuthenticationTokenByRetailerIDResult")
    protected String getShopperAuthenticationTokenByRetailerIDResult;

    /**
     * Gets the value of the getShopperAuthenticationTokenByRetailerIDResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetShopperAuthenticationTokenByRetailerIDResult() {
        return getShopperAuthenticationTokenByRetailerIDResult;
    }

    /**
     * Sets the value of the getShopperAuthenticationTokenByRetailerIDResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetShopperAuthenticationTokenByRetailerIDResult(String value) {
        this.getShopperAuthenticationTokenByRetailerIDResult = value;
    }

}
