
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperRedemptionsByShopperIdResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfRedemptionPointLifeCycleOrderItem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperRedemptionsByShopperIdResult"
})
@XmlRootElement(name = "GetShopperRedemptionsByShopperIdResponse")
public class GetShopperRedemptionsByShopperIdResponse {

    @XmlElement(name = "GetShopperRedemptionsByShopperIdResult")
    protected ArrayOfRedemptionPointLifeCycleOrderItem getShopperRedemptionsByShopperIdResult;

    /**
     * Gets the value of the getShopperRedemptionsByShopperIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRedemptionPointLifeCycleOrderItem }
     *     
     */
    public ArrayOfRedemptionPointLifeCycleOrderItem getGetShopperRedemptionsByShopperIdResult() {
        return getShopperRedemptionsByShopperIdResult;
    }

    /**
     * Sets the value of the getShopperRedemptionsByShopperIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRedemptionPointLifeCycleOrderItem }
     *     
     */
    public void setGetShopperRedemptionsByShopperIdResult(ArrayOfRedemptionPointLifeCycleOrderItem value) {
        this.getShopperRedemptionsByShopperIdResult = value;
    }

}
