
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rewardProductId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="rewardCatalogId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rewardProductId",
    "rewardCatalogId"
})
@XmlRootElement(name = "AddRewardProductToCatalog")
public class AddRewardProductToCatalog {

    protected int rewardProductId;
    protected int rewardCatalogId;

    /**
     * Gets the value of the rewardProductId property.
     * 
     */
    public int getRewardProductId() {
        return rewardProductId;
    }

    /**
     * Sets the value of the rewardProductId property.
     * 
     */
    public void setRewardProductId(int value) {
        this.rewardProductId = value;
    }

    /**
     * Gets the value of the rewardCatalogId property.
     * 
     */
    public int getRewardCatalogId() {
        return rewardCatalogId;
    }

    /**
     * Sets the value of the rewardCatalogId property.
     * 
     */
    public void setRewardCatalogId(int value) {
        this.rewardCatalogId = value;
    }

}
