
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ShopperDetailedTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShopperDetailedTransaction"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LoyaltyLabOrderHeaderId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReceiptDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RegisterId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SalesAssociateId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StoreId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="LoyaltyLabStoreId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LoyaltyLabShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="IsAnonymous" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="LoyaltyLabFileImportId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="Details" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfTransactionDetail" minOccurs="0"/&gt;
 *         &lt;element name="Tenders" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfTransactionTender" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShopperDetailedTransaction", propOrder = {
    "loyaltyLabOrderHeaderId",
    "transactionId",
    "receiptDateTime",
    "registerId",
    "salesAssociateId",
    "storeId",
    "lastModifiedDateTime",
    "loyaltyLabStoreId",
    "loyaltyLabShopperId",
    "isAnonymous",
    "loyaltyLabFileImportId",
    "createDateTime",
    "details",
    "tenders"
})
public class ShopperDetailedTransaction {

    @XmlElement(name = "LoyaltyLabOrderHeaderId")
    protected int loyaltyLabOrderHeaderId;
    @XmlElement(name = "TransactionId")
    protected String transactionId;
    @XmlElement(name = "ReceiptDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receiptDateTime;
    @XmlElement(name = "RegisterId")
    protected String registerId;
    @XmlElement(name = "SalesAssociateId")
    protected String salesAssociateId;
    @XmlElement(name = "StoreId")
    protected String storeId;
    @XmlElement(name = "LastModifiedDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;
    @XmlElement(name = "LoyaltyLabStoreId")
    protected int loyaltyLabStoreId;
    @XmlElement(name = "LoyaltyLabShopperId")
    protected int loyaltyLabShopperId;
    @XmlElement(name = "IsAnonymous")
    protected boolean isAnonymous;
    @XmlElement(name = "LoyaltyLabFileImportId")
    protected int loyaltyLabFileImportId;
    @XmlElement(name = "CreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDateTime;
    @XmlElement(name = "Details")
    protected ArrayOfTransactionDetail details;
    @XmlElement(name = "Tenders")
    protected ArrayOfTransactionTender tenders;

    /**
     * Gets the value of the loyaltyLabOrderHeaderId property.
     * 
     */
    public int getLoyaltyLabOrderHeaderId() {
        return loyaltyLabOrderHeaderId;
    }

    /**
     * Sets the value of the loyaltyLabOrderHeaderId property.
     * 
     */
    public void setLoyaltyLabOrderHeaderId(int value) {
        this.loyaltyLabOrderHeaderId = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the receiptDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceiptDateTime() {
        return receiptDateTime;
    }

    /**
     * Sets the value of the receiptDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceiptDateTime(XMLGregorianCalendar value) {
        this.receiptDateTime = value;
    }

    /**
     * Gets the value of the registerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterId() {
        return registerId;
    }

    /**
     * Sets the value of the registerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterId(String value) {
        this.registerId = value;
    }

    /**
     * Gets the value of the salesAssociateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesAssociateId() {
        return salesAssociateId;
    }

    /**
     * Sets the value of the salesAssociateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesAssociateId(String value) {
        this.salesAssociateId = value;
    }

    /**
     * Gets the value of the storeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * Sets the value of the storeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreId(String value) {
        this.storeId = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the loyaltyLabStoreId property.
     * 
     */
    public int getLoyaltyLabStoreId() {
        return loyaltyLabStoreId;
    }

    /**
     * Sets the value of the loyaltyLabStoreId property.
     * 
     */
    public void setLoyaltyLabStoreId(int value) {
        this.loyaltyLabStoreId = value;
    }

    /**
     * Gets the value of the loyaltyLabShopperId property.
     * 
     */
    public int getLoyaltyLabShopperId() {
        return loyaltyLabShopperId;
    }

    /**
     * Sets the value of the loyaltyLabShopperId property.
     * 
     */
    public void setLoyaltyLabShopperId(int value) {
        this.loyaltyLabShopperId = value;
    }

    /**
     * Gets the value of the isAnonymous property.
     * 
     */
    public boolean isIsAnonymous() {
        return isAnonymous;
    }

    /**
     * Sets the value of the isAnonymous property.
     * 
     */
    public void setIsAnonymous(boolean value) {
        this.isAnonymous = value;
    }

    /**
     * Gets the value of the loyaltyLabFileImportId property.
     * 
     */
    public int getLoyaltyLabFileImportId() {
        return loyaltyLabFileImportId;
    }

    /**
     * Sets the value of the loyaltyLabFileImportId property.
     * 
     */
    public void setLoyaltyLabFileImportId(int value) {
        this.loyaltyLabFileImportId = value;
    }

    /**
     * Gets the value of the createDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the value of the createDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDateTime(XMLGregorianCalendar value) {
        this.createDateTime = value;
    }

    /**
     * Gets the value of the details property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTransactionDetail }
     *     
     */
    public ArrayOfTransactionDetail getDetails() {
        return details;
    }

    /**
     * Sets the value of the details property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTransactionDetail }
     *     
     */
    public void setDetails(ArrayOfTransactionDetail value) {
        this.details = value;
    }

    /**
     * Gets the value of the tenders property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTransactionTender }
     *     
     */
    public ArrayOfTransactionTender getTenders() {
        return tenders;
    }

    /**
     * Sets the value of the tenders property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTransactionTender }
     *     
     */
    public void setTenders(ArrayOfTransactionTender value) {
        this.tenders = value;
    }

}
