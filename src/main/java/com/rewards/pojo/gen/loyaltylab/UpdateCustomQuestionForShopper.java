
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="answerSet" type="{http://www.loyaltylab.com/loyaltyapi/}ShopperQuestionAnswerSet" minOccurs="0"/&gt;
 *         &lt;element name="channel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "shopperId",
    "answerSet",
    "channel"
})
@XmlRootElement(name = "UpdateCustomQuestionForShopper")
public class UpdateCustomQuestionForShopper {

    protected int shopperId;
    protected ShopperQuestionAnswerSet answerSet;
    protected String channel;

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the answerSet property.
     * 
     * @return
     *     possible object is
     *     {@link ShopperQuestionAnswerSet }
     *     
     */
    public ShopperQuestionAnswerSet getAnswerSet() {
        return answerSet;
    }

    /**
     * Sets the value of the answerSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShopperQuestionAnswerSet }
     *     
     */
    public void setAnswerSet(ShopperQuestionAnswerSet value) {
        this.answerSet = value;
    }

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannel(String value) {
        this.channel = value;
    }

}
