
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetRewardCatalogByIdResult" type="{http://www.loyaltylab.com/loyaltyapi/}RewardCatalog" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRewardCatalogByIdResult"
})
@XmlRootElement(name = "GetRewardCatalogByIdResponse")
public class GetRewardCatalogByIdResponse {

    @XmlElement(name = "GetRewardCatalogByIdResult")
    protected RewardCatalog getRewardCatalogByIdResult;

    /**
     * Gets the value of the getRewardCatalogByIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link RewardCatalog }
     *     
     */
    public RewardCatalog getGetRewardCatalogByIdResult() {
        return getRewardCatalogByIdResult;
    }

    /**
     * Sets the value of the getRewardCatalogByIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardCatalog }
     *     
     */
    public void setGetRewardCatalogByIdResult(RewardCatalog value) {
        this.getRewardCatalogByIdResult = value;
    }

}
