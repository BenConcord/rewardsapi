
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentifierTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IdentifierTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ShopperId"/&gt;
 *     &lt;enumeration value="EmailAddress"/&gt;
 *     &lt;enumeration value="LoyaltyCard"/&gt;
 *     &lt;enumeration value="PhoneNumber"/&gt;
 *     &lt;enumeration value="RetailerShopperId"/&gt;
 *     &lt;enumeration value="UserName"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "IdentifierTypes")
@XmlEnum
public enum IdentifierTypes {

    @XmlEnumValue("ShopperId")
    SHOPPER_ID("ShopperId"),
    @XmlEnumValue("EmailAddress")
    EMAIL_ADDRESS("EmailAddress"),
    @XmlEnumValue("LoyaltyCard")
    LOYALTY_CARD("LoyaltyCard"),
    @XmlEnumValue("PhoneNumber")
    PHONE_NUMBER("PhoneNumber"),
    @XmlEnumValue("RetailerShopperId")
    RETAILER_SHOPPER_ID("RetailerShopperId"),
    @XmlEnumValue("UserName")
    USER_NAME("UserName");
    private final String value;

    IdentifierTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IdentifierTypes fromValue(String v) {
        for (IdentifierTypes c: IdentifierTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
