
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetCodesForShopperResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfShopperRewardItemRedemption" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCodesForShopperResult"
})
@XmlRootElement(name = "GetCodesForShopperResponse")
public class GetCodesForShopperResponse {

    @XmlElement(name = "GetCodesForShopperResult")
    protected ArrayOfShopperRewardItemRedemption getCodesForShopperResult;

    /**
     * Gets the value of the getCodesForShopperResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShopperRewardItemRedemption }
     *     
     */
    public ArrayOfShopperRewardItemRedemption getGetCodesForShopperResult() {
        return getCodesForShopperResult;
    }

    /**
     * Sets the value of the getCodesForShopperResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShopperRewardItemRedemption }
     *     
     */
    public void setGetCodesForShopperResult(ArrayOfShopperRewardItemRedemption value) {
        this.getCodesForShopperResult = value;
    }

}
