
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfBulkCustomAttributeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfBulkCustomAttributeInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BulkCustomAttributeInfo" type="{http://www.loyaltylab.com/loyaltyapi/}BulkCustomAttributeInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfBulkCustomAttributeInfo", propOrder = {
    "bulkCustomAttributeInfo"
})
public class ArrayOfBulkCustomAttributeInfo {

    @XmlElement(name = "BulkCustomAttributeInfo", nillable = true)
    protected List<BulkCustomAttributeInfo> bulkCustomAttributeInfo;

    /**
     * Gets the value of the bulkCustomAttributeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bulkCustomAttributeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBulkCustomAttributeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BulkCustomAttributeInfo }
     * 
     * 
     */
    public List<BulkCustomAttributeInfo> getBulkCustomAttributeInfo() {
        if (bulkCustomAttributeInfo == null) {
            bulkCustomAttributeInfo = new ArrayList<BulkCustomAttributeInfo>();
        }
        return this.bulkCustomAttributeInfo;
    }

}
