
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PointPurchaseResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PointPurchaseResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Success" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="PurchaseId" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="PointsChanged" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointsRemaining" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PointPurchaseResult", propOrder = {
    "success",
    "purchaseId",
    "pointsChanged",
    "pointsRemaining"
})
public class PointPurchaseResult {

    @XmlElement(name = "Success")
    protected boolean success;
    @XmlElement(name = "PurchaseId", required = true)
    protected String purchaseId;
    @XmlElement(name = "PointsChanged")
    protected int pointsChanged;
    @XmlElement(name = "PointsRemaining")
    protected int pointsRemaining;

    /**
     * Gets the value of the success property.
     * 
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     * 
     */
    public void setSuccess(boolean value) {
        this.success = value;
    }

    /**
     * Gets the value of the purchaseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseId() {
        return purchaseId;
    }

    /**
     * Sets the value of the purchaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseId(String value) {
        this.purchaseId = value;
    }

    /**
     * Gets the value of the pointsChanged property.
     * 
     */
    public int getPointsChanged() {
        return pointsChanged;
    }

    /**
     * Sets the value of the pointsChanged property.
     * 
     */
    public void setPointsChanged(int value) {
        this.pointsChanged = value;
    }

    /**
     * Gets the value of the pointsRemaining property.
     * 
     */
    public int getPointsRemaining() {
        return pointsRemaining;
    }

    /**
     * Sets the value of the pointsRemaining property.
     * 
     */
    public void setPointsRemaining(int value) {
        this.pointsRemaining = value;
    }

}
