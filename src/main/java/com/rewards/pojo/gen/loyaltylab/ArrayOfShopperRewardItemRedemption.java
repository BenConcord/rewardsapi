
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfShopperRewardItemRedemption complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfShopperRewardItemRedemption"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShopperRewardItemRedemption" type="{http://www.loyaltylab.com/loyaltyapi/}ShopperRewardItemRedemption" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfShopperRewardItemRedemption", propOrder = {
    "shopperRewardItemRedemption"
})
public class ArrayOfShopperRewardItemRedemption {

    @XmlElement(name = "ShopperRewardItemRedemption", nillable = true)
    protected List<ShopperRewardItemRedemption> shopperRewardItemRedemption;

    /**
     * Gets the value of the shopperRewardItemRedemption property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shopperRewardItemRedemption property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShopperRewardItemRedemption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShopperRewardItemRedemption }
     * 
     * 
     */
    public List<ShopperRewardItemRedemption> getShopperRewardItemRedemption() {
        if (shopperRewardItemRedemption == null) {
            shopperRewardItemRedemption = new ArrayList<ShopperRewardItemRedemption>();
        }
        return this.shopperRewardItemRedemption;
    }

}
