
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fromLoyaltyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="toCharityOrganizationId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="pointsToGift" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="donationText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fromLoyaltyId",
    "toCharityOrganizationId",
    "pointsToGift",
    "donationText"
})
@XmlRootElement(name = "DonateToCharity")
public class DonateToCharity {

    protected String fromLoyaltyId;
    protected int toCharityOrganizationId;
    protected int pointsToGift;
    protected String donationText;

    /**
     * Gets the value of the fromLoyaltyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromLoyaltyId() {
        return fromLoyaltyId;
    }

    /**
     * Sets the value of the fromLoyaltyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromLoyaltyId(String value) {
        this.fromLoyaltyId = value;
    }

    /**
     * Gets the value of the toCharityOrganizationId property.
     * 
     */
    public int getToCharityOrganizationId() {
        return toCharityOrganizationId;
    }

    /**
     * Sets the value of the toCharityOrganizationId property.
     * 
     */
    public void setToCharityOrganizationId(int value) {
        this.toCharityOrganizationId = value;
    }

    /**
     * Gets the value of the pointsToGift property.
     * 
     */
    public int getPointsToGift() {
        return pointsToGift;
    }

    /**
     * Sets the value of the pointsToGift property.
     * 
     */
    public void setPointsToGift(int value) {
        this.pointsToGift = value;
    }

    /**
     * Gets the value of the donationText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDonationText() {
        return donationText;
    }

    /**
     * Sets the value of the donationText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDonationText(String value) {
        this.donationText = value;
    }

}
