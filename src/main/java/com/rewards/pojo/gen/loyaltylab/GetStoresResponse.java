
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetStoresResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfStore" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getStoresResult"
})
@XmlRootElement(name = "GetStoresResponse")
public class GetStoresResponse {

    @XmlElement(name = "GetStoresResult")
    protected ArrayOfStore getStoresResult;

    /**
     * Gets the value of the getStoresResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfStore }
     *     
     */
    public ArrayOfStore getGetStoresResult() {
        return getStoresResult;
    }

    /**
     * Sets the value of the getStoresResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfStore }
     *     
     */
    public void setGetStoresResult(ArrayOfStore value) {
        this.getStoresResult = value;
    }

}
