
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EventAwards complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EventAwards"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EventInstance" type="{http://www.loyaltylab.com/loyaltyapi/}EventInstance" minOccurs="0"/&gt;
 *         &lt;element name="EventAwardDetails" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfEventAwardDetail" minOccurs="0"/&gt;
 *         &lt;element name="TotalPointsEarned" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventAwards", propOrder = {
    "eventInstance",
    "eventAwardDetails",
    "totalPointsEarned"
})
public class EventAwards {

    @XmlElement(name = "EventInstance")
    protected EventInstance eventInstance;
    @XmlElement(name = "EventAwardDetails")
    protected ArrayOfEventAwardDetail eventAwardDetails;
    @XmlElement(name = "TotalPointsEarned")
    protected int totalPointsEarned;

    /**
     * Gets the value of the eventInstance property.
     * 
     * @return
     *     possible object is
     *     {@link EventInstance }
     *     
     */
    public EventInstance getEventInstance() {
        return eventInstance;
    }

    /**
     * Sets the value of the eventInstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventInstance }
     *     
     */
    public void setEventInstance(EventInstance value) {
        this.eventInstance = value;
    }

    /**
     * Gets the value of the eventAwardDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEventAwardDetail }
     *     
     */
    public ArrayOfEventAwardDetail getEventAwardDetails() {
        return eventAwardDetails;
    }

    /**
     * Sets the value of the eventAwardDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEventAwardDetail }
     *     
     */
    public void setEventAwardDetails(ArrayOfEventAwardDetail value) {
        this.eventAwardDetails = value;
    }

    /**
     * Gets the value of the totalPointsEarned property.
     * 
     */
    public int getTotalPointsEarned() {
        return totalPointsEarned;
    }

    /**
     * Sets the value of the totalPointsEarned property.
     * 
     */
    public void setTotalPointsEarned(int value) {
        this.totalPointsEarned = value;
    }

}
