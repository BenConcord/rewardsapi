
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperPointBalanceByPointGroupResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperPointBalanceByPointGroupResult"
})
@XmlRootElement(name = "GetShopperPointBalanceByPointGroupResponse")
public class GetShopperPointBalanceByPointGroupResponse {

    @XmlElement(name = "GetShopperPointBalanceByPointGroupResult")
    protected int getShopperPointBalanceByPointGroupResult;

    /**
     * Gets the value of the getShopperPointBalanceByPointGroupResult property.
     * 
     */
    public int getGetShopperPointBalanceByPointGroupResult() {
        return getShopperPointBalanceByPointGroupResult;
    }

    /**
     * Sets the value of the getShopperPointBalanceByPointGroupResult property.
     * 
     */
    public void setGetShopperPointBalanceByPointGroupResult(int value) {
        this.getShopperPointBalanceByPointGroupResult = value;
    }

}
