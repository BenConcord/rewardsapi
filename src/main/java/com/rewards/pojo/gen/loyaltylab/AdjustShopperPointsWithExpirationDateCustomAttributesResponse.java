
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AdjustShopperPointsWithExpirationDateCustomAttributesResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "adjustShopperPointsWithExpirationDateCustomAttributesResult"
})
@XmlRootElement(name = "AdjustShopperPointsWithExpirationDateCustomAttributesResponse")
public class AdjustShopperPointsWithExpirationDateCustomAttributesResponse {

    @XmlElement(name = "AdjustShopperPointsWithExpirationDateCustomAttributesResult")
    protected int adjustShopperPointsWithExpirationDateCustomAttributesResult;

    /**
     * Gets the value of the adjustShopperPointsWithExpirationDateCustomAttributesResult property.
     * 
     */
    public int getAdjustShopperPointsWithExpirationDateCustomAttributesResult() {
        return adjustShopperPointsWithExpirationDateCustomAttributesResult;
    }

    /**
     * Sets the value of the adjustShopperPointsWithExpirationDateCustomAttributesResult property.
     * 
     */
    public void setAdjustShopperPointsWithExpirationDateCustomAttributesResult(int value) {
        this.adjustShopperPointsWithExpirationDateCustomAttributesResult = value;
    }

}
