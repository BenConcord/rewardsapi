
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GiftPointsResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "giftPointsResult"
})
@XmlRootElement(name = "GiftPointsResponse")
public class GiftPointsResponse {

    @XmlElement(name = "GiftPointsResult")
    protected int giftPointsResult;

    /**
     * Gets the value of the giftPointsResult property.
     * 
     */
    public int getGiftPointsResult() {
        return giftPointsResult;
    }

    /**
     * Sets the value of the giftPointsResult property.
     * 
     */
    public void setGiftPointsResult(int value) {
        this.giftPointsResult = value;
    }

}
