
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateRewardProductResult" type="{http://www.loyaltylab.com/loyaltyapi/}RewardProduct" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createRewardProductResult"
})
@XmlRootElement(name = "CreateRewardProductResponse")
public class CreateRewardProductResponse {

    @XmlElement(name = "CreateRewardProductResult")
    protected RewardProduct createRewardProductResult;

    /**
     * Gets the value of the createRewardProductResult property.
     * 
     * @return
     *     possible object is
     *     {@link RewardProduct }
     *     
     */
    public RewardProduct getCreateRewardProductResult() {
        return createRewardProductResult;
    }

    /**
     * Sets the value of the createRewardProductResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardProduct }
     *     
     */
    public void setCreateRewardProductResult(RewardProduct value) {
        this.createRewardProductResult = value;
    }

}
