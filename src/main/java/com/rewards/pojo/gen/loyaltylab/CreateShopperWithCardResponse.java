
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateShopperWithCardResult" type="{http://www.loyaltylab.com/loyaltyapi/}Shopper" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createShopperWithCardResult"
})
@XmlRootElement(name = "CreateShopperWithCardResponse")
public class CreateShopperWithCardResponse {

    @XmlElement(name = "CreateShopperWithCardResult")
    protected Shopper createShopperWithCardResult;

    /**
     * Gets the value of the createShopperWithCardResult property.
     * 
     * @return
     *     possible object is
     *     {@link Shopper }
     *     
     */
    public Shopper getCreateShopperWithCardResult() {
        return createShopperWithCardResult;
    }

    /**
     * Sets the value of the createShopperWithCardResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Shopper }
     *     
     */
    public void setCreateShopperWithCardResult(Shopper value) {
        this.createShopperWithCardResult = value;
    }

}
