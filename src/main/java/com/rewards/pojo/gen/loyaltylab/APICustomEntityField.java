
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for APICustomEntityField complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="APICustomEntityField"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomEntityFieldName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomEntityFieldValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "APICustomEntityField", propOrder = {
    "customEntityFieldName",
    "customEntityFieldValue"
})
public class APICustomEntityField {

    @XmlElement(name = "CustomEntityFieldName")
    protected String customEntityFieldName;
    @XmlElement(name = "CustomEntityFieldValue", required = true, nillable = true)
    protected String customEntityFieldValue;

    /**
     * Gets the value of the customEntityFieldName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomEntityFieldName() {
        return customEntityFieldName;
    }

    /**
     * Sets the value of the customEntityFieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomEntityFieldName(String value) {
        this.customEntityFieldName = value;
    }

    /**
     * Gets the value of the customEntityFieldValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomEntityFieldValue() {
        return customEntityFieldValue;
    }

    /**
     * Sets the value of the customEntityFieldValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomEntityFieldValue(String value) {
        this.customEntityFieldValue = value;
    }

}
