
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for OrderItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OrderItemId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RegisteredCardId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RetailerOrderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailerGUID" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="StoreId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RetailerOrderDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="DollarRevenue" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ProductId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAnonymous" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="RetailerStoreId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailerRegisterId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FileImportId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="OriginalOrderItemId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Latest" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="RetailerSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailerShipDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RetailerReceiptDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RetailerSalesAssociateId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsEligibleForAward" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderItem", propOrder = {
    "orderItemId",
    "shopperId",
    "registeredCardId",
    "retailerOrderId",
    "retailerGUID",
    "storeId",
    "retailerOrderDateTime",
    "quantity",
    "dollarRevenue",
    "productId",
    "sku",
    "isAnonymous",
    "retailerStoreId",
    "retailerRegisterId",
    "fileImportId",
    "originalOrderItemId",
    "latest",
    "retailerSequenceNumber",
    "retailerShipDateTime",
    "retailerReceiptDateTime",
    "retailerSalesAssociateId",
    "isEligibleForAward"
})
public class OrderItem {

    @XmlElement(name = "OrderItemId")
    protected int orderItemId;
    @XmlElement(name = "ShopperId")
    protected int shopperId;
    @XmlElement(name = "RegisteredCardId")
    protected int registeredCardId;
    @XmlElement(name = "RetailerOrderId")
    protected String retailerOrderId;
    @XmlElement(name = "RetailerGUID", required = true)
    protected String retailerGUID;
    @XmlElement(name = "StoreId")
    protected int storeId;
    @XmlElement(name = "RetailerOrderDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar retailerOrderDateTime;
    @XmlElement(name = "Quantity")
    protected int quantity;
    @XmlElement(name = "DollarRevenue", required = true)
    protected BigDecimal dollarRevenue;
    @XmlElement(name = "ProductId")
    protected int productId;
    @XmlElement(name = "SKU")
    protected String sku;
    @XmlElement(name = "IsAnonymous")
    protected boolean isAnonymous;
    @XmlElement(name = "RetailerStoreId")
    protected String retailerStoreId;
    @XmlElement(name = "RetailerRegisterId")
    protected String retailerRegisterId;
    @XmlElement(name = "FileImportId")
    protected int fileImportId;
    @XmlElement(name = "OriginalOrderItemId")
    protected int originalOrderItemId;
    @XmlElement(name = "Latest")
    protected boolean latest;
    @XmlElement(name = "RetailerSequenceNumber")
    protected String retailerSequenceNumber;
    @XmlElement(name = "RetailerShipDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar retailerShipDateTime;
    @XmlElement(name = "RetailerReceiptDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar retailerReceiptDateTime;
    @XmlElement(name = "RetailerSalesAssociateId")
    protected String retailerSalesAssociateId;
    @XmlElement(name = "IsEligibleForAward")
    protected boolean isEligibleForAward;

    /**
     * Gets the value of the orderItemId property.
     * 
     */
    public int getOrderItemId() {
        return orderItemId;
    }

    /**
     * Sets the value of the orderItemId property.
     * 
     */
    public void setOrderItemId(int value) {
        this.orderItemId = value;
    }

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the registeredCardId property.
     * 
     */
    public int getRegisteredCardId() {
        return registeredCardId;
    }

    /**
     * Sets the value of the registeredCardId property.
     * 
     */
    public void setRegisteredCardId(int value) {
        this.registeredCardId = value;
    }

    /**
     * Gets the value of the retailerOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerOrderId() {
        return retailerOrderId;
    }

    /**
     * Sets the value of the retailerOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerOrderId(String value) {
        this.retailerOrderId = value;
    }

    /**
     * Gets the value of the retailerGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerGUID() {
        return retailerGUID;
    }

    /**
     * Sets the value of the retailerGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerGUID(String value) {
        this.retailerGUID = value;
    }

    /**
     * Gets the value of the storeId property.
     * 
     */
    public int getStoreId() {
        return storeId;
    }

    /**
     * Sets the value of the storeId property.
     * 
     */
    public void setStoreId(int value) {
        this.storeId = value;
    }

    /**
     * Gets the value of the retailerOrderDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRetailerOrderDateTime() {
        return retailerOrderDateTime;
    }

    /**
     * Sets the value of the retailerOrderDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRetailerOrderDateTime(XMLGregorianCalendar value) {
        this.retailerOrderDateTime = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     */
    public void setQuantity(int value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the dollarRevenue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDollarRevenue() {
        return dollarRevenue;
    }

    /**
     * Sets the value of the dollarRevenue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDollarRevenue(BigDecimal value) {
        this.dollarRevenue = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     */
    public void setProductId(int value) {
        this.productId = value;
    }

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSKU() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSKU(String value) {
        this.sku = value;
    }

    /**
     * Gets the value of the isAnonymous property.
     * 
     */
    public boolean isIsAnonymous() {
        return isAnonymous;
    }

    /**
     * Sets the value of the isAnonymous property.
     * 
     */
    public void setIsAnonymous(boolean value) {
        this.isAnonymous = value;
    }

    /**
     * Gets the value of the retailerStoreId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerStoreId() {
        return retailerStoreId;
    }

    /**
     * Sets the value of the retailerStoreId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerStoreId(String value) {
        this.retailerStoreId = value;
    }

    /**
     * Gets the value of the retailerRegisterId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerRegisterId() {
        return retailerRegisterId;
    }

    /**
     * Sets the value of the retailerRegisterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerRegisterId(String value) {
        this.retailerRegisterId = value;
    }

    /**
     * Gets the value of the fileImportId property.
     * 
     */
    public int getFileImportId() {
        return fileImportId;
    }

    /**
     * Sets the value of the fileImportId property.
     * 
     */
    public void setFileImportId(int value) {
        this.fileImportId = value;
    }

    /**
     * Gets the value of the originalOrderItemId property.
     * 
     */
    public int getOriginalOrderItemId() {
        return originalOrderItemId;
    }

    /**
     * Sets the value of the originalOrderItemId property.
     * 
     */
    public void setOriginalOrderItemId(int value) {
        this.originalOrderItemId = value;
    }

    /**
     * Gets the value of the latest property.
     * 
     */
    public boolean isLatest() {
        return latest;
    }

    /**
     * Sets the value of the latest property.
     * 
     */
    public void setLatest(boolean value) {
        this.latest = value;
    }

    /**
     * Gets the value of the retailerSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerSequenceNumber() {
        return retailerSequenceNumber;
    }

    /**
     * Sets the value of the retailerSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerSequenceNumber(String value) {
        this.retailerSequenceNumber = value;
    }

    /**
     * Gets the value of the retailerShipDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRetailerShipDateTime() {
        return retailerShipDateTime;
    }

    /**
     * Sets the value of the retailerShipDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRetailerShipDateTime(XMLGregorianCalendar value) {
        this.retailerShipDateTime = value;
    }

    /**
     * Gets the value of the retailerReceiptDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRetailerReceiptDateTime() {
        return retailerReceiptDateTime;
    }

    /**
     * Sets the value of the retailerReceiptDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRetailerReceiptDateTime(XMLGregorianCalendar value) {
        this.retailerReceiptDateTime = value;
    }

    /**
     * Gets the value of the retailerSalesAssociateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerSalesAssociateId() {
        return retailerSalesAssociateId;
    }

    /**
     * Sets the value of the retailerSalesAssociateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerSalesAssociateId(String value) {
        this.retailerSalesAssociateId = value;
    }

    /**
     * Gets the value of the isEligibleForAward property.
     * 
     */
    public boolean isIsEligibleForAward() {
        return isEligibleForAward;
    }

    /**
     * Sets the value of the isEligibleForAward property.
     * 
     */
    public void setIsEligibleForAward(boolean value) {
        this.isEligibleForAward = value;
    }

}
