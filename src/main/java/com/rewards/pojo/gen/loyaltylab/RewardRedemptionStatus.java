
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RewardRedemptionStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RewardRedemptionStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="UnknownError"/&gt;
 *     &lt;enumeration value="Success"/&gt;
 *     &lt;enumeration value="RewardNumberDoesNotMatchShopper"/&gt;
 *     &lt;enumeration value="InvalidRewardNumber"/&gt;
 *     &lt;enumeration value="ExpiredRewardNumber"/&gt;
 *     &lt;enumeration value="RewardAlreadyRedeemed"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "RewardRedemptionStatus")
@XmlEnum
public enum RewardRedemptionStatus {

    @XmlEnumValue("UnknownError")
    UNKNOWN_ERROR("UnknownError"),
    @XmlEnumValue("Success")
    SUCCESS("Success"),
    @XmlEnumValue("RewardNumberDoesNotMatchShopper")
    REWARD_NUMBER_DOES_NOT_MATCH_SHOPPER("RewardNumberDoesNotMatchShopper"),
    @XmlEnumValue("InvalidRewardNumber")
    INVALID_REWARD_NUMBER("InvalidRewardNumber"),
    @XmlEnumValue("ExpiredRewardNumber")
    EXPIRED_REWARD_NUMBER("ExpiredRewardNumber"),
    @XmlEnumValue("RewardAlreadyRedeemed")
    REWARD_ALREADY_REDEEMED("RewardAlreadyRedeemed");
    private final String value;

    RewardRedemptionStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RewardRedemptionStatus fromValue(String v) {
        for (RewardRedemptionStatus c: RewardRedemptionStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
