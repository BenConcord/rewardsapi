
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShopperAnswer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShopperAnswer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.loyaltylab.com/loyaltyapi/}QuestionnaireAnswer"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShopperSelected" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShopperAnswer", propOrder = {
    "shopperSelected"
})
public class ShopperAnswer
    extends QuestionnaireAnswer
{

    @XmlElement(name = "ShopperSelected")
    protected boolean shopperSelected;

    /**
     * Gets the value of the shopperSelected property.
     * 
     */
    public boolean isShopperSelected() {
        return shopperSelected;
    }

    /**
     * Sets the value of the shopperSelected property.
     * 
     */
    public void setShopperSelected(boolean value) {
        this.shopperSelected = value;
    }

}
