
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetCustomEntitiesByReferenceTagResult" type="{http://www.loyaltylab.com/loyaltyapi/}CustomEntitiesWrapper" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomEntitiesByReferenceTagResult"
})
@XmlRootElement(name = "GetCustomEntitiesByReferenceTagResponse")
public class GetCustomEntitiesByReferenceTagResponse {

    @XmlElement(name = "GetCustomEntitiesByReferenceTagResult")
    protected CustomEntitiesWrapper getCustomEntitiesByReferenceTagResult;

    /**
     * Gets the value of the getCustomEntitiesByReferenceTagResult property.
     * 
     * @return
     *     possible object is
     *     {@link CustomEntitiesWrapper }
     *     
     */
    public CustomEntitiesWrapper getGetCustomEntitiesByReferenceTagResult() {
        return getCustomEntitiesByReferenceTagResult;
    }

    /**
     * Sets the value of the getCustomEntitiesByReferenceTagResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomEntitiesWrapper }
     *     
     */
    public void setGetCustomEntitiesByReferenceTagResult(CustomEntitiesWrapper value) {
        this.getCustomEntitiesByReferenceTagResult = value;
    }

}
