
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AddShopperToTierResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "addShopperToTierResult"
})
@XmlRootElement(name = "AddShopperToTierResponse")
public class AddShopperToTierResponse {

    @XmlElement(name = "AddShopperToTierResult")
    protected boolean addShopperToTierResult;

    /**
     * Gets the value of the addShopperToTierResult property.
     * 
     */
    public boolean isAddShopperToTierResult() {
        return addShopperToTierResult;
    }

    /**
     * Sets the value of the addShopperToTierResult property.
     * 
     */
    public void setAddShopperToTierResult(boolean value) {
        this.addShopperToTierResult = value;
    }

}
