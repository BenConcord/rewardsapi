
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OfferExtended complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferExtended"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OfferId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RetailerGUID" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="OfferName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RecordType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QualifyingTypeId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="AwardTypeId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EndDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="QualifyStartDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="QualifyEndDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ActualStartDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ActualEndDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ClipLimit" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Headline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OfferText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Terms" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImageUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MediaUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BuyUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InfoUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAutoClip" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ShopperRedemptionLimit" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TargetRedemptionLimit" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EmailNotificationThreshold" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="FlashTemplateStyleId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UpdateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CreatedByUserId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LastModifiedByUserId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="IsAlwaysAwarded" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="UseFixedEligibilityList" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="RelativeQualificationPeriod" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PostCompletionScoringDays" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PostCompletionAwardingDays" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="VisibleInShowcase" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferExtended", propOrder = {
    "offerId",
    "retailerGUID",
    "offerName",
    "recordType",
    "qualifyingTypeId",
    "awardTypeId",
    "status",
    "startDateTime",
    "endDateTime",
    "qualifyStartDateTime",
    "qualifyEndDateTime",
    "actualStartDateTime",
    "actualEndDateTime",
    "clipLimit",
    "headline",
    "offerText",
    "terms",
    "imageUrl",
    "mediaUrl",
    "buyUrl",
    "infoUrl",
    "isAutoClip",
    "shopperRedemptionLimit",
    "targetRedemptionLimit",
    "emailNotificationThreshold",
    "flashTemplateStyleId",
    "updateDateTime",
    "createdByUserId",
    "lastModifiedByUserId",
    "isAlwaysAwarded",
    "useFixedEligibilityList",
    "relativeQualificationPeriod",
    "postCompletionScoringDays",
    "postCompletionAwardingDays",
    "visibleInShowcase"
})
public class OfferExtended {

    @XmlElement(name = "OfferId")
    protected int offerId;
    @XmlElement(name = "RetailerGUID", required = true)
    protected String retailerGUID;
    @XmlElement(name = "OfferName")
    protected String offerName;
    @XmlElement(name = "RecordType")
    protected String recordType;
    @XmlElement(name = "QualifyingTypeId")
    protected int qualifyingTypeId;
    @XmlElement(name = "AwardTypeId")
    protected int awardTypeId;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "StartDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDateTime;
    @XmlElement(name = "EndDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDateTime;
    @XmlElement(name = "QualifyStartDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar qualifyStartDateTime;
    @XmlElement(name = "QualifyEndDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar qualifyEndDateTime;
    @XmlElement(name = "ActualStartDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actualStartDateTime;
    @XmlElement(name = "ActualEndDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actualEndDateTime;
    @XmlElement(name = "ClipLimit")
    protected int clipLimit;
    @XmlElement(name = "Headline")
    protected String headline;
    @XmlElement(name = "OfferText")
    protected String offerText;
    @XmlElement(name = "Terms")
    protected String terms;
    @XmlElement(name = "ImageUrl")
    protected String imageUrl;
    @XmlElement(name = "MediaUrl")
    protected String mediaUrl;
    @XmlElement(name = "BuyUrl")
    protected String buyUrl;
    @XmlElement(name = "InfoUrl")
    protected String infoUrl;
    @XmlElement(name = "IsAutoClip")
    protected int isAutoClip;
    @XmlElement(name = "ShopperRedemptionLimit")
    protected int shopperRedemptionLimit;
    @XmlElement(name = "TargetRedemptionLimit")
    protected int targetRedemptionLimit;
    @XmlElement(name = "EmailNotificationThreshold")
    protected int emailNotificationThreshold;
    @XmlElement(name = "FlashTemplateStyleId")
    protected int flashTemplateStyleId;
    @XmlElement(name = "UpdateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateDateTime;
    @XmlElement(name = "CreatedByUserId")
    protected int createdByUserId;
    @XmlElement(name = "LastModifiedByUserId")
    protected int lastModifiedByUserId;
    @XmlElement(name = "IsAlwaysAwarded")
    protected boolean isAlwaysAwarded;
    @XmlElement(name = "UseFixedEligibilityList")
    protected boolean useFixedEligibilityList;
    @XmlElement(name = "RelativeQualificationPeriod")
    protected int relativeQualificationPeriod;
    @XmlElement(name = "PostCompletionScoringDays")
    protected int postCompletionScoringDays;
    @XmlElement(name = "PostCompletionAwardingDays")
    protected int postCompletionAwardingDays;
    @XmlElement(name = "VisibleInShowcase")
    protected boolean visibleInShowcase;

    /**
     * Gets the value of the offerId property.
     * 
     */
    public int getOfferId() {
        return offerId;
    }

    /**
     * Sets the value of the offerId property.
     * 
     */
    public void setOfferId(int value) {
        this.offerId = value;
    }

    /**
     * Gets the value of the retailerGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerGUID() {
        return retailerGUID;
    }

    /**
     * Sets the value of the retailerGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerGUID(String value) {
        this.retailerGUID = value;
    }

    /**
     * Gets the value of the offerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferName() {
        return offerName;
    }

    /**
     * Sets the value of the offerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferName(String value) {
        this.offerName = value;
    }

    /**
     * Gets the value of the recordType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * Sets the value of the recordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordType(String value) {
        this.recordType = value;
    }

    /**
     * Gets the value of the qualifyingTypeId property.
     * 
     */
    public int getQualifyingTypeId() {
        return qualifyingTypeId;
    }

    /**
     * Sets the value of the qualifyingTypeId property.
     * 
     */
    public void setQualifyingTypeId(int value) {
        this.qualifyingTypeId = value;
    }

    /**
     * Gets the value of the awardTypeId property.
     * 
     */
    public int getAwardTypeId() {
        return awardTypeId;
    }

    /**
     * Sets the value of the awardTypeId property.
     * 
     */
    public void setAwardTypeId(int value) {
        this.awardTypeId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the startDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDateTime() {
        return startDateTime;
    }

    /**
     * Sets the value of the startDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDateTime(XMLGregorianCalendar value) {
        this.startDateTime = value;
    }

    /**
     * Gets the value of the endDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDateTime() {
        return endDateTime;
    }

    /**
     * Sets the value of the endDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDateTime(XMLGregorianCalendar value) {
        this.endDateTime = value;
    }

    /**
     * Gets the value of the qualifyStartDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQualifyStartDateTime() {
        return qualifyStartDateTime;
    }

    /**
     * Sets the value of the qualifyStartDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQualifyStartDateTime(XMLGregorianCalendar value) {
        this.qualifyStartDateTime = value;
    }

    /**
     * Gets the value of the qualifyEndDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQualifyEndDateTime() {
        return qualifyEndDateTime;
    }

    /**
     * Sets the value of the qualifyEndDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQualifyEndDateTime(XMLGregorianCalendar value) {
        this.qualifyEndDateTime = value;
    }

    /**
     * Gets the value of the actualStartDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualStartDateTime() {
        return actualStartDateTime;
    }

    /**
     * Sets the value of the actualStartDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualStartDateTime(XMLGregorianCalendar value) {
        this.actualStartDateTime = value;
    }

    /**
     * Gets the value of the actualEndDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualEndDateTime() {
        return actualEndDateTime;
    }

    /**
     * Sets the value of the actualEndDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualEndDateTime(XMLGregorianCalendar value) {
        this.actualEndDateTime = value;
    }

    /**
     * Gets the value of the clipLimit property.
     * 
     */
    public int getClipLimit() {
        return clipLimit;
    }

    /**
     * Sets the value of the clipLimit property.
     * 
     */
    public void setClipLimit(int value) {
        this.clipLimit = value;
    }

    /**
     * Gets the value of the headline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeadline() {
        return headline;
    }

    /**
     * Sets the value of the headline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeadline(String value) {
        this.headline = value;
    }

    /**
     * Gets the value of the offerText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferText() {
        return offerText;
    }

    /**
     * Sets the value of the offerText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferText(String value) {
        this.offerText = value;
    }

    /**
     * Gets the value of the terms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerms() {
        return terms;
    }

    /**
     * Sets the value of the terms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerms(String value) {
        this.terms = value;
    }

    /**
     * Gets the value of the imageUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets the value of the imageUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageUrl(String value) {
        this.imageUrl = value;
    }

    /**
     * Gets the value of the mediaUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediaUrl() {
        return mediaUrl;
    }

    /**
     * Sets the value of the mediaUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediaUrl(String value) {
        this.mediaUrl = value;
    }

    /**
     * Gets the value of the buyUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyUrl() {
        return buyUrl;
    }

    /**
     * Sets the value of the buyUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyUrl(String value) {
        this.buyUrl = value;
    }

    /**
     * Gets the value of the infoUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoUrl() {
        return infoUrl;
    }

    /**
     * Sets the value of the infoUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoUrl(String value) {
        this.infoUrl = value;
    }

    /**
     * Gets the value of the isAutoClip property.
     * 
     */
    public int getIsAutoClip() {
        return isAutoClip;
    }

    /**
     * Sets the value of the isAutoClip property.
     * 
     */
    public void setIsAutoClip(int value) {
        this.isAutoClip = value;
    }

    /**
     * Gets the value of the shopperRedemptionLimit property.
     * 
     */
    public int getShopperRedemptionLimit() {
        return shopperRedemptionLimit;
    }

    /**
     * Sets the value of the shopperRedemptionLimit property.
     * 
     */
    public void setShopperRedemptionLimit(int value) {
        this.shopperRedemptionLimit = value;
    }

    /**
     * Gets the value of the targetRedemptionLimit property.
     * 
     */
    public int getTargetRedemptionLimit() {
        return targetRedemptionLimit;
    }

    /**
     * Sets the value of the targetRedemptionLimit property.
     * 
     */
    public void setTargetRedemptionLimit(int value) {
        this.targetRedemptionLimit = value;
    }

    /**
     * Gets the value of the emailNotificationThreshold property.
     * 
     */
    public int getEmailNotificationThreshold() {
        return emailNotificationThreshold;
    }

    /**
     * Sets the value of the emailNotificationThreshold property.
     * 
     */
    public void setEmailNotificationThreshold(int value) {
        this.emailNotificationThreshold = value;
    }

    /**
     * Gets the value of the flashTemplateStyleId property.
     * 
     */
    public int getFlashTemplateStyleId() {
        return flashTemplateStyleId;
    }

    /**
     * Sets the value of the flashTemplateStyleId property.
     * 
     */
    public void setFlashTemplateStyleId(int value) {
        this.flashTemplateStyleId = value;
    }

    /**
     * Gets the value of the updateDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUpdateDateTime() {
        return updateDateTime;
    }

    /**
     * Sets the value of the updateDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUpdateDateTime(XMLGregorianCalendar value) {
        this.updateDateTime = value;
    }

    /**
     * Gets the value of the createdByUserId property.
     * 
     */
    public int getCreatedByUserId() {
        return createdByUserId;
    }

    /**
     * Sets the value of the createdByUserId property.
     * 
     */
    public void setCreatedByUserId(int value) {
        this.createdByUserId = value;
    }

    /**
     * Gets the value of the lastModifiedByUserId property.
     * 
     */
    public int getLastModifiedByUserId() {
        return lastModifiedByUserId;
    }

    /**
     * Sets the value of the lastModifiedByUserId property.
     * 
     */
    public void setLastModifiedByUserId(int value) {
        this.lastModifiedByUserId = value;
    }

    /**
     * Gets the value of the isAlwaysAwarded property.
     * 
     */
    public boolean isIsAlwaysAwarded() {
        return isAlwaysAwarded;
    }

    /**
     * Sets the value of the isAlwaysAwarded property.
     * 
     */
    public void setIsAlwaysAwarded(boolean value) {
        this.isAlwaysAwarded = value;
    }

    /**
     * Gets the value of the useFixedEligibilityList property.
     * 
     */
    public boolean isUseFixedEligibilityList() {
        return useFixedEligibilityList;
    }

    /**
     * Sets the value of the useFixedEligibilityList property.
     * 
     */
    public void setUseFixedEligibilityList(boolean value) {
        this.useFixedEligibilityList = value;
    }

    /**
     * Gets the value of the relativeQualificationPeriod property.
     * 
     */
    public int getRelativeQualificationPeriod() {
        return relativeQualificationPeriod;
    }

    /**
     * Sets the value of the relativeQualificationPeriod property.
     * 
     */
    public void setRelativeQualificationPeriod(int value) {
        this.relativeQualificationPeriod = value;
    }

    /**
     * Gets the value of the postCompletionScoringDays property.
     * 
     */
    public int getPostCompletionScoringDays() {
        return postCompletionScoringDays;
    }

    /**
     * Sets the value of the postCompletionScoringDays property.
     * 
     */
    public void setPostCompletionScoringDays(int value) {
        this.postCompletionScoringDays = value;
    }

    /**
     * Gets the value of the postCompletionAwardingDays property.
     * 
     */
    public int getPostCompletionAwardingDays() {
        return postCompletionAwardingDays;
    }

    /**
     * Sets the value of the postCompletionAwardingDays property.
     * 
     */
    public void setPostCompletionAwardingDays(int value) {
        this.postCompletionAwardingDays = value;
    }

    /**
     * Gets the value of the visibleInShowcase property.
     * 
     */
    public boolean isVisibleInShowcase() {
        return visibleInShowcase;
    }

    /**
     * Sets the value of the visibleInShowcase property.
     * 
     */
    public void setVisibleInShowcase(boolean value) {
        this.visibleInShowcase = value;
    }

}
