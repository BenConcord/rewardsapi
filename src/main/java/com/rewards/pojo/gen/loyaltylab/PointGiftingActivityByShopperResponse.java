
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PointGiftingActivityByShopperResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfPointActivity" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pointGiftingActivityByShopperResult"
})
@XmlRootElement(name = "PointGiftingActivityByShopperResponse")
public class PointGiftingActivityByShopperResponse {

    @XmlElement(name = "PointGiftingActivityByShopperResult")
    protected ArrayOfPointActivity pointGiftingActivityByShopperResult;

    /**
     * Gets the value of the pointGiftingActivityByShopperResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPointActivity }
     *     
     */
    public ArrayOfPointActivity getPointGiftingActivityByShopperResult() {
        return pointGiftingActivityByShopperResult;
    }

    /**
     * Sets the value of the pointGiftingActivityByShopperResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPointActivity }
     *     
     */
    public void setPointGiftingActivityByShopperResult(ArrayOfPointActivity value) {
        this.pointGiftingActivityByShopperResult = value;
    }

}
