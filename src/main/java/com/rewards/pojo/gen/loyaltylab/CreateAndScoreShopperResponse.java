
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateAndScoreShopperResult" type="{http://www.loyaltylab.com/loyaltyapi/}Shopper" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createAndScoreShopperResult"
})
@XmlRootElement(name = "CreateAndScoreShopperResponse")
public class CreateAndScoreShopperResponse {

    @XmlElement(name = "CreateAndScoreShopperResult")
    protected Shopper createAndScoreShopperResult;

    /**
     * Gets the value of the createAndScoreShopperResult property.
     * 
     * @return
     *     possible object is
     *     {@link Shopper }
     *     
     */
    public Shopper getCreateAndScoreShopperResult() {
        return createAndScoreShopperResult;
    }

    /**
     * Sets the value of the createAndScoreShopperResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Shopper }
     *     
     */
    public void setCreateAndScoreShopperResult(Shopper value) {
        this.createAndScoreShopperResult = value;
    }

}
