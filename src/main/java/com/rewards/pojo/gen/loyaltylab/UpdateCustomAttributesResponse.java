
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateCustomAttributesResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfCustomAttributeUpdateResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateCustomAttributesResult"
})
@XmlRootElement(name = "UpdateCustomAttributesResponse")
public class UpdateCustomAttributesResponse {

    @XmlElement(name = "UpdateCustomAttributesResult")
    protected ArrayOfCustomAttributeUpdateResult updateCustomAttributesResult;

    /**
     * Gets the value of the updateCustomAttributesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomAttributeUpdateResult }
     *     
     */
    public ArrayOfCustomAttributeUpdateResult getUpdateCustomAttributesResult() {
        return updateCustomAttributesResult;
    }

    /**
     * Sets the value of the updateCustomAttributesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomAttributeUpdateResult }
     *     
     */
    public void setUpdateCustomAttributesResult(ArrayOfCustomAttributeUpdateResult value) {
        this.updateCustomAttributesResult = value;
    }

}
