
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="loyaltyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="returnTransactions" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfReturnOrderItem" minOccurs="0"/&gt;
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loyaltyId",
    "returnTransactions",
    "amount"
})
@XmlRootElement(name = "ReversePurchaseAwardForReturns")
public class ReversePurchaseAwardForReturns {

    protected String loyaltyId;
    protected ArrayOfReturnOrderItem returnTransactions;
    protected int amount;

    /**
     * Gets the value of the loyaltyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyId() {
        return loyaltyId;
    }

    /**
     * Sets the value of the loyaltyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyId(String value) {
        this.loyaltyId = value;
    }

    /**
     * Gets the value of the returnTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfReturnOrderItem }
     *     
     */
    public ArrayOfReturnOrderItem getReturnTransactions() {
        return returnTransactions;
    }

    /**
     * Sets the value of the returnTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfReturnOrderItem }
     *     
     */
    public void setReturnTransactions(ArrayOfReturnOrderItem value) {
        this.returnTransactions = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAmount(int value) {
        this.amount = value;
    }

}
