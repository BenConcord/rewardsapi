
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productsToPrice" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfPointPricingData" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "productsToPrice"
})
@XmlRootElement(name = "GetCurrentPointPrices")
public class GetCurrentPointPrices {

    protected ArrayOfPointPricingData productsToPrice;

    /**
     * Gets the value of the productsToPrice property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPointPricingData }
     *     
     */
    public ArrayOfPointPricingData getProductsToPrice() {
        return productsToPrice;
    }

    /**
     * Sets the value of the productsToPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPointPricingData }
     *     
     */
    public void setProductsToPrice(ArrayOfPointPricingData value) {
        this.productsToPrice = value;
    }

}
