
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="loyaltyLabPurchaseId" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="externalPurchaseId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loyaltyLabPurchaseId",
    "externalPurchaseId"
})
@XmlRootElement(name = "AssociatePurchase")
public class AssociatePurchase {

    @XmlElement(required = true)
    protected String loyaltyLabPurchaseId;
    protected String externalPurchaseId;

    /**
     * Gets the value of the loyaltyLabPurchaseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyLabPurchaseId() {
        return loyaltyLabPurchaseId;
    }

    /**
     * Sets the value of the loyaltyLabPurchaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyLabPurchaseId(String value) {
        this.loyaltyLabPurchaseId = value;
    }

    /**
     * Gets the value of the externalPurchaseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalPurchaseId() {
        return externalPurchaseId;
    }

    /**
     * Sets the value of the externalPurchaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalPurchaseId(String value) {
        this.externalPurchaseId = value;
    }

}
