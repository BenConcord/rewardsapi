
package com.rewards.pojo.gen.loyaltylab;

import com.rewards.pojo.gen.instorecard.KohlsIVRShopperLookupResultValues;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for KohlsIVRShopperLookupResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KohlsIVRShopperLookupResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Result" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}KohlsIVRShopperLookupResultValues"/&gt;
 *         &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PointBalance" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="LastSaleAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="LastSaleDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KohlsIVRShopperLookupResult", propOrder = {
    "result",
    "url",
    "pointBalance",
    "lastSaleAmount",
    "lastSaleDate"
})
public class KohlsIVRShopperLookupResult {

    @XmlElement(name = "Result", required = true)
    @XmlSchemaType(name = "string")
    protected KohlsIVRShopperLookupResultValues result;
    @XmlElement(name = "URL")
    protected String url;
    @XmlElement(name = "PointBalance")
    protected Integer pointBalance;
    @XmlElement(name = "LastSaleAmount")
    protected BigDecimal lastSaleAmount;
    @XmlElement(name = "LastSaleDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastSaleDate;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link KohlsIVRShopperLookupResultValues }
     *     
     */
    public KohlsIVRShopperLookupResultValues getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link KohlsIVRShopperLookupResultValues }
     *     
     */
    public void setResult(KohlsIVRShopperLookupResultValues value) {
        this.result = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURL() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURL(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the pointBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPointBalance() {
        return pointBalance;
    }

    /**
     * Sets the value of the pointBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPointBalance(Integer value) {
        this.pointBalance = value;
    }

    /**
     * Gets the value of the lastSaleAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLastSaleAmount() {
        return lastSaleAmount;
    }

    /**
     * Sets the value of the lastSaleAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLastSaleAmount(BigDecimal value) {
        this.lastSaleAmount = value;
    }

    /**
     * Gets the value of the lastSaleDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastSaleDate() {
        return lastSaleDate;
    }

    /**
     * Sets the value of the lastSaleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastSaleDate(XMLGregorianCalendar value) {
        this.lastSaleDate = value;
    }

}
