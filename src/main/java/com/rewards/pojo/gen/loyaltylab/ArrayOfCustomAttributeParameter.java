
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfCustomAttributeParameter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomAttributeParameter"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomAttributeParameter" type="{http://www.loyaltylab.com/loyaltyapi/}CustomAttributeParameter" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomAttributeParameter", propOrder = {
    "customAttributeParameter"
})
@XmlRootElement
public class ArrayOfCustomAttributeParameter {

    @XmlElement(name = "CustomAttributeParameter", nillable = true)
    protected List<CustomAttributeParameter> customAttributeParameter;

    /**
     * Gets the value of the customAttributeParameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customAttributeParameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomAttributeParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomAttributeParameter }
     * 
     * 
     */
    public List<CustomAttributeParameter> getCustomAttributeParameter() {
        if (customAttributeParameter == null) {
            customAttributeParameter = new ArrayList<CustomAttributeParameter>();
        }
        return this.customAttributeParameter;
    }

}
