
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetPointPricesResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfProductToPrice" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPointPricesResult"
})
@XmlRootElement(name = "GetPointPricesResponse")
public class GetPointPricesResponse {

    @XmlElement(name = "GetPointPricesResult")
    protected ArrayOfProductToPrice getPointPricesResult;

    /**
     * Gets the value of the getPointPricesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProductToPrice }
     *     
     */
    public ArrayOfProductToPrice getGetPointPricesResult() {
        return getPointPricesResult;
    }

    /**
     * Sets the value of the getPointPricesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProductToPrice }
     *     
     */
    public void setGetPointPricesResult(ArrayOfProductToPrice value) {
        this.getPointPricesResult = value;
    }

}
