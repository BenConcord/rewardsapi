
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetNonTransactionBasedPointsResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfKohlsNonTransaction" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getNonTransactionBasedPointsResult"
})
@XmlRootElement(name = "GetNonTransactionBasedPointsResponse")
public class GetNonTransactionBasedPointsResponse {

    @XmlElement(name = "GetNonTransactionBasedPointsResult")
    protected ArrayOfKohlsNonTransaction getNonTransactionBasedPointsResult;

    /**
     * Gets the value of the getNonTransactionBasedPointsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKohlsNonTransaction }
     *     
     */
    public ArrayOfKohlsNonTransaction getGetNonTransactionBasedPointsResult() {
        return getNonTransactionBasedPointsResult;
    }

    /**
     * Sets the value of the getNonTransactionBasedPointsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKohlsNonTransaction }
     *     
     */
    public void setGetNonTransactionBasedPointsResult(ArrayOfKohlsNonTransaction value) {
        this.getNonTransactionBasedPointsResult = value;
    }

}
