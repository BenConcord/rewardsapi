
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetProfilesByShopperIdResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfProfile" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getProfilesByShopperIdResult"
})
@XmlRootElement(name = "GetProfilesByShopperIdResponse")
public class GetProfilesByShopperIdResponse {

    @XmlElement(name = "GetProfilesByShopperIdResult")
    protected ArrayOfProfile getProfilesByShopperIdResult;

    /**
     * Gets the value of the getProfilesByShopperIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProfile }
     *     
     */
    public ArrayOfProfile getGetProfilesByShopperIdResult() {
        return getProfilesByShopperIdResult;
    }

    /**
     * Sets the value of the getProfilesByShopperIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProfile }
     *     
     */
    public void setGetProfilesByShopperIdResult(ArrayOfProfile value) {
        this.getProfilesByShopperIdResult = value;
    }

}
