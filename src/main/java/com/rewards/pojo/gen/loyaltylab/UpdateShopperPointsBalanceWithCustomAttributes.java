
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="points" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customAttributeHeads" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfBulkCustomAttributeHead" minOccurs="0"/&gt;
 *         &lt;element name="pointTypeId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="expirationDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "shopperId",
    "points",
    "description",
    "customAttributeHeads",
    "pointTypeId",
    "expirationDateTime"
})
@XmlRootElement(name = "UpdateShopperPointsBalanceWithCustomAttributes")
public class UpdateShopperPointsBalanceWithCustomAttributes {

    protected int shopperId;
    protected int points;
    protected String description;
    protected ArrayOfBulkCustomAttributeHead customAttributeHeads;
    protected int pointTypeId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDateTime;

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the points property.
     * 
     */
    public int getPoints() {
        return points;
    }

    /**
     * Sets the value of the points property.
     * 
     */
    public void setPoints(int value) {
        this.points = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the customAttributeHeads property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBulkCustomAttributeHead }
     *     
     */
    public ArrayOfBulkCustomAttributeHead getCustomAttributeHeads() {
        return customAttributeHeads;
    }

    /**
     * Sets the value of the customAttributeHeads property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBulkCustomAttributeHead }
     *     
     */
    public void setCustomAttributeHeads(ArrayOfBulkCustomAttributeHead value) {
        this.customAttributeHeads = value;
    }

    /**
     * Gets the value of the pointTypeId property.
     * 
     */
    public int getPointTypeId() {
        return pointTypeId;
    }

    /**
     * Sets the value of the pointTypeId property.
     * 
     */
    public void setPointTypeId(int value) {
        this.pointTypeId = value;
    }

    /**
     * Gets the value of the expirationDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDateTime() {
        return expirationDateTime;
    }

    /**
     * Sets the value of the expirationDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDateTime(XMLGregorianCalendar value) {
        this.expirationDateTime = value;
    }

}
