
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetRewardProductByUniqueSKUResult" type="{http://www.loyaltylab.com/loyaltyapi/}RewardProduct" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRewardProductByUniqueSKUResult"
})
@XmlRootElement(name = "GetRewardProductByUniqueSKUResponse")
public class GetRewardProductByUniqueSKUResponse {

    @XmlElement(name = "GetRewardProductByUniqueSKUResult")
    protected RewardProduct getRewardProductByUniqueSKUResult;

    /**
     * Gets the value of the getRewardProductByUniqueSKUResult property.
     * 
     * @return
     *     possible object is
     *     {@link RewardProduct }
     *     
     */
    public RewardProduct getGetRewardProductByUniqueSKUResult() {
        return getRewardProductByUniqueSKUResult;
    }

    /**
     * Sets the value of the getRewardProductByUniqueSKUResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardProduct }
     *     
     */
    public void setGetRewardProductByUniqueSKUResult(RewardProduct value) {
        this.getRewardProductByUniqueSKUResult = value;
    }

}
