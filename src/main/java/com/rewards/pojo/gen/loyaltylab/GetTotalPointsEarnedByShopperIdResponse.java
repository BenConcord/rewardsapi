
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetTotalPointsEarnedByShopperIdResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTotalPointsEarnedByShopperIdResult"
})
@XmlRootElement(name = "GetTotalPointsEarnedByShopperIdResponse")
public class GetTotalPointsEarnedByShopperIdResponse {

    @XmlElement(name = "GetTotalPointsEarnedByShopperIdResult")
    protected int getTotalPointsEarnedByShopperIdResult;

    /**
     * Gets the value of the getTotalPointsEarnedByShopperIdResult property.
     * 
     */
    public int getGetTotalPointsEarnedByShopperIdResult() {
        return getTotalPointsEarnedByShopperIdResult;
    }

    /**
     * Sets the value of the getTotalPointsEarnedByShopperIdResult property.
     * 
     */
    public void setGetTotalPointsEarnedByShopperIdResult(int value) {
        this.getTotalPointsEarnedByShopperIdResult = value;
    }

}
