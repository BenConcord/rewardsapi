
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetCodesWithSkuForShopperResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfShopperRewardItemRedemptionExtended" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCodesWithSkuForShopperResult"
})
@XmlRootElement(name = "GetCodesWithSkuForShopperResponse")
public class GetCodesWithSkuForShopperResponse {

    @XmlElement(name = "GetCodesWithSkuForShopperResult")
    protected ArrayOfShopperRewardItemRedemptionExtended getCodesWithSkuForShopperResult;

    /**
     * Gets the value of the getCodesWithSkuForShopperResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShopperRewardItemRedemptionExtended }
     *     
     */
    public ArrayOfShopperRewardItemRedemptionExtended getGetCodesWithSkuForShopperResult() {
        return getCodesWithSkuForShopperResult;
    }

    /**
     * Sets the value of the getCodesWithSkuForShopperResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShopperRewardItemRedemptionExtended }
     *     
     */
    public void setGetCodesWithSkuForShopperResult(ArrayOfShopperRewardItemRedemptionExtended value) {
        this.getCodesWithSkuForShopperResult = value;
    }

}
