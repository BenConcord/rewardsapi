
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExtendedShopper complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtendedShopper"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Shopper" type="{http://www.loyaltylab.com/loyaltyapi/}Shopper" minOccurs="0"/&gt;
 *         &lt;element name="CurrentPoints" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtendedShopper", propOrder = {
    "shopper",
    "currentPoints"
})
public class ExtendedShopper {

    @XmlElement(name = "Shopper")
    protected Shopper shopper;
    @XmlElement(name = "CurrentPoints")
    protected int currentPoints;

    /**
     * Gets the value of the shopper property.
     * 
     * @return
     *     possible object is
     *     {@link Shopper }
     *     
     */
    public Shopper getShopper() {
        return shopper;
    }

    /**
     * Sets the value of the shopper property.
     * 
     * @param value
     *     allowed object is
     *     {@link Shopper }
     *     
     */
    public void setShopper(Shopper value) {
        this.shopper = value;
    }

    /**
     * Gets the value of the currentPoints property.
     * 
     */
    public int getCurrentPoints() {
        return currentPoints;
    }

    /**
     * Sets the value of the currentPoints property.
     * 
     */
    public void setCurrentPoints(int value) {
        this.currentPoints = value;
    }

}
