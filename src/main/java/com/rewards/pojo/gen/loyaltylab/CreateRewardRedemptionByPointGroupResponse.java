
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateRewardRedemptionByPointGroupResult" type="{http://www.loyaltylab.com/loyaltyapi/}Redemption" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createRewardRedemptionByPointGroupResult"
})
@XmlRootElement(name = "CreateRewardRedemptionByPointGroupResponse")
public class CreateRewardRedemptionByPointGroupResponse {

    @XmlElement(name = "CreateRewardRedemptionByPointGroupResult")
    protected Redemption createRewardRedemptionByPointGroupResult;

    /**
     * Gets the value of the createRewardRedemptionByPointGroupResult property.
     * 
     * @return
     *     possible object is
     *     {@link Redemption }
     *     
     */
    public Redemption getCreateRewardRedemptionByPointGroupResult() {
        return createRewardRedemptionByPointGroupResult;
    }

    /**
     * Sets the value of the createRewardRedemptionByPointGroupResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Redemption }
     *     
     */
    public void setCreateRewardRedemptionByPointGroupResult(Redemption value) {
        this.createRewardRedemptionByPointGroupResult = value;
    }

}
