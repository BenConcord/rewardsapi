
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateShopperPointsBalanceWithCustomAttributesResult" type="{http://www.loyaltylab.com/loyaltyapi/}AuditTrailGroup" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateShopperPointsBalanceWithCustomAttributesResult"
})
@XmlRootElement(name = "UpdateShopperPointsBalanceWithCustomAttributesResponse")
public class UpdateShopperPointsBalanceWithCustomAttributesResponse {

    @XmlElement(name = "UpdateShopperPointsBalanceWithCustomAttributesResult")
    protected AuditTrailGroup updateShopperPointsBalanceWithCustomAttributesResult;

    /**
     * Gets the value of the updateShopperPointsBalanceWithCustomAttributesResult property.
     * 
     * @return
     *     possible object is
     *     {@link AuditTrailGroup }
     *     
     */
    public AuditTrailGroup getUpdateShopperPointsBalanceWithCustomAttributesResult() {
        return updateShopperPointsBalanceWithCustomAttributesResult;
    }

    /**
     * Sets the value of the updateShopperPointsBalanceWithCustomAttributesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditTrailGroup }
     *     
     */
    public void setUpdateShopperPointsBalanceWithCustomAttributesResult(AuditTrailGroup value) {
        this.updateShopperPointsBalanceWithCustomAttributesResult = value;
    }

}
