
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RedeemShopperRewardCerificateResult" type="{http://www.loyaltylab.com/loyaltyapi/}RewardRedemptionStatus"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "redeemShopperRewardCerificateResult"
})
@XmlRootElement(name = "RedeemShopperRewardCerificateResponse")
public class RedeemShopperRewardCerificateResponse {

    @XmlElement(name = "RedeemShopperRewardCerificateResult", required = true)
    @XmlSchemaType(name = "string")
    protected RewardRedemptionStatus redeemShopperRewardCerificateResult;

    /**
     * Gets the value of the redeemShopperRewardCerificateResult property.
     * 
     * @return
     *     possible object is
     *     {@link RewardRedemptionStatus }
     *     
     */
    public RewardRedemptionStatus getRedeemShopperRewardCerificateResult() {
        return redeemShopperRewardCerificateResult;
    }

    /**
     * Sets the value of the redeemShopperRewardCerificateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardRedemptionStatus }
     *     
     */
    public void setRedeemShopperRewardCerificateResult(RewardRedemptionStatus value) {
        this.redeemShopperRewardCerificateResult = value;
    }

}
