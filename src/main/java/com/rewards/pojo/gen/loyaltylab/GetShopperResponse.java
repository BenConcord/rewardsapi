
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperResult" type="{http://www.loyaltylab.com/loyaltyapi/}ExtendedShopper" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperResult"
})
@XmlRootElement(name = "GetShopperResponse")
public class GetShopperResponse {

    @XmlElement(name = "GetShopperResult")
    protected ExtendedShopper getShopperResult;

    /**
     * Gets the value of the getShopperResult property.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedShopper }
     *     
     */
    public ExtendedShopper getGetShopperResult() {
        return getShopperResult;
    }

    /**
     * Sets the value of the getShopperResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedShopper }
     *     
     */
    public void setGetShopperResult(ExtendedShopper value) {
        this.getShopperResult = value;
    }

}
