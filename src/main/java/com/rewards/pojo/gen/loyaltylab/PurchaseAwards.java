
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PurchaseAwards complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseAwards"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StoreId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SalesAssociateId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegisterId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReceiptDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="BasketAwards" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfAwardDetail" minOccurs="0"/&gt;
 *         &lt;element name="LineItems" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfLineItemWithAwards" minOccurs="0"/&gt;
 *         &lt;element name="Tenders" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfTenderWithAwards" minOccurs="0"/&gt;
 *         &lt;element name="TotalPointsEarned" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseAwards", propOrder = {
    "shopperId",
    "transactionId",
    "storeId",
    "salesAssociateId",
    "registerId",
    "receiptDateTime",
    "basketAwards",
    "lineItems",
    "tenders",
    "totalPointsEarned"
})
public class PurchaseAwards {

    @XmlElement(name = "ShopperId")
    protected int shopperId;
    @XmlElement(name = "TransactionId")
    protected String transactionId;
    @XmlElement(name = "StoreId")
    protected String storeId;
    @XmlElement(name = "SalesAssociateId")
    protected String salesAssociateId;
    @XmlElement(name = "RegisterId")
    protected String registerId;
    @XmlElement(name = "ReceiptDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receiptDateTime;
    @XmlElement(name = "BasketAwards")
    protected ArrayOfAwardDetail basketAwards;
    @XmlElement(name = "LineItems")
    protected ArrayOfLineItemWithAwards lineItems;
    @XmlElement(name = "Tenders")
    protected ArrayOfTenderWithAwards tenders;
    @XmlElement(name = "TotalPointsEarned")
    protected int totalPointsEarned;

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the transactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the value of the transactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Gets the value of the storeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * Sets the value of the storeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreId(String value) {
        this.storeId = value;
    }

    /**
     * Gets the value of the salesAssociateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesAssociateId() {
        return salesAssociateId;
    }

    /**
     * Sets the value of the salesAssociateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesAssociateId(String value) {
        this.salesAssociateId = value;
    }

    /**
     * Gets the value of the registerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterId() {
        return registerId;
    }

    /**
     * Sets the value of the registerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterId(String value) {
        this.registerId = value;
    }

    /**
     * Gets the value of the receiptDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceiptDateTime() {
        return receiptDateTime;
    }

    /**
     * Sets the value of the receiptDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceiptDateTime(XMLGregorianCalendar value) {
        this.receiptDateTime = value;
    }

    /**
     * Gets the value of the basketAwards property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAwardDetail }
     *     
     */
    public ArrayOfAwardDetail getBasketAwards() {
        return basketAwards;
    }

    /**
     * Sets the value of the basketAwards property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAwardDetail }
     *     
     */
    public void setBasketAwards(ArrayOfAwardDetail value) {
        this.basketAwards = value;
    }

    /**
     * Gets the value of the lineItems property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfLineItemWithAwards }
     *     
     */
    public ArrayOfLineItemWithAwards getLineItems() {
        return lineItems;
    }

    /**
     * Sets the value of the lineItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfLineItemWithAwards }
     *     
     */
    public void setLineItems(ArrayOfLineItemWithAwards value) {
        this.lineItems = value;
    }

    /**
     * Gets the value of the tenders property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTenderWithAwards }
     *     
     */
    public ArrayOfTenderWithAwards getTenders() {
        return tenders;
    }

    /**
     * Sets the value of the tenders property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTenderWithAwards }
     *     
     */
    public void setTenders(ArrayOfTenderWithAwards value) {
        this.tenders = value;
    }

    /**
     * Gets the value of the totalPointsEarned property.
     * 
     */
    public int getTotalPointsEarned() {
        return totalPointsEarned;
    }

    /**
     * Sets the value of the totalPointsEarned property.
     * 
     */
    public void setTotalPointsEarned(int value) {
        this.totalPointsEarned = value;
    }

}
