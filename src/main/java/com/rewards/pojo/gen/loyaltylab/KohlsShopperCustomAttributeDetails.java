
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for KohlsShopperCustomAttributeDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KohlsShopperCustomAttributeDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DateOfBirth" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="AlternatePhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsKCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAssociate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TierValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KohlsShopperCustomAttributeDetails", propOrder = {
    "dateOfBirth",
    "alternatePhone",
    "isKCC",
    "isAssociate",
    "tierValue"
})
public class KohlsShopperCustomAttributeDetails {

    @XmlElement(name = "DateOfBirth", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlElement(name = "AlternatePhone")
    protected String alternatePhone;
    @XmlElement(name = "IsKCC")
    protected String isKCC;
    @XmlElement(name = "IsAssociate")
    protected String isAssociate;
    @XmlElement(name = "TierValue")
    protected String tierValue;

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the alternatePhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternatePhone() {
        return alternatePhone;
    }

    /**
     * Sets the value of the alternatePhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternatePhone(String value) {
        this.alternatePhone = value;
    }

    /**
     * Gets the value of the isKCC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsKCC() {
        return isKCC;
    }

    /**
     * Sets the value of the isKCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsKCC(String value) {
        this.isKCC = value;
    }

    /**
     * Gets the value of the isAssociate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAssociate() {
        return isAssociate;
    }

    /**
     * Sets the value of the isAssociate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAssociate(String value) {
        this.isAssociate = value;
    }

    /**
     * Gets the value of the tierValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierValue() {
        return tierValue;
    }

    /**
     * Sets the value of the tierValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierValue(String value) {
        this.tierValue = value;
    }

}
