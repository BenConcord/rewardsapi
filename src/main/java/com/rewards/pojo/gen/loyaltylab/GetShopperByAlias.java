
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aliasParams" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfKohlsAliasParams" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "aliasParams"
})
@XmlRootElement(name = "GetShopperByAlias")
public class GetShopperByAlias {

    protected ArrayOfKohlsAliasParams aliasParams;

    /**
     * Gets the value of the aliasParams property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKohlsAliasParams }
     *     
     */
    public ArrayOfKohlsAliasParams getAliasParams() {
        return aliasParams;
    }

    /**
     * Sets the value of the aliasParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKohlsAliasParams }
     *     
     */
    public void setAliasParams(ArrayOfKohlsAliasParams value) {
        this.aliasParams = value;
    }

}
