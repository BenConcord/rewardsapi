
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="emailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="communicationID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="forceEmailSend" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "emailAddress",
    "communicationID",
    "forceEmailSend"
})
@XmlRootElement(name = "SendCommunicationToEmailAddress")
public class SendCommunicationToEmailAddress {

    protected String emailAddress;
    protected int communicationID;
    protected boolean forceEmailSend;

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the communicationID property.
     * 
     */
    public int getCommunicationID() {
        return communicationID;
    }

    /**
     * Sets the value of the communicationID property.
     * 
     */
    public void setCommunicationID(int value) {
        this.communicationID = value;
    }

    /**
     * Gets the value of the forceEmailSend property.
     * 
     */
    public boolean isForceEmailSend() {
        return forceEmailSend;
    }

    /**
     * Sets the value of the forceEmailSend property.
     * 
     */
    public void setForceEmailSend(boolean value) {
        this.forceEmailSend = value;
    }

}
