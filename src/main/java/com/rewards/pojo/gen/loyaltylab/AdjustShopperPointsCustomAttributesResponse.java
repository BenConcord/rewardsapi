
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AdjustShopperPointsCustomAttributesResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "adjustShopperPointsCustomAttributesResult"
})
@XmlRootElement(name = "AdjustShopperPointsCustomAttributesResponse")
public class AdjustShopperPointsCustomAttributesResponse {

    @XmlElement(name = "AdjustShopperPointsCustomAttributesResult")
    protected int adjustShopperPointsCustomAttributesResult;

    /**
     * Gets the value of the adjustShopperPointsCustomAttributesResult property.
     * 
     */
    public int getAdjustShopperPointsCustomAttributesResult() {
        return adjustShopperPointsCustomAttributesResult;
    }

    /**
     * Sets the value of the adjustShopperPointsCustomAttributesResult property.
     * 
     */
    public void setAdjustShopperPointsCustomAttributesResult(int value) {
        this.adjustShopperPointsCustomAttributesResult = value;
    }

}
