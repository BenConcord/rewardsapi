
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="retailerShopperId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{http://www.loyaltylab.com/loyaltyapi/}PointStateEnum"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retailerShopperId",
    "state"
})
@XmlRootElement(name = "GetShopperPointBalanceByPointState")
public class GetShopperPointBalanceByPointState {

    protected String retailerShopperId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected PointStateEnum state;

    /**
     * Gets the value of the retailerShopperId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerShopperId() {
        return retailerShopperId;
    }

    /**
     * Sets the value of the retailerShopperId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerShopperId(String value) {
        this.retailerShopperId = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link PointStateEnum }
     *     
     */
    public PointStateEnum getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointStateEnum }
     *     
     */
    public void setState(PointStateEnum value) {
        this.state = value;
    }

}
