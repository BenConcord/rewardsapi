
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfTenderWithAwards complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTenderWithAwards"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Tender" type="{http://www.loyaltylab.com/loyaltyapi/}TenderWithAwards" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTenderWithAwards", propOrder = {
    "tender"
})
public class ArrayOfTenderWithAwards {

    @XmlElement(name = "Tender", nillable = true)
    protected List<TenderWithAwards> tender;

    /**
     * Gets the value of the tender property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tender property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTender().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TenderWithAwards }
     * 
     * 
     */
    public List<TenderWithAwards> getTender() {
        if (tender == null) {
            tender = new ArrayList<TenderWithAwards>();
        }
        return this.tender;
    }

}
