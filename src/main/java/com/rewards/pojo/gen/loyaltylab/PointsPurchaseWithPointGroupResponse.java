
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PointsPurchaseWithPointGroupResult" type="{http://www.loyaltylab.com/loyaltyapi/}PointPurchaseResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pointsPurchaseWithPointGroupResult"
})
@XmlRootElement(name = "PointsPurchaseWithPointGroupResponse")
public class PointsPurchaseWithPointGroupResponse {

    @XmlElement(name = "PointsPurchaseWithPointGroupResult")
    protected PointPurchaseResult pointsPurchaseWithPointGroupResult;

    /**
     * Gets the value of the pointsPurchaseWithPointGroupResult property.
     * 
     * @return
     *     possible object is
     *     {@link PointPurchaseResult }
     *     
     */
    public PointPurchaseResult getPointsPurchaseWithPointGroupResult() {
        return pointsPurchaseWithPointGroupResult;
    }

    /**
     * Sets the value of the pointsPurchaseWithPointGroupResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointPurchaseResult }
     *     
     */
    public void setPointsPurchaseWithPointGroupResult(PointPurchaseResult value) {
        this.pointsPurchaseWithPointGroupResult = value;
    }

}
