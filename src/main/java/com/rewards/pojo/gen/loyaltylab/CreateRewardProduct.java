
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rewardProduct" type="{http://www.loyaltylab.com/loyaltyapi/}RewardProduct" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rewardProduct"
})
@XmlRootElement(name = "CreateRewardProduct")
public class CreateRewardProduct {

    protected RewardProduct rewardProduct;

    /**
     * Gets the value of the rewardProduct property.
     * 
     * @return
     *     possible object is
     *     {@link RewardProduct }
     *     
     */
    public RewardProduct getRewardProduct() {
        return rewardProduct;
    }

    /**
     * Sets the value of the rewardProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardProduct }
     *     
     */
    public void setRewardProduct(RewardProduct value) {
        this.rewardProduct = value;
    }

}
