
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfPointPricingData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPointPricingData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PointPricingData" type="{http://www.loyaltylab.com/loyaltyapi/}PointPricingData" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPointPricingData", propOrder = {
    "pointPricingData"
})
public class ArrayOfPointPricingData {

    @XmlElement(name = "PointPricingData", nillable = true)
    protected List<PointPricingData> pointPricingData;

    /**
     * Gets the value of the pointPricingData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pointPricingData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPointPricingData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PointPricingData }
     * 
     * 
     */
    public List<PointPricingData> getPointPricingData() {
        if (pointPricingData == null) {
            pointPricingData = new ArrayList<PointPricingData>();
        }
        return this.pointPricingData;
    }

}
