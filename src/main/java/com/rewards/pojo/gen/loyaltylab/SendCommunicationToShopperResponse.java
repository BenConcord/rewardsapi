
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendCommunicationToShopperResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendCommunicationToShopperResult"
})
@XmlRootElement(name = "SendCommunicationToShopperResponse")
public class SendCommunicationToShopperResponse {

    @XmlElement(name = "SendCommunicationToShopperResult")
    protected String sendCommunicationToShopperResult;

    /**
     * Gets the value of the sendCommunicationToShopperResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendCommunicationToShopperResult() {
        return sendCommunicationToShopperResult;
    }

    /**
     * Sets the value of the sendCommunicationToShopperResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendCommunicationToShopperResult(String value) {
        this.sendCommunicationToShopperResult = value;
    }

}
