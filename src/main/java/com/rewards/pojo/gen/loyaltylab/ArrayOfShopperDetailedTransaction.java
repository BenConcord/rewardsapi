
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfShopperDetailedTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfShopperDetailedTransaction"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShopperDetailedTransaction" type="{http://www.loyaltylab.com/loyaltyapi/}ShopperDetailedTransaction" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfShopperDetailedTransaction", propOrder = {
    "shopperDetailedTransaction"
})
public class ArrayOfShopperDetailedTransaction {

    @XmlElement(name = "ShopperDetailedTransaction", nillable = true)
    protected List<ShopperDetailedTransaction> shopperDetailedTransaction;

    /**
     * Gets the value of the shopperDetailedTransaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shopperDetailedTransaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShopperDetailedTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShopperDetailedTransaction }
     * 
     * 
     */
    public List<ShopperDetailedTransaction> getShopperDetailedTransaction() {
        if (shopperDetailedTransaction == null) {
            shopperDetailedTransaction = new ArrayList<ShopperDetailedTransaction>();
        }
        return this.shopperDetailedTransaction;
    }

}
