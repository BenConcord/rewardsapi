
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="eventinstance" type="{http://www.loyaltylab.com/loyaltyapi/}EventInstance" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "eventinstance"
})
@XmlRootElement(name = "CreateEventInstance")
public class CreateEventInstance {

    protected EventInstance eventinstance;

    /**
     * Gets the value of the eventinstance property.
     * 
     * @return
     *     possible object is
     *     {@link EventInstance }
     *     
     */
    public EventInstance getEventinstance() {
        return eventinstance;
    }

    /**
     * Sets the value of the eventinstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventInstance }
     *     
     */
    public void setEventinstance(EventInstance value) {
        this.eventinstance = value;
    }

}
