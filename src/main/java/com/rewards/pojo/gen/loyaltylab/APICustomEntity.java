
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for APICustomEntity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="APICustomEntity"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomEntityFields" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfAPICustomEntityField" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "APICustomEntity", propOrder = {
    "customEntityFields"
})
public class APICustomEntity {

    @XmlElement(name = "CustomEntityFields")
    protected ArrayOfAPICustomEntityField customEntityFields;

    /**
     * Gets the value of the customEntityFields property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAPICustomEntityField }
     *     
     */
    public ArrayOfAPICustomEntityField getCustomEntityFields() {
        return customEntityFields;
    }

    /**
     * Sets the value of the customEntityFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAPICustomEntityField }
     *     
     */
    public void setCustomEntityFields(ArrayOfAPICustomEntityField value) {
        this.customEntityFields = value;
    }

}
