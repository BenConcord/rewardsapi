
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DonateToCharityResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "donateToCharityResult"
})
@XmlRootElement(name = "DonateToCharityResponse")
public class DonateToCharityResponse {

    @XmlElement(name = "DonateToCharityResult")
    protected boolean donateToCharityResult;

    /**
     * Gets the value of the donateToCharityResult property.
     * 
     */
    public boolean isDonateToCharityResult() {
        return donateToCharityResult;
    }

    /**
     * Sets the value of the donateToCharityResult property.
     * 
     */
    public void setDonateToCharityResult(boolean value) {
        this.donateToCharityResult = value;
    }

}
