
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetReferedFriendsResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfReferedFriend" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getReferedFriendsResult"
})
@XmlRootElement(name = "GetReferedFriendsResponse")
public class GetReferedFriendsResponse {

    @XmlElement(name = "GetReferedFriendsResult")
    protected ArrayOfReferedFriend getReferedFriendsResult;

    /**
     * Gets the value of the getReferedFriendsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfReferedFriend }
     *     
     */
    public ArrayOfReferedFriend getGetReferedFriendsResult() {
        return getReferedFriendsResult;
    }

    /**
     * Sets the value of the getReferedFriendsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfReferedFriend }
     *     
     */
    public void setGetReferedFriendsResult(ArrayOfReferedFriend value) {
        this.getReferedFriendsResult = value;
    }

}
