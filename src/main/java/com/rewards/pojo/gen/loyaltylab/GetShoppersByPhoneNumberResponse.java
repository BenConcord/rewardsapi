
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShoppersByPhoneNumberResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfShopper" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShoppersByPhoneNumberResult"
})
@XmlRootElement(name = "GetShoppersByPhoneNumberResponse")
public class GetShoppersByPhoneNumberResponse {

    @XmlElement(name = "GetShoppersByPhoneNumberResult")
    protected ArrayOfShopper getShoppersByPhoneNumberResult;

    /**
     * Gets the value of the getShoppersByPhoneNumberResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShopper }
     *     
     */
    public ArrayOfShopper getGetShoppersByPhoneNumberResult() {
        return getShoppersByPhoneNumberResult;
    }

    /**
     * Sets the value of the getShoppersByPhoneNumberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShopper }
     *     
     */
    public void setGetShoppersByPhoneNumberResult(ArrayOfShopper value) {
        this.getShoppersByPhoneNumberResult = value;
    }

}
