
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for RewardProduct complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RewardProduct"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RewardProductId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RedeemValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ImageURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BrandName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastUpdate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="UPCCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InternalNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImageDownloaded" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="DollarAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RewardProduct", propOrder = {
    "rewardProductId",
    "sku",
    "itemName",
    "description",
    "redeemValue",
    "imageURL",
    "brandName",
    "status",
    "lastUpdate",
    "upcCode",
    "internalNote",
    "imageDownloaded",
    "dollarAmount"
})
public class RewardProduct {

    @XmlElement(name = "RewardProductId")
    protected int rewardProductId;
    @XmlElement(name = "SKU")
    protected String sku;
    @XmlElement(name = "ItemName")
    protected String itemName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "RedeemValue")
    protected int redeemValue;
    @XmlElement(name = "ImageURL")
    protected String imageURL;
    @XmlElement(name = "BrandName")
    protected String brandName;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "LastUpdate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastUpdate;
    @XmlElement(name = "UPCCode")
    protected String upcCode;
    @XmlElement(name = "InternalNote")
    protected String internalNote;
    @XmlElement(name = "ImageDownloaded")
    protected boolean imageDownloaded;
    @XmlElement(name = "DollarAmount", required = true)
    protected BigDecimal dollarAmount;

    /**
     * Gets the value of the rewardProductId property.
     * 
     */
    public int getRewardProductId() {
        return rewardProductId;
    }

    /**
     * Sets the value of the rewardProductId property.
     * 
     */
    public void setRewardProductId(int value) {
        this.rewardProductId = value;
    }

    /**
     * Gets the value of the sku property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSKU() {
        return sku;
    }

    /**
     * Sets the value of the sku property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSKU(String value) {
        this.sku = value;
    }

    /**
     * Gets the value of the itemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Sets the value of the itemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the redeemValue property.
     * 
     */
    public int getRedeemValue() {
        return redeemValue;
    }

    /**
     * Sets the value of the redeemValue property.
     * 
     */
    public void setRedeemValue(int value) {
        this.redeemValue = value;
    }

    /**
     * Gets the value of the imageURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * Sets the value of the imageURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageURL(String value) {
        this.imageURL = value;
    }

    /**
     * Gets the value of the brandName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Sets the value of the brandName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the lastUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdate() {
        return lastUpdate;
    }

    /**
     * Sets the value of the lastUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdate(XMLGregorianCalendar value) {
        this.lastUpdate = value;
    }

    /**
     * Gets the value of the upcCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPCCode() {
        return upcCode;
    }

    /**
     * Sets the value of the upcCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPCCode(String value) {
        this.upcCode = value;
    }

    /**
     * Gets the value of the internalNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalNote() {
        return internalNote;
    }

    /**
     * Sets the value of the internalNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalNote(String value) {
        this.internalNote = value;
    }

    /**
     * Gets the value of the imageDownloaded property.
     * 
     */
    public boolean isImageDownloaded() {
        return imageDownloaded;
    }

    /**
     * Sets the value of the imageDownloaded property.
     * 
     */
    public void setImageDownloaded(boolean value) {
        this.imageDownloaded = value;
    }

    /**
     * Gets the value of the dollarAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDollarAmount() {
        return dollarAmount;
    }

    /**
     * Sets the value of the dollarAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDollarAmount(BigDecimal value) {
        this.dollarAmount = value;
    }

}
