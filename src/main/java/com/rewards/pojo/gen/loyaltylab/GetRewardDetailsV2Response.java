
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetRewardDetails_V2Result" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfKohlsAwardDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRewardDetailsV2Result"
})
@XmlRootElement(name = "GetRewardDetails_V2Response")
public class GetRewardDetailsV2Response {

    @XmlElement(name = "GetRewardDetails_V2Result")
    protected ArrayOfKohlsAwardDetails getRewardDetailsV2Result;

    /**
     * Gets the value of the getRewardDetailsV2Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfKohlsAwardDetails }
     *     
     */
    public ArrayOfKohlsAwardDetails getGetRewardDetailsV2Result() {
        return getRewardDetailsV2Result;
    }

    /**
     * Sets the value of the getRewardDetailsV2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfKohlsAwardDetails }
     *     
     */
    public void setGetRewardDetailsV2Result(ArrayOfKohlsAwardDetails value) {
        this.getRewardDetailsV2Result = value;
    }

}
