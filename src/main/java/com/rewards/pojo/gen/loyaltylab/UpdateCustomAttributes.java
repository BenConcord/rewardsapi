
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomAttributeParameters" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfCustomAttributeParameter" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customAttributeParameters"
})
@XmlRootElement(name = "UpdateCustomAttributes")
public class UpdateCustomAttributes {

    @XmlElement(name = "CustomAttributeParameters")
    protected ArrayOfCustomAttributeParameter customAttributeParameters;

    /**
     * Gets the value of the customAttributeParameters property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomAttributeParameter }
     *     
     */
    public ArrayOfCustomAttributeParameter getCustomAttributeParameters() {
        return customAttributeParameters;
    }

    /**
     * Sets the value of the customAttributeParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomAttributeParameter }
     *     
     */
    public void setCustomAttributeParameters(ArrayOfCustomAttributeParameter value) {
        this.customAttributeParameters = value;
    }

}
