
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PointActivity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PointActivity"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PointSharingActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActivityDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ActivityText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PointValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PointActivity", propOrder = {
    "pointSharingActivityType",
    "activityDateTime",
    "activityText",
    "pointValue"
})
public class PointActivity {

    @XmlElement(name = "PointSharingActivityType")
    protected String pointSharingActivityType;
    @XmlElement(name = "ActivityDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activityDateTime;
    @XmlElement(name = "ActivityText")
    protected String activityText;
    @XmlElement(name = "PointValue")
    protected int pointValue;

    /**
     * Gets the value of the pointSharingActivityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointSharingActivityType() {
        return pointSharingActivityType;
    }

    /**
     * Sets the value of the pointSharingActivityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointSharingActivityType(String value) {
        this.pointSharingActivityType = value;
    }

    /**
     * Gets the value of the activityDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivityDateTime() {
        return activityDateTime;
    }

    /**
     * Sets the value of the activityDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivityDateTime(XMLGregorianCalendar value) {
        this.activityDateTime = value;
    }

    /**
     * Gets the value of the activityText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityText() {
        return activityText;
    }

    /**
     * Sets the value of the activityText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityText(String value) {
        this.activityText = value;
    }

    /**
     * Gets the value of the pointValue property.
     * 
     */
    public int getPointValue() {
        return pointValue;
    }

    /**
     * Sets the value of the pointValue property.
     * 
     */
    public void setPointValue(int value) {
        this.pointValue = value;
    }

}
