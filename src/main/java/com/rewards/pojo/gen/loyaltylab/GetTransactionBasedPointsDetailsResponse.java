
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetTransactionBasedPointsDetailsResult" type="{http://www.loyaltylab.com/loyaltyapi/}KohlsTransactionDetail" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionBasedPointsDetailsResult"
})
@XmlRootElement(name = "GetTransactionBasedPointsDetailsResponse")
public class GetTransactionBasedPointsDetailsResponse {

    @XmlElement(name = "GetTransactionBasedPointsDetailsResult")
    protected KohlsTransactionDetail getTransactionBasedPointsDetailsResult;

    /**
     * Gets the value of the getTransactionBasedPointsDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link KohlsTransactionDetail }
     *     
     */
    public KohlsTransactionDetail getGetTransactionBasedPointsDetailsResult() {
        return getTransactionBasedPointsDetailsResult;
    }

    /**
     * Sets the value of the getTransactionBasedPointsDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link KohlsTransactionDetail }
     *     
     */
    public void setGetTransactionBasedPointsDetailsResult(KohlsTransactionDetail value) {
        this.getTransactionBasedPointsDetailsResult = value;
    }

}
