
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shopperIdentifier" type="{http://www.loyaltylab.com/loyaltyapi/}ShopperIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="rewardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "shopperIdentifier",
    "rewardNumber"
})
@XmlRootElement(name = "RedeemShopperRewardCerificate")
public class RedeemShopperRewardCerificate {

    protected ShopperIdentifier shopperIdentifier;
    protected String rewardNumber;

    /**
     * Gets the value of the shopperIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link ShopperIdentifier }
     *     
     */
    public ShopperIdentifier getShopperIdentifier() {
        return shopperIdentifier;
    }

    /**
     * Sets the value of the shopperIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShopperIdentifier }
     *     
     */
    public void setShopperIdentifier(ShopperIdentifier value) {
        this.shopperIdentifier = value;
    }

    /**
     * Gets the value of the rewardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRewardNumber() {
        return rewardNumber;
    }

    /**
     * Sets the value of the rewardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRewardNumber(String value) {
        this.rewardNumber = value;
    }

}
