
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperRedemptionsByLoyaltyProgramIdResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfRedemptionPointLifeCycleOrderItem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperRedemptionsByLoyaltyProgramIdResult"
})
@XmlRootElement(name = "GetShopperRedemptionsByLoyaltyProgramIdResponse")
public class GetShopperRedemptionsByLoyaltyProgramIdResponse {

    @XmlElement(name = "GetShopperRedemptionsByLoyaltyProgramIdResult")
    protected ArrayOfRedemptionPointLifeCycleOrderItem getShopperRedemptionsByLoyaltyProgramIdResult;

    /**
     * Gets the value of the getShopperRedemptionsByLoyaltyProgramIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRedemptionPointLifeCycleOrderItem }
     *     
     */
    public ArrayOfRedemptionPointLifeCycleOrderItem getGetShopperRedemptionsByLoyaltyProgramIdResult() {
        return getShopperRedemptionsByLoyaltyProgramIdResult;
    }

    /**
     * Sets the value of the getShopperRedemptionsByLoyaltyProgramIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRedemptionPointLifeCycleOrderItem }
     *     
     */
    public void setGetShopperRedemptionsByLoyaltyProgramIdResult(ArrayOfRedemptionPointLifeCycleOrderItem value) {
        this.getShopperRedemptionsByLoyaltyProgramIdResult = value;
    }

}
