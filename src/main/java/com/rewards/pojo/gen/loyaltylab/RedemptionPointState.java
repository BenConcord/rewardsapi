
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RedemptionPointState complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RedemptionPointState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RetailerGUID" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="RedemptionId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PointsPendingEarn" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointsAvailable" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointsPendingRedeem" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointsRedeemed" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointsExpired" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointsCancelled" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ExpirationDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="IsAnyPointAvailable" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RedemptionPointState", propOrder = {
    "retailerGUID",
    "redemptionId",
    "shopperId",
    "pointType",
    "pointsPendingEarn",
    "pointsAvailable",
    "pointsPendingRedeem",
    "pointsRedeemed",
    "pointsExpired",
    "pointsCancelled",
    "createDateTime",
    "expirationDateTime",
    "isAnyPointAvailable"
})
public class RedemptionPointState {

    @XmlElement(name = "RetailerGUID", required = true)
    protected String retailerGUID;
    @XmlElement(name = "RedemptionId")
    protected int redemptionId;
    @XmlElement(name = "ShopperId")
    protected int shopperId;
    @XmlElement(name = "PointType")
    protected String pointType;
    @XmlElement(name = "PointsPendingEarn")
    protected int pointsPendingEarn;
    @XmlElement(name = "PointsAvailable")
    protected int pointsAvailable;
    @XmlElement(name = "PointsPendingRedeem")
    protected int pointsPendingRedeem;
    @XmlElement(name = "PointsRedeemed")
    protected int pointsRedeemed;
    @XmlElement(name = "PointsExpired")
    protected int pointsExpired;
    @XmlElement(name = "PointsCancelled")
    protected int pointsCancelled;
    @XmlElement(name = "CreateDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDateTime;
    @XmlElement(name = "ExpirationDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDateTime;
    @XmlElement(name = "IsAnyPointAvailable")
    protected int isAnyPointAvailable;

    /**
     * Gets the value of the retailerGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerGUID() {
        return retailerGUID;
    }

    /**
     * Sets the value of the retailerGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerGUID(String value) {
        this.retailerGUID = value;
    }

    /**
     * Gets the value of the redemptionId property.
     * 
     */
    public int getRedemptionId() {
        return redemptionId;
    }

    /**
     * Sets the value of the redemptionId property.
     * 
     */
    public void setRedemptionId(int value) {
        this.redemptionId = value;
    }

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the pointType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointType() {
        return pointType;
    }

    /**
     * Sets the value of the pointType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointType(String value) {
        this.pointType = value;
    }

    /**
     * Gets the value of the pointsPendingEarn property.
     * 
     */
    public int getPointsPendingEarn() {
        return pointsPendingEarn;
    }

    /**
     * Sets the value of the pointsPendingEarn property.
     * 
     */
    public void setPointsPendingEarn(int value) {
        this.pointsPendingEarn = value;
    }

    /**
     * Gets the value of the pointsAvailable property.
     * 
     */
    public int getPointsAvailable() {
        return pointsAvailable;
    }

    /**
     * Sets the value of the pointsAvailable property.
     * 
     */
    public void setPointsAvailable(int value) {
        this.pointsAvailable = value;
    }

    /**
     * Gets the value of the pointsPendingRedeem property.
     * 
     */
    public int getPointsPendingRedeem() {
        return pointsPendingRedeem;
    }

    /**
     * Sets the value of the pointsPendingRedeem property.
     * 
     */
    public void setPointsPendingRedeem(int value) {
        this.pointsPendingRedeem = value;
    }

    /**
     * Gets the value of the pointsRedeemed property.
     * 
     */
    public int getPointsRedeemed() {
        return pointsRedeemed;
    }

    /**
     * Sets the value of the pointsRedeemed property.
     * 
     */
    public void setPointsRedeemed(int value) {
        this.pointsRedeemed = value;
    }

    /**
     * Gets the value of the pointsExpired property.
     * 
     */
    public int getPointsExpired() {
        return pointsExpired;
    }

    /**
     * Sets the value of the pointsExpired property.
     * 
     */
    public void setPointsExpired(int value) {
        this.pointsExpired = value;
    }

    /**
     * Gets the value of the pointsCancelled property.
     * 
     */
    public int getPointsCancelled() {
        return pointsCancelled;
    }

    /**
     * Sets the value of the pointsCancelled property.
     * 
     */
    public void setPointsCancelled(int value) {
        this.pointsCancelled = value;
    }

    /**
     * Gets the value of the createDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Sets the value of the createDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDateTime(XMLGregorianCalendar value) {
        this.createDateTime = value;
    }

    /**
     * Gets the value of the expirationDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDateTime() {
        return expirationDateTime;
    }

    /**
     * Sets the value of the expirationDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDateTime(XMLGregorianCalendar value) {
        this.expirationDateTime = value;
    }

    /**
     * Gets the value of the isAnyPointAvailable property.
     * 
     */
    public int getIsAnyPointAvailable() {
        return isAnyPointAvailable;
    }

    /**
     * Sets the value of the isAnyPointAvailable property.
     * 
     */
    public void setIsAnyPointAvailable(int value) {
        this.isAnyPointAvailable = value;
    }

}
