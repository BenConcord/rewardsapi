
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetCurrentPointPricesResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfPointPricingData" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCurrentPointPricesResult"
})
@XmlRootElement(name = "GetCurrentPointPricesResponse")
public class GetCurrentPointPricesResponse {

    @XmlElement(name = "GetCurrentPointPricesResult")
    protected ArrayOfPointPricingData getCurrentPointPricesResult;

    /**
     * Gets the value of the getCurrentPointPricesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPointPricingData }
     *     
     */
    public ArrayOfPointPricingData getGetCurrentPointPricesResult() {
        return getCurrentPointPricesResult;
    }

    /**
     * Sets the value of the getCurrentPointPricesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPointPricingData }
     *     
     */
    public void setGetCurrentPointPricesResult(ArrayOfPointPricingData value) {
        this.getCurrentPointPricesResult = value;
    }

}
