
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AssociatePurchaseResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "associatePurchaseResult"
})
@XmlRootElement(name = "AssociatePurchaseResponse")
public class AssociatePurchaseResponse {

    @XmlElement(name = "AssociatePurchaseResult")
    protected boolean associatePurchaseResult;

    /**
     * Gets the value of the associatePurchaseResult property.
     * 
     */
    public boolean isAssociatePurchaseResult() {
        return associatePurchaseResult;
    }

    /**
     * Sets the value of the associatePurchaseResult property.
     * 
     */
    public void setAssociatePurchaseResult(boolean value) {
        this.associatePurchaseResult = value;
    }

}
