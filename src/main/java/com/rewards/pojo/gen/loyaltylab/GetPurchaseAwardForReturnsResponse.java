
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetPurchaseAwardForReturnsResult" type="{http://www.loyaltylab.com/loyaltyapi/}ReturnsAward" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPurchaseAwardForReturnsResult"
})
@XmlRootElement(name = "GetPurchaseAwardForReturnsResponse")
public class GetPurchaseAwardForReturnsResponse {

    @XmlElement(name = "GetPurchaseAwardForReturnsResult")
    protected ReturnsAward getPurchaseAwardForReturnsResult;

    /**
     * Gets the value of the getPurchaseAwardForReturnsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnsAward }
     *     
     */
    public ReturnsAward getGetPurchaseAwardForReturnsResult() {
        return getPurchaseAwardForReturnsResult;
    }

    /**
     * Sets the value of the getPurchaseAwardForReturnsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnsAward }
     *     
     */
    public void setGetPurchaseAwardForReturnsResult(ReturnsAward value) {
        this.getPurchaseAwardForReturnsResult = value;
    }

}
