
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShopperTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShopperTransaction"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LineItem" type="{http://www.loyaltylab.com/loyaltyapi/}OrderItem" minOccurs="0"/&gt;
 *         &lt;element name="PurchasedProduct" type="{http://www.loyaltylab.com/loyaltyapi/}Product" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShopperTransaction", propOrder = {
    "lineItem",
    "purchasedProduct"
})
public class ShopperTransaction {

    @XmlElement(name = "LineItem")
    protected OrderItem lineItem;
    @XmlElement(name = "PurchasedProduct")
    protected Product purchasedProduct;

    /**
     * Gets the value of the lineItem property.
     * 
     * @return
     *     possible object is
     *     {@link OrderItem }
     *     
     */
    public OrderItem getLineItem() {
        return lineItem;
    }

    /**
     * Sets the value of the lineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderItem }
     *     
     */
    public void setLineItem(OrderItem value) {
        this.lineItem = value;
    }

    /**
     * Gets the value of the purchasedProduct property.
     * 
     * @return
     *     possible object is
     *     {@link Product }
     *     
     */
    public Product getPurchasedProduct() {
        return purchasedProduct;
    }

    /**
     * Sets the value of the purchasedProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link Product }
     *     
     */
    public void setPurchasedProduct(Product value) {
        this.purchasedProduct = value;
    }

}
