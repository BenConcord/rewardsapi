
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fromLoyaltyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="toLoyaltyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="pointsToGift" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="fromShopperText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="toShopperText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fromLoyaltyId",
    "toLoyaltyId",
    "pointsToGift",
    "fromShopperText",
    "toShopperText"
})
@XmlRootElement(name = "GiftPoints")
public class GiftPoints {

    protected String fromLoyaltyId;
    protected String toLoyaltyId;
    protected int pointsToGift;
    protected String fromShopperText;
    protected String toShopperText;

    /**
     * Gets the value of the fromLoyaltyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromLoyaltyId() {
        return fromLoyaltyId;
    }

    /**
     * Sets the value of the fromLoyaltyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromLoyaltyId(String value) {
        this.fromLoyaltyId = value;
    }

    /**
     * Gets the value of the toLoyaltyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToLoyaltyId() {
        return toLoyaltyId;
    }

    /**
     * Sets the value of the toLoyaltyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToLoyaltyId(String value) {
        this.toLoyaltyId = value;
    }

    /**
     * Gets the value of the pointsToGift property.
     * 
     */
    public int getPointsToGift() {
        return pointsToGift;
    }

    /**
     * Sets the value of the pointsToGift property.
     * 
     */
    public void setPointsToGift(int value) {
        this.pointsToGift = value;
    }

    /**
     * Gets the value of the fromShopperText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromShopperText() {
        return fromShopperText;
    }

    /**
     * Sets the value of the fromShopperText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromShopperText(String value) {
        this.fromShopperText = value;
    }

    /**
     * Gets the value of the toShopperText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToShopperText() {
        return toShopperText;
    }

    /**
     * Sets the value of the toShopperText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToShopperText(String value) {
        this.toShopperText = value;
    }

}
