
package com.rewards.pojo.gen.loyaltylab;

import com.rewards.pojo.gen.instorecard.ISCLogTransactionType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfISCLogTransactionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfISCLogTransactionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ISCLogTransactionType" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCLogTransactionType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfISCLogTransactionType", propOrder = {
    "iscLogTransactionType"
})
public class ArrayOfISCLogTransactionType {

    @XmlElement(name = "ISCLogTransactionType", nillable = true)
    protected List<ISCLogTransactionType> iscLogTransactionType;

    /**
     * Gets the value of the iscLogTransactionType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the iscLogTransactionType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getISCLogTransactionType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ISCLogTransactionType }
     * 
     * 
     */
    public List<ISCLogTransactionType> getISCLogTransactionType() {
        if (iscLogTransactionType == null) {
            iscLogTransactionType = new ArrayList<ISCLogTransactionType>();
        }
        return this.iscLogTransactionType;
    }

}
