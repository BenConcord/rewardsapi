
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shopperid" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="orderItem" type="{http://www.loyaltylab.com/loyaltyapi/}OrderItem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "shopperid",
    "orderItem"
})
@XmlRootElement(name = "AddTransaction")
public class AddTransaction {

    protected int shopperid;
    protected OrderItem orderItem;

    /**
     * Gets the value of the shopperid property.
     * 
     */
    public int getShopperid() {
        return shopperid;
    }

    /**
     * Sets the value of the shopperid property.
     * 
     */
    public void setShopperid(int value) {
        this.shopperid = value;
    }

    /**
     * Gets the value of the orderItem property.
     * 
     * @return
     *     possible object is
     *     {@link OrderItem }
     *     
     */
    public OrderItem getOrderItem() {
        return orderItem;
    }

    /**
     * Sets the value of the orderItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderItem }
     *     
     */
    public void setOrderItem(OrderItem value) {
        this.orderItem = value;
    }

}
