
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperPointBalanceByPointStateResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperPointBalanceByPointStateResult"
})
@XmlRootElement(name = "GetShopperPointBalanceByPointStateResponse")
public class GetShopperPointBalanceByPointStateResponse {

    @XmlElement(name = "GetShopperPointBalanceByPointStateResult")
    protected int getShopperPointBalanceByPointStateResult;

    /**
     * Gets the value of the getShopperPointBalanceByPointStateResult property.
     * 
     */
    public int getGetShopperPointBalanceByPointStateResult() {
        return getShopperPointBalanceByPointStateResult;
    }

    /**
     * Sets the value of the getShopperPointBalanceByPointStateResult property.
     * 
     */
    public void setGetShopperPointBalanceByPointStateResult(int value) {
        this.getShopperPointBalanceByPointStateResult = value;
    }

}
