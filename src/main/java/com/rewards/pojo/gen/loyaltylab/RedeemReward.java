
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pointRedemptionCatalogId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="shopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pointRedemptionCatalogId",
    "shopperId"
})
@XmlRootElement(name = "RedeemReward")
public class RedeemReward {

    protected int pointRedemptionCatalogId;
    protected int shopperId;

    /**
     * Gets the value of the pointRedemptionCatalogId property.
     * 
     */
    public int getPointRedemptionCatalogId() {
        return pointRedemptionCatalogId;
    }

    /**
     * Sets the value of the pointRedemptionCatalogId property.
     * 
     */
    public void setPointRedemptionCatalogId(int value) {
        this.pointRedemptionCatalogId = value;
    }

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

}
