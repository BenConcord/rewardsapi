
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateRewardProductResult" type="{http://www.loyaltylab.com/loyaltyapi/}RewardProduct" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateRewardProductResult"
})
@XmlRootElement(name = "UpdateRewardProductResponse")
public class UpdateRewardProductResponse {

    @XmlElement(name = "UpdateRewardProductResult")
    protected RewardProduct updateRewardProductResult;

    /**
     * Gets the value of the updateRewardProductResult property.
     * 
     * @return
     *     possible object is
     *     {@link RewardProduct }
     *     
     */
    public RewardProduct getUpdateRewardProductResult() {
        return updateRewardProductResult;
    }

    /**
     * Sets the value of the updateRewardProductResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardProduct }
     *     
     */
    public void setUpdateRewardProductResult(RewardProduct value) {
        this.updateRewardProductResult = value;
    }

}
