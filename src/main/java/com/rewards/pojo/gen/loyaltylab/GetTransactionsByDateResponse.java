
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetTransactionsByDateResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfShopperTransaction" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionsByDateResult"
})
@XmlRootElement(name = "GetTransactionsByDateResponse")
public class GetTransactionsByDateResponse {

    @XmlElement(name = "GetTransactionsByDateResult")
    protected ArrayOfShopperTransaction getTransactionsByDateResult;

    /**
     * Gets the value of the getTransactionsByDateResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfShopperTransaction }
     *     
     */
    public ArrayOfShopperTransaction getGetTransactionsByDateResult() {
        return getTransactionsByDateResult;
    }

    /**
     * Sets the value of the getTransactionsByDateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfShopperTransaction }
     *     
     */
    public void setGetTransactionsByDateResult(ArrayOfShopperTransaction value) {
        this.getTransactionsByDateResult = value;
    }

}
