
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EventInstance" type="{http://www.loyaltylab.com/loyaltyapi/}EventInstance" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "eventInstance"
})
@XmlRootElement(name = "CreateAndAwardEventInstance")
public class CreateAndAwardEventInstance {

    @XmlElement(name = "EventInstance")
    protected EventInstance eventInstance;

    /**
     * Gets the value of the eventInstance property.
     * 
     * @return
     *     possible object is
     *     {@link EventInstance }
     *     
     */
    public EventInstance getEventInstance() {
        return eventInstance;
    }

    /**
     * Sets the value of the eventInstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventInstance }
     *     
     */
    public void setEventInstance(EventInstance value) {
        this.eventInstance = value;
    }

}
