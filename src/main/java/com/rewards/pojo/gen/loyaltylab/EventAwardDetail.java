
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EventAwardDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EventAwardDetail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OfferId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointsEarned" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AwardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RewardItem" type="{http://www.loyaltylab.com/loyaltyapi/}RewardCode" minOccurs="0"/&gt;
 *         &lt;element name="CreditAward" type="{http://www.loyaltylab.com/loyaltyapi/}CreditAward" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventAwardDetail", propOrder = {
    "offerId",
    "pointsEarned",
    "pointType",
    "awardType",
    "rewardItem",
    "creditAward"
})
public class EventAwardDetail {

    @XmlElement(name = "OfferId")
    protected int offerId;
    @XmlElement(name = "PointsEarned")
    protected int pointsEarned;
    @XmlElement(name = "PointType")
    protected String pointType;
    @XmlElement(name = "AwardType")
    protected String awardType;
    @XmlElement(name = "RewardItem")
    protected RewardCode rewardItem;
    @XmlElement(name = "CreditAward")
    protected CreditAward creditAward;

    /**
     * Gets the value of the offerId property.
     * 
     */
    public int getOfferId() {
        return offerId;
    }

    /**
     * Sets the value of the offerId property.
     * 
     */
    public void setOfferId(int value) {
        this.offerId = value;
    }

    /**
     * Gets the value of the pointsEarned property.
     * 
     */
    public int getPointsEarned() {
        return pointsEarned;
    }

    /**
     * Sets the value of the pointsEarned property.
     * 
     */
    public void setPointsEarned(int value) {
        this.pointsEarned = value;
    }

    /**
     * Gets the value of the pointType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointType() {
        return pointType;
    }

    /**
     * Sets the value of the pointType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointType(String value) {
        this.pointType = value;
    }

    /**
     * Gets the value of the awardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardType() {
        return awardType;
    }

    /**
     * Sets the value of the awardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardType(String value) {
        this.awardType = value;
    }

    /**
     * Gets the value of the rewardItem property.
     * 
     * @return
     *     possible object is
     *     {@link RewardCode }
     *     
     */
    public RewardCode getRewardItem() {
        return rewardItem;
    }

    /**
     * Sets the value of the rewardItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardCode }
     *     
     */
    public void setRewardItem(RewardCode value) {
        this.rewardItem = value;
    }

    /**
     * Gets the value of the creditAward property.
     * 
     * @return
     *     possible object is
     *     {@link CreditAward }
     *     
     */
    public CreditAward getCreditAward() {
        return creditAward;
    }

    /**
     * Sets the value of the creditAward property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditAward }
     *     
     */
    public void setCreditAward(CreditAward value) {
        this.creditAward = value;
    }

}
