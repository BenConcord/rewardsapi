
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for Redemption complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Redemption"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RedemptionId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ProcessBatchId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ProcessingStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailerGUID" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="ReferenceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReferenceId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RegisteredCardId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="IsDeferredAward" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="IsDeferredAwardRedemption" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="AwardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AwardText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AwardMonetaryValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="FileIsProcessed" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="ShopperEmailed" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="ProcessDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CashCost" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CashLiability" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="GoodsCost" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Revenue" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="PointsEarned" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LastOrderItemDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RewardProductId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PointTypeId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Redemption", propOrder = {
    "redemptionId",
    "processBatchId",
    "processingStatus",
    "retailerGUID",
    "referenceType",
    "referenceId",
    "shopperId",
    "registeredCardId",
    "isDeferredAward",
    "isDeferredAwardRedemption",
    "awardType",
    "awardText",
    "awardMonetaryValue",
    "fileIsProcessed",
    "shopperEmailed",
    "processDateTime",
    "cashCost",
    "cashLiability",
    "goodsCost",
    "revenue",
    "pointsEarned",
    "lastOrderItemDateTime",
    "rewardProductId",
    "pointTypeId"
})
public class Redemption {

    @XmlElement(name = "RedemptionId")
    protected int redemptionId;
    @XmlElement(name = "ProcessBatchId")
    protected int processBatchId;
    @XmlElement(name = "ProcessingStatus")
    protected String processingStatus;
    @XmlElement(name = "RetailerGUID", required = true)
    protected String retailerGUID;
    @XmlElement(name = "ReferenceType")
    protected String referenceType;
    @XmlElement(name = "ReferenceId")
    protected int referenceId;
    @XmlElement(name = "ShopperId")
    protected int shopperId;
    @XmlElement(name = "RegisteredCardId")
    protected int registeredCardId;
    @XmlElement(name = "IsDeferredAward")
    protected boolean isDeferredAward;
    @XmlElement(name = "IsDeferredAwardRedemption")
    protected boolean isDeferredAwardRedemption;
    @XmlElement(name = "AwardType")
    protected String awardType;
    @XmlElement(name = "AwardText")
    protected String awardText;
    @XmlElement(name = "AwardMonetaryValue", required = true)
    protected BigDecimal awardMonetaryValue;
    @XmlElement(name = "FileIsProcessed")
    protected boolean fileIsProcessed;
    @XmlElement(name = "ShopperEmailed")
    protected boolean shopperEmailed;
    @XmlElement(name = "ProcessDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar processDateTime;
    @XmlElement(name = "CashCost", required = true)
    protected BigDecimal cashCost;
    @XmlElement(name = "CashLiability", required = true)
    protected BigDecimal cashLiability;
    @XmlElement(name = "GoodsCost", required = true)
    protected BigDecimal goodsCost;
    @XmlElement(name = "Revenue", required = true)
    protected BigDecimal revenue;
    @XmlElement(name = "PointsEarned")
    protected int pointsEarned;
    @XmlElement(name = "LastOrderItemDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastOrderItemDateTime;
    @XmlElement(name = "RewardProductId")
    protected int rewardProductId;
    @XmlElement(name = "PointTypeId")
    protected int pointTypeId;

    /**
     * Gets the value of the redemptionId property.
     * 
     */
    public int getRedemptionId() {
        return redemptionId;
    }

    /**
     * Sets the value of the redemptionId property.
     * 
     */
    public void setRedemptionId(int value) {
        this.redemptionId = value;
    }

    /**
     * Gets the value of the processBatchId property.
     * 
     */
    public int getProcessBatchId() {
        return processBatchId;
    }

    /**
     * Sets the value of the processBatchId property.
     * 
     */
    public void setProcessBatchId(int value) {
        this.processBatchId = value;
    }

    /**
     * Gets the value of the processingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessingStatus() {
        return processingStatus;
    }

    /**
     * Sets the value of the processingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessingStatus(String value) {
        this.processingStatus = value;
    }

    /**
     * Gets the value of the retailerGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerGUID() {
        return retailerGUID;
    }

    /**
     * Sets the value of the retailerGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerGUID(String value) {
        this.retailerGUID = value;
    }

    /**
     * Gets the value of the referenceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceType() {
        return referenceType;
    }

    /**
     * Sets the value of the referenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceType(String value) {
        this.referenceType = value;
    }

    /**
     * Gets the value of the referenceId property.
     * 
     */
    public int getReferenceId() {
        return referenceId;
    }

    /**
     * Sets the value of the referenceId property.
     * 
     */
    public void setReferenceId(int value) {
        this.referenceId = value;
    }

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the registeredCardId property.
     * 
     */
    public int getRegisteredCardId() {
        return registeredCardId;
    }

    /**
     * Sets the value of the registeredCardId property.
     * 
     */
    public void setRegisteredCardId(int value) {
        this.registeredCardId = value;
    }

    /**
     * Gets the value of the isDeferredAward property.
     * 
     */
    public boolean isIsDeferredAward() {
        return isDeferredAward;
    }

    /**
     * Sets the value of the isDeferredAward property.
     * 
     */
    public void setIsDeferredAward(boolean value) {
        this.isDeferredAward = value;
    }

    /**
     * Gets the value of the isDeferredAwardRedemption property.
     * 
     */
    public boolean isIsDeferredAwardRedemption() {
        return isDeferredAwardRedemption;
    }

    /**
     * Sets the value of the isDeferredAwardRedemption property.
     * 
     */
    public void setIsDeferredAwardRedemption(boolean value) {
        this.isDeferredAwardRedemption = value;
    }

    /**
     * Gets the value of the awardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardType() {
        return awardType;
    }

    /**
     * Sets the value of the awardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardType(String value) {
        this.awardType = value;
    }

    /**
     * Gets the value of the awardText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardText() {
        return awardText;
    }

    /**
     * Sets the value of the awardText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardText(String value) {
        this.awardText = value;
    }

    /**
     * Gets the value of the awardMonetaryValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAwardMonetaryValue() {
        return awardMonetaryValue;
    }

    /**
     * Sets the value of the awardMonetaryValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAwardMonetaryValue(BigDecimal value) {
        this.awardMonetaryValue = value;
    }

    /**
     * Gets the value of the fileIsProcessed property.
     * 
     */
    public boolean isFileIsProcessed() {
        return fileIsProcessed;
    }

    /**
     * Sets the value of the fileIsProcessed property.
     * 
     */
    public void setFileIsProcessed(boolean value) {
        this.fileIsProcessed = value;
    }

    /**
     * Gets the value of the shopperEmailed property.
     * 
     */
    public boolean isShopperEmailed() {
        return shopperEmailed;
    }

    /**
     * Sets the value of the shopperEmailed property.
     * 
     */
    public void setShopperEmailed(boolean value) {
        this.shopperEmailed = value;
    }

    /**
     * Gets the value of the processDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProcessDateTime() {
        return processDateTime;
    }

    /**
     * Sets the value of the processDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProcessDateTime(XMLGregorianCalendar value) {
        this.processDateTime = value;
    }

    /**
     * Gets the value of the cashCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCashCost() {
        return cashCost;
    }

    /**
     * Sets the value of the cashCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCashCost(BigDecimal value) {
        this.cashCost = value;
    }

    /**
     * Gets the value of the cashLiability property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCashLiability() {
        return cashLiability;
    }

    /**
     * Sets the value of the cashLiability property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCashLiability(BigDecimal value) {
        this.cashLiability = value;
    }

    /**
     * Gets the value of the goodsCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGoodsCost() {
        return goodsCost;
    }

    /**
     * Sets the value of the goodsCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGoodsCost(BigDecimal value) {
        this.goodsCost = value;
    }

    /**
     * Gets the value of the revenue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRevenue() {
        return revenue;
    }

    /**
     * Sets the value of the revenue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRevenue(BigDecimal value) {
        this.revenue = value;
    }

    /**
     * Gets the value of the pointsEarned property.
     * 
     */
    public int getPointsEarned() {
        return pointsEarned;
    }

    /**
     * Sets the value of the pointsEarned property.
     * 
     */
    public void setPointsEarned(int value) {
        this.pointsEarned = value;
    }

    /**
     * Gets the value of the lastOrderItemDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastOrderItemDateTime() {
        return lastOrderItemDateTime;
    }

    /**
     * Sets the value of the lastOrderItemDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastOrderItemDateTime(XMLGregorianCalendar value) {
        this.lastOrderItemDateTime = value;
    }

    /**
     * Gets the value of the rewardProductId property.
     * 
     */
    public int getRewardProductId() {
        return rewardProductId;
    }

    /**
     * Sets the value of the rewardProductId property.
     * 
     */
    public void setRewardProductId(int value) {
        this.rewardProductId = value;
    }

    /**
     * Gets the value of the pointTypeId property.
     * 
     */
    public int getPointTypeId() {
        return pointTypeId;
    }

    /**
     * Sets the value of the pointTypeId property.
     * 
     */
    public void setPointTypeId(int value) {
        this.pointTypeId = value;
    }

}
