
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionDetail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Product" type="{http://www.loyaltylab.com/loyaltyapi/}TransactionProduct" minOccurs="0"/&gt;
 *         &lt;element name="LineItem" type="{http://www.loyaltylab.com/loyaltyapi/}TransactionLineItem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionDetail", propOrder = {
    "product",
    "lineItem"
})
public class TransactionDetail {

    @XmlElement(name = "Product")
    protected TransactionProduct product;
    @XmlElement(name = "LineItem")
    protected TransactionLineItem lineItem;

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionProduct }
     *     
     */
    public TransactionProduct getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionProduct }
     *     
     */
    public void setProduct(TransactionProduct value) {
        this.product = value;
    }

    /**
     * Gets the value of the lineItem property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionLineItem }
     *     
     */
    public TransactionLineItem getLineItem() {
        return lineItem;
    }

    /**
     * Sets the value of the lineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionLineItem }
     *     
     */
    public void setLineItem(TransactionLineItem value) {
        this.lineItem = value;
    }

}
