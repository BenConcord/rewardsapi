
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OfferStatusExtended complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferStatusExtended"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OfferScore" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Offer" type="{http://www.loyaltylab.com/loyaltyapi/}OfferExtended" minOccurs="0"/&gt;
 *         &lt;element name="IsClipped" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="LastUpdate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="DisplayRank" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="OfferCategoryList" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfOfferCategory" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferStatusExtended", propOrder = {
    "offerScore",
    "offer",
    "isClipped",
    "lastUpdate",
    "displayRank",
    "offerCategoryList"
})
public class OfferStatusExtended {

    @XmlElement(name = "OfferScore")
    protected int offerScore;
    @XmlElement(name = "Offer")
    protected OfferExtended offer;
    @XmlElement(name = "IsClipped")
    protected boolean isClipped;
    @XmlElement(name = "LastUpdate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastUpdate;
    @XmlElement(name = "DisplayRank")
    protected int displayRank;
    @XmlElement(name = "OfferCategoryList")
    protected ArrayOfOfferCategory offerCategoryList;

    /**
     * Gets the value of the offerScore property.
     * 
     */
    public int getOfferScore() {
        return offerScore;
    }

    /**
     * Sets the value of the offerScore property.
     * 
     */
    public void setOfferScore(int value) {
        this.offerScore = value;
    }

    /**
     * Gets the value of the offer property.
     * 
     * @return
     *     possible object is
     *     {@link OfferExtended }
     *     
     */
    public OfferExtended getOffer() {
        return offer;
    }

    /**
     * Sets the value of the offer property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferExtended }
     *     
     */
    public void setOffer(OfferExtended value) {
        this.offer = value;
    }

    /**
     * Gets the value of the isClipped property.
     * 
     */
    public boolean isIsClipped() {
        return isClipped;
    }

    /**
     * Sets the value of the isClipped property.
     * 
     */
    public void setIsClipped(boolean value) {
        this.isClipped = value;
    }

    /**
     * Gets the value of the lastUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdate() {
        return lastUpdate;
    }

    /**
     * Sets the value of the lastUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdate(XMLGregorianCalendar value) {
        this.lastUpdate = value;
    }

    /**
     * Gets the value of the displayRank property.
     * 
     */
    public int getDisplayRank() {
        return displayRank;
    }

    /**
     * Sets the value of the displayRank property.
     * 
     */
    public void setDisplayRank(int value) {
        this.displayRank = value;
    }

    /**
     * Gets the value of the offerCategoryList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOfferCategory }
     *     
     */
    public ArrayOfOfferCategory getOfferCategoryList() {
        return offerCategoryList;
    }

    /**
     * Sets the value of the offerCategoryList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOfferCategory }
     *     
     */
    public void setOfferCategoryList(ArrayOfOfferCategory value) {
        this.offerCategoryList = value;
    }

}
