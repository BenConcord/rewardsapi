
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperProgramsResult" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfProgram" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperProgramsResult"
})
@XmlRootElement(name = "GetShopperProgramsResponse")
public class GetShopperProgramsResponse {

    @XmlElement(name = "GetShopperProgramsResult")
    protected ArrayOfProgram getShopperProgramsResult;

    /**
     * Gets the value of the getShopperProgramsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProgram }
     *     
     */
    public ArrayOfProgram getGetShopperProgramsResult() {
        return getShopperProgramsResult;
    }

    /**
     * Sets the value of the getShopperProgramsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProgram }
     *     
     */
    public void setGetShopperProgramsResult(ArrayOfProgram value) {
        this.getShopperProgramsResult = value;
    }

}
