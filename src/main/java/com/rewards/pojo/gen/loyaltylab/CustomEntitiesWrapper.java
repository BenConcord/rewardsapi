
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomEntitiesWrapper complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomEntitiesWrapper"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Shopper" type="{http://www.loyaltylab.com/loyaltyapi/}Shopper" minOccurs="0"/&gt;
 *         &lt;element name="Redemption" type="{http://www.loyaltylab.com/loyaltyapi/}Redemption" minOccurs="0"/&gt;
 *         &lt;element name="RewardProduct" type="{http://www.loyaltylab.com/loyaltyapi/}RewardProduct" minOccurs="0"/&gt;
 *         &lt;element name="CustomEntities" type="{http://www.loyaltylab.com/loyaltyapi/}ArrayOfAPICustomEntity" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomEntitiesWrapper", propOrder = {
    "shopper",
    "redemption",
    "rewardProduct",
    "customEntities"
})
public class CustomEntitiesWrapper {

    @XmlElement(name = "Shopper")
    protected Shopper shopper;
    @XmlElement(name = "Redemption")
    protected Redemption redemption;
    @XmlElement(name = "RewardProduct")
    protected RewardProduct rewardProduct;
    @XmlElement(name = "CustomEntities")
    protected ArrayOfAPICustomEntity customEntities;

    /**
     * Gets the value of the shopper property.
     * 
     * @return
     *     possible object is
     *     {@link Shopper }
     *     
     */
    public Shopper getShopper() {
        return shopper;
    }

    /**
     * Sets the value of the shopper property.
     * 
     * @param value
     *     allowed object is
     *     {@link Shopper }
     *     
     */
    public void setShopper(Shopper value) {
        this.shopper = value;
    }

    /**
     * Gets the value of the redemption property.
     * 
     * @return
     *     possible object is
     *     {@link Redemption }
     *     
     */
    public Redemption getRedemption() {
        return redemption;
    }

    /**
     * Sets the value of the redemption property.
     * 
     * @param value
     *     allowed object is
     *     {@link Redemption }
     *     
     */
    public void setRedemption(Redemption value) {
        this.redemption = value;
    }

    /**
     * Gets the value of the rewardProduct property.
     * 
     * @return
     *     possible object is
     *     {@link RewardProduct }
     *     
     */
    public RewardProduct getRewardProduct() {
        return rewardProduct;
    }

    /**
     * Sets the value of the rewardProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link RewardProduct }
     *     
     */
    public void setRewardProduct(RewardProduct value) {
        this.rewardProduct = value;
    }

    /**
     * Gets the value of the customEntities property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAPICustomEntity }
     *     
     */
    public ArrayOfAPICustomEntity getCustomEntities() {
        return customEntities;
    }

    /**
     * Sets the value of the customEntities property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAPICustomEntity }
     *     
     */
    public void setCustomEntities(ArrayOfAPICustomEntity value) {
        this.customEntities = value;
    }

}
