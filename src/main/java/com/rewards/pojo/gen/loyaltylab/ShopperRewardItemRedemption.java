
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ShopperRewardItemRedemption complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShopperRewardItemRedemption"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShopperId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RedemptionId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RewardItemId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RetailerGuid" type="{http://microsoft.com/wsdl/types/}guid"/&gt;
 *         &lt;element name="DisplayedValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RewardItemTypeId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RewardItemTypeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ValueTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActualValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExpirationText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RedemptionExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="IssuedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShopperRewardItemRedemption", propOrder = {
    "shopperId",
    "redemptionId",
    "rewardItemId",
    "retailerGuid",
    "displayedValue",
    "rewardItemTypeId",
    "rewardItemTypeDesc",
    "code",
    "url",
    "valueTypeCode",
    "actualValue",
    "expirationText",
    "expirationDate",
    "redemptionExpirationDate",
    "issuedDate"
})
@XmlSeeAlso({
    ShopperRewardItemRedemptionExtended.class
})
public class ShopperRewardItemRedemption {

    @XmlElement(name = "ShopperId")
    protected int shopperId;
    @XmlElement(name = "RedemptionId")
    protected int redemptionId;
    @XmlElement(name = "RewardItemId")
    protected int rewardItemId;
    @XmlElement(name = "RetailerGuid", required = true)
    protected String retailerGuid;
    @XmlElement(name = "DisplayedValue")
    protected String displayedValue;
    @XmlElement(name = "RewardItemTypeId")
    protected int rewardItemTypeId;
    @XmlElement(name = "RewardItemTypeDesc")
    protected String rewardItemTypeDesc;
    @XmlElement(name = "Code")
    protected String code;
    @XmlElement(name = "Url")
    protected String url;
    @XmlElement(name = "ValueTypeCode")
    protected String valueTypeCode;
    @XmlElement(name = "ActualValue")
    protected String actualValue;
    @XmlElement(name = "ExpirationText")
    protected String expirationText;
    @XmlElement(name = "ExpirationDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(name = "RedemptionExpirationDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar redemptionExpirationDate;
    @XmlElement(name = "IssuedDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar issuedDate;

    /**
     * Gets the value of the shopperId property.
     * 
     */
    public int getShopperId() {
        return shopperId;
    }

    /**
     * Sets the value of the shopperId property.
     * 
     */
    public void setShopperId(int value) {
        this.shopperId = value;
    }

    /**
     * Gets the value of the redemptionId property.
     * 
     */
    public int getRedemptionId() {
        return redemptionId;
    }

    /**
     * Sets the value of the redemptionId property.
     * 
     */
    public void setRedemptionId(int value) {
        this.redemptionId = value;
    }

    /**
     * Gets the value of the rewardItemId property.
     * 
     */
    public int getRewardItemId() {
        return rewardItemId;
    }

    /**
     * Sets the value of the rewardItemId property.
     * 
     */
    public void setRewardItemId(int value) {
        this.rewardItemId = value;
    }

    /**
     * Gets the value of the retailerGuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailerGuid() {
        return retailerGuid;
    }

    /**
     * Sets the value of the retailerGuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailerGuid(String value) {
        this.retailerGuid = value;
    }

    /**
     * Gets the value of the displayedValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayedValue() {
        return displayedValue;
    }

    /**
     * Sets the value of the displayedValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayedValue(String value) {
        this.displayedValue = value;
    }

    /**
     * Gets the value of the rewardItemTypeId property.
     * 
     */
    public int getRewardItemTypeId() {
        return rewardItemTypeId;
    }

    /**
     * Sets the value of the rewardItemTypeId property.
     * 
     */
    public void setRewardItemTypeId(int value) {
        this.rewardItemTypeId = value;
    }

    /**
     * Gets the value of the rewardItemTypeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRewardItemTypeDesc() {
        return rewardItemTypeDesc;
    }

    /**
     * Sets the value of the rewardItemTypeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRewardItemTypeDesc(String value) {
        this.rewardItemTypeDesc = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the valueTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueTypeCode() {
        return valueTypeCode;
    }

    /**
     * Sets the value of the valueTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueTypeCode(String value) {
        this.valueTypeCode = value;
    }

    /**
     * Gets the value of the actualValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualValue() {
        return actualValue;
    }

    /**
     * Sets the value of the actualValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualValue(String value) {
        this.actualValue = value;
    }

    /**
     * Gets the value of the expirationText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationText() {
        return expirationText;
    }

    /**
     * Sets the value of the expirationText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationText(String value) {
        this.expirationText = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the redemptionExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRedemptionExpirationDate() {
        return redemptionExpirationDate;
    }

    /**
     * Sets the value of the redemptionExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRedemptionExpirationDate(XMLGregorianCalendar value) {
        this.redemptionExpirationDate = value;
    }

    /**
     * Gets the value of the issuedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getIssuedDate() {
        return issuedDate;
    }

    /**
     * Sets the value of the issuedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setIssuedDate(XMLGregorianCalendar value) {
        this.issuedDate = value;
    }

}
