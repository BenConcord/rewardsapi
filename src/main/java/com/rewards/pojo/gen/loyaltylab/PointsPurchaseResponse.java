
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PointsPurchaseResult" type="{http://www.loyaltylab.com/loyaltyapi/}PointPurchaseResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pointsPurchaseResult"
})
@XmlRootElement(name = "PointsPurchaseResponse")
public class PointsPurchaseResponse {

    @XmlElement(name = "PointsPurchaseResult")
    protected PointPurchaseResult pointsPurchaseResult;

    /**
     * Gets the value of the pointsPurchaseResult property.
     * 
     * @return
     *     possible object is
     *     {@link PointPurchaseResult }
     *     
     */
    public PointPurchaseResult getPointsPurchaseResult() {
        return pointsPurchaseResult;
    }

    /**
     * Sets the value of the pointsPurchaseResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointPurchaseResult }
     *     
     */
    public void setPointsPurchaseResult(PointPurchaseResult value) {
        this.pointsPurchaseResult = value;
    }

}
