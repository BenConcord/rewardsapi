
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetShopperPointBalanceByRetailerIDResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getShopperPointBalanceByRetailerIDResult"
})
@XmlRootElement(name = "GetShopperPointBalanceByRetailerIDResponse")
public class GetShopperPointBalanceByRetailerIDResponse {

    @XmlElement(name = "GetShopperPointBalanceByRetailerIDResult")
    protected int getShopperPointBalanceByRetailerIDResult;

    /**
     * Gets the value of the getShopperPointBalanceByRetailerIDResult property.
     * 
     */
    public int getGetShopperPointBalanceByRetailerIDResult() {
        return getShopperPointBalanceByRetailerIDResult;
    }

    /**
     * Sets the value of the getShopperPointBalanceByRetailerIDResult property.
     * 
     */
    public void setGetShopperPointBalanceByRetailerIDResult(int value) {
        this.getShopperPointBalanceByRetailerIDResult = value;
    }

}
