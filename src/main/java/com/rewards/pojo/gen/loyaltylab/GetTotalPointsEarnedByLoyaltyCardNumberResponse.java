
package com.rewards.pojo.gen.loyaltylab;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetTotalPointsEarnedByLoyaltyCardNumberResult" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTotalPointsEarnedByLoyaltyCardNumberResult"
})
@XmlRootElement(name = "GetTotalPointsEarnedByLoyaltyCardNumberResponse")
public class GetTotalPointsEarnedByLoyaltyCardNumberResponse {

    @XmlElement(name = "GetTotalPointsEarnedByLoyaltyCardNumberResult")
    protected int getTotalPointsEarnedByLoyaltyCardNumberResult;

    /**
     * Gets the value of the getTotalPointsEarnedByLoyaltyCardNumberResult property.
     * 
     */
    public int getGetTotalPointsEarnedByLoyaltyCardNumberResult() {
        return getTotalPointsEarnedByLoyaltyCardNumberResult;
    }

    /**
     * Sets the value of the getTotalPointsEarnedByLoyaltyCardNumberResult property.
     * 
     */
    public void setGetTotalPointsEarnedByLoyaltyCardNumberResult(int value) {
        this.getTotalPointsEarnedByLoyaltyCardNumberResult = value;
    }

}
