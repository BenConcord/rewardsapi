
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ShopperType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShopperType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShopperID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShopperName" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}PersonNameType" minOccurs="0"/&gt;
 *         &lt;element name="ShopperAddress" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}AddressType" minOccurs="0"/&gt;
 *         &lt;element name="ShopperPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShopperEMail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LoyaltyProgramID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LoyaltyMember" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="RegistrationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EMailOptIn" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="PhoneOptIn" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="MailOptIn" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="SourceReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShopperStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailerShopperCreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RetailerRegistered" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomAttributes" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}CustomAttributeValueType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShopperType", propOrder = {
    "shopperID",
    "shopperName",
    "shopperAddress",
    "shopperPhoneNumber",
    "shopperEMail",
    "loyaltyProgramID",
    "loyaltyMember",
    "registrationDate",
    "eMailOptIn",
    "phoneOptIn",
    "mailOptIn",
    "sourceReference",
    "shopperStatus",
    "retailerShopperCreationDate",
    "retailerRegistered",
    "userName",
    "customAttributes"
})
public class ShopperType {

    @XmlElement(name = "ShopperID")
    protected String shopperID;
    @XmlElement(name = "ShopperName")
    protected PersonNameType shopperName;
    @XmlElement(name = "ShopperAddress")
    protected AddressType shopperAddress;
    @XmlElement(name = "ShopperPhoneNumber")
    protected String shopperPhoneNumber;
    @XmlElement(name = "ShopperEMail")
    protected String shopperEMail;
    @XmlElement(name = "LoyaltyProgramID")
    protected String loyaltyProgramID;
    @XmlElement(name = "LoyaltyMember")
    protected boolean loyaltyMember;
    @XmlElement(name = "RegistrationDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar registrationDate;
    @XmlElement(name = "EMailOptIn")
    protected boolean eMailOptIn;
    @XmlElement(name = "PhoneOptIn")
    protected boolean phoneOptIn;
    @XmlElement(name = "MailOptIn")
    protected boolean mailOptIn;
    @XmlElement(name = "SourceReference")
    protected String sourceReference;
    @XmlElement(name = "ShopperStatus")
    protected String shopperStatus;
    @XmlElement(name = "RetailerShopperCreationDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar retailerShopperCreationDate;
    @XmlElement(name = "RetailerRegistered")
    protected boolean retailerRegistered;
    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "CustomAttributes")
    protected CustomAttributeValueType customAttributes;

    /**
     * Gets the value of the shopperID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopperID() {
        return shopperID;
    }

    /**
     * Sets the value of the shopperID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopperID(String value) {
        this.shopperID = value;
    }

    /**
     * Gets the value of the shopperName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameType }
     *     
     */
    public PersonNameType getShopperName() {
        return shopperName;
    }

    /**
     * Sets the value of the shopperName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameType }
     *     
     */
    public void setShopperName(PersonNameType value) {
        this.shopperName = value;
    }

    /**
     * Gets the value of the shopperAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressType }
     *     
     */
    public AddressType getShopperAddress() {
        return shopperAddress;
    }

    /**
     * Sets the value of the shopperAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressType }
     *     
     */
    public void setShopperAddress(AddressType value) {
        this.shopperAddress = value;
    }

    /**
     * Gets the value of the shopperPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopperPhoneNumber() {
        return shopperPhoneNumber;
    }

    /**
     * Sets the value of the shopperPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopperPhoneNumber(String value) {
        this.shopperPhoneNumber = value;
    }

    /**
     * Gets the value of the shopperEMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopperEMail() {
        return shopperEMail;
    }

    /**
     * Sets the value of the shopperEMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopperEMail(String value) {
        this.shopperEMail = value;
    }

    /**
     * Gets the value of the loyaltyProgramID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyProgramID() {
        return loyaltyProgramID;
    }

    /**
     * Sets the value of the loyaltyProgramID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyProgramID(String value) {
        this.loyaltyProgramID = value;
    }

    /**
     * Gets the value of the loyaltyMember property.
     * 
     */
    public boolean isLoyaltyMember() {
        return loyaltyMember;
    }

    /**
     * Sets the value of the loyaltyMember property.
     * 
     */
    public void setLoyaltyMember(boolean value) {
        this.loyaltyMember = value;
    }

    /**
     * Gets the value of the registrationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRegistrationDate() {
        return registrationDate;
    }

    /**
     * Sets the value of the registrationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRegistrationDate(XMLGregorianCalendar value) {
        this.registrationDate = value;
    }

    /**
     * Gets the value of the eMailOptIn property.
     * 
     */
    public boolean isEMailOptIn() {
        return eMailOptIn;
    }

    /**
     * Sets the value of the eMailOptIn property.
     * 
     */
    public void setEMailOptIn(boolean value) {
        this.eMailOptIn = value;
    }

    /**
     * Gets the value of the phoneOptIn property.
     * 
     */
    public boolean isPhoneOptIn() {
        return phoneOptIn;
    }

    /**
     * Sets the value of the phoneOptIn property.
     * 
     */
    public void setPhoneOptIn(boolean value) {
        this.phoneOptIn = value;
    }

    /**
     * Gets the value of the mailOptIn property.
     * 
     */
    public boolean isMailOptIn() {
        return mailOptIn;
    }

    /**
     * Sets the value of the mailOptIn property.
     * 
     */
    public void setMailOptIn(boolean value) {
        this.mailOptIn = value;
    }

    /**
     * Gets the value of the sourceReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceReference() {
        return sourceReference;
    }

    /**
     * Sets the value of the sourceReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceReference(String value) {
        this.sourceReference = value;
    }

    /**
     * Gets the value of the shopperStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopperStatus() {
        return shopperStatus;
    }

    /**
     * Sets the value of the shopperStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopperStatus(String value) {
        this.shopperStatus = value;
    }

    /**
     * Gets the value of the retailerShopperCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRetailerShopperCreationDate() {
        return retailerShopperCreationDate;
    }

    /**
     * Sets the value of the retailerShopperCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRetailerShopperCreationDate(XMLGregorianCalendar value) {
        this.retailerShopperCreationDate = value;
    }

    /**
     * Gets the value of the retailerRegistered property.
     * 
     */
    public boolean isRetailerRegistered() {
        return retailerRegistered;
    }

    /**
     * Sets the value of the retailerRegistered property.
     * 
     */
    public void setRetailerRegistered(boolean value) {
        this.retailerRegistered = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributeValueType }
     *     
     */
    public CustomAttributeValueType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributeValueType }
     *     
     */
    public void setCustomAttributes(CustomAttributeValueType value) {
        this.customAttributes = value;
    }

}
