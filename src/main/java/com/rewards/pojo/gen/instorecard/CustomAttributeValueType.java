
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomAttributeValueType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomAttributeValueType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NumericType" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ArrayOfCustomAttributeTypeNumeric" minOccurs="0"/&gt;
 *         &lt;element name="DateTimeType" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ArrayOfCustomAttributeTypeDateTime" minOccurs="0"/&gt;
 *         &lt;element name="LookupType" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ArrayOfCustomAttributeTypeLookup" minOccurs="0"/&gt;
 *         &lt;element name="TextType" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ArrayOfCustomAttributeTypeText" minOccurs="0"/&gt;
 *         &lt;element name="MonthDayType" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ArrayOfCustomAttributeTypeDateTime" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomAttributeValueType", propOrder = {
    "numericType",
    "dateTimeType",
    "lookupType",
    "textType",
    "monthDayType"
})
public class CustomAttributeValueType {

    @XmlElement(name = "NumericType")
    protected ArrayOfCustomAttributeTypeNumeric numericType;
    @XmlElement(name = "DateTimeType")
    protected ArrayOfCustomAttributeTypeDateTime dateTimeType;
    @XmlElement(name = "LookupType")
    protected ArrayOfCustomAttributeTypeLookup lookupType;
    @XmlElement(name = "TextType")
    protected ArrayOfCustomAttributeTypeText textType;
    @XmlElement(name = "MonthDayType")
    protected ArrayOfCustomAttributeTypeDateTime monthDayType;

    /**
     * Gets the value of the numericType property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomAttributeTypeNumeric }
     *     
     */
    public ArrayOfCustomAttributeTypeNumeric getNumericType() {
        return numericType;
    }

    /**
     * Sets the value of the numericType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomAttributeTypeNumeric }
     *     
     */
    public void setNumericType(ArrayOfCustomAttributeTypeNumeric value) {
        this.numericType = value;
    }

    /**
     * Gets the value of the dateTimeType property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomAttributeTypeDateTime }
     *     
     */
    public ArrayOfCustomAttributeTypeDateTime getDateTimeType() {
        return dateTimeType;
    }

    /**
     * Sets the value of the dateTimeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomAttributeTypeDateTime }
     *     
     */
    public void setDateTimeType(ArrayOfCustomAttributeTypeDateTime value) {
        this.dateTimeType = value;
    }

    /**
     * Gets the value of the lookupType property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomAttributeTypeLookup }
     *     
     */
    public ArrayOfCustomAttributeTypeLookup getLookupType() {
        return lookupType;
    }

    /**
     * Sets the value of the lookupType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomAttributeTypeLookup }
     *     
     */
    public void setLookupType(ArrayOfCustomAttributeTypeLookup value) {
        this.lookupType = value;
    }

    /**
     * Gets the value of the textType property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomAttributeTypeText }
     *     
     */
    public ArrayOfCustomAttributeTypeText getTextType() {
        return textType;
    }

    /**
     * Sets the value of the textType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomAttributeTypeText }
     *     
     */
    public void setTextType(ArrayOfCustomAttributeTypeText value) {
        this.textType = value;
    }

    /**
     * Gets the value of the monthDayType property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomAttributeTypeDateTime }
     *     
     */
    public ArrayOfCustomAttributeTypeDateTime getMonthDayType() {
        return monthDayType;
    }

    /**
     * Sets the value of the monthDayType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomAttributeTypeDateTime }
     *     
     */
    public void setMonthDayType(ArrayOfCustomAttributeTypeDateTime value) {
        this.monthDayType = value;
    }

}
