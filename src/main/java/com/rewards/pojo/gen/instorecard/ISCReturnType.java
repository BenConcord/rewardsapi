
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ISCReturnType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ISCReturnType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemSKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Quantity" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCLogQuantity" minOccurs="0"/&gt;
 *         &lt;element name="ExtendedAmount" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCLogAmountType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionLink" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}TransactionLinkType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ISCReturnType", propOrder = {
    "lineNumber",
    "itemSKU",
    "quantity",
    "extendedAmount",
    "transactionLink"
})
public class ISCReturnType {

    @XmlElement(name = "LineNumber")
    protected String lineNumber;
    @XmlElement(name = "ItemSKU")
    protected String itemSKU;
    @XmlElement(name = "Quantity")
    protected ISCLogQuantity quantity;
    @XmlElement(name = "ExtendedAmount")
    protected ISCLogAmountType extendedAmount;
    @XmlElement(name = "TransactionLink")
    protected TransactionLinkType transactionLink;

    /**
     * Gets the value of the lineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineNumber(String value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the itemSKU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * Sets the value of the itemSKU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemSKU(String value) {
        this.itemSKU = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link ISCLogQuantity }
     *     
     */
    public ISCLogQuantity getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISCLogQuantity }
     *     
     */
    public void setQuantity(ISCLogQuantity value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the extendedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ISCLogAmountType }
     *     
     */
    public ISCLogAmountType getExtendedAmount() {
        return extendedAmount;
    }

    /**
     * Sets the value of the extendedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISCLogAmountType }
     *     
     */
    public void setExtendedAmount(ISCLogAmountType value) {
        this.extendedAmount = value;
    }

    /**
     * Gets the value of the transactionLink property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionLinkType }
     *     
     */
    public TransactionLinkType getTransactionLink() {
        return transactionLink;
    }

    /**
     * Sets the value of the transactionLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionLinkType }
     *     
     */
    public void setTransactionLink(TransactionLinkType value) {
        this.transactionLink = value;
    }

}
