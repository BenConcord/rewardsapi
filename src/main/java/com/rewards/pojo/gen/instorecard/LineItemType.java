
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LineItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LineItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Return" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCReturnType" minOccurs="0"/&gt;
 *           &lt;element name="Sale" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCSaleType" minOccurs="0"/&gt;
 *           &lt;element name="Tender" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}TenderAmountType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItemType", propOrder = {
    "_return",
    "sale",
    "tender"
})
public class LineItemType {

    @XmlElement(name = "Return")
    protected ISCReturnType _return;
    @XmlElement(name = "Sale")
    protected ISCSaleType sale;
    @XmlElement(name = "Tender")
    protected TenderAmountType tender;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link ISCReturnType }
     *     
     */
    public ISCReturnType getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISCReturnType }
     *     
     */
    public void setReturn(ISCReturnType value) {
        this._return = value;
    }

    /**
     * Gets the value of the sale property.
     * 
     * @return
     *     possible object is
     *     {@link ISCSaleType }
     *     
     */
    public ISCSaleType getSale() {
        return sale;
    }

    /**
     * Sets the value of the sale property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISCSaleType }
     *     
     */
    public void setSale(ISCSaleType value) {
        this.sale = value;
    }

    /**
     * Gets the value of the tender property.
     * 
     * @return
     *     possible object is
     *     {@link TenderAmountType }
     *     
     */
    public TenderAmountType getTender() {
        return tender;
    }

    /**
     * Sets the value of the tender property.
     * 
     * @param value
     *     allowed object is
     *     {@link TenderAmountType }
     *     
     */
    public void setTender(TenderAmountType value) {
        this.tender = value;
    }

}
