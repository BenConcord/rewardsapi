
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CardCreditDebitTypeCardType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CardCreditDebitTypeCardType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Visa"/&gt;
 *     &lt;enumeration value="MasterCard"/&gt;
 *     &lt;enumeration value="American Express"/&gt;
 *     &lt;enumeration value="Discover"/&gt;
 *     &lt;enumeration value="Diners Club"/&gt;
 *     &lt;enumeration value="Japanese Commerce Bank"/&gt;
 *     &lt;enumeration value="Non-Credit Card Loyalty Card"/&gt;
 *     &lt;enumeration value="Other"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CardCreditDebitTypeCardType")
@XmlEnum
public enum CardCreditDebitTypeCardType {

    @XmlEnumValue("Visa")
    VISA("Visa"),
    @XmlEnumValue("MasterCard")
    MASTER_CARD("MasterCard"),
    @XmlEnumValue("American Express")
    AMERICAN_EXPRESS("American Express"),
    @XmlEnumValue("Discover")
    DISCOVER("Discover"),
    @XmlEnumValue("Diners Club")
    DINERS_CLUB("Diners Club"),
    @XmlEnumValue("Japanese Commerce Bank")
    JAPANESE_COMMERCE_BANK("Japanese Commerce Bank"),
    @XmlEnumValue("Non-Credit Card Loyalty Card")
    NON_CREDIT_CARD_LOYALTY_CARD("Non-Credit Card Loyalty Card"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    CardCreditDebitTypeCardType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CardCreditDebitTypeCardType fromValue(String v) {
        for (CardCreditDebitTypeCardType c: CardCreditDebitTypeCardType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
