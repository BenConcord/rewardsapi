
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PersonNameType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonNameType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PersonFullName" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}PersonFullNameType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonNameType", propOrder = {
    "personFullName"
})
public class PersonNameType {

    @XmlElement(name = "PersonFullName")
    protected PersonFullNameType personFullName;

    /**
     * Gets the value of the personFullName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonFullNameType }
     *     
     */
    public PersonFullNameType getPersonFullName() {
        return personFullName;
    }

    /**
     * Sets the value of the personFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonFullNameType }
     *     
     */
    public void setPersonFullName(PersonFullNameType value) {
        this.personFullName = value;
    }

}
