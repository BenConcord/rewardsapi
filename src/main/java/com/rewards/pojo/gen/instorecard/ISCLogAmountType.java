
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;


/**
 * <p>Java class for ISCLogAmountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ISCLogAmountType"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;decimal"&gt;
 *       &lt;attribute name="Currency" use="required" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCLogCurrencyCodeType" /&gt;
 *       &lt;attribute name="ForeignAmount" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ISCLogAmountType", propOrder = {
    "value"
})
public class ISCLogAmountType {

    @XmlValue
    protected BigDecimal value;
    @XmlAttribute(name = "Currency", required = true)
    protected ISCLogCurrencyCodeType currency;
    @XmlAttribute(name = "ForeignAmount", required = true)
    protected BigDecimal foreignAmount;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link ISCLogCurrencyCodeType }
     *     
     */
    public ISCLogCurrencyCodeType getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISCLogCurrencyCodeType }
     *     
     */
    public void setCurrency(ISCLogCurrencyCodeType value) {
        this.currency = value;
    }

    /**
     * Gets the value of the foreignAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getForeignAmount() {
        return foreignAmount;
    }

    /**
     * Sets the value of the foreignAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setForeignAmount(BigDecimal value) {
        this.foreignAmount = value;
    }

}
