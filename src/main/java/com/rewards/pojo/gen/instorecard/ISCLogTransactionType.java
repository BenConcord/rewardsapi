
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ISCLogTransactionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ISCLogTransactionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RetailStoreID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegisterID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SalesAssociateID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReceiptDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="Shopper" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ShopperType" minOccurs="0"/&gt;
 *         &lt;element name="LineItem" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}LineItemType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ISCLogTransactionType", propOrder = {
    "transactionID",
    "retailStoreID",
    "registerID",
    "salesAssociateID",
    "receiptDateTime",
    "shopper",
    "lineItem"
})
public class ISCLogTransactionType {

    @XmlElement(name = "TransactionID")
    protected String transactionID;
    @XmlElement(name = "RetailStoreID")
    protected String retailStoreID;
    @XmlElement(name = "RegisterID")
    protected String registerID;
    @XmlElement(name = "SalesAssociateID")
    protected String salesAssociateID;
    @XmlElement(name = "ReceiptDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receiptDateTime;
    @XmlElement(name = "Shopper")
    protected ShopperType shopper;
    @XmlElement(name = "LineItem")
    protected List<LineItemType> lineItem;

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the retailStoreID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailStoreID() {
        return retailStoreID;
    }

    /**
     * Sets the value of the retailStoreID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailStoreID(String value) {
        this.retailStoreID = value;
    }

    /**
     * Gets the value of the registerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterID() {
        return registerID;
    }

    /**
     * Sets the value of the registerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterID(String value) {
        this.registerID = value;
    }

    /**
     * Gets the value of the salesAssociateID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesAssociateID() {
        return salesAssociateID;
    }

    /**
     * Sets the value of the salesAssociateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesAssociateID(String value) {
        this.salesAssociateID = value;
    }

    /**
     * Gets the value of the receiptDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceiptDateTime() {
        return receiptDateTime;
    }

    /**
     * Sets the value of the receiptDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceiptDateTime(XMLGregorianCalendar value) {
        this.receiptDateTime = value;
    }

    /**
     * Gets the value of the shopper property.
     * 
     * @return
     *     possible object is
     *     {@link ShopperType }
     *     
     */
    public ShopperType getShopper() {
        return shopper;
    }

    /**
     * Sets the value of the shopper property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShopperType }
     *     
     */
    public void setShopper(ShopperType value) {
        this.shopper = value;
    }

    /**
     * Gets the value of the lineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineItemType }
     * 
     * 
     */
    public List<LineItemType> getLineItem() {
        if (lineItem == null) {
            lineItem = new ArrayList<LineItemType>();
        }
        return this.lineItem;
    }

}
