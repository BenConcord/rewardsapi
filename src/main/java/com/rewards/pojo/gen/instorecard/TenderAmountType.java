
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for TenderAmountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TenderAmountType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Amount" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCLogAmountType" minOccurs="0"/&gt;
 *         &lt;element name="ShopperCreditCard" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}CardCreditDebitType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="TenderType" use="required" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCLogTenderType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TenderAmountType", propOrder = {
    "amount",
    "shopperCreditCard"
})
public class TenderAmountType {

    @XmlElement(name = "Amount")
    protected ISCLogAmountType amount;
    @XmlElement(name = "ShopperCreditCard")
    protected CardCreditDebitType shopperCreditCard;
    @XmlAttribute(name = "TenderType", required = true)
    protected ISCLogTenderType tenderType;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link ISCLogAmountType }
     *     
     */
    public ISCLogAmountType getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISCLogAmountType }
     *     
     */
    public void setAmount(ISCLogAmountType value) {
        this.amount = value;
    }

    /**
     * Gets the value of the shopperCreditCard property.
     * 
     * @return
     *     possible object is
     *     {@link CardCreditDebitType }
     *     
     */
    public CardCreditDebitType getShopperCreditCard() {
        return shopperCreditCard;
    }

    /**
     * Sets the value of the shopperCreditCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardCreditDebitType }
     *     
     */
    public void setShopperCreditCard(CardCreditDebitType value) {
        this.shopperCreditCard = value;
    }

    /**
     * Gets the value of the tenderType property.
     * 
     * @return
     *     possible object is
     *     {@link ISCLogTenderType }
     *     
     */
    public ISCLogTenderType getTenderType() {
        return tenderType;
    }

    /**
     * Sets the value of the tenderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISCLogTenderType }
     *     
     */
    public void setTenderType(ISCLogTenderType value) {
        this.tenderType = value;
    }

}
