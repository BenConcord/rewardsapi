
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ISCLogCurrencyCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ISCLogCurrencyCodeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AUD"/&gt;
 *     &lt;enumeration value="GBP"/&gt;
 *     &lt;enumeration value="NZD"/&gt;
 *     &lt;enumeration value="USD"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ISCLogCurrencyCodeType")
@XmlEnum
public enum ISCLogCurrencyCodeType {

    AUD,
    GBP,
    NZD,
    USD;

    public String value() {
        return name();
    }

    public static ISCLogCurrencyCodeType fromValue(String v) {
        return valueOf(v);
    }

}
