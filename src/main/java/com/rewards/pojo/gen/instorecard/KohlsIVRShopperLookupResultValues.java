
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KohlsIVRShopperLookupResultValues.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="KohlsIVRShopperLookupResultValues"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="MaximumExceeded"/&gt;
 *     &lt;enumeration value="NoMatch"/&gt;
 *     &lt;enumeration value="Match"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "KohlsIVRShopperLookupResultValues")
@XmlEnum
public enum KohlsIVRShopperLookupResultValues {

    @XmlEnumValue("MaximumExceeded")
    MAXIMUM_EXCEEDED("MaximumExceeded"),
    @XmlEnumValue("NoMatch")
    NO_MATCH("NoMatch"),
    @XmlEnumValue("Match")
    MATCH("Match");
    private final String value;

    KohlsIVRShopperLookupResultValues(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static KohlsIVRShopperLookupResultValues fromValue(String v) {
        for (KohlsIVRShopperLookupResultValues c: KohlsIVRShopperLookupResultValues.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
