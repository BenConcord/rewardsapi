
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for CustomEntityDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomEntityDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReferenceTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomEntityDataRow" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}CustomEntityDataRowType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomEntityDataType", propOrder = {
    "referenceTag",
    "customEntityDataRow"
})
public class CustomEntityDataType {

    @XmlElement(name = "ReferenceTag")
    protected String referenceTag;
    @XmlElement(name = "CustomEntityDataRow")
    protected List<CustomEntityDataRowType> customEntityDataRow;

    /**
     * Gets the value of the referenceTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceTag() {
        return referenceTag;
    }

    /**
     * Sets the value of the referenceTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceTag(String value) {
        this.referenceTag = value;
    }

    /**
     * Gets the value of the customEntityDataRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customEntityDataRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomEntityDataRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomEntityDataRowType }
     * 
     * 
     */
    public List<CustomEntityDataRowType> getCustomEntityDataRow() {
        if (customEntityDataRow == null) {
            customEntityDataRow = new ArrayList<CustomEntityDataRowType>();
        }
        return this.customEntityDataRow;
    }

}
