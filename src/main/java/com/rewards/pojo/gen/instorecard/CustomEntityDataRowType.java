
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomEntityDataRowType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomEntityDataRowType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomEntityFields" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ArrayOfCustomEntityFieldType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomEntityDataRowType", propOrder = {
    "customEntityFields"
})
public class CustomEntityDataRowType {

    @XmlElement(name = "CustomEntityFields")
    protected ArrayOfCustomEntityFieldType customEntityFields;

    /**
     * Gets the value of the customEntityFields property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomEntityFieldType }
     *     
     */
    public ArrayOfCustomEntityFieldType getCustomEntityFields() {
        return customEntityFields;
    }

    /**
     * Sets the value of the customEntityFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomEntityFieldType }
     *     
     */
    public void setCustomEntityFields(ArrayOfCustomEntityFieldType value) {
        this.customEntityFields = value;
    }

}
