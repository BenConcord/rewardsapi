
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for CustomEntityFieldType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomEntityFieldType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomEntityFieldName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomEntityFieldValue" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;any maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="IsNull" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomEntityFieldType", propOrder = {
    "customEntityFieldName",
    "customEntityFieldValue"
})
public class CustomEntityFieldType {

    @XmlElement(name = "CustomEntityFieldName")
    protected String customEntityFieldName;
    @XmlElement(name = "CustomEntityFieldValue")
    protected CustomEntityFieldType.CustomEntityFieldValue customEntityFieldValue;

    /**
     * Gets the value of the customEntityFieldName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomEntityFieldName() {
        return customEntityFieldName;
    }

    /**
     * Sets the value of the customEntityFieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomEntityFieldName(String value) {
        this.customEntityFieldName = value;
    }

    /**
     * Gets the value of the customEntityFieldValue property.
     * 
     * @return
     *     possible object is
     *     {@link CustomEntityFieldType.CustomEntityFieldValue }
     *     
     */
    public CustomEntityFieldType.CustomEntityFieldValue getCustomEntityFieldValue() {
        return customEntityFieldValue;
    }

    /**
     * Sets the value of the customEntityFieldValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomEntityFieldType.CustomEntityFieldValue }
     *     
     */
    public void setCustomEntityFieldValue(CustomEntityFieldType.CustomEntityFieldValue value) {
        this.customEntityFieldValue = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;any maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="IsNull" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "content"
    })
    public static class CustomEntityFieldValue {

        @XmlMixed
        @XmlAnyElement(lax = true)
        protected List<Object> content;
        @XmlAttribute(name = "IsNull", required = true)
        protected boolean isNull;

        /**
         * Gets the value of the content property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the content property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * {@link Object }
         * 
         * 
         */
        public List<Object> getContent() {
            if (content == null) {
                content = new ArrayList<Object>();
            }
            return this.content;
        }

        /**
         * Gets the value of the isNull property.
         * 
         */
        public boolean isIsNull() {
            return isNull;
        }

        /**
         * Sets the value of the isNull property.
         * 
         */
        public void setIsNull(boolean value) {
            this.isNull = value;
        }

    }

}
