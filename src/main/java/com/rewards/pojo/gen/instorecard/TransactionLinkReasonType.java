
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionLinkReasonType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionLinkReasonType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Return"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TransactionLinkReasonType")
@XmlEnum
public enum TransactionLinkReasonType {

    @XmlEnumValue("Return")
    RETURN("Return");
    private final String value;

    TransactionLinkReasonType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionLinkReasonType fromValue(String v) {
        for (TransactionLinkReasonType c: TransactionLinkReasonType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
