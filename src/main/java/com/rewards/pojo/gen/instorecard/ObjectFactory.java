
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.instorecard.schema.v1.retailintegration package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.instorecard.schema.v1.retailintegration
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ISCLogTransactionType }
     * 
     */
    public ISCLogTransactionType createISCLogTransactionType() {
        return new ISCLogTransactionType();
    }

    /**
     * Create an instance of {@link ShopperType }
     * 
     */
    public ShopperType createShopperType() {
        return new ShopperType();
    }

    /**
     * Create an instance of {@link PersonNameType }
     * 
     */
    public PersonNameType createPersonNameType() {
        return new PersonNameType();
    }

    /**
     * Create an instance of {@link PersonFullNameType }
     * 
     */
    public PersonFullNameType createPersonFullNameType() {
        return new PersonFullNameType();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link CustomAttributeValueType }
     * 
     */
    public CustomAttributeValueType createCustomAttributeValueType() {
        return new CustomAttributeValueType();
    }

    /**
     * Create an instance of {@link ArrayOfCustomAttributeTypeNumeric }
     * 
     */
    public ArrayOfCustomAttributeTypeNumeric createArrayOfCustomAttributeTypeNumeric() {
        return new ArrayOfCustomAttributeTypeNumeric();
    }

    /**
     * Create an instance of {@link CustomAttributeTypeNumeric }
     * 
     */
    public CustomAttributeTypeNumeric createCustomAttributeTypeNumeric() {
        return new CustomAttributeTypeNumeric();
    }

    /**
     * Create an instance of {@link ArrayOfCustomAttributeTypeDateTime }
     * 
     */
    public ArrayOfCustomAttributeTypeDateTime createArrayOfCustomAttributeTypeDateTime() {
        return new ArrayOfCustomAttributeTypeDateTime();
    }

    /**
     * Create an instance of {@link CustomAttributeTypeDateTime }
     * 
     */
    public CustomAttributeTypeDateTime createCustomAttributeTypeDateTime() {
        return new CustomAttributeTypeDateTime();
    }

    /**
     * Create an instance of {@link ArrayOfCustomAttributeTypeLookup }
     * 
     */
    public ArrayOfCustomAttributeTypeLookup createArrayOfCustomAttributeTypeLookup() {
        return new ArrayOfCustomAttributeTypeLookup();
    }

    /**
     * Create an instance of {@link CustomAttributeTypeLookup }
     * 
     */
    public CustomAttributeTypeLookup createCustomAttributeTypeLookup() {
        return new CustomAttributeTypeLookup();
    }

    /**
     * Create an instance of {@link ArrayOfCustomAttributeTypeText }
     * 
     */
    public ArrayOfCustomAttributeTypeText createArrayOfCustomAttributeTypeText() {
        return new ArrayOfCustomAttributeTypeText();
    }

    /**
     * Create an instance of {@link CustomAttributeTypeText }
     * 
     */
    public CustomAttributeTypeText createCustomAttributeTypeText() {
        return new CustomAttributeTypeText();
    }

    /**
     * Create an instance of {@link LineItemType }
     * 
     */
    public LineItemType createLineItemType() {
        return new LineItemType();
    }

    /**
     * Create an instance of {@link ISCReturnType }
     * 
     */
    public ISCReturnType createISCReturnType() {
        return new ISCReturnType();
    }

    /**
     * Create an instance of {@link ISCLogQuantity }
     * 
     */
    public ISCLogQuantity createISCLogQuantity() {
        return new ISCLogQuantity();
    }

    /**
     * Create an instance of {@link ISCLogAmountType }
     * 
     */
    public ISCLogAmountType createISCLogAmountType() {
        return new ISCLogAmountType();
    }

    /**
     * Create an instance of {@link TransactionLinkType }
     * 
     */
    public TransactionLinkType createTransactionLinkType() {
        return new TransactionLinkType();
    }

    /**
     * Create an instance of {@link ISCSaleType }
     * 
     */
    public ISCSaleType createISCSaleType() {
        return new ISCSaleType();
    }

    /**
     * Create an instance of {@link TenderAmountType }
     * 
     */
    public TenderAmountType createTenderAmountType() {
        return new TenderAmountType();
    }

    /**
     * Create an instance of {@link CardCreditDebitType }
     * 
     */
    public CardCreditDebitType createCardCreditDebitType() {
        return new CardCreditDebitType();
    }

}
