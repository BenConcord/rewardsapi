
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfCustomAttributeTypeLookup complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomAttributeTypeLookup"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomAttribute" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}CustomAttributeTypeLookup" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomAttributeTypeLookup", propOrder = {
    "customAttribute"
})
public class ArrayOfCustomAttributeTypeLookup {

    @XmlElement(name = "CustomAttribute")
    protected List<CustomAttributeTypeLookup> customAttribute;

    /**
     * Gets the value of the customAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomAttributeTypeLookup }
     * 
     * 
     */
    public List<CustomAttributeTypeLookup> getCustomAttribute() {
        if (customAttribute == null) {
            customAttribute = new ArrayList<CustomAttributeTypeLookup>();
        }
        return this.customAttribute;
    }

}
