
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfCustomAttributeTypeNumeric complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomAttributeTypeNumeric"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomAttribute" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}CustomAttributeTypeNumeric" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomAttributeTypeNumeric", propOrder = {
    "customAttribute"
})
public class ArrayOfCustomAttributeTypeNumeric {

    @XmlElement(name = "CustomAttribute")
    protected List<CustomAttributeTypeNumeric> customAttribute;

    /**
     * Gets the value of the customAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomAttributeTypeNumeric }
     * 
     * 
     */
    public List<CustomAttributeTypeNumeric> getCustomAttribute() {
        if (customAttribute == null) {
            customAttribute = new ArrayList<CustomAttributeTypeNumeric>();
        }
        return this.customAttribute;
    }

}
