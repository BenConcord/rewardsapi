
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ISCSaleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ISCSaleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemSKU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Quantity" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCLogQuantity" minOccurs="0"/&gt;
 *         &lt;element name="ExtendedAmount" type="{http://www.instorecard.com/schema/v1/RetailIntegration/}ISCLogAmountType" minOccurs="0"/&gt;
 *         &lt;element name="ShipDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ISCSaleType", propOrder = {
    "lineNumber",
    "itemSKU",
    "quantity",
    "extendedAmount",
    "shipDateTime"
})
public class ISCSaleType {

    @XmlElement(name = "LineNumber")
    protected String lineNumber;
    @XmlElement(name = "ItemSKU")
    protected String itemSKU;
    @XmlElement(name = "Quantity")
    protected ISCLogQuantity quantity;
    @XmlElement(name = "ExtendedAmount")
    protected ISCLogAmountType extendedAmount;
    @XmlElement(name = "ShipDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar shipDateTime;

    /**
     * Gets the value of the lineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineNumber(String value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the itemSKU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemSKU() {
        return itemSKU;
    }

    /**
     * Sets the value of the itemSKU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemSKU(String value) {
        this.itemSKU = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link ISCLogQuantity }
     *     
     */
    public ISCLogQuantity getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISCLogQuantity }
     *     
     */
    public void setQuantity(ISCLogQuantity value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the extendedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ISCLogAmountType }
     *     
     */
    public ISCLogAmountType getExtendedAmount() {
        return extendedAmount;
    }

    /**
     * Sets the value of the extendedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISCLogAmountType }
     *     
     */
    public void setExtendedAmount(ISCLogAmountType value) {
        this.extendedAmount = value;
    }

    /**
     * Gets the value of the shipDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getShipDateTime() {
        return shipDateTime;
    }

    /**
     * Sets the value of the shipDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setShipDateTime(XMLGregorianCalendar value) {
        this.shipDateTime = value;
    }

}
