
package com.rewards.pojo.gen.instorecard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ISCLogTenderType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ISCLogTenderType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Cash"/&gt;
 *     &lt;enumeration value="CreditDebit"/&gt;
 *     &lt;enumeration value="Check"/&gt;
 *     &lt;enumeration value="PurchaseOrder"/&gt;
 *     &lt;enumeration value="ManufacturerCoupon"/&gt;
 *     &lt;enumeration value="CoPay"/&gt;
 *     &lt;enumeration value="Loyalty"/&gt;
 *     &lt;enumeration value="TravelersCheck"/&gt;
 *     &lt;enumeration value="Points"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ISCLogTenderType")
@XmlEnum
public enum ISCLogTenderType {

    @XmlEnumValue("Cash")
    CASH("Cash"),
    @XmlEnumValue("CreditDebit")
    CREDIT_DEBIT("CreditDebit"),
    @XmlEnumValue("Check")
    CHECK("Check"),
    @XmlEnumValue("PurchaseOrder")
    PURCHASE_ORDER("PurchaseOrder"),
    @XmlEnumValue("ManufacturerCoupon")
    MANUFACTURER_COUPON("ManufacturerCoupon"),
    @XmlEnumValue("CoPay")
    CO_PAY("CoPay"),
    @XmlEnumValue("Loyalty")
    LOYALTY("Loyalty"),
    @XmlEnumValue("TravelersCheck")
    TRAVELERS_CHECK("TravelersCheck"),
    @XmlEnumValue("Points")
    POINTS("Points");
    private final String value;

    ISCLogTenderType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ISCLogTenderType fromValue(String v) {
        for (ISCLogTenderType c: ISCLogTenderType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
