
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for transferBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="transferType" type="{http://www.kohls.com/object/Loyalty/0.1/}typeBase" minOccurs="0"/&gt;
 *         &lt;element name="transferDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="transferText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="pointsTransferred" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferBase", propOrder = {
    "transferType",
    "transferDateTime",
    "transferText",
    "pointsTransferred"
})
public class TransferBase {

    protected TypeBase transferType;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transferDateTime;
    protected String transferText;
    protected Integer pointsTransferred;

    /**
     * Gets the value of the transferType property.
     * 
     * @return
     *     possible object is
     *     {@link TypeBase }
     *     
     */
    public TypeBase getTransferType() {
        return transferType;
    }

    /**
     * Sets the value of the transferType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeBase }
     *     
     */
    public void setTransferType(TypeBase value) {
        this.transferType = value;
    }

    /**
     * Gets the value of the transferDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransferDateTime() {
        return transferDateTime;
    }

    /**
     * Sets the value of the transferDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransferDateTime(XMLGregorianCalendar value) {
        this.transferDateTime = value;
    }

    /**
     * Gets the value of the transferText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferText() {
        return transferText;
    }

    /**
     * Sets the value of the transferText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferText(String value) {
        this.transferText = value;
    }

    /**
     * Gets the value of the pointsTransferred property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPointsTransferred() {
        return pointsTransferred;
    }

    /**
     * Sets the value of the pointsTransferred property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPointsTransferred(Integer value) {
        this.pointsTransferred = value;
    }

}
