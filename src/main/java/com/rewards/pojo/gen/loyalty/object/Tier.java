
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}CurrentTierDescription" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}NextTierMessage" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "currentTierDescription",
    "nextTierMessage"
})
@XmlRootElement(name = "Tier")
public class Tier {

    @XmlElement(name = "CurrentTierDescription")
    protected String currentTierDescription;
    @XmlElement(name = "NextTierMessage")
    protected String nextTierMessage;

    /**
     * Gets the value of the currentTierDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentTierDescription() {
        return currentTierDescription;
    }

    /**
     * Sets the value of the currentTierDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentTierDescription(String value) {
        this.currentTierDescription = value;
    }

    /**
     * Gets the value of the nextTierMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextTierMessage() {
        return nextTierMessage;
    }

    /**
     * Sets the value of the nextTierMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextTierMessage(String value) {
        this.nextTierMessage = value;
    }

}
