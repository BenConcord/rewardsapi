
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for refereesRespPayload complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="refereesRespPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}referer"/&gt;
 *         &lt;element name="referees" type="{http://www.kohls.com/object/Loyalty/0.1/}refereesResp"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "refereesRespPayload", propOrder = {
    "referer",
    "referees"
})
public class RefereesRespPayload {

    @XmlElement(required = true)
    protected String referer;
    @XmlElement(required = true)
    protected RefereesResp referees;

    /**
     * Gets the value of the referer property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getReferer() {
        return referer;
    }

    /**
     * Sets the value of the referer property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setReferer(String value) {
        this.referer = value;
    }

    /**
     * Gets the value of the referees property.
     *
     * @return
     *     possible object is
     *     {@link RefereesResp }
     *
     */
    public RefereesResp getReferees() {
        return referees;
    }

    /**
     * Sets the value of the referees property.
     *
     * @param value
     *     allowed object is
     *     {@link RefereesResp }
     *     
     */
    public void setReferees(RefereesResp value) {
        this.referees = value;
    }

}
