
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pointBalancesRespPayload complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pointBalancesRespPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}loyaltyNbr"/&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}pointBalances" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}pointTiers" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pointBalancesRespPayload", propOrder = {
    "loyaltyNbr",
    "pointBalances",
    "pointTiers"
})
public class PointBalancesRespPayload {

    @XmlElement(required = true)
    protected String loyaltyNbr;
    protected PointBalances pointBalances;
    protected PointTiers pointTiers;

    /**
     * Gets the value of the loyaltyNbr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLoyaltyNbr() {
        return loyaltyNbr;
    }

    /**
     * Sets the value of the loyaltyNbr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLoyaltyNbr(String value) {
        this.loyaltyNbr = value;
    }

    /**
     * Gets the value of the pointBalances property.
     *
     * @return
     *     possible object is
     *     {@link PointBalances }
     *
     */
    public PointBalances getPointBalances() {
        return pointBalances;
    }

    /**
     * Sets the value of the pointBalances property.
     *
     * @param value
     *     allowed object is
     *     {@link PointBalances }
     *
     */
    public void setPointBalances(PointBalances value) {
        this.pointBalances = value;
    }

    /**
     * Gets the value of the pointTiers property.
     *
     * @return
     *     possible object is
     *     {@link PointTiers }
     *
     */
    public PointTiers getPointTiers() {
        return pointTiers;
    }

    /**
     * Sets the value of the pointTiers property.
     *
     * @param value
     *     allowed object is
     *     {@link PointTiers }
     *     
     */
    public void setPointTiers(PointTiers value) {
        this.pointTiers = value;
    }

}
