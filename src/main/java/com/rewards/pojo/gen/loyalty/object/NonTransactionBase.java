
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for nonTransactionBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nonTransactionBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="eventType" type="{http://www.kohls.com/object/Loyalty/0.1/}typeBase" minOccurs="0"/&gt;
 *         &lt;element name="pointActivityDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="awardId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="awardName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="awardHeadline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="pointsAwarded" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nonTransactionBase", propOrder = {
    "eventType",
    "pointActivityDateTime",
    "awardId",
    "awardName",
    "awardHeadline",
    "pointsAwarded"
})
public class NonTransactionBase {

    protected TypeBase eventType;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar pointActivityDateTime;
    protected String awardId;
    protected String awardName;
    protected String awardHeadline;
    protected Integer pointsAwarded;

    /**
     * Gets the value of the eventType property.
     *
     * @return
     *     possible object is
     *     {@link TypeBase }
     *
     */
    public TypeBase getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     *
     * @param value
     *     allowed object is
     *     {@link TypeBase }
     *     
     */
    public void setEventType(TypeBase value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the pointActivityDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPointActivityDateTime() {
        return pointActivityDateTime;
    }

    /**
     * Sets the value of the pointActivityDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPointActivityDateTime(XMLGregorianCalendar value) {
        this.pointActivityDateTime = value;
    }

    /**
     * Gets the value of the awardId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardId() {
        return awardId;
    }

    /**
     * Sets the value of the awardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardId(String value) {
        this.awardId = value;
    }

    /**
     * Gets the value of the awardName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardName() {
        return awardName;
    }

    /**
     * Sets the value of the awardName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardName(String value) {
        this.awardName = value;
    }

    /**
     * Gets the value of the awardHeadline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardHeadline() {
        return awardHeadline;
    }

    /**
     * Sets the value of the awardHeadline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardHeadline(String value) {
        this.awardHeadline = value;
    }

    /**
     * Gets the value of the pointsAwarded property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPointsAwarded() {
        return pointsAwarded;
    }

    /**
     * Sets the value of the pointsAwarded property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPointsAwarded(Integer value) {
        this.pointsAwarded = value;
    }

}
