
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}loyaltyNbr"/&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}searchDateTime" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}mostRecentLimit" minOccurs="0"/&gt;
 *         &lt;element name="requestedActivityTypes" type="{http://www.kohls.com/object/Loyalty/0.1/}requestedTypes" minOccurs="0"/&gt;
 *         &lt;element name="requestedSortOrder" type="{http://www.kohls.com/object/Loyalty/0.1/}sortType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loyaltyNbr",
    "searchDateTime",
    "mostRecentLimit",
    "requestedActivityTypes",
    "requestedSortOrder"
})
@XmlRootElement(name = "loyaltyAccountActivityRequest")
public class LoyaltyAccountActivityRequest {

    @XmlElement(required = true)
    protected String loyaltyNbr;
    protected SearchDateTime searchDateTime;
    protected Integer mostRecentLimit;
    protected RequestedTypes requestedActivityTypes;
    @XmlElement(required = true, defaultValue = "desc")
    @XmlSchemaType(name = "string")
    protected SortType requestedSortOrder;

    /**
     * Gets the value of the loyaltyNbr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLoyaltyNbr() {
        return loyaltyNbr;
    }

    /**
     * Sets the value of the loyaltyNbr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLoyaltyNbr(String value) {
        this.loyaltyNbr = value;
    }

    /**
     * Gets the value of the searchDateTime property.
     *
     * @return
     *     possible object is
     *     {@link SearchDateTime }
     *
     */
    public SearchDateTime getSearchDateTime() {
        return searchDateTime;
    }

    /**
     * Sets the value of the searchDateTime property.
     *
     * @param value
     *     allowed object is
     *     {@link SearchDateTime }
     *
     */
    public void setSearchDateTime(SearchDateTime value) {
        this.searchDateTime = value;
    }

    /**
     * Gets the value of the mostRecentLimit property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getMostRecentLimit() {
        return mostRecentLimit;
    }

    /**
     * Sets the value of the mostRecentLimit property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setMostRecentLimit(Integer value) {
        this.mostRecentLimit = value;
    }

    /**
     * Gets the value of the requestedActivityTypes property.
     *
     * @return
     *     possible object is
     *     {@link RequestedTypes }
     *
     */
    public RequestedTypes getRequestedActivityTypes() {
        return requestedActivityTypes;
    }

    /**
     * Sets the value of the requestedActivityTypes property.
     *
     * @param value
     *     allowed object is
     *     {@link RequestedTypes }
     *     
     */
    public void setRequestedActivityTypes(RequestedTypes value) {
        this.requestedActivityTypes = value;
    }

    /**
     * Gets the value of the requestedSortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link SortType }
     *     
     */
    public SortType getRequestedSortOrder() {
        return requestedSortOrder;
    }

    /**
     * Sets the value of the requestedSortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortType }
     *     
     */
    public void setRequestedSortOrder(SortType value) {
        this.requestedSortOrder = value;
    }

}
