
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}Loyalty" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}Balances" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}Tier" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loyalty",
    "balances",
    "tier"
})
@XmlRootElement(name = "LoyaltyBalancesResponse")
public class LoyaltyBalancesResponse {

    @XmlElement(name = "Loyalty")
    protected Loyalty loyalty;
    @XmlElement(name = "Balances")
    protected Balances balances;
    @XmlElement(name = "Tier")
    protected Tier tier;

    /**
     * Gets the value of the loyalty property.
     *
     * @return
     *     possible object is
     *     {@link Loyalty }
     *
     */
    public Loyalty getLoyalty() {
        return loyalty;
    }

    /**
     * Sets the value of the loyalty property.
     *
     * @param value
     *     allowed object is
     *     {@link Loyalty }
     *
     */
    public void setLoyalty(Loyalty value) {
        this.loyalty = value;
    }

    /**
     * Gets the value of the balances property.
     *
     * @return
     *     possible object is
     *     {@link Balances }
     *
     */
    public Balances getBalances() {
        return balances;
    }

    /**
     * Sets the value of the balances property.
     *
     * @param value
     *     allowed object is
     *     {@link Balances }
     *     
     */
    public void setBalances(Balances value) {
        this.balances = value;
    }

    /**
     * Gets the value of the tier property.
     * 
     * @return
     *     possible object is
     *     {@link Tier }
     *     
     */
    public Tier getTier() {
        return tier;
    }

    /**
     * Sets the value of the tier property.
     * 
     * @param value
     *     allowed object is
     *     {@link Tier }
     *     
     */
    public void setTier(Tier value) {
        this.tier = value;
    }

}
