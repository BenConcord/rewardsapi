
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="validPostalCode" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validPostalCode"
})
@XmlRootElement(name = "validatePostalCodeResponse")
public class ValidatePostalCodeResponse {

    protected boolean validPostalCode;

    /**
     * Gets the value of the validPostalCode property.
     * 
     */
    public boolean isValidPostalCode() {
        return validPostalCode;
    }

    /**
     * Sets the value of the validPostalCode property.
     * 
     */
    public void setValidPostalCode(boolean value) {
        this.validPostalCode = value;
    }

}
