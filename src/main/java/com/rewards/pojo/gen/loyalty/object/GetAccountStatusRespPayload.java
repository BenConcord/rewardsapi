
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;


/**
 * <p>Java class for getAccountStatusRespPayload complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAccountStatusRespPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}loyaltyNbr"/&gt;
 *         &lt;element name="shopperStatus" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="loyaltyMemberStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="emailOptInStatus" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAccountStatusRespPayload", propOrder = {
    "loyaltyNbr",
    "shopperStatus",
    "loyaltyMemberStatus",
    "emailOptInStatus"
})
public class GetAccountStatusRespPayload {

    @XmlElement(required = true)
    protected String loyaltyNbr;
    @XmlElement(required = true)
    protected String shopperStatus;
    protected boolean loyaltyMemberStatus;
    @XmlElement(required = true)
    protected BigInteger emailOptInStatus;

    /**
     * Gets the value of the loyaltyNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyNbr() {
        return loyaltyNbr;
    }

    /**
     * Sets the value of the loyaltyNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyNbr(String value) {
        this.loyaltyNbr = value;
    }

    /**
     * Gets the value of the shopperStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopperStatus() {
        return shopperStatus;
    }

    /**
     * Sets the value of the shopperStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopperStatus(String value) {
        this.shopperStatus = value;
    }

    /**
     * Gets the value of the loyaltyMemberStatus property.
     * 
     */
    public boolean isLoyaltyMemberStatus() {
        return loyaltyMemberStatus;
    }

    /**
     * Sets the value of the loyaltyMemberStatus property.
     * 
     */
    public void setLoyaltyMemberStatus(boolean value) {
        this.loyaltyMemberStatus = value;
    }

    /**
     * Gets the value of the emailOptInStatus property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEmailOptInStatus() {
        return emailOptInStatus;
    }

    /**
     * Sets the value of the emailOptInStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEmailOptInStatus(BigInteger value) {
        this.emailOptInStatus = value;
    }

}
