
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="payload" type="{http://www.kohls.com/object/Loyalty/0.1/}createEventReqPayload"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "payload"
})
@XmlRootElement(name = "loyaltyCreateEventRequest")
public class LoyaltyCreateEventRequest {

    @XmlElement(required = true)
    protected CreateEventReqPayload payload;

    /**
     * Gets the value of the payload property.
     *
     * @return
     *     possible object is
     *     {@link CreateEventReqPayload }
     *
     */
    public CreateEventReqPayload getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     *
     * @param value
     *     allowed object is
     *     {@link CreateEventReqPayload }
     *     
     */
    public void setPayload(CreateEventReqPayload value) {
        this.payload = value;
    }

}
