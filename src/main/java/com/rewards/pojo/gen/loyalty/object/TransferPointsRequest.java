
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="transferType" type="{http://www.kohls.com/object/Loyalty/0.1/}subjectType"/&gt;
 *         &lt;element name="toSubjectId" type="{http://www.kohls.com/object/Loyalty/0.1/}subjectIdType"/&gt;
 *         &lt;element name="fromSubjectId" type="{http://www.kohls.com/object/Loyalty/0.1/}subjectIdType"/&gt;
 *         &lt;element name="toSubjectText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fromSubjectText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="pointValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="transferDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="toPointType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fromPointType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferType",
    "toSubjectId",
    "fromSubjectId",
    "toSubjectText",
    "fromSubjectText",
    "pointValue",
    "transferDate",
    "toPointType",
    "fromPointType"
})
@XmlRootElement(name = "transferPointsRequest")
public class TransferPointsRequest {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected SubjectType transferType;
    @XmlElement(required = true)
    protected SubjectIdType toSubjectId;
    @XmlElement(required = true)
    protected SubjectIdType fromSubjectId;
    protected String toSubjectText;
    protected String fromSubjectText;
    protected int pointValue;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transferDate;
    protected String toPointType;
    protected String fromPointType;

    /**
     * Gets the value of the transferType property.
     * 
     * @return
     *     possible object is
     *     {@link SubjectType }
     *     
     */
    public SubjectType getTransferType() {
        return transferType;
    }

    /**
     * Sets the value of the transferType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubjectType }
     *     
     */
    public void setTransferType(SubjectType value) {
        this.transferType = value;
    }

    /**
     * Gets the value of the toSubjectId property.
     * 
     * @return
     *     possible object is
     *     {@link SubjectIdType }
     *     
     */
    public SubjectIdType getToSubjectId() {
        return toSubjectId;
    }

    /**
     * Sets the value of the toSubjectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubjectIdType }
     *     
     */
    public void setToSubjectId(SubjectIdType value) {
        this.toSubjectId = value;
    }

    /**
     * Gets the value of the fromSubjectId property.
     * 
     * @return
     *     possible object is
     *     {@link SubjectIdType }
     *     
     */
    public SubjectIdType getFromSubjectId() {
        return fromSubjectId;
    }

    /**
     * Sets the value of the fromSubjectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubjectIdType }
     *     
     */
    public void setFromSubjectId(SubjectIdType value) {
        this.fromSubjectId = value;
    }

    /**
     * Gets the value of the toSubjectText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToSubjectText() {
        return toSubjectText;
    }

    /**
     * Sets the value of the toSubjectText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToSubjectText(String value) {
        this.toSubjectText = value;
    }

    /**
     * Gets the value of the fromSubjectText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromSubjectText() {
        return fromSubjectText;
    }

    /**
     * Sets the value of the fromSubjectText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromSubjectText(String value) {
        this.fromSubjectText = value;
    }

    /**
     * Gets the value of the pointValue property.
     * 
     */
    public int getPointValue() {
        return pointValue;
    }

    /**
     * Sets the value of the pointValue property.
     * 
     */
    public void setPointValue(int value) {
        this.pointValue = value;
    }

    /**
     * Gets the value of the transferDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransferDate() {
        return transferDate;
    }

    /**
     * Sets the value of the transferDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransferDate(XMLGregorianCalendar value) {
        this.transferDate = value;
    }

    /**
     * Gets the value of the toPointType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToPointType() {
        return toPointType;
    }

    /**
     * Sets the value of the toPointType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToPointType(String value) {
        this.toPointType = value;
    }

    /**
     * Gets the value of the fromPointType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromPointType() {
        return fromPointType;
    }

    /**
     * Sets the value of the fromPointType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromPointType(String value) {
        this.fromPointType = value;
    }

}
