
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for subjectType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="subjectType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Charity"/&gt;
 *     &lt;enumeration value="Sweepstakes"/&gt;
 *     &lt;enumeration value="Gifting"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "subjectType")
@XmlEnum
public enum SubjectType {

    @XmlEnumValue("Charity")
    CHARITY("Charity"),
    @XmlEnumValue("Sweepstakes")
    SWEEPSTAKES("Sweepstakes"),
    @XmlEnumValue("Gifting")
    GIFTING("Gifting");
    private final String value;

    SubjectType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SubjectType fromValue(String v) {
        for (SubjectType c: SubjectType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
