
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subjectsRequested" type="{http://www.kohls.com/object/Loyalty/0.1/}subjectType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="subjectId" type="{http://www.kohls.com/object/Loyalty/0.1/}subjectIdType" minOccurs="0"/&gt;
 *         &lt;element name="location" type="{http://www.kohls.com/object/Loyalty/0.1/}locationValueType" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "subjectsRequested",
    "subjectId",
    "location",
    "status"
})
@XmlRootElement(name = "getAvailableSubjectsRequest")
public class GetAvailableSubjectsRequest {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected List<SubjectType> subjectsRequested;
    protected SubjectIdType subjectId;
    protected LocationValueType location;
    protected String status;

    /**
     * Gets the value of the subjectsRequested property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subjectsRequested property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubjectsRequested().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubjectType }
     *
     *
     */
    public List<SubjectType> getSubjectsRequested() {
        if (subjectsRequested == null) {
            subjectsRequested = new ArrayList<SubjectType>();
        }
        return this.subjectsRequested;
    }

    /**
     * Gets the value of the subjectId property.
     *
     * @return
     *     possible object is
     *     {@link SubjectIdType }
     *
     */
    public SubjectIdType getSubjectId() {
        return subjectId;
    }

    /**
     * Sets the value of the subjectId property.
     *
     * @param value
     *     allowed object is
     *     {@link SubjectIdType }
     *
     */
    public void setSubjectId(SubjectIdType value) {
        this.subjectId = value;
    }

    /**
     * Gets the value of the location property.
     *
     * @return
     *     possible object is
     *     {@link LocationValueType }
     *
     */
    public LocationValueType getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     *
     * @param value
     *     allowed object is
     *     {@link LocationValueType }
     *     
     */
    public void setLocation(LocationValueType value) {
        this.location = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
