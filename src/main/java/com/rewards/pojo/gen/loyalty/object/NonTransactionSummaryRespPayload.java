
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nonTransactionSummaryRespPayload complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nonTransactionSummaryRespPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}loyaltyNbr"/&gt;
 *         &lt;element name="nonTransactions" type="{http://www.kohls.com/object/Loyalty/0.1/}nonTransactions"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nonTransactionSummaryRespPayload", propOrder = {
    "loyaltyNbr",
    "nonTransactions"
})
public class NonTransactionSummaryRespPayload {

    @XmlElement(required = true)
    protected String loyaltyNbr;
    @XmlElement(required = true)
    protected NonTransactions nonTransactions;

    /**
     * Gets the value of the loyaltyNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyNbr() {
        return loyaltyNbr;
    }

    /**
     * Sets the value of the loyaltyNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyNbr(String value) {
        this.loyaltyNbr = value;
    }

    /**
     * Gets the value of the nonTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link NonTransactions }
     *     
     */
    public NonTransactions getNonTransactions() {
        return nonTransactions;
    }

    /**
     * Sets the value of the nonTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonTransactions }
     *     
     */
    public void setNonTransactions(NonTransactions value) {
        this.nonTransactions = value;
    }

}
