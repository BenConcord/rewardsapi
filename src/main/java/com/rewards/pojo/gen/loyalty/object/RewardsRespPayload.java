
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rewardsRespPayload complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rewardsRespPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}loyaltyNbr"/&gt;
 *         &lt;element name="rewards" type="{http://www.kohls.com/object/Loyalty/0.1/}rewards"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rewardsRespPayload", propOrder = {
    "loyaltyNbr",
    "rewards"
})
public class RewardsRespPayload {

    @XmlElement(required = true)
    protected String loyaltyNbr;
    @XmlElement(required = true)
    protected Rewards rewards;

    /**
     * Gets the value of the loyaltyNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyNbr() {
        return loyaltyNbr;
    }

    /**
     * Sets the value of the loyaltyNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyNbr(String value) {
        this.loyaltyNbr = value;
    }

    /**
     * Gets the value of the rewards property.
     * 
     * @return
     *     possible object is
     *     {@link Rewards }
     *     
     */
    public Rewards getRewards() {
        return rewards;
    }

    /**
     * Sets the value of the rewards property.
     * 
     * @param value
     *     allowed object is
     *     {@link Rewards }
     *     
     */
    public void setRewards(Rewards value) {
        this.rewards = value;
    }

}
