
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for loyaltyProfile complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loyaltyProfile"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}loyaltyNbr" minOccurs="0"/&gt;
 *         &lt;element name="storeNbr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="origin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="person" type="{http://www.kohls.com/object/Loyalty/0.1/}person" minOccurs="0"/&gt;
 *         &lt;element name="address" type="{http://www.kohls.com/object/Loyalty/0.1/}address" minOccurs="0"/&gt;
 *         &lt;element name="phone" type="{http://www.kohls.com/object/Loyalty/0.1/}phoneNumber" minOccurs="0"/&gt;
 *         &lt;element name="emails" type="{http://www.kohls.com/object/Loyalty/0.1/}emails" minOccurs="0"/&gt;
 *         &lt;element name="tier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="memberSinceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="attributes" type="{http://www.kohls.com/object/Loyalty/0.1/}attributes" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loyaltyProfile", propOrder = {
    "loyaltyNbr",
    "storeNbr",
    "origin",
    "person",
    "address",
    "phone",
    "emails",
    "tier",
    "memberSinceDate",
    "attributes"
})
public class LoyaltyProfile {

    protected String loyaltyNbr;
    protected String storeNbr;
    protected String origin;
    protected Person person;
    protected Address address;
    protected PhoneNumber phone;
    protected Emails emails;
    protected String tier;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar memberSinceDate;
    protected Attributes attributes;

    /**
     * Gets the value of the loyaltyNbr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLoyaltyNbr() {
        return loyaltyNbr;
    }

    /**
     * Sets the value of the loyaltyNbr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLoyaltyNbr(String value) {
        this.loyaltyNbr = value;
    }

    /**
     * Gets the value of the storeNbr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStoreNbr() {
        return storeNbr;
    }

    /**
     * Sets the value of the storeNbr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStoreNbr(String value) {
        this.storeNbr = value;
    }

    /**
     * Gets the value of the origin property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Gets the value of the person property.
     *
     * @return
     *     possible object is
     *     {@link Person }
     *
     */
    public Person getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     *
     * @param value
     *     allowed object is
     *     {@link Person }
     *
     */
    public void setPerson(Person value) {
        this.person = value;
    }

    /**
     * Gets the value of the address property.
     *
     * @return
     *     possible object is
     *     {@link Address }
     *
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     *
     * @param value
     *     allowed object is
     *     {@link Address }
     *
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the phone property.
     *
     * @return
     *     possible object is
     *     {@link PhoneNumber }
     *
     */
    public PhoneNumber getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     *
     * @param value
     *     allowed object is
     *     {@link PhoneNumber }
     *
     */
    public void setPhone(PhoneNumber value) {
        this.phone = value;
    }

    /**
     * Gets the value of the emails property.
     *
     * @return
     *     possible object is
     *     {@link Emails }
     *
     */
    public Emails getEmails() {
        return emails;
    }

    /**
     * Sets the value of the emails property.
     *
     * @param value
     *     allowed object is
     *     {@link Emails }
     *
     */
    public void setEmails(Emails value) {
        this.emails = value;
    }

    /**
     * Gets the value of the tier property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTier() {
        return tier;
    }

    /**
     * Sets the value of the tier property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTier(String value) {
        this.tier = value;
    }

    /**
     * Gets the value of the memberSinceDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getMemberSinceDate() {
        return memberSinceDate;
    }

    /**
     * Sets the value of the memberSinceDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setMemberSinceDate(XMLGregorianCalendar value) {
        this.memberSinceDate = value;
    }

    /**
     * Gets the value of the attributes property.
     *
     * @return
     *     possible object is
     *     {@link Attributes }
     *
     */
    public Attributes getAttributes() {
        return attributes;
    }

    /**
     * Sets the value of the attributes property.
     *
     * @param value
     *     allowed object is
     *     {@link Attributes }
     *     
     */
    public void setAttributes(Attributes value) {
        this.attributes = value;
    }

}
