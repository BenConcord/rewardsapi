package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;

/**
 * @author Evgeni Stoykov
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "faultCode",
        "faultString",
        "faultActor"
})
@XmlRootElement(name = "SOAPFault")
public class SoapFault {

    @XmlElement(name = "faultcode")
    protected String faultCode;

    @XmlElement(name = "faultstring")
    protected String faultString;

    @XmlElement(name = "faultactor")
    protected String faultActor;

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public String getFaultString() {
        return faultString;
    }

    public void setFaultString(String faultString) {
        this.faultString = faultString;
    }

    public String getFaultActor() {
        return faultActor;
    }

    public void setFaultActor(String faultActor) {
        this.faultActor = faultActor;
    }
}
