
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for nonTransactions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nonTransactions"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nonTransaction" type="{http://www.kohls.com/object/Loyalty/0.1/}nonTransactionBase" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nonTransactions", propOrder = {
    "nonTransaction"
})
public class NonTransactions {

    protected List<NonTransactionBase> nonTransaction;

    /**
     * Gets the value of the nonTransaction property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nonTransaction property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNonTransaction().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NonTransactionBase }
     *
     *
     */
    public List<NonTransactionBase> getNonTransaction() {
        if (nonTransaction == null) {
            nonTransaction = new ArrayList<NonTransactionBase>();
        }
        return this.nonTransaction;
    }

}
