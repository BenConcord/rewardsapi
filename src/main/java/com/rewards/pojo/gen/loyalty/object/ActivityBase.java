
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for activityBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="activityBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="nonTransaction" type="{http://www.kohls.com/object/Loyalty/0.1/}nonTransactionBase" minOccurs="0"/&gt;
 *         &lt;element name="pointGifting" type="{http://www.kohls.com/object/Loyalty/0.1/}pointGiftingBase" minOccurs="0"/&gt;
 *         &lt;element name="transaction" type="{http://www.kohls.com/object/Loyalty/0.1/}transactionBase" minOccurs="0"/&gt;
 *         &lt;element name="transfer" type="{http://www.kohls.com/object/Loyalty/0.1/}transferBase" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "activityBase", propOrder = {
    "nonTransaction",
    "pointGifting",
    "transaction",
    "transfer"
})
public class ActivityBase {

    protected NonTransactionBase nonTransaction;
    protected PointGiftingBase pointGifting;
    protected TransactionBase transaction;
    protected TransferBase transfer;

    /**
     * Gets the value of the nonTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link NonTransactionBase }
     *     
     */
    public NonTransactionBase getNonTransaction() {
        return nonTransaction;
    }

    /**
     * Sets the value of the nonTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonTransactionBase }
     *     
     */
    public void setNonTransaction(NonTransactionBase value) {
        this.nonTransaction = value;
    }

    /**
     * Gets the value of the pointGifting property.
     * 
     * @return
     *     possible object is
     *     {@link PointGiftingBase }
     *     
     */
    public PointGiftingBase getPointGifting() {
        return pointGifting;
    }

    /**
     * Sets the value of the pointGifting property.
     * 
     * @param value
     *     allowed object is
     *     {@link PointGiftingBase }
     *     
     */
    public void setPointGifting(PointGiftingBase value) {
        this.pointGifting = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionBase }
     *     
     */
    public TransactionBase getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionBase }
     *     
     */
    public void setTransaction(TransactionBase value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the transfer property.
     * 
     * @return
     *     possible object is
     *     {@link TransferBase }
     *     
     */
    public TransferBase getTransfer() {
        return transfer;
    }

    /**
     * Sets the value of the transfer property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferBase }
     *     
     */
    public void setTransfer(TransferBase value) {
        this.transfer = value;
    }

}
