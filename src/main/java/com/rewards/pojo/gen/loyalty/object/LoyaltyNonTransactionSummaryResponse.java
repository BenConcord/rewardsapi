
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="payload" type="{http://www.kohls.com/object/Loyalty/0.1/}nonTransactionSummaryRespPayload" minOccurs="0"/&gt;
 *         &lt;element name="messages" type="{http://www.kohls.com/object/Loyalty/0.1/}messages" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "payload",
    "messages"
})
@XmlRootElement(name = "loyaltyNonTransactionSummaryResponse")
public class LoyaltyNonTransactionSummaryResponse {

    protected NonTransactionSummaryRespPayload payload;
    protected Messages messages;

    /**
     * Gets the value of the payload property.
     *
     * @return
     *     possible object is
     *     {@link NonTransactionSummaryRespPayload }
     *
     */
    public NonTransactionSummaryRespPayload getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     *
     * @param value
     *     allowed object is
     *     {@link NonTransactionSummaryRespPayload }
     *
     */
    public void setPayload(NonTransactionSummaryRespPayload value) {
        this.payload = value;
    }

    /**
     * Gets the value of the messages property.
     *
     * @return
     *     possible object is
     *     {@link Messages }
     *
     */
    public Messages getMessages() {
        return messages;
    }

    /**
     * Sets the value of the messages property.
     *
     * @param value
     *     allowed object is
     *     {@link Messages }
     *     
     */
    public void setMessages(Messages value) {
        this.messages = value;
    }

}
