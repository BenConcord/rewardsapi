
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}pointBalance" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pointBalance"
})
@XmlRootElement(name = "pointBalances")
public class PointBalances {

    @XmlElement(required = true)
    protected List<PointBalance> pointBalance;

    /**
     * Gets the value of the pointBalance property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pointBalance property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPointBalance().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PointBalance }
     *
     *
     */
    public List<PointBalance> getPointBalance() {
        if (pointBalance == null) {
            pointBalance = new ArrayList<PointBalance>();
        }
        return this.pointBalance;
    }

}
