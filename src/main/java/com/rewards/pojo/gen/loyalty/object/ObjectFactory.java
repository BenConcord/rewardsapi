
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.kohls.object.loyalty._0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Amount_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "Amount");
    private final static QName _Type_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "Type");
    private final static QName _IssueDate_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "IssueDate");
    private final static QName _ExpirationDate_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "ExpirationDate");
    private final static QName _NextThreshold_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "NextThreshold");
    private final static QName _CurrentTierDescription_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "CurrentTierDescription");
    private final static QName _LoyaltyID_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "LoyaltyID");
    private final static QName _NextTierMessage_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "NextTierMessage");
    private final static QName _ExternalTransactionId_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "externalTransactionId");
    private final static QName _LoyaltyNbr_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "loyaltyNbr");
    private final static QName _MostRecentLimit_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "mostRecentLimit");
    private final static QName _Referer_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "referer");
    private final static QName _TransactionBase_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "transactionBase");
    private final static QName _TransactionId_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "transactionId");
    private final static QName _TypeBase_QNAME = new QName("http://www.kohls.com/object/Loyalty/0.1/", "typeBase");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.kohls.object.loyalty._0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Balance }
     *
     */
    public Balance createBalance() {
        return new Balance();
    }

    /**
     * Create an instance of {@link Balances }
     *
     */
    public Balances createBalances() {
        return new Balances();
    }

    /**
     * Create an instance of {@link Loyalty }
     *
     */
    public Loyalty createLoyalty() {
        return new Loyalty();
    }

    /**
     * Create an instance of {@link LoyaltyBalancesRequest }
     *
     */
    public LoyaltyBalancesRequest createLoyaltyBalancesRequest() {
        return new LoyaltyBalancesRequest();
    }

    /**
     * Create an instance of {@link LoyaltyBalancesResponse }
     *
     */
    public LoyaltyBalancesResponse createLoyaltyBalancesResponse() {
        return new LoyaltyBalancesResponse();
    }

    /**
     * Create an instance of {@link Tier }
     *
     */
    public Tier createTier() {
        return new Tier();
    }

    /**
     * Create an instance of {@link DateRange }
     *
     */
    public DateRange createDateRange() {
        return new DateRange();
    }

    /**
     * Create an instance of {@link GetAccountStatusRequest }
     *
     */
    public GetAccountStatusRequest createGetAccountStatusRequest() {
        return new GetAccountStatusRequest();
    }

    /**
     * Create an instance of {@link GetAccountStatusResponse }
     *
     */
    public GetAccountStatusResponse createGetAccountStatusResponse() {
        return new GetAccountStatusResponse();
    }

    /**
     * Create an instance of {@link GetAccountStatusRespPayload }
     *
     */
    public GetAccountStatusRespPayload createGetAccountStatusRespPayload() {
        return new GetAccountStatusRespPayload();
    }

    /**
     * Create an instance of {@link Messages }
     *
     */
    public Messages createMessages() {
        return new Messages();
    }

    /**
     * Create an instance of {@link GetAvailableSubjectsRequest }
     *
     */
    public GetAvailableSubjectsRequest createGetAvailableSubjectsRequest() {
        return new GetAvailableSubjectsRequest();
    }

    /**
     * Create an instance of {@link SubjectIdType }
     *
     */
    public SubjectIdType createSubjectIdType() {
        return new SubjectIdType();
    }

    /**
     * Create an instance of {@link LocationValueType }
     *
     */
    public LocationValueType createLocationValueType() {
        return new LocationValueType();
    }

    /**
     * Create an instance of {@link GetAvailableSubjectsResponse }
     *
     */
    public GetAvailableSubjectsResponse createGetAvailableSubjectsResponse() {
        return new GetAvailableSubjectsResponse();
    }

    /**
     * Create an instance of {@link SubjectsRespPayload }
     *
     */
    public SubjectsRespPayload createSubjectsRespPayload() {
        return new SubjectsRespPayload();
    }

    /**
     * Create an instance of {@link LoyaltyAccountActivityRequest }
     *
     */
    public LoyaltyAccountActivityRequest createLoyaltyAccountActivityRequest() {
        return new LoyaltyAccountActivityRequest();
    }

    /**
     * Create an instance of {@link SearchDateTime }
     *
     */
    public SearchDateTime createSearchDateTime() {
        return new SearchDateTime();
    }

    /**
     * Create an instance of {@link RequestedTypes }
     *
     */
    public RequestedTypes createRequestedTypes() {
        return new RequestedTypes();
    }

    /**
     * Create an instance of {@link LoyaltyAccountActivityResponse }
     *
     */
    public LoyaltyAccountActivityResponse createLoyaltyAccountActivityResponse() {
        return new LoyaltyAccountActivityResponse();
    }

    /**
     * Create an instance of {@link AccountActivityRespPayload }
     *
     */
    public AccountActivityRespPayload createAccountActivityRespPayload() {
        return new AccountActivityRespPayload();
    }

    /**
     * Create an instance of {@link LoyaltyAvailableTransferAccountsRequest }
     *
     */
    public LoyaltyAvailableTransferAccountsRequest createLoyaltyAvailableTransferAccountsRequest() {
        return new LoyaltyAvailableTransferAccountsRequest();
    }

    /**
     * Create an instance of {@link LoyaltyAvailableTransferAccountsResponse }
     *
     */
    public LoyaltyAvailableTransferAccountsResponse createLoyaltyAvailableTransferAccountsResponse() {
        return new LoyaltyAvailableTransferAccountsResponse();
    }

    /**
     * Create an instance of {@link LoyaltyCreateEventRequest }
     *
     */
    public LoyaltyCreateEventRequest createLoyaltyCreateEventRequest() {
        return new LoyaltyCreateEventRequest();
    }

    /**
     * Create an instance of {@link CreateEventReqPayload }
     *
     */
    public CreateEventReqPayload createCreateEventReqPayload() {
        return new CreateEventReqPayload();
    }

    /**
     * Create an instance of {@link LoyaltyCreateEventResponse }
     *
     */
    public LoyaltyCreateEventResponse createLoyaltyCreateEventResponse() {
        return new LoyaltyCreateEventResponse();
    }

    /**
     * Create an instance of {@link LoyaltyMaintainAccountRequest }
     *
     */
    public LoyaltyMaintainAccountRequest createLoyaltyMaintainAccountRequest() {
        return new LoyaltyMaintainAccountRequest();
    }

    /**
     * Create an instance of {@link LoyaltyProfile }
     *
     */
    public LoyaltyProfile createLoyaltyProfile() {
        return new LoyaltyProfile();
    }

    /**
     * Create an instance of {@link LoyaltyMaintainAccountResponse }
     *
     */
    public LoyaltyMaintainAccountResponse createLoyaltyMaintainAccountResponse() {
        return new LoyaltyMaintainAccountResponse();
    }

    /**
     * Create an instance of {@link LoyaltyNonTransactionSummaryRequest }
     *
     */
    public LoyaltyNonTransactionSummaryRequest createLoyaltyNonTransactionSummaryRequest() {
        return new LoyaltyNonTransactionSummaryRequest();
    }

    /**
     * Create an instance of {@link LoyaltyNonTransactionSummaryResponse }
     *
     */
    public LoyaltyNonTransactionSummaryResponse createLoyaltyNonTransactionSummaryResponse() {
        return new LoyaltyNonTransactionSummaryResponse();
    }

    /**
     * Create an instance of {@link NonTransactionSummaryRespPayload }
     *
     */
    public NonTransactionSummaryRespPayload createNonTransactionSummaryRespPayload() {
        return new NonTransactionSummaryRespPayload();
    }

    /**
     * Create an instance of {@link LoyaltyPointBalancesRequest }
     *
     */
    public LoyaltyPointBalancesRequest createLoyaltyPointBalancesRequest() {
        return new LoyaltyPointBalancesRequest();
    }

    /**
     * Create an instance of {@link LoyaltyPointBalancesResponse }
     *
     */
    public LoyaltyPointBalancesResponse createLoyaltyPointBalancesResponse() {
        return new LoyaltyPointBalancesResponse();
    }

    /**
     * Create an instance of {@link PointBalancesRespPayload }
     *
     */
    public PointBalancesRespPayload createPointBalancesRespPayload() {
        return new PointBalancesRespPayload();
    }

    /**
     * Create an instance of {@link LoyaltyRewardsRequest }
     *
     */
    public LoyaltyRewardsRequest createLoyaltyRewardsRequest() {
        return new LoyaltyRewardsRequest();
    }

    /**
     * Create an instance of {@link LoyaltyRewardsResponse }
     *
     */
    public LoyaltyRewardsResponse createLoyaltyRewardsResponse() {
        return new LoyaltyRewardsResponse();
    }

    /**
     * Create an instance of {@link RewardsRespPayload }
     *
     */
    public RewardsRespPayload createRewardsRespPayload() {
        return new RewardsRespPayload();
    }

    /**
     * Create an instance of {@link LoyaltyTransactionDetailRequest }
     *
     */
    public LoyaltyTransactionDetailRequest createLoyaltyTransactionDetailRequest() {
        return new LoyaltyTransactionDetailRequest();
    }

    /**
     * Create an instance of {@link LoyaltyTransactionDetailResponse }
     *
     */
    public LoyaltyTransactionDetailResponse createLoyaltyTransactionDetailResponse() {
        return new LoyaltyTransactionDetailResponse();
    }

    /**
     * Create an instance of {@link TransactionDetailRespPayload }
     *
     */
    public TransactionDetailRespPayload createTransactionDetailRespPayload() {
        return new TransactionDetailRespPayload();
    }

    /**
     * Create an instance of {@link LoyaltyTransactionSummaryRequest }
     *
     */
    public LoyaltyTransactionSummaryRequest createLoyaltyTransactionSummaryRequest() {
        return new LoyaltyTransactionSummaryRequest();
    }

    /**
     * Create an instance of {@link LoyaltyTransactionSummaryResponse }
     *
     */
    public LoyaltyTransactionSummaryResponse createLoyaltyTransactionSummaryResponse() {
        return new LoyaltyTransactionSummaryResponse();
    }

    /**
     * Create an instance of {@link TransactionSummaryRespPayload }
     *
     */
    public TransactionSummaryRespPayload createTransactionSummaryRespPayload() {
        return new TransactionSummaryRespPayload();
    }

    /**
     * Create an instance of {@link PointBalance }
     *
     */
    public PointBalance createPointBalance() {
        return new PointBalance();
    }

    /**
     * Create an instance of {@link TypeBase }
     *
     */
    public TypeBase createTypeBase() {
        return new TypeBase();
    }

    /**
     * Create an instance of {@link PointBalances }
     *
     */
    public PointBalances createPointBalances() {
        return new PointBalances();
    }

    /**
     * Create an instance of {@link PointTier }
     *
     */
    public PointTier createPointTier() {
        return new PointTier();
    }

    /**
     * Create an instance of {@link PointTiers }
     *
     */
    public PointTiers createPointTiers() {
        return new PointTiers();
    }

    /**
     * Create an instance of {@link ReferAFriendRequest }
     *
     */
    public ReferAFriendRequest createReferAFriendRequest() {
        return new ReferAFriendRequest();
    }

    /**
     * Create an instance of {@link Referees }
     *
     */
    public Referees createReferees() {
        return new Referees();
    }

    /**
     * Create an instance of {@link ReferAFriendResponse }
     *
     */
    public ReferAFriendResponse createReferAFriendResponse() {
        return new ReferAFriendResponse();
    }

    /**
     * Create an instance of {@link RefereesRespPayload }
     *
     */
    public RefereesRespPayload createRefereesRespPayload() {
        return new RefereesRespPayload();
    }

    /**
     * Create an instance of {@link TransactionBase }
     *
     */
    public TransactionBase createTransactionBase() {
        return new TransactionBase();
    }

    /**
     * Create an instance of {@link TransferPointsRequest }
     *
     */
    public TransferPointsRequest createTransferPointsRequest() {
        return new TransferPointsRequest();
    }

    /**
     * Create an instance of {@link TransferPointsResponse }
     *
     */
    public TransferPointsResponse createTransferPointsResponse() {
        return new TransferPointsResponse();
    }

    /**
     * Create an instance of {@link ValidatePostalCodeRequest }
     *
     */
    public ValidatePostalCodeRequest createValidatePostalCodeRequest() {
        return new ValidatePostalCodeRequest();
    }

    /**
     * Create an instance of {@link ValidatePostalCodeResponse }
     *
     */
    public ValidatePostalCodeResponse createValidatePostalCodeResponse() {
        return new ValidatePostalCodeResponse();
    }

    /**
     * Create an instance of {@link Activities }
     *
     */
    public Activities createActivities() {
        return new Activities();
    }

    /**
     * Create an instance of {@link ActivityBase }
     *
     */
    public ActivityBase createActivityBase() {
        return new ActivityBase();
    }

    /**
     * Create an instance of {@link Address }
     *
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Attribute }
     *
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link Attributes }
     *
     */
    public Attributes createAttributes() {
        return new Attributes();
    }

    /**
     * Create an instance of {@link Country }
     *
     */
    public Country createCountry() {
        return new Country();
    }

    /**
     * Create an instance of {@link EmailAddress }
     *
     */
    public EmailAddress createEmailAddress() {
        return new EmailAddress();
    }

    /**
     * Create an instance of {@link Emails }
     *
     */
    public Emails createEmails() {
        return new Emails();
    }

    /**
     * Create an instance of {@link Message }
     *
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link NonTransactionBase }
     *
     */
    public NonTransactionBase createNonTransactionBase() {
        return new NonTransactionBase();
    }

    /**
     * Create an instance of {@link NonTransactions }
     *
     */
    public NonTransactions createNonTransactions() {
        return new NonTransactions();
    }

    /**
     * Create an instance of {@link Person }
     *
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link PhoneNumber }
     *
     */
    public PhoneNumber createPhoneNumber() {
        return new PhoneNumber();
    }

    /**
     * Create an instance of {@link PointGiftingBase }
     *
     */
    public PointGiftingBase createPointGiftingBase() {
        return new PointGiftingBase();
    }

    /**
     * Create an instance of {@link RefereeResp }
     *
     */
    public RefereeResp createRefereeResp() {
        return new RefereeResp();
    }

    /**
     * Create an instance of {@link RefereesResp }
     *
     */
    public RefereesResp createRefereesResp() {
        return new RefereesResp();
    }

    /**
     * Create an instance of {@link Reward }
     *
     */
    public Reward createReward() {
        return new Reward();
    }

    /**
     * Create an instance of {@link Rewards }
     *
     */
    public Rewards createRewards() {
        return new Rewards();
    }

    /**
     * Create an instance of {@link State }
     *
     */
    public State createState() {
        return new State();
    }

    /**
     * Create an instance of {@link Subject }
     *
     */
    public Subject createSubject() {
        return new Subject();
    }

    /**
     * Create an instance of {@link Subjects }
     *
     */
    public Subjects createSubjects() {
        return new Subjects();
    }

    /**
     * Create an instance of {@link TransactionItem }
     *
     */
    public TransactionItem createTransactionItem() {
        return new TransactionItem();
    }

    /**
     * Create an instance of {@link TransactionItems }
     *
     */
    public TransactionItems createTransactionItems() {
        return new TransactionItems();
    }

    /**
     * Create an instance of {@link Transactions }
     *
     */
    public Transactions createTransactions() {
        return new Transactions();
    }

    /**
     * Create an instance of {@link TransferBase }
     *
     */
    public TransferBase createTransferBase() {
        return new TransferBase();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "Amount")
    public JAXBElement<Long> createAmount(Long value) {
        return new JAXBElement<Long>(_Amount_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "Type")
    public JAXBElement<String> createType(String value) {
        return new JAXBElement<String>(_Type_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "IssueDate")
    public JAXBElement<XMLGregorianCalendar> createIssueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_IssueDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "ExpirationDate")
    public JAXBElement<XMLGregorianCalendar> createExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ExpirationDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "NextThreshold")
    public JAXBElement<Long> createNextThreshold(Long value) {
        return new JAXBElement<Long>(_NextThreshold_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "CurrentTierDescription")
    public JAXBElement<String> createCurrentTierDescription(String value) {
        return new JAXBElement<String>(_CurrentTierDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "LoyaltyID")
    public JAXBElement<String> createLoyaltyID(String value) {
        return new JAXBElement<String>(_LoyaltyID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "NextTierMessage")
    public JAXBElement<String> createNextTierMessage(String value) {
        return new JAXBElement<String>(_NextTierMessage_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "externalTransactionId")
    public JAXBElement<Integer> createExternalTransactionId(Integer value) {
        return new JAXBElement<Integer>(_ExternalTransactionId_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "loyaltyNbr")
    public JAXBElement<String> createLoyaltyNbr(String value) {
        return new JAXBElement<String>(_LoyaltyNbr_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "mostRecentLimit")
    public JAXBElement<Integer> createMostRecentLimit(Integer value) {
        return new JAXBElement<Integer>(_MostRecentLimit_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "referer")
    public JAXBElement<String> createReferer(String value) {
        return new JAXBElement<String>(_Referer_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionBase }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "transactionBase")
    public JAXBElement<TransactionBase> createTransactionBase(TransactionBase value) {
        return new JAXBElement<TransactionBase>(_TransactionBase_QNAME, TransactionBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "transactionId")
    public JAXBElement<String> createTransactionId(String value) {
        return new JAXBElement<String>(_TransactionId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeBase }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.kohls.com/object/Loyalty/0.1/", name = "typeBase")
    public JAXBElement<TypeBase> createTypeBase(TypeBase value) {
        return new JAXBElement<TypeBase>(_TypeBase_QNAME, TypeBase.class, null, value);
    }

}
