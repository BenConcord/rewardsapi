
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="balanceType" type="{http://www.kohls.com/object/Loyalty/0.1/}typeBase"/&gt;
 *         &lt;element name="balanceAmount" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="dateOfExpiration" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="pointsSetToExpire" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="rewardThresholdValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "balanceType",
    "balanceAmount",
    "dateOfExpiration",
    "pointsSetToExpire",
    "rewardThresholdValue"
})
@XmlRootElement(name = "pointBalance")
public class PointBalance {

    @XmlElement(required = true)
    protected TypeBase balanceType;
    protected int balanceAmount;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfExpiration;
    protected Integer pointsSetToExpire;
    protected Integer rewardThresholdValue;

    /**
     * Gets the value of the balanceType property.
     * 
     * @return
     *     possible object is
     *     {@link TypeBase }
     *     
     */
    public TypeBase getBalanceType() {
        return balanceType;
    }

    /**
     * Sets the value of the balanceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeBase }
     *     
     */
    public void setBalanceType(TypeBase value) {
        this.balanceType = value;
    }

    /**
     * Gets the value of the balanceAmount property.
     * 
     */
    public int getBalanceAmount() {
        return balanceAmount;
    }

    /**
     * Sets the value of the balanceAmount property.
     * 
     */
    public void setBalanceAmount(int value) {
        this.balanceAmount = value;
    }

    /**
     * Gets the value of the dateOfExpiration property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfExpiration() {
        return dateOfExpiration;
    }

    /**
     * Sets the value of the dateOfExpiration property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfExpiration(XMLGregorianCalendar value) {
        this.dateOfExpiration = value;
    }

    /**
     * Gets the value of the pointsSetToExpire property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPointsSetToExpire() {
        return pointsSetToExpire;
    }

    /**
     * Sets the value of the pointsSetToExpire property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPointsSetToExpire(Integer value) {
        this.pointsSetToExpire = value;
    }

    /**
     * Gets the value of the rewardThresholdValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRewardThresholdValue() {
        return rewardThresholdValue;
    }

    /**
     * Sets the value of the rewardThresholdValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRewardThresholdValue(Integer value) {
        this.rewardThresholdValue = value;
    }

}
