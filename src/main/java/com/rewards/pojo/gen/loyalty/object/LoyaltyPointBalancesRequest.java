
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}loyaltyNbr"/&gt;
 *         &lt;element name="requestedBalanceTypes" type="{http://www.kohls.com/object/Loyalty/0.1/}requestedTypes" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loyaltyNbr",
    "requestedBalanceTypes"
})
@XmlRootElement(name = "loyaltyPointBalancesRequest")
public class LoyaltyPointBalancesRequest {

    @XmlElement(required = true)
    protected String loyaltyNbr;
    protected RequestedTypes requestedBalanceTypes;

    /**
     * Gets the value of the loyaltyNbr property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLoyaltyNbr() {
        return loyaltyNbr;
    }

    /**
     * Sets the value of the loyaltyNbr property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLoyaltyNbr(String value) {
        this.loyaltyNbr = value;
    }

    /**
     * Gets the value of the requestedBalanceTypes property.
     *
     * @return
     *     possible object is
     *     {@link RequestedTypes }
     *
     */
    public RequestedTypes getRequestedBalanceTypes() {
        return requestedBalanceTypes;
    }

    /**
     * Sets the value of the requestedBalanceTypes property.
     *
     * @param value
     *     allowed object is
     *     {@link RequestedTypes }
     *     
     */
    public void setRequestedBalanceTypes(RequestedTypes value) {
        this.requestedBalanceTypes = value;
    }

}
