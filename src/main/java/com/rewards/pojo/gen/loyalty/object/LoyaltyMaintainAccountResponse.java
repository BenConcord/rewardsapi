
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="payload" type="{http://www.kohls.com/object/Loyalty/0.1/}loyaltyProfile"/&gt;
 *         &lt;element name="messages" type="{http://www.kohls.com/object/Loyalty/0.1/}messages" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "payload",
    "messages"
})
@XmlRootElement(name = "loyaltyMaintainAccountResponse")
public class LoyaltyMaintainAccountResponse {

    @XmlElement(required = true)
    protected LoyaltyProfile payload;
    protected Messages messages;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyProfile }
     *     
     */
    public LoyaltyProfile getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyProfile }
     *     
     */
    public void setPayload(LoyaltyProfile value) {
        this.payload = value;
    }

    /**
     * Gets the value of the messages property.
     * 
     * @return
     *     possible object is
     *     {@link Messages }
     *     
     */
    public Messages getMessages() {
        return messages;
    }

    /**
     * Sets the value of the messages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Messages }
     *     
     */
    public void setMessages(Messages value) {
        this.messages = value;
    }

}
