
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tierType" type="{http://www.kohls.com/object/Loyalty/0.1/}typeBase"/&gt;
 *         &lt;element name="currentTier" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nextTier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="neededForNextTier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tierType",
    "currentTier",
    "nextTier",
    "neededForNextTier"
})
@XmlRootElement(name = "pointTier")
public class PointTier {

    @XmlElement(required = true)
    protected TypeBase tierType;
    @XmlElement(required = true)
    protected String currentTier;
    protected String nextTier;
    protected String neededForNextTier;

    /**
     * Gets the value of the tierType property.
     * 
     * @return
     *     possible object is
     *     {@link TypeBase }
     *     
     */
    public TypeBase getTierType() {
        return tierType;
    }

    /**
     * Sets the value of the tierType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeBase }
     *     
     */
    public void setTierType(TypeBase value) {
        this.tierType = value;
    }

    /**
     * Gets the value of the currentTier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentTier() {
        return currentTier;
    }

    /**
     * Sets the value of the currentTier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentTier(String value) {
        this.currentTier = value;
    }

    /**
     * Gets the value of the nextTier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextTier() {
        return nextTier;
    }

    /**
     * Sets the value of the nextTier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextTier(String value) {
        this.nextTier = value;
    }

    /**
     * Gets the value of the neededForNextTier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNeededForNextTier() {
        return neededForNextTier;
    }

    /**
     * Sets the value of the neededForNextTier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNeededForNextTier(String value) {
        this.neededForNextTier = value;
    }

}
