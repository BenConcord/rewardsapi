
package com.rewards.pojo.gen.loyalty.object;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.kohls.com/object/Loyalty/0.1/}referer"/&gt;
 *         &lt;element name="referees" type="{http://www.kohls.com/object/Loyalty/0.1/}referees"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "referer",
    "referees"
})
@XmlRootElement(name = "referAFriendRequest")
public class ReferAFriendRequest {

    @XmlElement(required = true)
    protected String referer;
    @XmlElement(required = true)
    protected Referees referees;

    /**
     * Gets the value of the referer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferer() {
        return referer;
    }

    /**
     * Sets the value of the referer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferer(String value) {
        this.referer = value;
    }

    /**
     * Gets the value of the referees property.
     * 
     * @return
     *     possible object is
     *     {@link Referees }
     *     
     */
    public Referees getReferees() {
        return referees;
    }

    /**
     * Sets the value of the referees property.
     * 
     * @param value
     *     allowed object is
     *     {@link Referees }
     *     
     */
    public void setReferees(Referees value) {
        this.referees = value;
    }

}
