package com.rewards.session;

import com.rewards.pojo.gen.loyaltylab.AuthenticationResult;

public final class Authentication {

    public static AuthenticationResult auth = new AuthenticationResult();

    private Authentication() {}

    public static void set(AuthenticationResult a) {
        auth = a;
    }
}
