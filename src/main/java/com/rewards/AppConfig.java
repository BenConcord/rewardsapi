package com.rewards;

import com.rewards.pojo.gen.loyalty.service.KohlsLoyaltyOpenAPI;
import com.rewards.pojo.gen.loyaltylab.KohlsAPISoap;
import com.rewards.pojo.gen.loyaltylab.LoyaltyLabAPISoap;
import com.rewards.properties.IntegrationServerProperties;
import com.rewards.properties.integrationserver.Jms;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.camel.component.ActiveMQConfiguration;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.Component;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.DataFormat;
import org.apache.camel.component.mock.MockComponent;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    private final IntegrationServerProperties integrationServerProperties;
    private final CamelContext camelContext;

    @Autowired
    public AppConfig(CamelContext camelContext, IntegrationServerProperties integrationServerProperties,
                     @Value("${app.enableActiveMQ}") boolean enableActiveMQ) {
        this.camelContext = camelContext;
        this.integrationServerProperties = integrationServerProperties;
        camelContext.setStreamCaching(true);
        Component mqComponent = enableActiveMQ ?
                new ActiveMQComponent(jmsConfiguration(integrationServerProperties.getJms())) : new MockComponent();
        mqComponent.setCamelContext(camelContext);
        camelContext.addComponent("mq", mqComponent);
    }

    //Active MQ
    private ActiveMQConfiguration jmsConfiguration(Jms jmsConfig) {
        ActiveMQConfiguration activeMQConfiguration = new ActiveMQConfiguration();
        activeMQConfiguration.setConnectionFactory(pooledConnectionFactory(jmsConfig));
        activeMQConfiguration.setTransacted(false);
        activeMQConfiguration.setConcurrentConsumers(jmsConfig.getConcurrentConsumers());
        return activeMQConfiguration;
    }

    private PooledConnectionFactory pooledConnectionFactory(Jms jmsConfig) {
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setMaxConnections(jmsConfig.getMaxConnections());
        pooledConnectionFactory.setMaximumActiveSessionPerConnection(jmsConfig.getMaximumActiveSessionPerConnection());
        pooledConnectionFactory.setConnectionFactory(activeMqConnectionFactory(jmsConfig.getBrokerUrl()));
        return pooledConnectionFactory;
    }

    private ActiveMQConnectionFactory activeMqConnectionFactory(String brokerUrl) {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);
        return activeMQConnectionFactory;
    }

    //CXF
    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public ServletRegistrationBean cxfServlet(@Value("${app.servletMappingUri}") String servletMappingUri) {
        return new ServletRegistrationBean(new CXFServlet(), servletMappingUri);
    }

    @Bean
    public CxfEndpoint openApiEndpoint(@Value("${app.openApiEndpointUri}") String openApiEndpointUri) {
        CxfEndpoint endpoint = new CxfEndpoint();
        endpoint.setServiceClass(KohlsLoyaltyOpenAPI.class);
        endpoint.setAddress(openApiEndpointUri);
        endpoint.setBus(springBus());
        endpoint.setDataFormat(DataFormat.POJO);
        return endpoint;
    }

    @Bean
    public CxfEndpoint kohlsApiEndpoint() {
        CxfEndpoint endpoint = new CxfEndpoint();
        endpoint.setServiceClass(KohlsAPISoap.class);
        endpoint.setAddress(integrationServerProperties.getKohlsApi());
        endpoint.setBus(springBus());
        endpoint.setDataFormat(DataFormat.POJO);
        return endpoint;
    }

    @Bean
    public CxfEndpoint loyaltyLabApiEndpoint() {
        CxfEndpoint endpoint = new CxfEndpoint();
        endpoint.setServiceClass(LoyaltyLabAPISoap.class);
        endpoint.setAddress(integrationServerProperties.getLoyaltyLabApi());
        endpoint.setBus(springBus());
        endpoint.setDataFormat(DataFormat.POJO);
        return endpoint;
    }

}
