package com.rewards.camel.exception;

import com.rewards.exception.RewardsApiException;

/**
 * @author Evgeni Stoykov
 */
public class SoapHeaderException extends RewardsApiException {

    private static final long serialVersionUID = 1L;

    public SoapHeaderException(String message) {
        super(message);
    }

    public SoapHeaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public SoapHeaderException(Throwable cause) {
        super(cause);
    }
}
