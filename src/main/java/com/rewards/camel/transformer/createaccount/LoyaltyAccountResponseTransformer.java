package com.rewards.camel.transformer.createaccount;

import com.rewards.camel.constants.Properties;
import com.rewards.pojo.gen.loyalty.object.LoyaltyMaintainAccountRequest;
import com.rewards.pojo.gen.loyalty.object.LoyaltyMaintainAccountResponse;
import com.rewards.pojo.gen.loyalty.object.LoyaltyProfile;
import com.rewards.pojo.gen.loyaltylab.CreateAndScoreShopperResponse;
import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

/**
 * @author Evgeni Stoykov
 */
@Component
public class LoyaltyAccountResponseTransformer {

    public static final String NAME = "loyaltyAccountResponseTransformer";

    public LoyaltyMaintainAccountResponse transform(Exchange exchange) {
        LoyaltyMaintainAccountResponse response = new LoyaltyMaintainAccountResponse();
        LoyaltyMaintainAccountRequest loyaltyMaintainAccountRequest = exchange.getProperty(Properties.INPUT_BODY, LoyaltyMaintainAccountRequest.class);

        CreateAndScoreShopperResponse scoreShopperResponse =
                exchange.getProperty(Properties.CREATE_AND_SCORE_SHOPPER, CreateAndScoreShopperResponse.class);

        LoyaltyProfile payload = new LoyaltyProfile();
        payload.setLoyaltyNbr(scoreShopperResponse.getCreateAndScoreShopperResult().getRetailerShopperId());
        payload.setStoreNbr(loyaltyMaintainAccountRequest.getPayload().getStoreNbr());
        payload.setOrigin(loyaltyMaintainAccountRequest.getPayload().getOrigin());
        payload.setPerson(loyaltyMaintainAccountRequest.getPayload().getPerson());
        payload.setAddress(loyaltyMaintainAccountRequest.getPayload().getAddress());
        payload.setPhone(loyaltyMaintainAccountRequest.getPayload().getPhone());
        payload.setEmails(loyaltyMaintainAccountRequest.getPayload().getEmails());
        payload.setTier(loyaltyMaintainAccountRequest.getPayload().getTier());
        payload.setMemberSinceDate(loyaltyMaintainAccountRequest.getPayload().getMemberSinceDate());
        payload.setAttributes(loyaltyMaintainAccountRequest.getPayload().getAttributes());
        response.setPayload(payload);

        return response;
    }

}
