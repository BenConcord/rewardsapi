package com.rewards.camel.constants;

/**
 * @author Evgeni Stoykov
 */
public final class ExchangeContext {

    private ExchangeContext() {}

    public static final String PROPERTY = "${exchangeProperty.%s}";

    public static final String BODY = "${body}";
}
