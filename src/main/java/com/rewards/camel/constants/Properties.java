package com.rewards.camel.constants;

public final class Properties {

    private Properties() {}

    public static final String APP_START_LOG = "ApplicationStartLog";

    public static final String APP_END_LOG = "ApplicationEndLog";

    public static final String APP_FAILED_LOG = "ApplicationFailedLog";

    /**
     * content based id for logging
     */
    public static final String BODY_CORRELATION_ID = "BodyCorrelationID";

    /**
     * generated Id for each processing
     */
    public static final String CORRELATION_ID = "CorrelationID";

    /**
     * content based id sent to JMS
     */
    public static final String JMS_CORRELATION_ID = "JMSCorrelationID";

    public static final String TRANSACTION_ID = "TransactionID";

    public static final String TIME_STAMP = "TimeStamp";

    public static final String ADDRESS_VAR = "addressVar";

    public static final String CREATE_AND_SCORE_SHOPPER = "CreateAndScoreShopper";

    public static final String SCORE_SHOPPER = "ScoreShopper";

    public static final String UPDATE_CUSTOM_ATTRIBUTES = "UpdateCustomAttributes";

    public static final String MESSAGE_HEADER = "MessageHeader";

    public static final String INPUT_BODY = "MessageBody";

    public static final String OPERATION_NAME = "OperationName";

    public static final String SOAP_HEADERS = "SoapHeaders";

    public static final String CREATE_EVENT_TYPE_CODE = "TypeCode";
}
