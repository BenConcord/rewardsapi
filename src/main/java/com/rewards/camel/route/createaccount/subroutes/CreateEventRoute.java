package com.rewards.camel.route.createaccount.subroutes;

import com.rewards.camel.constants.ExchangeContext;
import com.rewards.camel.constants.Properties;
import com.rewards.camel.processor.createaccount.createevent.CreateEventEnricherProcessor;
import com.rewards.camel.processor.logging.AppEndLoggingProcessor;
import com.rewards.camel.processor.logging.AppStartLoggingProcessor;
import com.rewards.pojo.CreateEventInput;
import com.rewards.pojo.gen.loyalty.object.Address;
import com.rewards.pojo.gen.loyalty.object.LoyaltyMaintainAccountRequest;
import com.rewards.pojo.gen.loyaltylab.CreateAndScoreShopperResponse;
import com.rewards.properties.JmsProperties;
import com.rewards.properties.LoyaltyOpenApiProperties;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.JmsException;
import org.springframework.stereotype.Component;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Optional;

@Component
public class CreateEventRoute extends RouteBuilder {

    private final LoyaltyOpenApiProperties loyaltyOpenApiProperties;

    private final JmsProperties jmsProperties;

    private final String routeLogLevel;

    @Autowired
    public CreateEventRoute(LoyaltyOpenApiProperties loyaltyOpenApiProperties,
                            JmsProperties jmsProperties,
                            @Value("${app.routeLogLevel}") String routeLogLevel) {
        this.loyaltyOpenApiProperties = loyaltyOpenApiProperties;
        this.jmsProperties = jmsProperties;
        this.routeLogLevel = routeLogLevel;
    }

    @Override
    public void configure() throws Exception {

        LoggingLevel routeLogLevel = LoggingLevel.valueOf(this.routeLogLevel.toUpperCase());

        onException(JmsException.class)
                .maximumRedeliveries(jmsProperties.getMaximumRedeliveries())
                .redeliveryDelay(jmsProperties.getRedeliveryDelay());

        from("direct:CreateEvent").routeId("CreateEventSubRoute")
                .process(exchange -> { // create input for Create Event processing
                    CreateEventInput input = new CreateEventInput();
                    CreateAndScoreShopperResponse scoreShopperResponse =
                            exchange.getProperty(Properties.CREATE_AND_SCORE_SHOPPER, CreateAndScoreShopperResponse.class);
                    input.setLoyaltyNumber(scoreShopperResponse.getCreateAndScoreShopperResult().getRetailerShopperId());

                    LoyaltyMaintainAccountRequest loyaltyMaintainAccountRequest
                            = exchange.getProperty(Properties.INPUT_BODY, LoyaltyMaintainAccountRequest.class);
                    XMLGregorianCalendar dateOfBirth = loyaltyMaintainAccountRequest.getPayload().getPerson().getDateOfBirth();

                    input.setDateOfBirth(false);
                    Optional.ofNullable(dateOfBirth).ifPresent(i -> input.setDateOfBirth(true));

                    Address address = loyaltyMaintainAccountRequest.getPayload().getAddress();
                    input.setAddress(isAddressValid(address));

                    //JMS CorrelationID set for the following JMS calls
                    exchange.setProperty(Properties.JMS_CORRELATION_ID,
                            scoreShopperResponse.getCreateAndScoreShopperResult().getRetailerShopperId());
                    exchange.getIn().setBody(input);
                })
                .multicast().parallelProcessing(true).to("direct:CreateEventDoB", "direct:CreateEventAddress");

        from("direct:CreateEventDoB").routeId("CreateEventDoBSubRoute")
                .choice()
                .when(exchange -> exchange.getIn().getBody(CreateEventInput.class).getAddress())
                .process(exchange -> {
                    exchange.setProperty(Properties.CREATE_EVENT_TYPE_CODE,
                            loyaltyOpenApiProperties.getServices().getCreateAccount().getEventReferenceTagDob());
                })
                .process(CreateEventEnricherProcessor.NAME)
                .process(AppStartLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_START_LOG))
                .setHeader("JMSCorrelationID", simple(String.format(ExchangeContext.PROPERTY, Properties.JMS_CORRELATION_ID)))
                .setHeader("JMSExpiration", simple(String.valueOf(jmsProperties.getExpirationInSeconds())))
                .to("mq:queue:{{jms.createEventDestination}}?" +
                        "priority=" + jmsProperties.getPriority() +
                        "&deliveryMode=" + jmsProperties.getDeliveryMode())
                .log(routeLogLevel, ExchangeContext.BODY)
                .process(AppEndLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_END_LOG))
                .endChoice()
                .end();

        from("direct:CreateEventAddress").routeId("CreateEventAddressSubRoute")
                .choice()
                .when(exchange -> exchange.getIn().getBody(CreateEventInput.class).getDateOfBirth())
                .process(exchange -> {
                    exchange.setProperty(Properties.CREATE_EVENT_TYPE_CODE,
                            loyaltyOpenApiProperties.getServices().getCreateAccount().getEventReferenceTagAddress());
                })
                .process(CreateEventEnricherProcessor.NAME)
                .process(AppStartLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_START_LOG))
                .setHeader("JMSCorrelationID", simple(String.format(ExchangeContext.PROPERTY, Properties.JMS_CORRELATION_ID)))
                .setHeader("JMSExpiration", simple(String.valueOf(jmsProperties.getExpirationInSeconds())))
                .to("mq:queue:{{jms.createEventDestination}}?deliveryMode=" + jmsProperties.getDeliveryMode())
                .log(routeLogLevel, ExchangeContext.BODY)
                .process(AppEndLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_END_LOG))
                .endChoice()
                .end();
    }

    private boolean isAddressValid(Address address) {
        return Optional.ofNullable(address)
                .filter(addr -> addr.getLine1() != null && addr.getLine1().trim().length() > 0)
                .filter(addr -> addr.getCity() != null && addr.getCity().trim().length() > 0)
                .filter(addr -> addr.getPostalCode() != null && addr.getPostalCode().trim().length() > 0)
                .isPresent();
    }
}
