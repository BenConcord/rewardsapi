package com.rewards.camel.route.createaccount;

import com.rewards.camel.constants.ExchangeContext;
import com.rewards.camel.constants.Properties;
import com.rewards.camel.processor.auth.AuthenticationProcessor;
import com.rewards.camel.processor.common.MessageHeaderProcessor;
import com.rewards.camel.processor.createaccount.CreateAndScoreShopperProcessor;
import com.rewards.camel.processor.createaccount.MapAttributesProcessor;
import com.rewards.camel.processor.createaccount.UpdateCustomAttributesAndScoreShopperProcessor;
import com.rewards.camel.processor.exception.*;
import com.rewards.camel.processor.logging.AppEndLoggingProcessor;
import com.rewards.camel.processor.logging.AppFailedLoggingProcessor;
import com.rewards.camel.processor.logging.AppStartLoggingProcessor;
import com.rewards.camel.transformer.createaccount.LoyaltyAccountResponseTransformer;
import com.rewards.pojo.gen.loyalty.object.LoyaltyMaintainAccountRequest;
import com.rewards.pojo.gen.loyaltylab.CreateAndScoreShopperResponse;
import com.rewards.pojo.gen.loyaltylab.Shopper;
import com.rewards.properties.LoyaltyOpenApiProperties;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.transport.http.HTTPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

@Component
public class CreateAccountRoute extends RouteBuilder {

    private final LoyaltyOpenApiProperties apiProperties;

    private final CxfEndpoint kohlsApiEndpoint;

    private final String routeLogLevel;

    @Autowired
    public CreateAccountRoute(LoyaltyOpenApiProperties apiProperties,
                              @Qualifier("kohlsApiEndpoint") CxfEndpoint kohlsApiEndpoint,
                              @Value("${app.routeLogLevel}") String routeLogLevel) {
        this.apiProperties = apiProperties;
        this.kohlsApiEndpoint = kohlsApiEndpoint;
        this.routeLogLevel = routeLogLevel;
    }

    @Override
    public void configure() throws Exception {

        LoggingLevel routeLogLevel = LoggingLevel.valueOf(this.routeLogLevel.toUpperCase());

        onException(HttpOperationFailedException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(HttpFailedExceptionProcessor.NAME);

        onException(HTTPException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(HttpExceptionProcessor.NAME);

        onException(ConnectException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(ConnectExceptionProcessor.NAME);

        onException(SocketTimeoutException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(TimeoutExceptionProcessor.NAME);

        onException(SoapFault.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(FaultProcessor.NAME);

        onException(Exception.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(ExceptionProcessor.NAME);

        from("direct:CreateAccount").routeId("CreateAccountRoute")
                .choice()
                .when(exchange -> apiProperties.getServices().getCreateAccount().isEnableService())
                    //set content based correlation Id for logging
                    .process(exchange -> {
                        String loyaltyNbr = exchange
                                .getProperty(Properties.INPUT_BODY, LoyaltyMaintainAccountRequest.class)
                                .getPayload().getLoyaltyNbr();

                        exchange.setProperty(Properties.BODY_CORRELATION_ID,
                                StringUtils.isEmpty(loyaltyNbr) ? null : loyaltyNbr);
                    })
                    .process(AppStartLoggingProcessor.NAME)
                    .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_START_LOG))
                    .process(MapAttributesProcessor.NAME)
                    .log(routeLogLevel, String.format(ExchangeContext.PROPERTY, Properties.ADDRESS_VAR))
                    //auth
                    .process(AuthenticationProcessor.NAME)
                    //prepare CreateAndScore operation request
                    .process(CreateAndScoreShopperProcessor.NAME)
                    .log(routeLogLevel, ExchangeContext.BODY)
                    // call operation CreateAndScoreShopper
                    .to(kohlsApiEndpoint)
                    .process(exchange -> {
                        //handles output
                        List responseBody = exchange.getIn().getBody(List.class);
                        Shopper shopper = (Shopper) responseBody.get(0);
                        //logs shopper id response
                        log.debug("Response ShopperId: {}", shopper.getShopperId());
                        //response wrapper
                        CreateAndScoreShopperResponse createAndScoreShopperResponse = new CreateAndScoreShopperResponse();
                        createAndScoreShopperResponse.setCreateAndScoreShopperResult(shopper);
                        exchange.setProperty(Properties.CREATE_AND_SCORE_SHOPPER, createAndScoreShopperResponse);
                    })
                    //creates updateCustomAttributes and score shopper
                    .process(UpdateCustomAttributesAndScoreShopperProcessor.NAME)
                    .log(routeLogLevel, String.format(ExchangeContext.PROPERTY, Properties.CREATE_AND_SCORE_SHOPPER))
                    .log(routeLogLevel, String.format(ExchangeContext.PROPERTY, Properties.UPDATE_CUSTOM_ATTRIBUTES))
                    .log(routeLogLevel, String.format(ExchangeContext.PROPERTY, Properties.SCORE_SHOPPER))
                    .wireTap("direct:InvokeUpdateCustomAttributes").copy()
                    .wireTap("direct:InvokeScoreShopper").copy()
                    .wireTap("direct:CreateEvent").copy()
                    // transform output
                    .transform().method(LoyaltyAccountResponseTransformer.NAME)
                    // set output headers
                    .process(MessageHeaderProcessor.NAME)
                    //logs output operation
                    .log(routeLogLevel, ExchangeContext.BODY)
                    .process(AppEndLoggingProcessor.NAME)
                    .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_END_LOG))
                    .endChoice()
                .otherwise().to("direct:DisabledCreateAccountService")
                .end();
    }

}
