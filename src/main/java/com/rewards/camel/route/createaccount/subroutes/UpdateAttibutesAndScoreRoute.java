package com.rewards.camel.route.createaccount.subroutes;


import com.rewards.camel.constants.ExchangeContext;
import com.rewards.camel.constants.Properties;
import com.rewards.camel.processor.auth.AuthenticationProcessor;
import com.rewards.camel.processor.exception.*;
import com.rewards.camel.processor.logging.AppEndLoggingProcessor;
import com.rewards.camel.processor.logging.AppFailedLoggingProcessor;
import com.rewards.camel.processor.logging.AppStartLoggingProcessor;
import com.rewards.pojo.gen.loyaltylab.ArrayOfCustomAttributeParameter;
import com.rewards.pojo.gen.loyaltylab.ScoreShopper;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.transport.http.HTTPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

@Component
public class UpdateAttibutesAndScoreRoute extends RouteBuilder {

    private final AuthenticationProcessor authenticationProcessor;

    private final CxfEndpoint loyaltyLabApiEndpoint;

    private final String routeLogLevel;

    @Autowired
    public UpdateAttibutesAndScoreRoute(AuthenticationProcessor authenticationProcessor,
                                        @Qualifier("loyaltyLabApiEndpoint") CxfEndpoint loyaltyLabApiEndpoint,
                                        @Value("${app.routeLogLevel}") String routeLogLevel) {
        this.authenticationProcessor = authenticationProcessor;
        this.loyaltyLabApiEndpoint = loyaltyLabApiEndpoint;
        this.routeLogLevel = routeLogLevel;
    }

    @Override
    public void configure() throws Exception {

        LoggingLevel routeLogLevel = LoggingLevel.valueOf(this.routeLogLevel.toUpperCase());

        onException(HttpOperationFailedException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(HttpFailedExceptionProcessor.NAME);

        onException(HTTPException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(HttpExceptionProcessor.NAME);

        onException(ConnectException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(ConnectExceptionProcessor.NAME);

        onException(SocketTimeoutException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(TimeoutExceptionProcessor.NAME);

        onException(SoapFault.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(FaultProcessor.NAME);

        onException(Exception.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(ExceptionProcessor.NAME);

        from("direct:InvokeUpdateCustomAttributes").routeId("UpdateCustomAttributesSubRoute")
                .process(authenticationProcessor)
                .process(exchange -> {
                    ArrayOfCustomAttributeParameter updateCustomAttributes = exchange.getProperty(Properties.UPDATE_CUSTOM_ATTRIBUTES, ArrayOfCustomAttributeParameter.class);
                    exchange.getIn().setBody(updateCustomAttributes);
                    exchange.getIn().setHeader(CxfConstants.OPERATION_NAME, "UpdateCustomAttributes");

                })
                .process(AppStartLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_START_LOG))
                .to(loyaltyLabApiEndpoint)
                .log(routeLogLevel, ExchangeContext.BODY)
                .process(AppEndLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_END_LOG));

        from("direct:InvokeScoreShopper").routeId("UpdateScoreShopperSubRoute")
                .process(authenticationProcessor)
                .process(exchange -> {
                    ScoreShopper scoreShopper = exchange.getProperty(Properties.SCORE_SHOPPER, ScoreShopper.class);
                    exchange.getIn().setBody(scoreShopper.getShopperId());
                    exchange.getIn().setHeader(CxfConstants.OPERATION_NAME, "ScoreShopper");
                }) //missing start logging because the body is Integer
                .to(loyaltyLabApiEndpoint)
                //missing end logging because there is no output
                .log(routeLogLevel, ExchangeContext.BODY);
    }

}
