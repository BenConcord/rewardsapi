package com.rewards.camel.route.createaccount.subroutes;

import com.rewards.camel.constants.ExchangeContext;
import com.rewards.camel.processor.common.MessageHeaderProcessor;
import com.rewards.pojo.gen.loyalty.object.LoyaltyMaintainAccountRequest;
import com.rewards.pojo.gen.loyalty.object.LoyaltyMaintainAccountResponse;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DisabledCreateAccountServiceRoute extends RouteBuilder {

    private final String routeLogLevel;

    public DisabledCreateAccountServiceRoute(@Value("${app.routeLogLevel}") String routeLogLevel) {
        this.routeLogLevel = routeLogLevel;
    }

    @Override
    public void configure() throws Exception {

        LoggingLevel routeLogLevel = LoggingLevel.valueOf(this.routeLogLevel.toUpperCase());

        from("direct:DisabledCreateAccountService").routeId("DisabledCreateAccountServiceRoute")
                .process(MessageHeaderProcessor.NAME)
                .process(exchange -> {
                    LoyaltyMaintainAccountResponse response = new LoyaltyMaintainAccountResponse();
                    response.setPayload(exchange.getIn().getBody(LoyaltyMaintainAccountRequest.class).getPayload());
                    exchange.getIn().setBody(response);
                })
                .log(routeLogLevel, ExchangeContext.BODY);
    }
}
