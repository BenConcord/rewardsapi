package com.rewards.camel.route.auth;

import com.rewards.camel.constants.ExchangeContext;
import com.rewards.camel.constants.Properties;
import com.rewards.camel.processor.exception.*;
import com.rewards.camel.processor.logging.AppFailedLoggingProcessor;
import com.rewards.pojo.gen.loyaltylab.AuthenticationResult;
import com.rewards.properties.LoyaltyOpenApiProperties;
import com.rewards.session.Authentication;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.transport.http.HTTPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.Arrays;

@Component
public class AuthenticationRoute extends RouteBuilder {

    private final LoyaltyOpenApiProperties loyaltyOpenApiProperties;

    private final CxfEndpoint loyaltyLabApiEndpoint;

    private final String routeLogLevel;

    @Autowired
    public AuthenticationRoute(LoyaltyOpenApiProperties loyaltyOpenApiProperties,
                               @Qualifier("loyaltyLabApiEndpoint") CxfEndpoint loyaltyLabApiEndpoint,
                               @Value("${app.routeLogLevel}") String routeLogLevel) {
        this.loyaltyOpenApiProperties = loyaltyOpenApiProperties;
        this.loyaltyLabApiEndpoint = loyaltyLabApiEndpoint;
        this.routeLogLevel = routeLogLevel;
    }

    @Override
    public void configure() throws Exception {

        LoggingLevel routeLogLevel = LoggingLevel.valueOf(this.routeLogLevel.toUpperCase());

        onException(HttpOperationFailedException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(HttpFailedExceptionProcessor.NAME);

        onException(HTTPException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(HttpExceptionProcessor.NAME);

        onException(ConnectException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(ConnectExceptionProcessor.NAME);

        onException(SocketTimeoutException.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(TimeoutExceptionProcessor.NAME);

        onException(SoapFault.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(FaultProcessor.NAME);

        onException(Exception.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(ExceptionProcessor.NAME);

        from("timer:GetAuthentication?delay=0&period=" +
                loyaltyOpenApiProperties.getServices().getSessionManagement().getInterval())
                .startupOrder(1)
                .routeId("AuthenticationGetTokenRoute")
                .process(exchange -> {
                    exchange.getOut().setHeader(CxfConstants.OPERATION_NAME, "AuthenticateUser");
                    exchange.getOut().setBody(Arrays.asList(loyaltyOpenApiProperties.getUsername(), loyaltyOpenApiProperties.getPassword()));
                }).to(loyaltyLabApiEndpoint)
                .log(routeLogLevel, "Authentication process completed!")
                .process(exchange -> Authentication.set(exchange.getIn().getBody(AuthenticationResult.class)));
    }
}
