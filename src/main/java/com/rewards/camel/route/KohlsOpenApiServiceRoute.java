package com.rewards.camel.route;

import com.rewards.camel.constants.ExchangeContext;
import com.rewards.camel.constants.Properties;
import com.rewards.camel.processor.common.ExchangeInitialBodyProcessor;
import com.rewards.camel.processor.common.InitializeCorrelationIdProcessor;
import com.rewards.camel.processor.exception.ExceptionProcessor;
import com.rewards.camel.processor.logging.AppFailedLoggingProcessor;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class KohlsOpenApiServiceRoute extends RouteBuilder {

    private static final LoggingLevel ROUTE_LOG_LEVEL = LoggingLevel.INFO;

    private final CxfEndpoint openApiEndpoint;

    @Autowired
    public KohlsOpenApiServiceRoute(@Qualifier("openApiEndpoint") CxfEndpoint openApiEndpoint) {
        this.openApiEndpoint = openApiEndpoint;
    }

    @Override
    public void configure() throws Exception {

        onException(Exception.class)
                .handled(true)
                .process(AppFailedLoggingProcessor.NAME)
                .log(LoggingLevel.INFO, String.format(ExchangeContext.PROPERTY, Properties.APP_FAILED_LOG))
                .process(ExceptionProcessor.NAME);

        //TODO: check defaultFaultElement in TIBCO
        //TODO: validate error handling

        from(openApiEndpoint).routeId("KohlsOpenApiServiceRoute")
                .log(ROUTE_LOG_LEVEL, "Service request received for operations: ${header.operationName}")
                .process(ExchangeInitialBodyProcessor.NAME)
                .process(InitializeCorrelationIdProcessor.NAME)
                .recipientList(simple("direct:${header.operationName}"));
    }
}
