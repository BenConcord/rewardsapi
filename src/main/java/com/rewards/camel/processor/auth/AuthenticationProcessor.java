package com.rewards.camel.processor.auth;

import com.rewards.pojo.gen.loyaltylab.AuthenticationResult;
import com.rewards.session.Authentication;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.headers.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.Collections;

@Component
public class AuthenticationProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationProcessor.class);

    public static final String NAME = "authenticationProcessor";

    private static final String XML_STRING_HEADER = "<loy:AuthenticationResult xmlns=\"http://www.loyaltylab.com/loyaltyapi/\">" +
            "<RetailerGuid>%s</RetailerGuid>" +
            "<Authenticated>%s</Authenticated>" +
            "<Token>%s</Token>" +
            "<ICSUserID>%s</ICSUserID>" +
            "</loy:AuthenticationResult>";

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("Process: {} Authentication", exchange.getUnitOfWork().getRouteContext().getRoute().getId());

        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        AuthenticationResult auth = Authentication.auth;

        String xml = String.format(XML_STRING_HEADER, auth.getRetailerGuid(), auth.isAuthenticated(), auth.getToken(), auth.getICSUserID());

        try (StringReader sr = new StringReader(xml)) {
            InputSource inputSource = new InputSource(sr);
            Document header = db.parse(inputSource);

            QName qName = new QName("com.loyaltylab.loyaltyapi", "AuthenticationResult", "loy");
            SoapHeader authHeader = new SoapHeader(qName, header.getDocumentElement());
            authHeader.setDirection(Header.Direction.DIRECTION_INOUT);
            exchange.getOut().setHeader(Header.HEADER_LIST, Collections.singletonList(authHeader));
        }
    }
}
