package com.rewards.camel.processor.logging;

import com.rewards.camel.constants.Properties;
import com.rewards.pojo.logging.AppLogHeader;
import com.rewards.pojo.logging.AppLogMessage;
import com.rewards.pojo.logging.AppLogRequest;
import com.rewards.util.JaxbUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * @author Evgeni Stoykov
 */
@Component
public class AppFailedLoggingProcessor implements Processor {

    public static final String NAME = "appFailedLoggingProcessor";

    @Override
    public void process(Exchange exchange) throws Exception {
        AppLogRequest logRequest = new AppLogRequest();

        //log header
        AppLogHeader logHeader = new AppLogHeader();
        logHeader.setOperationName(exchange.getProperty(Properties.OPERATION_NAME, String.class));
        logHeader.setTransactionId(exchange.getProperty(Properties.TRANSACTION_ID, String.class));
        Optional<String> optCorrelationId = Optional.ofNullable(exchange.getProperty(Properties.BODY_CORRELATION_ID, String.class));
        logHeader.setCorrelationId(optCorrelationId.orElseGet(() -> exchange.getProperty(Properties.CORRELATION_ID, String.class)));
        String requestInfo = new StringJoiner(" ")
                .add(exchange.getUnitOfWork().getRouteContext().getRoute().getId())
                .add("Request Failed for CorrelationId:")
                .add(logHeader.getCorrelationId()).toString();
        logHeader.setRequestInfo(requestInfo);
        logHeader.setTimeStamp(System.currentTimeMillis());
        logRequest.setHeader(logHeader);

        //log message
        AppLogMessage logMessage = new AppLogMessage();
        Object body = exchange.getIn().getBody(List.class).get(0);
        String message = JaxbUtils.objectToString(body, body.getClass());
        logMessage.setBody(message);
        logRequest.setMessage(logMessage);

        logRequest.setStatus("FAILED");
        logRequest.setExceptionCode(exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class).getMessage());
        StackTraceElement[] stackTrace = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class).getStackTrace();
        String stackTraceMessage = Arrays.toString(stackTrace);
        logRequest.setStackTrace(stackTraceMessage);
        logRequest.setProcessId(exchange.getUnitOfWork().getRouteContext().getRoute().getId());

        exchange.setProperty(Properties.APP_FAILED_LOG, logRequest);
    }
}
