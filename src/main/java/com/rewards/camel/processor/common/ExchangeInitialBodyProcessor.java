package com.rewards.camel.processor.common;

import com.rewards.camel.constants.Properties;
import com.rewards.camel.exception.SoapHeaderException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.util.CastUtils;
import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.headers.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ExchangeInitialBodyProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(ExchangeInitialBodyProcessor.class);

    public static final String NAME = "exchangeInitialBodyProcessor";

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("Move body to exchange property");

        exchange.setProperty(Properties.INPUT_BODY, exchange.getIn().getBody());

        List<SoapHeader> soapHeaders = CastUtils.cast((List<?>) exchange.getIn().getHeader(Header.HEADER_LIST));
        if (soapHeaders == null || soapHeaders.isEmpty()) {
            throw new SoapHeaderException("Missing soap input header");
        }

        exchange.setProperty(Properties.SOAP_HEADERS, soapHeaders);
    }
}
