package com.rewards.camel.processor.common;

import com.rewards.camel.constants.Properties;
import com.rewards.pojo.gen.message_header.From;
import com.rewards.pojo.gen.message_header.MessageHeader;
import com.rewards.util.DateTime;
import com.rewards.util.JaxbUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.headers.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Component
public class MessageHeaderProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(MessageHeaderProcessor.class);

    public static final String NAME = "messageHeaderProcessor";

    @Value("${app.systemCode}")
    private String systemCode;

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("Process: Message Headers (BW)");
        SoapHeader soapHeader = (SoapHeader) exchange.getProperty(Properties.SOAP_HEADERS, List.class).get(0);

        JAXBContext context = JAXBContext.newInstance(MessageHeader.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        MessageHeader messageHeader = unmarshaller.unmarshal((Element) soapHeader.getObject(), MessageHeader.class).getValue();

        From fromOut = new From();
        fromOut.setSystemCode(systemCode);
        fromOut.setApp(messageHeader.getFrom().getApp());
        fromOut.setModule(messageHeader.getFrom().getModule());
        fromOut.setNodeID(messageHeader.getFrom().getNodeID());

        messageHeader.setFrom(fromOut);
        messageHeader.setCreateDateTime(DateTime.toXmlGregorianCalendarDateTime(LocalDateTime.now()));

        SoapHeader outSoapHeader = JaxbUtils.objectToSoapHeader("urn:kohls:xml:schemas:message-header:v1_0",
                "MessageHeader", messageHeader, MessageHeader.class);

        exchange.getIn().setHeader(Header.HEADER_LIST, Collections.singletonList(outSoapHeader));
        exchange.setProperty(Properties.MESSAGE_HEADER, messageHeader);
    }
}
