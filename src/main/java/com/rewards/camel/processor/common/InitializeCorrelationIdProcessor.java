package com.rewards.camel.processor.common;

import com.rewards.camel.constants.Properties;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class InitializeCorrelationIdProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(InitializeCorrelationIdProcessor.class);

    public static final String NAME = "initializeCorrelationIdProcessor";

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("Process: InitializeCorrelationId");
        String correlationId = UUID.randomUUID().toString();
        String transactionId = UUID.randomUUID().toString();
        String operationName = exchange.getIn().getHeader(CxfConstants.OPERATION_NAME, String.class);

        String subCorrelationId = correlationId.substring(0, correlationId.length() / 2);
        exchange.setProperty(Properties.CORRELATION_ID, subCorrelationId);
        exchange.setProperty(Properties.TRANSACTION_ID, transactionId);
        exchange.setProperty(Properties.OPERATION_NAME, operationName);
        exchange.setProperty(Properties.TIME_STAMP, System.currentTimeMillis());

        log.info("Generated correlation Id: {}", subCorrelationId);
        log.info("Generated transaction Id: {}", transactionId);
        log.info("Operation Id: {}", operationName);

    }
}
