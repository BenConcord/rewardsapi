package com.rewards.camel.processor.createaccount.createevent;

import com.rewards.camel.constants.Properties;
import com.rewards.pojo.CreateEventInput;
import com.rewards.pojo.gen.loyalty.object.CreateEventReqPayload;
import com.rewards.pojo.gen.loyalty.object.LoyaltyCreateEventRequest;
import com.rewards.pojo.gen.loyalty.object.TypeBase;
import com.rewards.util.DateTime;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author Evgeni Stoykov
 */
@Component
public class CreateEventEnricherProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(CreateEventEnricherProcessor.class);

    public static final String NAME = "createEventEnricherProcessor";

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("Process: CreateEventEnricher");
        LoyaltyCreateEventRequest request = new LoyaltyCreateEventRequest();
        CreateEventReqPayload payload = new CreateEventReqPayload();
        TypeBase base = new TypeBase();

        CreateEventInput input = exchange.getIn().getBody(CreateEventInput.class);
        payload.setLoyaltyNbr(input.getLoyaltyNumber());
        payload.setEventId(1);
        base.setTypeCode(exchange.getProperty(Properties.CREATE_EVENT_TYPE_CODE, String.class));
        payload.setEventDateTime(DateTime.toXmlGregorianCalendarDateTime(LocalDateTime.now()));
        payload.setEventType(base);
        request.setPayload(payload);

        exchange.getIn().setBody(request);
    }
}
