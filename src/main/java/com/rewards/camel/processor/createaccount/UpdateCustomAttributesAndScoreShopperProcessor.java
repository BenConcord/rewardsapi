package com.rewards.camel.processor.createaccount;

import com.rewards.camel.constants.Properties;
import com.rewards.pojo.gen.loyalty.object.*;
import com.rewards.pojo.gen.loyaltylab.*;
import com.rewards.util.DateTime;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

@Component
public class UpdateCustomAttributesAndScoreShopperProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(UpdateCustomAttributesAndScoreShopperProcessor.class);

    public static final String NAME = "updateCustomAttributesAndScoreShopperProcessor";

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("Process: UpdateCustomAttributesAndScoreShopperProcessor");

        CreateAndScoreShopperResponse shopperResponse = exchange.getProperty(Properties
                .CREATE_AND_SCORE_SHOPPER, CreateAndScoreShopperResponse.class);

        ScoreShopper scoreShopper = new ScoreShopper();
        scoreShopper.setShopperId(shopperResponse.getCreateAndScoreShopperResult().getShopperId());
        exchange.setProperty(Properties.SCORE_SHOPPER, scoreShopper);

        LoyaltyMaintainAccountRequest loyaltyMaintainAccountRequest = exchange.getProperty(Properties.INPUT_BODY, LoyaltyMaintainAccountRequest.class);

        ArrayOfCustomAttributeParameter arrayOfCustomAttributeParameter = transform(shopperResponse, loyaltyMaintainAccountRequest);

        exchange.setProperty(Properties.UPDATE_CUSTOM_ATTRIBUTES, arrayOfCustomAttributeParameter);
    }

    private ArrayOfCustomAttributeParameter transform(CreateAndScoreShopperResponse shopper, LoyaltyMaintainAccountRequest loyaltyMaintainAccountRequest) {
        // Main object attributes
        ArrayOfCustomAttributeParameter customAttributeParameters = new ArrayOfCustomAttributeParameter();

        CustomAttributeParameter customAttributeParameter = new CustomAttributeParameter();
        customAttributeParameter.setReferenceId(shopper.getCreateAndScoreShopperResult().getShopperId());
        customAttributeParameter.setReferenceType("S"); // Hardcoded. Required by business

        // Add an item to the custom attributes/parameters
        customAttributeParameters.getCustomAttributeParameter().add(customAttributeParameter);

        ArrayOfAttributeToSet attributeToSet = new ArrayOfAttributeToSet();
        customAttributeParameter.setAttributesToSet(attributeToSet);

        // Extract the payload to avoid code repetition
        Optional<LoyaltyProfile> payload = Optional.ofNullable(loyaltyMaintainAccountRequest)
                .map(LoyaltyMaintainAccountRequest::getPayload);

        AttributeToSet countryAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getAddress)
                .map(Address::getCountry)
                .map(Country::getValue)
                .filter(countryValue -> !countryValue.isEmpty())
                .ifPresent(countryValue -> {
                    countryAttr.setValue(countryValue);
                    countryAttr.setReferenceTag("Country");
                    attributeToSet.getAttributeToSet().add(countryAttr);
                });

        AttributeToSet countryCodeAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getAddress)
                .map(Address::getCountry)
                .map(Country::getCode)
                .filter(countryCode -> !countryCode.isEmpty())
                .ifPresent(countryCode -> {
                    countryCodeAttr.setValue(countryCode);
                    countryCodeAttr.setReferenceTag("CountryCode");
                    attributeToSet.getAttributeToSet().add(countryCodeAttr);
                });

        AttributeToSet isKccAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getPerson)
                .ifPresent(person -> {
                    isKccAttr.setReferenceTag("IsKCC");
                    isKccAttr.setValue(person.isIsKCC() ? "Y" : "N");
                    attributeToSet.getAttributeToSet().add(isKccAttr);
                });

        AttributeToSet isAssociateAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getPerson)
                .ifPresent(person -> {
                    isAssociateAttr.setReferenceTag("AssociateIndicator");
                    isAssociateAttr.setValue(person.isIsAssociate() ? "Y" : "N");
                    attributeToSet.getAttributeToSet().add(isAssociateAttr);
                });

        payload.map(LoyaltyProfile::getAttributes)
                .map(Attributes::getAttribute)
                .map(attributeList -> attributeList.stream()
                        .filter(attribute -> Optional.of(attribute.getTypeBase())
                                .map(TypeBase::getTypeCode)
                                .map("KohlsComId"::equalsIgnoreCase)
                                .orElse(false))
                        .filter(attribute -> attribute.getAttributeValue() != null && attribute.getAttributeValue().length() > 0))
                .map(Stream::findFirst)
                .flatMap(Function.identity())
                .ifPresent(attribute -> {
                    AttributeToSet kohlsComIdAttr = new AttributeToSet();
                    kohlsComIdAttr.setReferenceTag("KohlsComId");
                    kohlsComIdAttr.setValue(attribute.getAttributeValue());
                    attributeToSet.getAttributeToSet().add(kohlsComIdAttr);
                });

        AttributeToSet addressLine3Attr = new AttributeToSet();
        payload.map(LoyaltyProfile::getAddress)
                .map(Address::getLine3)
                .ifPresent(line3 -> {
                    addressLine3Attr.setReferenceTag("AddressLine3");
                    addressLine3Attr.setValue(line3);
                    attributeToSet.getAttributeToSet().add(addressLine3Attr);
                });

        AttributeToSet birthDateAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getPerson)
                .map(Person::getDateOfBirth)
                .ifPresent(birthDate -> {
                    birthDateAttr.setReferenceTag("BirthDate");
                    birthDateAttr.setValue(DateTime.format(birthDate));
                    attributeToSet.getAttributeToSet().add(birthDateAttr);
                });

        AttributeToSet isTierValueAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getPerson)
                .ifPresent(person -> {
                    isTierValueAttr.setReferenceTag("TierValue");
                    if (person.isIsKCC()) {
                        isTierValueAttr.setValue(payload.map(LoyaltyProfile::getTier)
                                .filter(("mvc")::equalsIgnoreCase).map(String::toUpperCase)
                                .orElse("Kohl's Charge"));
                    } else {
                        isTierValueAttr.setValue("Member");
                    }
                    attributeToSet.getAttributeToSet().add(isTierValueAttr);
                });

        AttributeToSet tierDateAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getTier)
                .filter(tier -> !tier.isEmpty())
                .ifPresent(tier -> {
                    tierDateAttr.setReferenceTag("TierDate");
                    tierDateAttr.setValue(LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateTime.DATE_TIME_PATTERN)));
                    attributeToSet.getAttributeToSet().add(tierDateAttr);
                });

        AttributeToSet groupAttr = new AttributeToSet();
        groupAttr.setReferenceTag("Group");
        groupAttr.setValue("B");
        attributeToSet.getAttributeToSet().add(groupAttr);

        AttributeToSet storeNbrAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getStoreNbr)
                .filter(storeNbr -> !storeNbr.isEmpty())
                .ifPresent(storeNbr -> {
                    storeNbrAttr.setReferenceTag("EnrollmentStore");
                    storeNbrAttr.setValue(storeNbr);
                    attributeToSet.getAttributeToSet().add(storeNbrAttr);
                });

        AttributeToSet lLFirstEnrollmentAttr = new AttributeToSet();
        lLFirstEnrollmentAttr.setReferenceTag("LLFirstEnrollment");
        lLFirstEnrollmentAttr.setValue(
                payload.map(LoyaltyProfile::getMemberSinceDate)
                        .map(DateTime::format)  //if not present set LocalDateTime
                        .orElse(LocalDateTime.now().format(DateTimeFormatter.ofPattern("uuuu-MM-dd")))
        );
        attributeToSet.getAttributeToSet().add(lLFirstEnrollmentAttr);

        AttributeToSet profileCompletionDateAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getOrigin)
                .filter(origin -> origin.matches("[BDEGHRSTZKO]"))
                .ifPresent(origin -> {
                    profileCompletionDateAttr.setReferenceTag("ProfileCompletionDate");
                    profileCompletionDateAttr.setValue(LocalDateTime.now().format(DateTimeFormatter.ofPattern("uuuu-MM-dd")));
                    attributeToSet.getAttributeToSet().add(profileCompletionDateAttr);
                });

        AttributeToSet enrollmentOriginAttr = new AttributeToSet();
        payload.map(LoyaltyProfile::getOrigin)
                .filter(origin -> !origin.isEmpty())
                .ifPresent(origin -> {
                    enrollmentOriginAttr.setValue(origin);
                    enrollmentOriginAttr.setReferenceTag("EnrollmentOrigin");
                    attributeToSet.getAttributeToSet().add(enrollmentOriginAttr);
                });

        return customAttributeParameters;
    }

}
