package com.rewards.camel.processor.createaccount;

import com.rewards.camel.constants.Properties;
import com.rewards.pojo.gen.loyalty.object.Address;
import com.rewards.pojo.gen.loyalty.object.LoyaltyMaintainAccountRequest;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class MapAttributesProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(MapAttributesProcessor.class);

    public static final String NAME = "mapAttributesProcessor";

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("Process: mapAttributes");

        LoyaltyMaintainAccountRequest request = exchange.getProperty(Properties.INPUT_BODY, LoyaltyMaintainAccountRequest.class);

        Address address = request.getPayload().getAddress();
        if (address != null && ("ly".equalsIgnoreCase(address.getSource())
                || "kc".equalsIgnoreCase(address.getSource())
                || "cm".equalsIgnoreCase(address.getSource())
                || "s".equalsIgnoreCase(address.getSource()))) {

            exchange.setProperty(Properties.ADDRESS_VAR, address);
        }

    }
}
