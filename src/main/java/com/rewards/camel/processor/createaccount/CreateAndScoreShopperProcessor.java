package com.rewards.camel.processor.createaccount;

import com.rewards.camel.constants.Properties;
import com.rewards.pojo.gen.loyalty.object.*;
import com.rewards.pojo.gen.loyaltylab.AuthenticationResult;
import com.rewards.pojo.gen.loyaltylab.Shopper;
import com.rewards.session.Authentication;
import com.rewards.util.DateTime;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.cxf.headers.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

@Component
public class CreateAndScoreShopperProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(CreateAndScoreShopperProcessor.class);

    public static final String NAME = "createAndScoreShopperProcessor";

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("Process: CreateAndScoreShopperProcessor");

        AuthenticationResult auth = Authentication.auth;
        LoyaltyMaintainAccountRequest body = exchange.getProperty(Properties.INPUT_BODY, LoyaltyMaintainAccountRequest.class);

        Shopper shopper = prepareInput(auth, body, exchange);

        exchange.getOut().setHeader(Header.HEADER_LIST, exchange.getIn().getHeader(Header.HEADER_LIST));
        exchange.getOut().setHeader(CxfConstants.OPERATION_NAME, "CreateAndScoreShopper");
        exchange.getOut().setBody(shopper);
    }

    private Shopper prepareInput(AuthenticationResult auth, LoyaltyMaintainAccountRequest body, Exchange exchange) {
        Shopper input = new Shopper();
        input.setShopperId(0);
        input.setRetailerGUID(auth.getRetailerGuid());

        // Finds and sets the Email address to the input
        Optional.of(body)
                .map(LoyaltyMaintainAccountRequest::getPayload)
                .map(LoyaltyProfile::getEmails)
                .map(Emails::getEmail)
                .map(emailList -> emailList.stream()
                        .filter(email -> Optional.of(email.getTypeBase())
                            .map(TypeBase::getTypeCode)
                            .map("loy"::equalsIgnoreCase)
                            .orElse(false))
                .map(EmailAddress::getEmailAddress))
                .map(Stream::findFirst)
                .flatMap(Function.identity())
                .ifPresent(input::setEmailAddress);

        input.setEmailFrequency(7);

        input.setStatus("A");

        input.setLastName(body.getPayload().getPerson().getLastName());

        input.setMiddleInitial(body.getPayload().getPerson().getMiddleName().substring(0, 1));

        input.setFirstName(body.getPayload().getPerson().getFirstName());

        Address address = exchange.getProperty(Properties.ADDRESS_VAR, Address.class);

        input.setAddress1(address.getLine1());

        input.setAddress2(address.getLine2());

        input.setCity(address.getCity());

        Optional.of(address)
                .map(Address::getState)
                .ifPresent(state -> {
                    if (state.getCode() != null) {
                        if (!state.getCode().isEmpty()) {
                            input.setState(state.getCode());
                        } else {
                            input.setState(state.getValue());
                        }
                    }
                });

        input.setZip(address.getPostalCode());

        input.setPhoneNumber(body.getPayload().getPhone().getPhoneNumber());

        LocalDateTime now = LocalDateTime.now();

        input.setProfileCreateDateTime(DateTime.toXmlGregorianCalendarDateTime(now));

        input.setProfileUpdateDateTime(DateTime.toXmlGregorianCalendarDateTime(now));

        if (body.getPayload().getMemberSinceDate() != null){
            input.setCreateDateTime(body.getPayload().getMemberSinceDate());
        } else{
            input.setCreateDateTime(DateTime.toXmlGregorianCalendarDateTime(now));
        }
        input.setPasswordLastChanged(DateTime.toXmlGregorianCalendarDateTime(now));

        input.setOrigin(body.getPayload().getOrigin());

        input.setFileImportId(0);

        input.setBulkEmail(1);
        input.setLoyaltyMember(true);

        input.setPersonStatus("P");

        input.setRetailerShopperCreationDate(DateTime.toXmlGregorianCalendarDateTime(now));

        input.setLoyaltyLabCreateDateTime(DateTime.toXmlGregorianCalendarDateTime(now));

        input.setStatusUpdateDateTime(DateTime.toXmlGregorianCalendarDateTime(now));

        return input;
    }
}
