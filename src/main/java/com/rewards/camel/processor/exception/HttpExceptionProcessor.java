package com.rewards.camel.processor.exception;

import com.rewards.pojo.gen.loyalty.object.SoapFault;
import com.rewards.properties.ExceptionProperties;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.transport.http.HTTPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.StringJoiner;

/**
 * @author Evgeni Stoykov
 */
@Component
public class HttpExceptionProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(HttpExceptionProcessor.class);

    public static final String NAME = "httpExceptionProcessor";

    @Autowired
    private ExceptionProperties exceptions;

    @Override
    public void process(Exchange exchange) throws Exception {
        HTTPException e = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, HTTPException.class);
        log.error("Error", e);

        SoapFault soapFault = new SoapFault();
        soapFault.setFaultCode(exceptions.getCodes().get(ExceptionProperties.Keys.SYS_MSG_2.getKey()));
        soapFault.setFaultString(new StringJoiner("-")
                .add(e.getMessage())
                .add(String.valueOf(e.getResponseCode()))
                .add(e.getResponseMessage())
                .add(e.getUrl().toString())
                .toString());
        soapFault.setFaultActor(Arrays.toString(e.getStackTrace()));
        exchange.getOut().setFault(true);
        exchange.getOut().setBody(soapFault);
    }
}
