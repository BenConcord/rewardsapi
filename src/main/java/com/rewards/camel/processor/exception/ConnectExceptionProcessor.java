package com.rewards.camel.processor.exception;

import com.rewards.pojo.gen.loyalty.object.SoapFault;
import com.rewards.properties.ExceptionProperties;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.ConnectException;
import java.util.Arrays;

/**
 * @author Evgeni Stoykov
 */
@Component
public class ConnectExceptionProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(HttpExceptionProcessor.class);

    public static final String NAME = "connectExceptionProcessor";

    @Autowired
    private ExceptionProperties exceptions;

    @Override
    public void process(Exchange exchange) throws Exception {
        ConnectException e = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, ConnectException.class);
        log.error("Error", e);

        SoapFault soapFault = new SoapFault();
        soapFault.setFaultCode(exceptions.getCodes().get(ExceptionProperties.Keys.SYS_MSG_2.getKey()));
        soapFault.setFaultString(e.getMessage());
        soapFault.setFaultActor(Arrays.toString(e.getStackTrace()));
        exchange.getOut().setFault(true);
        exchange.getOut().setBody(soapFault);
    }
}
