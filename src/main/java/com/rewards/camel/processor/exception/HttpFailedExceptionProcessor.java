package com.rewards.camel.processor.exception;

import com.rewards.pojo.gen.loyalty.object.SoapFault;
import com.rewards.properties.ExceptionProperties;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.StringJoiner;

/**
 * @author Evgeni Stoykov
 */
@Component
public class HttpFailedExceptionProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(HttpFailedExceptionProcessor.class);

    public static final String NAME = "httpExceptionProcessor";

    @Autowired
    private ExceptionProperties exceptions;

    @Override
    public void process(Exchange exchange) throws Exception {
        HttpOperationFailedException e = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, HttpOperationFailedException.class);
        log.error("Error", e);

        SoapFault soapFault = new SoapFault();
        soapFault.setFaultCode(exceptions.getCodes().get(ExceptionProperties.Keys.SYS_MSG_2.getKey()));
        soapFault.setFaultString(new StringJoiner("-")
                                        .add(e.getMessage())
                                        .add(String.valueOf(e.getStatusCode()))
                                        .add(e.getStatusText()).toString());
        soapFault.setFaultActor(Arrays.toString(e.getStackTrace()));
        exchange.getOut().setFault(true);
        exchange.getOut().setBody(soapFault);
    }
}