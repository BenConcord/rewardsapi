package com.rewards.camel.processor.exception;

import com.rewards.pojo.gen.loyalty.object.SoapFault;
import com.rewards.properties.ExceptionProperties;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.StringJoiner;

/**
 * @author Evgeni Stoykov
 */
@Component
public class TimeoutExceptionProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(TimeoutExceptionProcessor.class);

    public static final String NAME = "timeoutExceptionProcessor";

    @Autowired
    private ExceptionProperties exceptions;

    @Override
    public void process(Exchange exchange) throws Exception {
        SocketTimeoutException e = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, SocketTimeoutException.class);
        log.error("Error", e);

        SoapFault soapFault = new SoapFault();
        soapFault.setFaultCode(exceptions.getCodes().get(ExceptionProperties.Keys.APP_SVC.getKey()));
        soapFault.setFaultString(new StringJoiner("-")
                .add(e.getMessage())
                .add(e.getCause().getMessage()).toString());
        soapFault.setFaultActor(Arrays.toString(e.getStackTrace()));
        exchange.getOut().setFault(true);
        exchange.getOut().setBody(soapFault);
    }
}
