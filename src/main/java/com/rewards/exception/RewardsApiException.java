package com.rewards.exception;

/**
 * @author Stiliyan Atanasov <stiliyan.atanasov@concordusa.com>
 */
public class RewardsApiException extends Exception {

    private static final long serialVersionUID = 1L;

    public RewardsApiException(String message) {
        super(message);
    }

    public RewardsApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public RewardsApiException(Throwable cause) {
        super(cause);
    }
}
