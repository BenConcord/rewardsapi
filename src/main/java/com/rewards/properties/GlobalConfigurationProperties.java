package com.rewards.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("app")
@Component
public class GlobalConfigurationProperties {

    private boolean enableActiveMQ;

    private String systemCode;

    private String routeLogLevel;

    private String servletMappingUri;

    private String openApiEndpointUri;

    public boolean isEnableActiveMQ() {
        return enableActiveMQ;
    }

    public void setEnableActiveMQ(boolean enableActiveMQ) {
        this.enableActiveMQ = enableActiveMQ;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getRouteLogLevel() {
        return routeLogLevel;
    }

    public void setRouteLogLevel(String routeLogLevel) {
        this.routeLogLevel = routeLogLevel;
    }

    public String getServletMappingUri() {
        return servletMappingUri;
    }

    public void setServletMappingUri(String servletMappingUri) {
        this.servletMappingUri = servletMappingUri;
    }

    public String getOpenApiEndpointUri() {
        return openApiEndpointUri;
    }

    public void setOpenApiEndpointUri(String openApiEndpointUri) {
        this.openApiEndpointUri = openApiEndpointUri;
    }
}
