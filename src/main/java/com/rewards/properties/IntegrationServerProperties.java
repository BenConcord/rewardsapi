package com.rewards.properties;

import com.rewards.properties.integrationserver.Jms;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("integrationServer")
@Component
public class IntegrationServerProperties {

    private String loyaltyLabApi;

    private String kohlsApi;

    private Jms jms;

    private String username;

    private String password;

    public String getLoyaltyLabApi() {
        return loyaltyLabApi;
    }

    public void setLoyaltyLabApi(String loyaltyLabApi) {
        this.loyaltyLabApi = loyaltyLabApi;
    }

    public String getKohlsApi() {
        return kohlsApi;
    }

    public void setKohlsApi(String kohlsApi) {
        this.kohlsApi = kohlsApi;
    }

    public Jms getJms() {
        return jms;
    }

    public void setJms(Jms jms) {
        this.jms = jms;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
