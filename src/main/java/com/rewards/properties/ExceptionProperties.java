package com.rewards.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Evgeni Stoykov
 */
@ConfigurationProperties(prefix = "exceptions")
@Component
public class ExceptionProperties {

    private Map<String, String> codes;

    public Map<String, String> getCodes() {
        return codes;
    }

    public void setCodes(Map<String, String> codes) {
        this.codes = codes;
    }

    public enum Keys {

        APP_DATA("ApplicationDataException"),
        APP_SVC("ApplicationServiceException"),
        BUS_SVC("BusinessServiceException"),
        SYS_MSG_2("SystemMessagingException2");

        private String key;

        Keys(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }
    }
}
