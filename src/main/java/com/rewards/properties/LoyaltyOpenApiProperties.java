package com.rewards.properties;

import com.rewards.properties.loyaltyopenapi.Services;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("loyaltyOpenApi")
@Component
public class LoyaltyOpenApiProperties {

    private String username;

    private String password;

    private Services services;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Services getServices() {
        return services;
    }

    public void setServices(Services services) {
        this.services = services;
    }
}
