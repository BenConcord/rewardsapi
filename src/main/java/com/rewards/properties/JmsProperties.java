package com.rewards.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("jms")
@Component
public class JmsProperties {

    private String maximumRedeliveries;

    private String redeliveryDelay;

    private String createEventDestination;

    private int expirationInSeconds;

    private int priority;

    private String deliveryMode;

    public String getMaximumRedeliveries() {
        return maximumRedeliveries;
    }

    public void setMaximumRedeliveries(String maximumRedeliveries) {
        this.maximumRedeliveries = maximumRedeliveries;
    }

    public String getRedeliveryDelay() {
        return redeliveryDelay;
    }

    public void setRedeliveryDelay(String redeliveryDelay) {
        this.redeliveryDelay = redeliveryDelay;
    }

    public String getCreateEventDestination() {
        return createEventDestination;
    }

    public void setCreateEventDestination(String createEventDestination) {
        this.createEventDestination = createEventDestination;
    }

    public int getExpirationInSeconds() {
        return expirationInSeconds;
    }

    public void setExpirationInSeconds(int expirationInSeconds) {
        this.expirationInSeconds = expirationInSeconds;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

}
