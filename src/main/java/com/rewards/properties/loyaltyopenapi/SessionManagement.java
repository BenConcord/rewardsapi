package com.rewards.properties.loyaltyopenapi;

public class SessionManagement {

    private long interval;

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }
}
