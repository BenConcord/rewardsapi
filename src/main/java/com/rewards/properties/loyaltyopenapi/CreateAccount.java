package com.rewards.properties.loyaltyopenapi;

public class CreateAccount {

    private String id;

    private boolean enableService;

    private String eventReferenceTagDob;

    private String eventReferenceTagAddress;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isEnableService() {
        return enableService;
    }

    public void setEnableService(boolean enableService) {
        this.enableService = enableService;
    }

    public String getEventReferenceTagDob() {
        return eventReferenceTagDob;
    }

    public void setEventReferenceTagDob(String eventReferenceTagDob) {
        this.eventReferenceTagDob = eventReferenceTagDob;
    }

    public String getEventReferenceTagAddress() {
        return eventReferenceTagAddress;
    }

    public void setEventReferenceTagAddress(String eventReferenceTagAddress) {
        this.eventReferenceTagAddress = eventReferenceTagAddress;
    }
}
