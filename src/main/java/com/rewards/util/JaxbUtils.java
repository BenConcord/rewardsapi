package com.rewards.util;

import org.apache.cxf.binding.soap.SoapHeader;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * @author Evgeni Stoykov
 */
public final class JaxbUtils {

    private JaxbUtils() {}

    public static SoapHeader objectToSoapHeader(String namespace, String localPart, Object object, Class<?> clazz)
    throws ParserConfigurationException, JAXBException {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        JAXBContext context = JAXBContext.newInstance(clazz);

        StringWriter writer = new StringWriter();
        context.createMarshaller().marshal(object, writer);

        try (StringReader sr = new StringReader(writer.toString())) {
            InputSource inputSource = new InputSource(sr);
            Document header = db.parse(inputSource);
            return new SoapHeader(new QName(namespace, localPart), header.getDocumentElement());
        } catch (SAXException | IOException e) {
            throw new JAXBException(e.getMessage(), e);
        }
    }

    public static String objectToString(Object object, String contextPath) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(contextPath);
        StringWriter writer = new StringWriter();
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(object, writer);
        return writer.toString();
    }

    public static String objectToString(Object object, Class<?> clazz) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
        StringWriter writer = new StringWriter();
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(object, writer);
        return writer.toString();
    }

}
