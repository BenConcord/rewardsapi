package com.rewards.util;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class DateTime {

    private DateTime() {}

    public static final String DATE_TIME_PATTERN = "uuuu-MM-dd";

    public static XMLGregorianCalendar toXmlGregorianCalendarDateTime(LocalDateTime dateTime) {
        return XMLGregorianCalendarImpl.createDateTime(dateTime.getYear(), dateTime.getMonth().getValue(), dateTime.getDayOfMonth(), dateTime.getHour(), dateTime.getMinute(), dateTime.getSecond());
    }

    public static String format(XMLGregorianCalendar xmlDate) {
        LocalDate date = LocalDate.of(xmlDate.getYear(), xmlDate.getMonth(), xmlDate.getDay());
        return date.format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN));
    }

}
